@include('header')
<!--End Main Header -->


	<!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.@$setting->header)}});">
    	<div class="auto-container">
        	<h2>{!!ucfirst(@$data->name)!!}</h2>
			<div class="separater"></div>
        </div>
    </section>

    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('solutions')}}">Solutions</a> <span>/</span></li>
                <li>{!!ucfirst(@$data->name)!!}</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->

    <!-- FMCG Section -->
	<section class="testimonial-section gap" style="margin-top: 3%;">
		<div class="auto-container">

            <div class="row">
                <div class="col-lg-4">
                    <div class="text-center sm-mb-55px">
                        <img src="{{asset('storage/app/public/images/solutions/'.$data->image)}}" alt="{{@$data->image_name}}" style="margin-top: 0%;">
                    </div> 
                </div>
                <div class="col-lg-8">
                    <div class="inner-column">
                        <div class="sec-title">
                            <h3 style="font-family: Raleway;">{!!@$fmcg->name!!}</h3>
                            <div class="separater"></div>
                        <!-- <h5 style="font-weight: 500; padding-top: 2%; text-align: justify; padding-right: 5%; font-size: 17px;">Bumilink is a leading supplier of total logistic solutions to the Fast Moving Consumer Goods (FMCG) market. We have multi share warehouses, which are geographically capable of serving every area in Malaysia, and we offer the same within distribution and transport systems. At the same time, we continuously extend the flow of information in the logistics chain. Through knowledge sharing related to the complex processes in the modern FMCG logistics, Bumilink ensures that our customers' value chain is adapted to the conditions, and that the customer always has the optimum solution.
                        </h5> -->
                        <h5 style="font-weight: 500; padding-top: 2%; text-align: justify; padding-right: 5%; font-size: 17px;">{{strip_tags(@$data->descriptions)}} </h5>
                </div> 
            </div>
            <!-- <div class="text-center margin-top-35px">
                <a href="#" class="nile-bottom md">Show all <i class="fa fa-arrow-right"></i> </a>
            </div> -->
        </div>
    </div> 
	<!-- End FMCG Section --> 
	
	<!-- Line Break -->
	<section class="domestic-services-section gap" style="margin-top:-7%;">
	<div class="auto-container">
	<!-- <div class="sec-title centered">
		<h3 style="font-family:Raleway;">Apparel<span> Info</span></h3>
		<div class="separater"></div>  -->
		</div>
	</div>
	<hr style="margin-top:4%; color:#555555; ">
	<div class="tem-sec">
		<div class="row">
<!-- End Line Break -->

	<!-- FMCG Description -->
	<section class="domestic-services-section gap" style="margin-top:0%; ">
		<div class="auto-container">
		<div class="sec-title centered">
			<!-- <h3 style="font-family:Raleway;">Do You<span> Know?</span></h3> -->
			<h3 style="font-family:Raleway;">{!!@$fmcg->getSubCategory[0]['name']!!}</h3>
			<div class="separater"></div> 
            </div>
		</div> 
		<div class="tem-sec" style="margin-top:-5%;">
			<div class="row">
             @if(@$fmcg->getSubCategory[0] !=='')
              @foreach(@$fmcg->getSubCategory[0]->childsubcategory as $key=>$value)
                  <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
                <div class="col-lg-5 col-md-6">
                    <div class="service-icon-box" style="padding: 5%;">
                        <div class="icon"><span class="{{@$value->class_name}}" style="color: #eb0028; font-size:55px; padding-bottom:0px;"></div><br>
                            <!-- <h2 style="color: #eb0028; font-size:70px;">&#10112;</h2> -->
                            <h5 style="font-weight: 600; color:#eb0028;">{!!@$value->name!!}</h5>
                        <div class="des" style="font-size: 16px;">{!!@$value->descriptions!!}
                        </div>
                    </div>
                </div>
			   @endforeach
             @endif
                

            </div>
		</div>
	</div>
</section>
<!-- End FMCG Info --> 


              @if(@$fmcg->getSubCategory !=='')
              @foreach(@$fmcg->getSubCategory as $key=>$value)
              @if($key!=0)
              <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
<!-- Key Benefits & What We Offer Accordion -->
<section class="services-section-two" style="margin-top: -10%; ">
    <div class="auto-container">
        <!-- <div class="row clearfix">  -->
			<div class="tem-sec">
				<div class="row">

			<div class="col-lg-4">
                <div class="sec-title">
					<!-- <h3 style="font-family:Raleway;">Our Key Benefits &<span> What We Offer</span></h3> -->
					<h3 style="font-family:Raleway;">{!!@$value->name!!}</h3>
					<div class="separater"></div> 
					</div>
            </div> 

			<div class="col-lg-7">
			<div id="accordion" class="nile-accordion margin-top-30px sm-mb-45px">
				
                
				@if(@$value->childsubcategory!=='')
                 @foreach(@$value->childsubcategory as $keys=>$values)
                 <span id="{{preg_replace('/\s+/','',@$values->name)}}"></span>
				<div class="card">
					<div class="card-header" id="headingOne">
						<h5 class="mb-0">
							<button class="btn btn-block btn-link @if($keys==0) active @endif" data-toggle="collapse" data-target="#collapseOne{{@$keys}}" aria-expanded="true" aria-controls="collapseOne{{@$keys}}" style="font-size: 17px; font-weight:600;"><i class="fa {{@$values->class_name}}" style="padding-right:2%;"></i> {!!@$values->name!!} </button>
						</h5>
					</div>

					<div id="collapseOne{{@$keys}}" class="collapse @if($keys==0) show @endif" aria-labelledby="headingOne{{@$keys}}" data-parent="#accordion">
						<div class="card-body">
						{!!$values->descriptions!!}	
						</div>
					</div>
				</div>
				@endforeach
				@endif
				
				
			</div> 
            </div> 
            <div style="margin-bottom: 5%;"></div>
        </div>
    </div>
</div></section>
@endif
@endforeach
@endif




<!-- End Key Benefits & What We Offer Section --> 

<!-- Linked Overnight -->
<!-- <section class="nile-about-section" style="margin-top: 5%; ">
    <div class="auto-container">
        <div class="row clearfix">

                <div class="content-column col-lg-5 col-md-12 col-sm-12" style="margin-bottom: 2%;">
					<div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="sec-title-two sec-title">
							<h2>Key <span>Benefits</span></h2>
							<div class="separater"></div>
						</div></div>
                <div class="about-text margin-tb-25px">
                    <h5 style="padding-bottom: 2%; text-align: left;">
                    <ul>
						<li>&#10004;  Improve speed to market</li>
						<li>&#10004;  ain visibility to orders in your supply chain</li>
                        <li>&#10004;  Reduce inventory and operating costs through our campus model</li>
                        <li>&#10004;  Scale your network needs</li>
                        <li>&#10004;  Gain assistance with complex customs clearance requirements</li>
                    </ul>
                    </h5>
                </div>
                <a href="#" class="nile-bottom sm">Read More</a>

            </div>
            <div class="col-lg-6">
                <img src="images/resource/apparel-bg.png" alt="" style="padding-left: 7%;">
            </div>
            <div style="margin-bottom: 35%;"></div>
        </div>
    </div>
</div> -->
<!-- End We offer Section --> 

@include('footer')