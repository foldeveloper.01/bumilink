@include('header')
<!--End Main Header -->
	
	
	<!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.@$setting->header)}});">
    	<div class="auto-container">
        	<h2>International</h2>
			<div class="separater"></div>
        </div>
    </section>
    
    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('international')}}">International</a> <span>/</span></li>
                <li>{!!ucfirst(@$data->name)!!}</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->
    @if(@$data->getSubCategory !=='')
    @foreach(@$data->getSubCategory as $key=>$val)
    @if($key==0)	
    <span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>
	<!-- Cross Border Express Section -->
	<section class="testimonial-section gap" id="globalsameday" style="margin-top: 3%;">
		<div class="auto-container">
			<div class="sec-title centered">
				<!-- <h3>Cross Border <span>Express</span></h3> -->
                <h3>{!!@$val->name!!}</h3>
				<div class="separater"></div>
                <div class="text" style="padding-top:1%;padding-left: 5%; padding-right:5%; text-align:justify;">
                    {!!@$val->text!!}
                </div>
                <br>
			</div>
            <div class="row"  style="padding-top: 2%;"> 
                <div class="col-lg-5">
                    <div class="text-center sm-mb-55px">
                        <img src="{{asset('storage/app/public/images/services/'.@$val->image)}}" alt="{{@$val->image_name}}"> 
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="inner-column">
                         {!!@$val->descriptions!!}
                </div>
            </div> 
        </div>
    </div>
    @endif
    @if($key>0)
	<!-- Key Benefits -->
	<span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>
	<section class="domestic-services-section gap" id="linkedsameday">
		<div class="auto-container"> 
		<div class="sec-title centered">
			<!-- <h3>Key<span> Benefits</span></h3> -->
            <h3>{!!@$val->name!!}</h3>
			<div class="separater"></div> 
            </div>
		</div>
		<div class="tem-sec" style="padding-top: 3%; margin-right: 1%; margin-bottom: -3%;"> 
			<div class="row"> 
                   @if(@$val->childsubcategory!=='')
                      @foreach(@$val->childsubcategory as $key=>$value)
                      @if($loop->odd)
                    <div class="col-lg-3 col-sm-6"> 
                        <div class="cart-service background-main-color">
                            <div class="icon-one"><img src="{{asset('storage/app/public/images/services/'.@$value->image)}}" alt="{{@$value->image_name}}" width="55px;"></div>
                            <h2 style="font-weight: 600;">{!!@$value->name!!}</h2>
                            <hr>                             
                            @if($key==2 || $key=4 || $key=6)
                            <div class="text" style="font-size: 14px; text-align: justify; padding-bottom: 10%; ">
                            @else
                            <div class="text" style="font-size: 14px; text-align: justify; padding-bottom: 20%;"> 
                            @endif
                            {!!@$value->descriptions!!} </div>
                        </div>
                    </div>
                    @else
                    <div class="col-lg-3 col-sm-6">
                        <div class="cart-services background-second-color">
                            <div class="icon-one"><img src="{{asset('storage/app/public/images/services/'.@$value->image)}}" alt="{{@$value->image_name}}" width="45px;" style="margin-bottom: 4%;"></div>
                            <h2 style="color:#eb0028; font-weight: 600;">{!!@$value->name!!}
                            </h2>
                            <hr>
                            @if($key==2 || $key=4 || $key=6)
                            <div class="text" style="font-size: 14px; text-align: justify; padding-bottom: 10%; ">
                            @else
                            <div class="text" style="color:#212529; text-align: justify; font-size: 14px; padding-bottom: 26%;">
                            @endif
                            {!!@$value->descriptions!!} </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                    @endif              
                    
                </div>
            </div>
        </div>
        </div> 
    </div> 
	
	</div>
</section>
   @endif
     @endforeach
        @endif
<!-- End Key Benefits --> 
@include('footer')