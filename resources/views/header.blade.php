<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>{{@$setting->site_name}}</title>
<!-- Stylesheets -->
<!-- <link href="css/bootstrap.css" rel="stylesheet"> -->
<!-- <link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet"> -->

<link rel="stylesheet" type="text/css" href="{{ url('/css/bootstrap.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('/css/style.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('/css/responsive.css') }}" />

<link rel="shortcut icon" href="{{asset('storage/app/public/images/settings/'.@$setting->favicon)}}" type="image/x-icon">
<link rel="icon" href="{{asset('storage/app/public/images/settings/'.@$setting->favicon)}}" type="image/x-icon">
<!-- <link href="/asset/fontawesome6-2-0/css/all.css" rel="stylesheet"> -->
<!-- <link href="/asset/fontawesome6-2-0/css/brands.css" rel="stylesheet">
<link href="/asset/fontawesome6-2-0/css/solid.css" rel="stylesheet"> -->
<link rel="stylesheet" type="text/css" href="{{ url('/css/sweetalert2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('/css/toastr.css') }}" />

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="{{@$setting->meta_descriptions}}">
<meta name="author" content="{{@$setting->meta_author}}">
<meta name="keywords" content="{{@$setting->meta_keywords}}">

<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
<style> 
    #firstname-error, #heardabout-error, #message-error, #lastname-error, #title-error, #company-error, #mobile-error,
    #email-error, #address-error, #postal-error, #city-error, #state-error, #country-error, #errorToShow.error, #user_companyname-error,#user_title-error,#user_email-error,#user_phone-error,#user_ext-error,#user_fax-error,#user_name-error,#user_payablename-error,#user_mobiledirect-error,#user_faxpod-error,#p_address-error,#p_city-error,#p_postalzip-error,#p_state-error,#p_country-error,#b_address-error,#b_city-error,#b_postalzip-error,#b_state-error,#b_country-error,#b_our_services-error,#b_numberlocation-error,#b_typeofbusiness-error,#o_nameone-error,#o_titleone-error,#myfile-error,#classify-error,#bank_principalbank-error,#bank_branchlocation-error,#bank_contactbankreference-error,#bank_telephonebankreference-error,#bank_extbankreference-error,#bank_accountbankreference-error,#bank_approve-error {
         color: #dc3545;
         font-size: 14px;
    }
    </style>
</head>

<body class="hidden-bar-wrapper">

<div class="page-wrapper">

    <!-- Preloader -->
    <div class="preloader"></div>
    <header class="main-header header-style-two">
			<div class="header-auto-container">
			<div class="header-inner">
				<!--Header Top-->
				<div class="header-top">

					<div class="clearfix">
						<!--Top Right-->
						<div class="top-right">

							<!-- Right List -->
							<ul class="right-list">
								<li><span class="icon flaticon-mail"></span><a href="mailto:@if(@$setting->email) {{@$setting->email}}@endif">@if(@$setting->email) {{@$setting->email}}@endif</a></li>
								<li><span class="icon flaticon-phone-contact"></span><a href="tel:@if(@$setting->phone) {{@$setting->phone}}@endif">@if(@$setting->phone) {{@$setting->phone}}@endif</a></li>
								<li><span class="icon flaticon-fax"></span><a href="tel:@if(@$setting->fax) {{@$setting->fax}}@endif">@if(@$setting->fax) {{@$setting->fax}}@endif</a></li>
							</ul>							
							<!--Social Box-->
							<ul class="social-box">
								 @foreach($sociallink as $key=>$social) 
                                          @if($social->name=="instagram")
                                             <li><a target="_blank" href="{{$social->link}}"><span class="fa fa-instagram"></span></a></li>
                                          @endif
                                          @if($social->name=="facebook")
                                             <li><a target="_blank" href="{{$social->link}}"><span class="fa fa-facebook"></span></a></li>
                                          @endif
                                           @if($social->name=="twitter")
                                             <li><a target="_blank" href="{{$social->link}}"><span class="fa fa-twitter"></span></a></li>
                                          @endif
                                           @if($social->name=="youtube")
                                             <li><a target="_blank" href="{{$social->link}}"><span class="fa fa-youtube"></span></a></li>
                                          @endif
                                        @endforeach
							</ul>

						</div>

					</div>

				</div>

				<!--Header-Upper-->
				<div class="header-upper">
					<div class="auto-container">
					<div class="clearfix">

						<div class="pull-left logo-box">
							<div class="logo"><a href="{{url('/')}}"><img src="{{asset('storage/app/public/images/settings/'.@$setting->logo)}}" alt="" title=""></a></div>
						</div>

						<div class="pull-right upper-right">

							<!--Header Lower-->
							<div class="header-lower"> 

									<div class="nav-outer clearfix">
										<!-- Main Menu -->
										<nav class="main-menu navbar-expand-md">
											<div class="navbar-header">
												<!-- Toggle Button -->
												<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
												</button>
											</div>

											<div class="navbar-collapse collapse clearfix" id="navbarSupportedContent">
												<ul class="navigation clearfix">
													<li @if(Request::segment(1)=="") class="current" @endif ><a href="{{url('/')}}">Home</i></a> <!-- <li class="current dropdown"><a href="#">Home<i class="fa fa-angle-down"></i></a> 
														<ul> 
															<li><a href="#">Home Page 01</a></li> 
															<li><a href="index-2.html">Home Page 02</a></li>
														   <li class="dropdown"><a href="#">Header Styles</a>
															   <ul>
																   <li><a href="#">Header Style 01</a></li>
																   <li><a href="index-2.html">Header Style 02</a></li>
															   </ul>
														   </li> 
													   </ul> -->
												   </li>
												   <li @if(Request::segment(1)=="about") class="current" @endif><a href="{{url('about')}}">About Us</a></li> 
												   <li class="dropdown @if(Request::segment(1)=="services" || Request::segment(1)=="domesticservices" || Request::segment(1)=="international" || Request::segment(1)=="globallogistic" || Request::segment(1)=="custombrokerage" || Request::segment(1)=="cargoinsurance") current @endif"> 
													   <a href="{{url('services')}}">Our Services<i class="fa fa-angle-down"></i></a>
													   <ul> 
														   <li class="dropdown"><a href="{{url('domesticservices')}}">Domestic Services</a>
															   <ul>
																<li><a href="{{url('domesticservices')}}#linkedsameday">Linked Same Day</a></li>
																<li><a href="{{url('domesticservices')}}#linkedovernight">Linked Overnight</a></li>
																<li><a href="{{url('domesticservices')}}#linkedeconomy">Linked Economy</a></li>
																<li><a href="{{url('domesticservices')}}#transportation">Transportation (LTL/FTL)</a></li>
																<li><a href="{{url('domesticservices')}}#homedeliveries">Home Deliveries & e-Commerce </a></li>
																<li><a href="{{url('domesticservices')}}#retailmerchandising">Retail Merchandising
																</a></li>
															   </ul>
														   </li>  
														   <li class="dropdown"><a href="{{url('international')}}">International</a>
															   <ul>
																<!--<li><a href="{{url('international')}}#globalsameday">Global Same Day
																</a></li>-->
																<li><a href="{{url('international')}}#globalpackageexpress">Global Package Express</a></li>
																<li><a href="{{url('international')}}#globalexpeditedfreight">Global Expedited Freight
																</a></li>
																<li><a href="{{url('international')}}#globaleconomyfreight">Global Economy Freight 
																</a></li>
																<li><a href="{{url('international')}}#globaloceanfreightlclfcl">Global Ocean Freight (LCL/FCL)
																</a></li>
																<li><a href="{{url('international')}}#globalimportfreightrp">Global Import Freight R/P
																</a></li>
																<li><a href="{{url('international')}}#crossborderexpress">Cross Border Express
																</a></li>
															   </ul>
														   </li> 
														   <li class="dropdown"><a href="{{url('globallogistic')}}">Global Logistic</a>
															   <ul>
																   <li><a href="{{url('globallogistic')}}#services">Services</a></li>
																   <li><a href="{{url('globallogistic')}}#contractwarehousinganddistributionservice">Contract Warehousing and Distribution Service
																   </a></li>
																   <li><a href="{{url('globallogistic')}}#corporatedistributionmanagement">Corporate Distribution Management
																   </a></li>
																   <li><a href="{{url('globallogistic')}}#importinventorymanagement">Import Inventory Management
																   </a></li>
															   </ul>
														   </li> 
														   <li><a href="{{url('custombrokerage')}}">Custom Brokerage</a></li> 
                                                           <li><a href="{{url('cargoinsurance')}}">Cargo Insurances</a></li> 
                                                        </ul>
								                    <li class="dropdown @if(Request::segment(1)=="solutions" || Request::segment(1)=="apparel" || Request::segment(1)=="aviation" || Request::segment(1)=="banking" || Request::segment(1)=="ecommerce" || Request::segment(1)=="fmcg" || Request::segment(1)=="healthcaremedicallogistics" || Request::segment(1)=="manufacturinglogistics" || Request::segment(1)=="technology" || Request::segment(1)=="customstaxconsultancy" || Request::segment(1)=="dedicatedfleetlogistics" || Request::segment(1)=="supplychainmanagement" || Request::segment(1)=="wms") current @endif"><a href="{{url('solutions')}}">Solutions<i class="fa fa-angle-down"></i></a> 
									                <ul>
										                <li><a href="{{url('solutions/apparel')}}">Apparel</a></li>
										                <li><a href="{{url('solutions/aviation')}}">Aviation</a></li>
										                <li><a href="{{url('solutions/banking')}}">Banking/Finance</a></li>
										                <li><a href="{{url('solutions/ecommerce')}}">E-Commerce</a></li>
										                <li><a href="{{url('solutions/fmcg')}}">FMCG</a></li>
										                <li><a href="{{url('solutions/healthcaremedicallogistics')}}">Healthcare/Medical Logistics</a></li>
										                <li><a href="{{url('solutions/manufacturinglogistics')}}">Manufacturing Logistics</a></li>
										                <li><a href="{{url('solutions/technology')}}">Technology</a></li>
										                <li><a href="{{url('solutions/customstaxconsultancy')}}">Customs Tax Consultancy</a></li>
										                <li><a href="{{url('solutions/dedicatedfleetlogistics')}}">Dedicated Fleet Logistics </a></li>
										                <li><a href="{{url('solutions/supplychainmanagement')}}">Supply Chain Management </a></li>
										                <li><a href="{{url('solutions/wms')}}">WMS </a></li> 
                                                    </ul>
								                </li>
								                    <li  @if(Request::segment(1)=="contact") class="current" @endif><a href="{{url('contact')}}">Contact</a> </li>
									                <li @if(Request::segment(1)=="getaquote") class="current" @endif ><a href="{{url('getaquote')}}">Get A Quote</a> 
                                                </li> 
												</ul>
											</div>
										</nav>

										<!-- Main Menu End-->
										<div class="outer-box clearfix">

											<!--Option Box-->
											<div class="option-box">

												<!--Search Box-->
												<div class="search-box-outer">
													<div class="dropdown">
														<button class="search-box-btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="flaticon-route"></span></button>
														<ul class="dropdown-menu pull-right search-panel" aria-labelledby="dropdownMenu3">
															<li class="panel-outer">
																<div class="form-container">
																	<form method="post" action="blog.html">
																		<div class="form-group">
																			<input type="search" name="field-name" value="" placeholder="Track Your Shipment" required>
																			<button type="submit" class="search-btn"><span class="fa fa-search"></span></button>
																		</div>
																	</form>
																</div>
															</li>
														</ul>
													</div>
												</div>

											</div>
										</div>
									</div>

							</div>
							<!--End Header Lower-->

						</div>

					</div>

				</div>
			<!--End Header Upper-->
			</div>
			</div>
        </div>

		<!--Sticky Header-->
        <div class="sticky-header">
        	<div class="auto-container clearfix">
            	<!--Logo-->
            	<div class="logo pull-left">
                	<a href="{{url('/')}}" class="img-responsive"><img src="{{asset('images/bumilink-logo-small.png')}}" alt="" title=""></a>
                </div>

                <!--Right Col-->
                <div class="right-col pull-right">
                	<!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <div class="navbar-collapse collapse clearfix" id="navbarSupportedContent1">
                            <ul class="navigation clearfix">
								<li @if(Request::segment(1)=="") class="current" @endif><a href="{{url('/')}}">Home</i></a> <!-- <li class="current dropdown"><a href="#">Home<i class="fa fa-angle-down"></i></a> 
									<ul> 
										<li><a href="index.html">Home Page 01</a></li> 
										<li><a href="index-2.html">Home Page 02</a></li>
									   <li class="dropdown"><a href="#">Header Styles</a>
										   <ul>
											   <li><a href="index.html">Header Style 01</a></li>
											   <li><a href="index-2.html">Header Style 02</a></li>
										   </ul>
									   </li> 
								   </ul> -->
							   </li>
							   <li @if(Request::segment(1)=="about") class="current" @endif><a href="{{url('about')}}">About Us</a></li> 
							   <div class="navbar-collapse collapse clearfix" id="navbarSupportedContent">
								   <ul class="navigation clearfix"> 
								   <li class="dropdown @if(Request::segment(1)=="services" || Request::segment(1)=="domesticservices" || Request::segment(1)=="international" || Request::segment(1)=="globallogistic" || Request::segment(1)=="custombrokerage" || Request::segment(1)=="cargoinsurance" ) current @endif" > 
								   <a href="{{url('services')}}">Our Services<i class="fa fa-angle-down"></i></a>
								   <ul> 
									   <li class="dropdown"><a href="{{url('domesticservices')}}">Domestic Services</a>
										   <ul>
											   <li><a href="{{url('domesticservices')}}#linkedsameday">Linked Same Day</a></li>
											   <li><a href="{{url('domesticservices')}}#linkedovernight">Linked Overnight</a></li>
											   <li><a href="{{url('domesticservices')}}#linkedeconomy">Linked Economy</a></li>
											   <li><a href="{{url('domesticservices')}}#transportation">Transportation (LTL/FTL)</a></li>
											   <li><a href="{{url('domesticservices')}}#homedeliveries">Home Deliveries & e-Commerce </a></li>
											   <li><a href="{{url('domesticservices')}}#retailmerchandising">Retail Merchandising
											   </a></li>
										   </ul>
									   </li> 
									   <li class="dropdown"><a href="{{url('international')}}">International</a>
                                        <ul>
                                           <!-- <li><a href="{{url('international')}}#globalsameday">Global Same Day
                                            </a></li>-->
                                            <li><a href="{{url('international')}}#globalpackageexpress">Global Package Express</a></li>
                                            <li><a href="{{url('international')}}#globalexpeditedfreight">Global Expedited Freight
                                            </a></li>
                                            <li><a href="{{url('international')}}#globaleconomyfreight">Global Economy Freight 
                                            </a></li>
                                            <li><a href="{{url('international')}}#globaloceanfreightlclfcl">Global Ocean Freight (LCL/FCL)
                                            </a></li>
                                            <li><a href="{{url('international')}}#globalimportfreightrp">Global Import Freight R/P
                                            </a></li>
                                            <li><a href="{{url('crossborderexpress')}}">Cross Border Express
                                            </a></li>
                                        </ul>
                                    </li> 
									   <li class="dropdown"><a href="{{url('globallogistic')}}">Global Logistic</a>
										   <ul>
											<li><a href="{{url('globallogistic')}}#services">Services</a></li>
											<li><a href="{{url('globallogistic')}}#contractwarehousinganddistributionservice">Contract Warehousing and Distribution Service
											</a></li>
											<li><a href="{{url('globallogistic')}}#corporatedistributionmanagement">Corporate Distribution Management
											</a></li>
											<li><a href="{{url('globallogistic')}}#importinventorymanagement">Import Inventory Management
											</a></li>
										   </ul>
									   </li>  
									   <li><a href="{{url('custombrokerage')}}">Custom Brokerage</a></li>
									   <li><a href="{{url('cargoinsurance')}}">Cargo Insurances</a></li> 
								   </ul>
								   <li class="dropdown @if(Request::segment(1)=="solutions" || Request::segment(1)=="apparel" || Request::segment(1)=="aviation" || Request::segment(1)=="banking" || Request::segment(1)=="ecommerce" || Request::segment(1)=="fmcg" || Request::segment(1)=="healthcaremedicallogistics" || Request::segment(1)=="manufacturinglogistics" || Request::segment(1)=="technology" || Request::segment(1)=="customstaxconsultancy" || Request::segment(1)=="dedicatedfleetlogistics" || Request::segment(1)=="supplychainmanagement" || Request::segment(1)=="wms") current @endif"><a href="{{url('solutions')}}">Solutions<i class="fa fa-angle-down"></i></a> 
									   <ul>
										   <li><a href="{{url('solutions/apparel')}}">Apparel</a></li>
										   <li><a href="{{url('solutions/aviation')}}">Aviation</a></li>
										   <li><a href="{{url('solutions/banking')}}">Banking/Finance</a></li>
										   <li><a href="{{url('solutions/ecommerce')}}">E-Commerce</a></li>
										   <li><a href="{{url('solutions/fmcg')}}">FMCG</a></li>
										   <li><a href="{{url('solutions/healthcaremedicallogistics')}}">Healthcare/Medical Logistics</a></li>
										   <li><a href="{{url('solutions/manufacturinglogistics')}}">Manufacturing Logistics </a></li>
										   <li><a href="{{url('solutions/technology')}}">Technology</a></li>
										   <li><a href="{{url('solutions/customstaxconsultancy')}}">Customs Tax Consultancy</a></li>
										   <li><a href="{{url('solutions/dedicatedfleetlogistics')}}">Dedicated Fleet Logistics </a></li>
										   <li><a href="{{url('solutions/supplychainmanagement')}}">Supply Chain Management </a></li>
										   <li><a href="{{url('solutions/wms')}}">WMS </a></li> 
									   </ul>
								   </li>
								   <li @if(Request::segment(1)=="contact") class="current" @endif><a href="{{url('contact')}}">Contact</a> </li>
									   <li @if(Request::segment(1)=="getaquote") class="current" @endif><a  href="{{url('getaquote')}}">Get A Quote</a> 
								   </li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
        <!--End Sticky Header-->

    </header>
