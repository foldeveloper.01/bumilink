@include('header')
<!--End Main Header -->
	
	
	<!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.@$setting->header)}});">
    	<div class="auto-container">
        	<h2>{{@$data->name}}</h2>
			<div class="separater"></div>
        </div>
    </section>
    
    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('services')}}">Services</a> <span>/</span></li>
                <li>{!!@$data->name!!}</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->
	
	<!-- Global Same Day Section -->
	<section class="testimonial-section gap" style="margin-top: 3%;">
		<div class="auto-container">
			<div class="sec-title centered">
				<h3>{!!@$data->getSubCategory[0]['name']!!}</h3>
				<!-- <h3>Global <span>Same Day</span></h3> -->
				<div class="separater"></div>
                <br>
			</div> 

@if(@$data->getSubCategory[0] !=='')
            @foreach(@$data->getSubCategory[0]->childsubcategory as $key=>$val)
<!-- Services  -->
<span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>
@if($key==0)
<section class="nile-about-section" id="services" style="margin-top: 3%;">
@elseif($key==1)
<section class="nile-about-section" id="contractwarehousinganddistributionservice" style="margin-top: 3%;">
@elseif($key==2)
<section class="nile-about-section" id="corporatedistributionmanagement" style="margin-top: 3%;">
@elseif($key==3)
<section class="nile-about-section" id="importinventorymanagement" style="margin-top: 3%;">
@endif
    <div class="auto-container">
        <div class="row clearfix"> 
            <div class="col-lg-6">
                <img src="{{asset('storage/app/public/images/services/'.$val->image)}}" alt="{{@$val->image_name}}" style="padding-right: 7%;">
            </div>

            <div class="content-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column wowallow fadeInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="sec-title-two sec-title">
                        <h2>{!!@$val->name!!}</h2>
                        <div class="separater"></div>
                    </div></div>
            <div class="about-text margin-tb-25px" style="padding-right: 1%;">
                {!! @$val->descriptions!!}
            </div>
        </div>
    </div>
	</div>
</div></section>
<!-- End Services  -->
 @endforeach
            @endif
	
	<!--Main Footer-->
@include('footer')