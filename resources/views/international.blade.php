@include('header')
<!--End Main Header -->
	
	
	<!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.$setting->header)}});">
    	<div class="auto-container">
        	<h2>International</h2>
			<div class="separater"></div>
        </div>
    </section>
    
    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('services')}}">Services</a> <span>/</span></li>
                <li>{!!@$data->name!!}</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->
	
	<!-- Global Same Day Section -->
	<section class="nile-about-section" id="globalsameday" style="margin-top: 3%;">
		@if(@$data->getSubCategory[0] !=='')
            @foreach(@$data->getSubCategory as $key=>$val)
            @if($key==0)
            <span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>
		<div class="auto-container">
			<div class="sec-title centered"><!-- <h3 style="font-family: Raleway;">Global <span>Same Day</span></h3> -->
				<!--<h3 style="font-family: Raleway;">{!!@$val->name!!}</h3>-->
				<!--<div class="separater"></div>
                <br>-->
			</div>

            <div class="row">

                <div class="col-lg-4">
                 @if(@$val->childsubcategory!=='')
                 @foreach(@$val->childsubcategory as $key=>$value)
                 @if($value->position=='left')
                    <div class="service-icon-box">
                        <div><span class="{!!@$value->class_name!!}" style="font-size:55px; color:#eb0028; "></div>
							<h5 style="font-weight: 600; color:#eb0028;">{!!@$value->name!!}</h5>
                        <div class="des">{!!@$value->descriptions!!}</div><br>
                    </div>
                    @endif
                 @endforeach
               @endif
                    
                </div>
                <div class="col-lg-4">
                    <div class="text-center sm-mb-45px">
                        <img src="{{asset('storage/app/public/images/services/'.@$val->image)}}" alt="{{@$val->image_name}}" class="border-radius-500">
                        <div class="des" style="padding-left:5%; padding-right: 5%; padding-top: 6%;"><b>{!!@$val->descriptions!!} </b></div>
                    </div>
                </div>

                <div class="col-lg-4">
                	@if(@$val->childsubcategory!=='')
                    @foreach(@$val->childsubcategory as $key=>$value)
                    @if($value->position=='right')
                     <div class="service-icon-box">
                        <div><span class="{!!@$value->class_name!!}" style="font-size:55px; color:#eb0028; "></div>
							<h5 style="font-weight: 600; color:#eb0028;">{!!@$value->name!!}</h5>
                        <div class="des">{!!@$value->descriptions!!}</div>
                    </div>
                    @endif
                    @endforeach
                    @endif
                    
                </div> 
            </div>
             @endif
        @endforeach
               @endif
            <!-- <div class="text-center margin-top-35px">
                <a href="#" class="nile-bottom md">Show all <i class="fa fa-arrow-right"></i> </a>
            </div> -->

        </div>
    </div>
    <!-- End Global Same Day Section -->

@if(@$data->getSubCategory[0] !=='')
            @foreach(@$data->getSubCategory as $key=>$val)
            @if($key==1)
	<!-- Linked Same Day -->
	<section class="domestic-services-section gap" id="linkedsameday" style="margin-top:-3%;">
	    <span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>
		<div class="auto-container">
		<div class="sec-title centered">
			<!-- <h3 style="font-family: Raleway;">Inter<span>national</span></h3> -->
			<h3 style="font-family: Raleway;">{!!@$val->name!!}</h3>
			<div class="separater"></div> 
            </div>
		</div>
		<div class="tem-sec"  style="margin-top:-3%">
			<div class="row">
               @if(@$val->childsubcategory!=='')
               @foreach(@$val->childsubcategory as $key=>$value)
               @if($value->position=='left')
                <div class="col-lg-5 col-md-6" id="globalpackageexpress">
                    <div class="international_service-icon-box wowallow fadeInDown" data-wow-delay="0ms" data-wow-duration="800ms">
                        <div class="icon"><span class="{{@$value->class_name}}" style="color: #eb0028; font-size:55px; padding-bottom:35px;"></div><br>
							<!-- <h3 style="font-weight: 600; color:#eb0028;">Global Package Express</h3> -->
							<h3 style="font-weight: 600; color:#eb0028;">{!!@$value->name!!}</h3>
                        <div align="center"><hr width="80px;" color="#eb0028" size="10;"></div>
                        <div class="des">{!!@$value->descriptions!!}</div>
                    </div>
                </div>
                @endif
                @if($value->position=='right')
                <div class="col-lg-5 col-md-6" id="globaloceanfreightlclfcl">
                    <div class="international_service-icon-box wowallow fadeInDown" data-wow-delay="0ms" data-wow-duration="800ms">
                        <div class="icon"><span class="{{@$value->class_name}}" style="color: #eb0028; font-size:55px; padding-bottom:35px;"></div><br>
							<!-- <h3 style="font-weight: 600; color:#eb0028;">Global Ocean Freight (LCL/FCL)
							</h3> -->
							<h3 style="font-weight: 600; color:#eb0028;">{!!@$value->name!!}
							</h3>
                        <div align="center"><hr width="80px;" color="#eb0028" size="10;"></div>
                        <div class="des">{!!@$value->descriptions!!}</div>
                    </div>
                </div>
                @endif
                @endforeach
                @endif
            </div>
		</div>
	</div>
</section>
@endif
        @endforeach
               @endif

<!-- End Global Economy Freight --> 
	
	@if(@$data->getSubCategory !=='')
            @foreach(@$data->getSubCategory as $key=>$val)
            @if($key>1)
            @if($loop->odd)
            <span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>
	<!-- Global Import Freight R/P  -->
<section class="nile-about-section" id="globalimportfreightrp" style="margin-top: 6%;">
    <div class="auto-container">
        <div class="row clearfix">

                <div class="content-column col-lg-6 col-md-12 col-sm-12" style="margin-bottom: 2%;">
					<div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="sec-title-two sec-title">
							<!-- <h2>Global Import<span><br> Freight R/P</span></h2> -->
							<h2>{!!@$val->name!!}</h2>
							<div class="separater"></div>
						</div></div>
                <div class="about-text margin-tb-25px">
                    <h5 style="padding-bottom: 3%;">{!!@$val->text!!}</h5>
                    {!!@$val->descriptions!!}
                </div>
                <!-- <a href="#" class="nile-bottom sm">Read More</a> -->

            </div>
            <div class="col-lg-6">
                <img src="{{asset('storage/app/public/images/services/'.@$val->image)}}" alt="{!!@$val->image_name!!}" style="padding-left: 7%;">
            </div>
        </div>
    </div>
</div>
<!-- Global Import Freight R/P -->
@else
<!-- Cross Border Express  -->
<span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>
<section class="nile-about-section" id="crossborderexpress" style="margin-top: 3%;">
    <div class="auto-container">
        <div class="row clearfix"> 
            <div class="col-lg-6">
                <img src="{{asset('storage/app/public/images/services/'.@$val->image)}}" alt="{{@$val->image_name}}" style="padding-right: 7%;">
            </div>

            <div class="content-column col-lg-6 col-md-12 col-sm-12" style="margin-bottom: 2%;">
                <div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="sec-title-two sec-title">
                        <!-- <h2>Cross Border<span> Express</span></h2> -->
                        <h2>{!!@$value->name!!}</h2>
                        <div class="separater"></div>
                    </div></div>
            <div class="about-text margin-tb-25px">
                <h5  style="padding-bottom: 3%;">{!!@$value->text!!}</h5>{!!@$value->descriptions!!}
            </div>
            <a href="{{url('crossborderexpress')}}" class="nile-bottom sm">Read More</a> 
        </div>
    </div>
</div>
</section>
@endif
  @endif
        @endforeach
               @endif
	<!--End Sponsors Section-->
@include('footer')