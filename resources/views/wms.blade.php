@include('header')
<!--End Main Header -->

	<!--Page Title-->
    <section class="page-title" style="background-image:url('{{asset('images/background/about-bg.png')}}')">
    	<div class="auto-container">
        	<h2>{!!ucfirst(@$data->name)!!}</h2>
			<div class="separater"></div>
        </div>
    </section>

    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('solutions')}}">Solutions</a> <span>/</span></li>
                <li>{!!ucfirst(@$data->name)!!}</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->

    <!-- Cross Border Express Section -->
	<section class="testimonial-section gap" id="globalsameday" style="margin-top: 3%;">
		<div class="auto-container">

            <div class="row">
                <div class="col-lg-4">
                    <div class="text-center sm-mb-55px">
                        <img src="{{asset('storage/app/public/images/solutions/'.$data->image)}}" alt="{{$data->image_name}}" style="margin-top: 0%;">
                    </div> 
                </div>
                <div class="col-lg-8">
                    <div class="inner-column">
                        <div class="sec-title">
                            <h3 style="font-family: Raleway;"> {!! @$data->name !!}  </h3>
                            <div class="separater"></div>
                        </h5>  <h5 style="font-weight: 500; padding-top: 2%; text-align: justify; padding-right: 5%; font-size: 17px;">{!! @$data->descriptions !!} </h5>
                </div>
            </div>
        </div>
    </div> 

<!-- Line Break -->
	<section class="domestic-services-section gap" style="margin-top:-6%;">
		<div class="auto-container">
		<!-- <div class="sec-title centered">
			<h3 style="font-family:Raleway;">Apparel<span> Info</span></h3>
			<div class="separater"></div>  -->
			</div>
		</div>
		<hr style="margin-top:4%; color:#555555; ">
		<div class="tem-sec">
			<div class="row"> 
			</div>
		</div>
	</section>
<!-- End Line Break -->

<!-- Warehouse Management System Elements -->
<section class="nile-about-section" style="margin-top: -3%; ">

@if(@$subcategory!=='' && @$subcategory!==null)
                @foreach(@$subcategory as $key=>$value)
                @if($loop->odd)
                <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
    <div class="auto-container">
        <div class="row clearfix">
                <div class="content-column col-lg-5 col-md-12 col-sm-12" style="margin-bottom: 2%;">
					<div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="sec-title-two sec-title">
							<h2> {!!@$value->name!!} </h2>
							<div class="separater"></div>
						</div></div>
                <div class="about-text margin-tb-25px">
                    <h6 style="padding-bottom: 2%; text-align: left;">
                    <ul> 
                        {!! @$value->descriptions !!}
                    </ul>
                    </h6>
                </div>
            </div>
            <div class="col-lg-6">
                <img src="{{asset('storage/app/public/images/solutions/'.@$value->image)}}" alt="{{@$value->image_name}}" style="padding-left: 7%;">
            </div> 
            <div style="margin-bottom: 60%;"></div>
        </div>
    </div>
    @else
    <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-lg-6">
                <img src="{{asset('storage/app/public/images/solutions/'.@$value->image)}}" alt="{{@$value->image_name}}" style="padding-left: 7%;">
            </div> 
            <div style="margin-bottom: 60%;"></div>
                <div class="content-column col-lg-5 col-md-12 col-sm-12" style="margin-bottom: 2%;">
                    <div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="sec-title-two sec-title">
                            <h2> {!!@$value->name!!} </h2>
                            <div class="separater"></div>
                        </div></div>
                <div class="about-text margin-tb-25px">
                    <h6 style="padding-bottom: 2%; text-align: left;">
                    <ul> 
                        {!! @$value->descriptions !!}
                    </ul>
                    </h6>
                </div>
            </div>            
        </div>
    </div>    
    @endif
  @endforeach
@endif


</div>
<!-- End We offer Section --> 
@include('footer')