@include('header')
<!--End Main Header -->


	<!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.$setting->header)}});">
    	<div class="auto-container">
        	<h2>{!!ucfirst(@$data->name)!!}</h2>
			<div class="separater"></div>
        </div>
    </section>

    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('solutions')}}">Solutions</a> <span>/</span></li>
                <li>{!!ucfirst(@$data->name)!!}</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->

    <!-- Cross Border Express Section -->
	<section class="testimonial-section gap" id="globalsameday" style="margin-top: 3%;">
		<div class="auto-container">

            <div class="row">
                <div class="col-lg-4">
                    <div class="text-center sm-mb-55px">
                        <img src="{{asset('storage/app/public/images/solutions/'.$data->image)}}" alt="" style="margin-top: -5%;">
                    </div>
                </div> 
                <div class="col-lg-8">
                    <div class="inner-column">
                        <div class="sec-title">
                            <h3 style="font-family: Raleway;">{!!@$healthcare->name!!}</span></h3>
                            <div class="separater"></div> <h5 style="font-weight: 500; padding-top: 2%; text-align: justify; padding-right: 5%;font-size: 17px;">{!!$data->descriptions!!}</h5>
                </div>
            </div>
            <!-- <div class="text-center margin-top-35px">
                <a href="#" class="nile-bottom md">Show all <i class="fa fa-arrow-right"></i> </a>
            </div> -->
        </div>
    </div> 
	
	<!-- Line Break -->
		<section class="domestic-services-section gap" style="margin-top:-7%;">
			<div class="auto-container">
			<!-- <div class="sec-title centered">
				<h3 style="font-family:Raleway;">Apparel<span> Info</span></h3>
				<div class="separater"></div>  -->
				</div>
			</div>
			<hr style="margin-top:4%; color:#555555; ">
			<div class="tem-sec">
				<div class="row"> 
	<!-- End Line Break -->

    <!-- Healthcare Supply Chain-->
	<section class="domestic-services-section gap" style="margin-top: -1%; padding-right: 5%;">
		<div class="auto-container">
		<div class="sec-title centered">
			<!-- <h3 style="font-family: Raleway;">Healthcare<span> Supply Chain</span></h3> -->
            <h3 style="font-family: Raleway;">{!!@$healthcare->getSubCategory[0]['name']!!}</h3>
			<div class="separater"></div> 
            </div>
		</div>
		<div class="tem-sec">
			<div class="row">

            @if(@$healthcare->getSubCategory[0] !=='')
            @foreach(@$healthcare->getSubCategory[0]->childsubcategory as $key=>$value)
            <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
                <div class="col-lg-3 col-md-6">
                    <div class="service-icon-box">
                        <div class="icon"><span class="{{@$value->class_name}}" style="color: #eb0028; font-size:55px; padding-bottom:15px;"></div><br>
							<h5 style="font-weight: 600; color:#eb0028;">{!!@$value->name!!}</h5>
                        <div class="des">{!!@$value->descriptions!!}
						</div>
                    </div>
                </div>
            
            @endforeach
            @endif
               
            </div>
		</div>
	</div>
</section>
<!-- End Healthcare Supply Chain --> 

<!-- We Offer -->
<section class="nile-about-section" style="margin-top: -6%; ">
    <div class="auto-container">

       @if(@$healthcare->getSubCategory !=='')
            @foreach(@$healthcare->getSubCategory as $key=>$value)
              @if($key>0)
              @if($loop->odd-1)
              <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
        <div class="row clearfix">
                <div class="content-column col-lg-5 col-md-12 col-sm-12" style="margin-bottom: 2%;">
					<div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="sec-title-two sec-title">
							<!-- <h2>We <span>Offer</span></h2> -->
                            <h2> {!!@$value->name!!} </h2>
							<div class="separater"></div>
						</div></div>
                <div class="about-text margin-tb-25px">
                    <h5 style="padding-bottom: 2%; text-align: left;"></h5>
                    
                        {!!@$value->descriptions!!}
                    
                </div>
                <!-- <a href="#" class="nile-bottom sm">Read More</a> -->

            </div>
            <div class="col-lg-6">
                <img src="{{asset('storage/app/public/images/solutions/'.@$value->image)}}" alt="{{@$value->image_name}}" style="padding-left: 7%;">
            </div>
            <div style="margin-bottom: 48%;"></div>
        </div>
        @else
        <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
        <div class="row clearfix">
            <div class="col-lg-6">
                <img src="{{asset('storage/app/public/images/solutions/'.@$value->image)}}" alt="{{@$value->image_name}}" style="padding-left: 7%;">
            </div>
                <div class="content-column col-lg-5 col-md-12 col-sm-12" style="margin-bottom: 2%;">
                    <div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="sec-title-two sec-title">
                            <!-- <h2>We <span>Offer</span></h2> -->
                            <h2>{!!@$value->name!!}</h2>
                            <div class="separater"></div>
                        </div></div>
                <div class="about-text margin-tb-25px">
                    <h5 style="padding-bottom: 2%; text-align: left;"></h5>
                     {!!@$value->descriptions!!}
                </div>
            </div>            
            <div style="margin-bottom: 48%;"></div>
        </div>
        @endif
        @endif
        @endforeach
        @endif



    </div>
</div>
<!-- End We offer Section --> 
@include('footer')