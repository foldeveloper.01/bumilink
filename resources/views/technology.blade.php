@include('header')
<!--End Main Header -->


	<!--Page Title-->
    <section class="page-title" style="background-image:url('{{asset('images/background/about-bg.png')}}')">
    	<div class="auto-container">
        	<h2>{!!ucfirst(@$data->name)!!}</h2>
			<div class="separater"></div>
        </div>
    </section>

    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('solutions')}}">Solutions</a> <span>/</span></li>
                <li>{!!ucfirst(@$data->name)!!}</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->

<!-- Technology Section --> 
<section class="testimonial-section gap" id="globalsameday" style="margin-top: 3%; margin-bottom: -8%;">
		<section class="auto-container">

            <div class="row">
                <div class="col-lg-4">
                    <div class="text-center sm-mb-55px">
                        <img src="{{asset('storage/app/public/images/solutions/'.$data->image)}}" alt="" style="margin-top: -5%;">
                    </div> 
                </div>
                <div class="col-lg-8">
                    <div class="inner-column">
                        <div class="sec-title">
                            <h3 style="font-family: Raleway;"> {!!@$data->name!!} </h3>
                            <div class="separater"></div>
                        <h5 style="font-weight: 500; padding-top: 2%; text-align: justify; padding-right: 5%; font-size: 17px;">{!!@$data->descriptions!!} </h5>
                </div>
            </div>
            <!-- <div class="text-center margin-top-35px">
                <a href="#" class="nile-bottom md">Show all <i class="fa fa-arrow-right"></i> </a>
            </div> -->
        </div>
    </div> 
</section></section>
<!-- End Technology Section -->

<!-- Apparel Description -->
	<section class="domestic-services-section gap" style="margin-top:-10%;">
		<div class="auto-container">
		<!-- <div class="sec-title centered">
			<h3 style="font-family:Raleway;">Apparel<span> Info</span></h3>
			<div class="separater"></div>  -->
            </div>
		</div>
        <hr style="margin-top:3%; color:#555555; ">
		<div class="tem-sec">
			<div class="row">
              @if(@$subcategory!=='' && @$subcategory!==null)
                @foreach(@$subcategory as $key=>$value)
                <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
                <div class="col-lg-3 col-md-6" style="margin-bottom: 7%;">
                    <div class="service-icon-box">
                        <div class="icon"><span class="{{@$value->class_name }}" style="color: #eb0028; font-size:55px; padding-bottom:35px;"></div><br>
                            <!-- <h2 style="color: #eb0028; font-size:70px;">&#10112;</h2> -->
                            <h5 style="font-weight: 600; color:#eb0028;"> {!!@$value->name!!} </h5>
                        <div align="center"><hr width="90px;" color="#eb0028" size="10;"></div>
                        <div class="des" style="font-size: 16px;">{!! @$value->descriptions !!} 
                        </div>
                    </div>
                </div>
                @endforeach
                @endif

            </div>
		</div>
</section>
<!-- End Apparel Info --> 
@include('footer')