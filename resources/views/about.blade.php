@include('header')
<!--End Main Header -->
	
	
	<!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.@$setting->header)}});">
    	<div class="auto-container">
        	<h2>About Us</h2>
			<div class="separater"></div>
        </div>
    </section>
    
    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('/')}}">Home</a> <span>/</span></li>
                <li>About Us</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->
	

	<!-- Vision and Mission -->
	<section class="team-section gap">
		<div class="auto-container">
		<div class="sec-title centered">
			<?php if(str_word_count(@$category->name)==1){ ?>
            <?php
            $output=substr(@$category->name, 2, strlen(@$category->name));
            $output1=substr(@$category->name, 0, 2);
            ?>
           <h3><?php  echo @$output1;?><span> <?php echo @$output;?> </span> </h3>
           <?php }else{ ?>
            <?php $output2=strtok(@$category->name, " ");
            $output3=substr(strstr(@$category->name," "), 1);
            ?>
            <h3>
          <?php echo @$output2; ?><span> <?php echo @$output3;?> </span>
          <?php } ?></h3>
			<div class="separater"></div>
		</div>
		<div class="tem-sec">
			<div class="row">
				<div class="col-lg-6 col-md-12 col-sm-12">
					<div class="tm-bx">
						<div class="tm-thmb">
							<img src="images/resource/home-vision.png" style="padding-bottom: 5%;"alt="Team-Image.jpg"></img>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12 col-sm-12">
					<div class="tm-bx">


						<div class="tm-thmb">

							@if(@$vission_data!=='' && @$vission_data!==null)
                               @foreach(@$vission_data as $key=>$value)
                            @if(@$key==0)
							<div class="tm-inf wowallow fadeInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
									<br>
									<span class="icon {{@$value->class_name}}" style="font-size:60px; color:#eb0028"></span>
									<h3 span style="color:#eb0028; font-weight: 700; line-height:60px;">{{@$value->name}}</h3>
									<span class="designation" style="text-align: justify; font-size: 12px;"> {!! @$value->descriptions!!}</span>
							</div>
							@endif
                            @if(@$key==1)
							 <div class="tm-bx">
								<div class="tm-thmb">
									<div class="tm-inff wowallow fadeInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
										<br>
										<span class="icon {{@$value->class_name}}" style="font-size:63px; color:#eb0028;text-align: center; padding-left: 44%; padding-right: 44%;"></span>
										<h3 span style="color:#eb0028; font-weight: 700; line-height:60px; text-align: center;">{{@$value->name}}</h3>
										<span class="designation" style="text-align: justify; font-size: 12px;">{!! @$value->descriptions!!}</span>									
									</div>
								</div>
							</div>
							@endif
							@endforeach
							@endif


				</div>

				 
			</div>
		</div><!-- Team Sec -->
	</div>
</section>
<!-- End Vision and Mission Sec -->

<!-- Our way of business Section -->
<section class="testimonial-section gap">
	<div class="auto-container">
		<div class="sec-title centered">
			<h3>Our <span>Way Of</span> Business</h3>
			<div class="separater"></div>
		</div>


		<div class="two-item-carousel owl-carousel owl-theme">
			<!-- Testimonial Block -->
          @if(@$ourbuiness!=='')
				@foreach(@$ourbuiness as $key=>$val)
			<div class="testimonial-block">
				<div class="inner-box">
					<div class="{{@$val->class_name}}" style="color:#eb0028; font-size: 35px;"></div> 
					<?php if(str_word_count(@$val->title)==1){ ?>
						<?php
						$output=substr(@$val->title, 2, strlen(@$val->title));
						$output1=substr(@$val->title, 0, 2);
						?>
                       <h3 style="color:black; font-weight: 600; line-height:50px;"><span style="color:#eb0028; font-weight: 600; line-height:50px;"><?php  echo @$output1;?></span><?php echo @$output;?></h3>
					<?php }else{ ?>
						<?php $output2=strtok(@$val->title, " ");
						$output3=substr(strstr(@$val->title," "), 1);
						?>
						<h3 style="color:black; font-weight: 600; line-height:50px;"><span style="color:#eb0028; font-weight: 600; line-height:50px;">
				    <?php echo @$output2; ?> </span> <?php echo @$output3;?>
				    <?php } ?> </h3>
					<div class="text-{{$key}}">{{strip_tags(@$val->description)}}</div>
			    </div> 
		    </div>
		    @endforeach
		    @endif

		</div>
		<!-- Testimonial Block -->
			
			
	</div>
</section>
<!-- End our way of business Section -->
	

	<!-- Why Bumilink Section -->
	<div class="nile-about-section">
        <div class="auto-container">
            <div class="row clearfix">

                    <!-- Content Column -->
				<div class="content-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="sec-title-two sec-title">
							<?php if(str_word_count(@$category1->name)==1){ ?>
                                        <?php
                                        $output=substr(@$category1->name, 2, strlen(@$category1->name));
                                        $output1=substr(@$category1->name, 0, 2);
                                        ?>
                                       <h3><?php  echo @$output1;?><span> <?php echo @$output;?> </span> </h3>
                                    <?php }else{ ?>
                                        <?php $output2=strtok(@$category1->name, " ");
                                        $output3=substr(strstr(@$category1->name," "), 1);
                                        ?>
                                        <h3>
                                    <?php echo @$output2; ?><span> <?php echo @$output3;?> </span> 
                                    <?php } ?> </h3>
							<div class="separater" style="margin-bottom: 5%;"></div>
						</div>

                    <div class="about-text margin-tb-25px"  style="margin-bottom: 2%;">{!! @$category1->descriptions!!}</div>
                    <a href="{{url('services')}}" class="nile-bottom sm" style="margin-top:2%; margin-bottom:5%;">Our Services</a> 

                    <div id="accordion" class="nile-accordion margin-top-80px sm-mb-45px" style="margin-bottom: 3%;">
                        @if(@$whyusebumilink!=='' && @$whyusebumilink!==null)
                         @foreach(@$whyusebumilink as $key=>$value)
                          @if(@$value->position=='left')
                        <div class="card">
                            <div class="card-header" id="headingOne{{$key}}">
                                <h5 class="mb-0">
                                    <button class="btn btn-block btn-link @if($key==0) active @endif" data-toggle="collapse" data-target="#collapseOne{{$key}}" aria-expanded="true" aria-controls="collapseOne{{$key}}"><span class="flaticon {{@$value->class_name}}"></span> {{@$value->name}} </button>
                                </h5>
                            </div>

                            <div id="collapseOne{{$key}}" class="collapse @if($key==0) show @endif" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body" style="font-size:15px;"> {!! @$value->descriptions !!} </div>
                            </div>
                        </div>
                           @endif
                         @endforeach
                       @endif
						
                    </div> 
				</div> 
			</div>
                        <div class="content-column col-lg-6 col-md-12 col-sm-12" >
							<div class="row"> 
								
                          
                          @if(@$whyusebumilink!=='' && @$whyusebumilink!==null)
                            @foreach(@$whyusebumilink as $key=>$value)
                             @if(@$value->position=='right')
                             @if($loop->odd)
							<div class="col-sm-6" style="margin-top: 1.5%">
                              <img src="{{asset('storage/app/public/images/subpages/'.@$value->image)}}" alt="{{@$value->image_name}}" height="450px;">
                        	</div>
							<div class="col-sm-6" style="margin-top: 1.5%">
                            	<div class="cart-service background-main-color">
                                	<div class="icon"><span class="flaticon {{@$value->class_name}}" style="font-size: 45px;"></div>
                                	<h2>{{@$value->name}}</h2>
                                	<hr>
                                	<div class="text" style="text-align: justify;">{!!@$value->descriptions!!}</div>
                            	</div>
                        	</div>
                        	 @else
							<div class="col-sm-6" style="margin-top: 1.5%"> 
								<div class="cart-service background-main-color">
                                	<div class="icon"><span class="flaticon {{@$value->class_name}}" style="font-size: 45px;"></div>
                                	<h2>{{@$value->name}}</h2>
                                	<hr>
                                	<div class="text" style="text-align: justify;">{!!@$value->descriptions!!}</div>
                            	</div>
                        	</div>
							<div class="col-sm-6" style="margin-top: 1.5%">
                            	<img src="{{asset('storage/app/public/images/subpages/'.@$value->image)}}" alt="{{@$value->image_name}}" height="450px;">
                        	</div>
                        	@endif
                        	@endif
                         @endforeach
                       @endif 


                	</div>
            	</div>
        	</div>
    	</div>
	
	<!-- Team Block -->
	<!-- <section class = "team-section">
            <div class="auto-container">
			<div class="sec-title centered">
				<h3>Professional <span>Team</span></h3>
				<div class="separater"></div>
			</div>
			<div class="tem-sec">
				<div class="row">
					<div class="col-md-3 col-sm-6 col-lg-3">
						<div class="tm-bx wowallow fadeInLeft">
							<div class="tm-thmb">
								<a href="#" title=""><img src="images/clients/38.jpg" alt="Team-Image.jpg"></a>
								<div class="tm-inf">
									<h5><a href="team-detail.html" title="">Tim Patinson</a></h5>
									<span class ="designation">Cargo Supervisor</span>
									<ul class="social-icon-one">
										<li><a href="#"><span class="fa fa-twitter"></span></a></li>
										<li><a href="#"><span class="fa fa-facebook"></span></a></li>
										<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					 <div class="col-md-3 col-sm-6 col-lg-3">
						<div class="tm-bx wowallow fadeInDown">
							<div class="tm-thmb">
								<a href="#" title=""><img src="images/clients/39.jpg" alt="Team-Image.jpg"></a>
								<div class="tm-inf">
									<h5><a href="team-detail.html" title="">Jina Patinson</a></h5>
									<span class ="designation">Shipping Manager</span>
									<ul class="social-icon-one">
										<li><a href="#"><span class="fa fa-twitter"></span></a></li>
										<li><a href="#"><span class="fa fa-facebook"></span></a></li>
										<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					 <div class="col-md-3 col-sm-6 col-lg-3">
						<div class="tm-bx wowallow fadeInUp">
							<div class="tm-thmb">
								<a href="#" title=""><img src="images/clients/40.jpg" alt="Team-Image.jpg"></a>
								<div class="tm-inf">
									<h5><a href="team-detail.html" title="">Mark Hustler</a></h5>
									<span class ="designation">Tracking Dept</span>
									<ul class="social-icon-one">
										<li><a href="#"><span class="fa fa-twitter"></span></a></li>
										<li><a href="#"><span class="fa fa-facebook"></span></a></li>
										<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-lg-3">
						<div class="tm-bx wowallow fadeInRight">
							<div class="tm-thmb">
								<a href="#" title=""><img src="images/clients/41.jpg" alt="Team-Image.jpg"></a>
								<div class="tm-inf">
									<h5><a href="team-detail.html" title="">John Doe</a></h5>
									<span class ="designation">IT Technician</span>
									<ul class="social-icon-one">
										<li><a href="#"><span class="fa fa-twitter"></span></a></li>
										<li><a href="#"><span class="fa fa-facebook"></span></a></li>
										<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </section> -->
	<!-- Team Sec -->
	
	<!--Sponsors Section-->
	<!-- <section class="sponsors-section">
		<div class="auto-container">
			
			<div class="carousel-outer">
                --Sponsors Slider--
                <ul class="sponsors-carousel owl-carousel owl-theme">
                    <li><div class="image-box"><a href="#"><img src="images/clients/1.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/2.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/3.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/1.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/4.png" alt=""></a></div></li>
					<li><div class="image-box"><a href="#"><img src="images/clients/1.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/2.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/3.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/1.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/4.png" alt=""></a></div></li>
                </ul>
            </div>
			
		</div>
	</section> -->
	<!--End Sponsors Section-->
	@include('footer')
