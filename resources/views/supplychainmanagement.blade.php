@include('header')
<!--End Main Header -->


	<!--Page Title-->
    <section class="page-title" style="background-image:url('{{asset('images/background/about-bg.png')}}')">
    	<div class="auto-container">
        	<h2>{!!ucfirst(@$data->name)!!}</h2>
			<div class="separater"></div>
        </div>
    </section>

    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('solutions')}}">Solutions</a> <span>/</span></li>
                <li>{!!ucfirst(@$data->name)!!}</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->

    <!-- Cross Border Express Section -->
	<section class="testimonial-section gap" id="globalsameday" style="margin-top: 3%;">
		<div class="auto-container">

            <div class="row">
                <div class="col-lg-4">
                    <div class="text-center sm-mb-55px">
                        <img src="{{asset('storage/app/public/images/solutions/'.$data->image)}}" alt="" style="margin-top: -5%;">
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="inner-column">
                        <div class="sec-title">
                            <h3 style="font-family: Raleway;">{!!@$supplychainmanagement->name!!}</h3>
                            <div class="separater"></div>
                        <h5 style="font-weight: 500; padding-top: 2%; text-align: justify; padding-right: 5%; font-size: 17px;">{!!@$data->descriptions!!} </h5>
                </div>
            </div>
        </div>
    </div> 

<!-- Line Break -->
	<section class="domestic-services-section gap" style="margin-top:-7%;">
		<div class="auto-container">
		<!-- <div class="sec-title centered">
			<h3 style="font-family:Raleway;">Apparel<span> Info</span></h3>
			<div class="separater"></div>  -->
			</div>
		</div>
		<hr style="margin-top:4%; color:#555555; ">
		<div class="tem-sec">
			<div class="row"> 
			</div>
		</div>
	</section>
<!-- End Line Break -->

<!-- Customers & Suppliers Section -->
<section class="nile-about-section" style="margin-top: -3%; margin-bottom: 6%;">
	<div class="auto-container">
		<div class="sec-title centered">
			<h3 style="font-family: Raleway;"> {!!@$supplychainmanagement->getSubCategory['0']['name']!!} </h3>
			<div class="separater"></div>
			<br>
		</div>

		<div class="row" style="margin-top: -3%;">

		  @if(@$supplychainmanagement->getSubCategory['0']['childsubcategory'] !=='')
                   @foreach(@$supplychainmanagement->getSubCategory['0']['childsubcategory'] as $key=>$value)
                   <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
			<div class="col-lg-4">
				<div class="service-icon-box" >
					<div><span class="{{@$value->class_name}}" style="font-size:50px; color:#eb0028;"></div>
						<h5 style="font-weight: 600; color:#eb0028; margin-top: 3%;">{!!@$value->name!!}</h5>
					<div class="des font-16"> {!! @$value->descriptions !!}
					</div><br>
				</div> 
			</div>
			@endforeach
           @endif
			
		</div> 
		<!-- <div class="text-center margin-top-35px">
			<a href="#" class="nile-bottom md">Show all <i class="fa fa-arrow-right"></i> </a>
		</div> -->

	</div>
</section>
<!-- End Customers & Suppliers Section -->  



@if(@$supplychainmanagement!=='' && @$supplychainmanagement!==null)
                @foreach(@$supplychainmanagement->getSubCategory as $key=>$value)
                @if($key!=0)
                <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
<!-- Marketing Fulfillment -->
<section class="services-section-two" style="margin-top: -5%; margin-bottom: -2%;">
    <div class="auto-container">
        <div class="row clearfix">
                <div class="content-column col-lg-4 col-md-12 col-sm-12" style="margin-bottom: 2%;">
					<div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="sec-title-two sec-title" style="text-align: left;">
							<!-- <h2>Our <span>Solutions</span></h2> -->
							<h2>{!!@$value->name!!}</h2>
							<div class="separater"></div>
							<h6 style="padding-top: 3%; padding-right:6%; text-align:justify; font-size: 17px;"> {{@$value->class_name}} </h6>
						</div></div>
                <div class="about-text margin-tb-25px">
                    <h5 style="text-align: left;"></h5>
						<div class="des" style="padding-right: 6%;">
				</div> 
			</div>
            </div>
            <div class="col-lg-7">
                <div class="about-text margin-tb-25px">
                    <h5 style="text-align: justify; padding-right: 4%; font-size: 18px;">
						{!! @$value->descriptions !!}
				</div> 
			</div>
            </div>
            <!-- <div style="margin-bottom: 40%;"></div> -->
        </div>
    </div>
</div>
@endif
@endforeach
@endif


</div>
<!-- End Marketing Fulfillment Section --> 
@include('footer')