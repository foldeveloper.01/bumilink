@include('header')
<!--End Main Header --> 

	<!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.@$setting->header)}});">
    	<div class="auto-container">
        	<h2>{!!ucfirst(@$data->name)!!}</h2>
			<div class="separater"></div>
        </div>
    </section>

    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('solutions')}}">Solutions</a> <span>/</span></li>
                <li>{!!ucfirst(@$data->name)!!}</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->

<!-- Customs Tax Consultancy Section -->
	<section class="testimonial-section gap" style="margin-top: 3%;">
		<div class="auto-container">

            <div class="row">
                <div class="col-lg-4">
                    <div class="text-center sm-mb-55px">
                        <img src="{{asset('storage/app/public/images/solutions/'.$data->image)}}" alt="" style="margin-top: -5%;">
                    </div> 
                </div>
                <div class="col-lg-8">
                    <div class="inner-column">
                        <div class="sec-title">
                            <h3 style="font-family: Raleway;">{!!@$customstaxconsultancy->name!!}</h3>
                            <div class="separater"></div>
                        <h5 style="font-weight: 500; padding-top: 2%; text-align: justify; padding-right: 5%; font-size: 17px;">{!!@$data->descriptions!!} </h5>
                </div>
            </div>
            <!-- <div class="text-center margin-top-35px">
                <a href="#" class="nile-bottom md">Show all <i class="fa fa-arrow-right"></i> </a>
            </div> -->
        </div>
    </div></div>
</section>
<!-- Customs Tax Consultancy Section -->


@if(@$customstaxconsultancy->getSubCategory !=='')
            @foreach(@$customstaxconsultancy->getSubCategory as $key=>$val)
<!-- Key Benefits & What We Offer Accordion -->
<section class="services-section-two" style="margin-top: -3%;">
    <div class="auto-container">
        <!-- <div class="row clearfix">  -->
			<div class="tem-sec">
				<div class="row">
			<div class="col-lg-4">
                <div class="sec-title">
					<!-- <h3 style="font-family:Raleway;">Our Key Services &<span> Key Benefits</span></h3> --> 
					<h3 style="font-family:Raleway;">{!!@$val->name!!}</h3> 
					<div class="separater"></div> 
					</div>
            </div>
			<div class="col-lg-7">
			<div id="accordion" class="nile-accordion margin-top-30px sm-mb-45px">
				
                @if(@$val->childsubcategory !=='')
                @foreach(@$val->childsubcategory as $keys=>$value)
                <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
				<div class="card">
					<div class="card-header" id="headingOne">
						<h5 class="mb-0">
							<button class="btn btn-block btn-link @if($keys==0) active @endif" data-toggle="collapse" data-target="#collapseOne{{@$keys}}" aria-expanded="true" aria-controls="collapseOne{{@$keys}}" style="font-size: 17px; font-weight:600;"><i class="fa {{@$value->class_name}}" style="padding-right:2%;"></i>{!!@$value->name!!}</button>
						</h5>
					</div>
					<div id="collapseOne{{@$keys}}" class="collapse @if($keys==0) show @endif" aria-labelledby="headingOne" data-parent="#accordion">
						<div class="card-body">
							{!! @$value->descriptions!!}
						</div>
					</div>
				</div>				
				@endforeach
				@endif
				
				


            </div> 
            <div style="margin-bottom: 3%;"></div>
        </div>
    </div>
</div></section>
  @endforeach
     @endif
   

<!-- End Key Benefits & What We Offer Section --> 
@include('footer')