@include('header')
<!--End Main Header -->
	
	
	<!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.@$setting->header)}});">
    	<div class="auto-container">
        	<h2>Cargo Insurance</h2>
			<div class="separater"></div>
        </div>
    </section>
    
    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('services')}}">Services</a> <span>/</span></li>
                <li>{!!@$data->name!!}</li>
            </ul>
        </div>
    </div>
    <!--End Page Title--> 

	<!-- Cargo Insurance Section -->
	@if(@$data->getSubCategory !=='')
            @foreach(@$data->getSubCategory as $key=>$val)
            <span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>
	<section class="nile-about-section" style="margin-top: 3%;">
		<div class="auto-container">
			<div class="sec-title centered">
				<!-- <h3 style="font-family: Raleway;">Cargo <span>Insurance</span></h3> -->
				<h3 style="font-family: Raleway;">{!!@$val->name!!}</h3>
				<div class="separater"></div><br>
            </div> 
				<div class="text" style="text-align: justify; padding-left: 5%; padding-right: 5%; margin-top: -3%; font-size: 16px;">
					{!! @$val->text !!}
                </div> 
                    <div class="row"> 
                
						<div class="row"  style="padding-top: 2%; margin-top: 3%; margin-bottom: 6.5%;"> 
							<div class="col-lg-5">
								<div class="text-center sm-mb-55px">
									<img src="{{asset('storage/app/public/images/services/'.$val->image)}}" alt="{{@$val->image_name}}"> 
								</div>
							</div>
							<div class="col-lg-7">
								
								{!! @$val->descriptions !!}
							</div>
			
			
			
						</div> 
			
						<!-- <div class="text-center margin-top-35px">
							<a href="#" class="nile-bottom md">Show all <i class="fa fa-arrow-right"></i> </a>
						</div> -->
			
					</div>
				</div>
			</div> 
	</section>
	@endforeach
	@endif


    <!-- End Cargo Insurance Section --> 
@include('footer')