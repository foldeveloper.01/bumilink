@include('header')
<!--End Main Header -->
	
	<!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.@$setting->header)}});">
    	<div class="auto-container">
        	<h2>Our Solutions</h2>
			<div class="separater"></div>
        </div>
    </section>
    
    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('/')}}">Home</a> <span>/</span></li>
                <li>Our Solutions</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->
	
<!-- Fleet Section -->
<section class="fleet-section">
    <div class="auto-container">
        <div class="row clearfix">
            
            <!-- Fleet Block -->
            @if($data!=='')
            @foreach($data as $key=>$value)
            <span id="{{preg_replace('/\s+/','',@$value->page_url)}}"></span>
            <div class="fleet-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image">
                        <a href="{{url('solutions/'.@$value->page_url)}}"><img src="{{asset('storage/app/public/images/solutions/'.$value->image)}}" alt="{{@$value->image_name}}"/></a>
                    </div>
                    <div class="lower-content">
                        <h3><a href="{{url('solutions/'.@$value->page_url)}}">{!!@$value->name!!}</a></h3>
                        <ul class="fleet-list">
                            <!-- <li><h5 style="color:#eb0028; font-weight: 600;">Description:</h5></li> -->
                            <li style="text-align: justify; padding-right: 1.5%;">{{ strip_tags($value->descriptions) }}</li>   
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
            
            
        </div>
        
        <!--Styled Pagination-->
        <!-- <ul class="styled-pagination text-center">
            <li class="prev"><a href="#"><span class="fa fa-angle-left"></span></a></li>
            <li><a href="#">1</a></li>
            <li><a href="#" class="active">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li class="next"><a href="#"><span class="fa fa-angle-right"></span></a></li>
        </ul>                 -->
        <!--End Styled Pagination-->
        
    </div>
</section>
<!-- End Our Solution Section --> 
@include('footer')