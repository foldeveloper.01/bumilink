@include('header')
<!--End Main Header -->
    
	
	<!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.$setting->header)}});">
    	<div class="auto-container">
        	<h2>Track & Trace</h2>
			<div class="separater"></div>
        </div>
    </section>
    
    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('/')}}">Home</a> <span>/</span></li>
                <li>Track & Trace</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
				
				<!--Content Side-->
                <div class="content-side col-lg-9 col-md-12 col-sm-12">
                	<div class="track-section">
						<!-- Sec Title Two -->
						<div class="sec-title-two sec-title">
							<h2>Track & <span>Trace Shipment</span></h2>
							<div class="separater"></div>
						</div>
						
						<!-- Quote Form Box -->
						<div class="quote-form-box">
							<!--Login Form-->
							<form method="post" action="contact.html">
								<div class="row clearfix">
									
									<!-- Form Group -->
									<div class="form-group option-box col-lg-12 col-md-12 col-sm-12">
										<div class="radio-box">
											<input type="radio" name="gender" id="male">&nbsp; 
											<label for="male">Air Freight</label>
										</div>
										<div class="radio-box">
											<input type="radio" name="gender" id="female">&nbsp; 
											<label for="female">Road Freight</label>
										</div>
										<div class="radio-box">
											<input type="radio" name="gender" id="other">&nbsp; 
											<label for="other">Sea Freight</label>
										</div>
										<div class="radio-box">
											<input type="radio" name="gender" id="others">&nbsp; 
											<label for="other">Insurance</label>
										</div>
									</div>
								</div>
							</form>
						</div>
						
						<!-- Track Form -->
						<div class="track-form-two">
							<form method="post" action="quote.html">
								<div class="form-group">
									<label>Enter Tracking Number Here</label>
								</div>
								<div class="form-group">
									<input type="text" name="text" placeholder="Enter your tracking number e.g CRG-11-XXXX">
									<button type="submit" class="theme-btn submit-btn">Track Your Shipment</button>
								</div>
							</form>
						</div>
						
						<!-- Tracking Info -->
						<div class="tracking-info-detail">
							<div class="tracking-box">
								<div class="tracking-time-box">
									<div class="tracking-time">Nov 26, 2018</div>
									<span>03:36 AM</span>
								</div>
								<div class="tracking-location">
									<strong>Delivered in California </strong>
									Carga Office Near 10-G Location South
								</div>
							</div>
							
							<div class="tracking-box">
								<div class="tracking-time-box">
									<div class="tracking-time">Nov 28, 2018</div>
									<span>05:40 PM</span>
								</div>
								<div class="tracking-location style-two">
									<strong>Out For Delivery </strong>
									Union WareHouse 5th Avenue Block
								</div>
							</div>
							
							<div class="tracking-box">
								<div class="tracking-time-box">
									<div class="tracking-time">Nov 29, 2018</div>
									<span>12:35 PM</span>
								</div>
								<div class="tracking-location style-three">
									<span class="dott"></span>
									<strong>Arrive at Post Office</strong>
									Local Post Office In Near Vicinity
								</div>
							</div>
							
							<div class="tracking-box">
								<div class="tracking-time-box">
									<div class="tracking-time">Dec 02, 2018</div>
									<span>03:10 AM</span>
								</div>
								<div class="tracking-location style-three">
									<span class="dott"></span>
									<strong>Departed Shipping Facility</strong>
									Carga Office Departure Location Road
								</div>
							</div>
							
							<div class="tracking-box">
								<div class="tracking-time-box">
									<div class="tracking-time">Dec 04, 2018</div>
									<span>09:00 AM</span>
								</div>
								<div class="tracking-location style-three">
									<span class="dott"></span>
									<strong>Picked up by Shipping Partner </strong>
									Will Be Delivered at Your Location
								</div>
							</div>
							
						</div>
						
						<!--Map Outer-->
						<div class="map-outer">
							<!--Map Canvas-->
							<div class="map-canvas"
								data-zoom="12"
								data-lat="-37.817085"
								data-lng="144.955631"
								data-type="roadmap"
								data-hue="#ffc400"
								data-title="Envato"
								data-icon-path="images/icons/map-marker.png"
								data-content="Melbourne VIC 3000, Australia<br><a href='mailto:info@youremail.com'>info@youremail.com</a>">
							</div>
						</div>
						
					</div>
				</div>
				
				<!--Sidebar Side-->
                <div class="sidebar-side col-lg-3 col-md-12 col-sm-12">
                	<aside class="sidebar">
						
						<!-- Search -->
						<div class="sidebar-widget search-box">
							<div class="sidebar-title">
								<h2>Search</h2>
							</div>
							<form method="post" action="contact.html">
								<div class="form-group">
									<input type="search" name="search-field" value="" placeholder="Search" required>
									<button type="submit"><span class="icon fa fa-search"></span></button>
								</div>
							</form>
						</div>
						
						<!-- Category Box -->
						<div class="sidebar-widget category-box">
							<div class="sidebar-title">
								<h2>Categories</h2>
							</div>
							<ul class="category-list">
								<li><a href="#">Air Freight Services <span>( 10 )</span></a></li>
								<li><a href="#">Sea Frieght <span>( 06 )</span></a></li>
								<li><a href="#">Industrial fire training <span>( 03 )</span></a></li>
								<li><a href="#">Warehousing <span>( 07 )</span></a></li>
								<li><a href="#">Transport <span>( 12 )</span></a></li>
								<li><a href="#">Cargo Logistic <span>( 02 )</span></a></li>
							</ul>
						</div>
						
						<!--Popular Posts-->
						<div class="sidebar-widget popular-posts">
							<div class="sidebar-title">
								<h2>Latest Post</h2>
							</div>

							<article class="post">
								<figure class="post-thumb"><a href="blog-single.html"><img src="images/resource/post-thumb-1.jpg" alt=""></a></figure>
								<h4><a href="blog-single.html">Excepteur sint occaecat</a></h4>
								<div class="text">Quisque nec auctor, tincidunt ligulagh.</div>
								<div class="post-info">Oct 25, 2018</div>
							</article>

							<article class="post">
								<figure class="post-thumb"><a href="blog-single.html"><img src="images/resource/post-thumb-2.jpg" alt=""></a></figure>
								<h4><a href="blog-single.html">Excepteur sint occaecat</a></h4>
								<div class="text">Quisque nec auctor, tincidunt ligulagh.</div>
								<div class="post-info">Oct 25, 2018</div>
							</article>
							
							<article class="post">
								<figure class="post-thumb"><a href="blog-single.html"><img src="images/resource/post-thumb-3.jpg" alt=""></a></figure>
								<h4><a href="blog-single.html">Excepteur sint occaecat</a></h4>
								<div class="text">Quisque nec auctor, tincidunt ligulagh.</div>
								<div class="post-info">Oct 25, 2018</div>
							</article>

						</div>
						
					</aside>
				</div>
				
			</div>
		</div>
	</div>
	
	<!--Main Footer-->
@include('footer')