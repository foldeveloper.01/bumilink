@include('header')
<!--End Main Header -->


	<!--Page Title-->
   <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.$setting->header)}});">
    	<div class="auto-container">
        	<h2>{!!ucfirst(@$data->name)!!}</h2>
			<div class="separater"></div>
        </div>
    </section>

    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('solutions')}}">Solutions</a> <span>/</span></li>
                <li>{!!ucfirst(@$data->name)!!}</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->

    <!-- Cross Border Express Section -->
	<section class="testimonial-section gap" id="globalsameday" style="margin-top: 3%;">
		<div class="auto-container">

            <div class="row">
                <div class="col-lg-4">
                    <div class="text-center sm-mb-55px">
                        <img src="{{asset('storage/app/public/images/solutions/'.$data->image)}}" alt="{{@$data->image_name}}" style="margin-top: -5%;">
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="inner-column">
                        <div class="sec-title">
                           <h3 style="font-family: Raleway;">{!!@$data->name!!}</h3>
                            <div class="separater"></div>
                            <h5 style="font-weight: 500; padding-top: 2%; text-align: justify; padding-right: 5%; font-size: 18px;"> {!!@$data->descriptions!!} </h5>
                </div>
            </div>
            <!-- <div class="text-center margin-top-35px">
                <a href="#" class="nile-bottom md">Show all <i class="fa fa-arrow-right"></i> </a>
            </div> -->
        </div>
    </div> 
	
	<!-- Line Break -->
	<section class="domestic-services-section gap" style="margin-top:-10%;">
		<div class="auto-container">
		<!-- <div class="sec-title centered">
			<h3 style="font-family:Raleway;">Apparel<span> Info</span></h3>
			<div class="separater"></div>  -->
            </div>
		</div>
        <hr style="margin-top:7%; color:#555555; ">
		<div class="tem-sec">
			<div class="row">
	<!-- End Line Break -->

<!-- Logistics & Fulfillment Services -->
<section class="nile-about-section" style="margin-top: 6%; margin-bottom: 5%; ">
	<span id="{{preg_replace('/\s+/','',@$ecommerce->getSubCategory[0]['name'])}}"></span>
    <div class="auto-container">
        <div class="row clearfix">

                <div class="content-column col-lg-5 col-md-12 col-sm-12" style="margin-bottom: 0%;">
					<div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="sec-title-two sec-title">
							<!-- <h2 style="font-size: 44px;">Logistics & <span>Fulfillment Services</span></h2> -->
							<h2 style="font-size: 44px;">{!!@$ecommerce->getSubCategory[0]['name']!!}</span></h2>
							<div class="separater"></div>
						</div></div>
						<div class="about-text margin-tb-20px">
							<!-- <h5  style="padding-bottom: 3%;">All the services include: </h5> -->
							<ul style="text-align: left;">
								{!! @$ecommerce->getSubCategory[0]['descriptions'] !!}
							</ul></div>
                <!-- <a href="#" class="nile-bottom sm">Read More</a> -->

            </div>
            <div class="col-lg-6">
                <img src="{{asset('storage/app/public/images/solutions/'.@$ecommerce->getSubCategory[0]['image'])}}" alt="{{@$ecommerce->getSubCategory[0]['image_name']}}" style="padding-left: 7%;">
            </div> 
            <div style="margin-bottom: 0%;"></div>
        </div>
    </div>
</div>
<!-- End Logistics & Fulfillment Services Section --> 

<!-- E-commerce Solution Section -->
<span id="{{preg_replace('/\s+/','',@$ecommerce->getSubCategory[1]['name'])}}"></span>
<section class="nile-about-section" style="margin-top: 1%;">
	<div class="auto-container">
		<div class="sec-title centered">
			<!-- <h3 style="font-family: Raleway;">E-Commerce <span>Solutions</span></h3> -->
			<h3 style="font-family: Raleway;">{!!@$ecommerce->getSubCategory[1]['name']!!}</h3>
			<div class="separater"></div>
			<br>
		</div>

  
		<div class="row">
			@if(@$ecommerce->getSubCategory[1] !=='')
            @foreach(@$ecommerce->getSubCategory[1]->childsubcategory as $key=>$value)
			@if($value->position=='left')
			<span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
			<div class="col-lg-4">
				<div class="service-icon-box">
					<div><span class="{{@$value->class_name}}" style="font-size:50px; color:#eb0028;"></div>
						<h5 style="font-weight: 600; color:#eb0028; margin-top: 3%;">{!!@$value->name!!}</h5>
					<div class="des">{!!@$value->descriptions!!}
					</div><br>
				</div> 
			</div>
			@endif
			@endforeach
            @endif
			
			<div class="col-lg-4">
				<div class="text-center sm-mb-45px"> 
					<img src="{{asset('storage/app/public/images/solutions/'.@$ecommerce->getSubCategory[1]['image'])}}" alt="{{@$ecommerce->getSubCategory[1]['image_name']}}" class="border-radius-500">
					<div class="des" style="padding-left:5%; padding-right: 5%; padding-top: 6%;"><b></b></div>
				</div>
			</div>
			
			@if(@$ecommerce->getSubCategory[1] !=='')
            @foreach(@$ecommerce->getSubCategory[1]->childsubcategory as $key=>$value)
			@if($value->position=='right')
			<span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
			<div class="col-lg-4">
				<div class="service-icon-box">
					<div><span class="{!!@$value->class_name!!}" style="font-size:55px; color:#eb0028; "></div>
						<h5 style="font-weight: 600; color:#eb0028; margin-top: 2%;"> {!!@$value->name!!}</h5>
					<div class="des">{!!@$value->descriptions!!}
					</div>
				</div> 
			</div> 
			@endif
			@endforeach
            @endif
		</div> 
    
    


		<!-- <div class="text-center margin-top-35px">
			<a href="#" class="nile-bottom md">Show all <i class="fa fa-arrow-right"></i> </a>
		</div> -->

	</div>
</div>
<!-- E-commerce Solution Section -->  

	<!-- Apparel Description -->
	<span id="{{preg_replace('/\s+/','',@$ecommerce->getSubCategory[2]['name'])}}"></span>
	<section class="domestic-services-section gap" style="margin-top:1%;">
		<div class="auto-container">
		<div class="sec-title centered">
			<!-- <h3 style="font-family:Raleway;">Development<span> & Hosting</span></h3> -->
			<h3 style="font-family:Raleway;"> {!!@$ecommerce->getSubCategory[2]['name']!!} </h3>
			<div class="separater"></div> 
            </div>
		</div> 
		<div class="tem-sec">
			<div class="row">

@if(@$ecommerce->getSubCategory[2] !=='')
  @foreach(@$ecommerce->getSubCategory[2]->childsubcategory as $key=>$value)
  @if($value->position=='left')
  <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
                <div class="col-lg-5 col-md-6">
                    <div class="service-icon-box-ecommerce">
                        <div class="icon"><span class="{{@$value->class_name}}" style="color: #eb0028; font-size:60px; padding-bottom:35px;"></div><br>
                            <!-- <h2 style="color: #eb0028; font-size:70px;">&#10112;</h2> -->
                            <h5 style="font-weight: 600; color:#eb0028;"> {!!@$value->name!!} </h5>
                        <div align="center"><hr width="90px;" color="#eb0028" size="10;"></div>
                        <div class="des" style="font-size: 16px;">
							{!!@$value->descriptions!!}
                        </div>
                    </div>
                </div>
    @endif
    @if($value->position=='right')
    <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
                <div class="col-lg-5 col-md-6">
                    <div class="service-icon-box-ecommerce">
                        <div class="icon"><span class="{{@$value->class_name}}" style="color: #eb0028; font-size:60px; padding-bottom:45px;"></div><br> 
                            <h5 style="font-weight: 600; color:#eb0028;"> {!!@$value->name!!}
							</h5>
                        <div align="center"><hr width="90px;" color="#eb0028" size="10;"></div>
                        <div class="des"  style="font-size: 16px;">
                        	{!!@$value->descriptions!!}
                        </div>
                    </div>
                </div>
    @endif
    @endforeach
    @endif



            </div>
		</div>
	</div>
</section>
<!-- End Development Hosting -->

<!-- Marketing Fulfillment -->
<section class="services-section-two" style="margin-top: -5%;">
    <div class="auto-container">
        
  @if(@$ecommerce->getSubCategory !=='')
  @foreach(@$ecommerce->getSubCategory as $key=>$value)
  @if($key>2)
  <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
        <div class="row clearfix">

                <div class="content-column col-lg-4 col-md-12 col-sm-12" style="margin-bottom: 2%;">
					<div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="sec-title-two sec-title" style="text-align: left;">
							<!-- <h2>Marketing <span>Fulfillment</span></h2> -->
							<h2> {!!@$value->name!!} </h2>
							<div class="separater"></div>
						</div></div>

               </div>

            <div class="col-lg-7">
                <div class="about-text margin-tb-25px">
                      {!!@$value->descriptions!!}                    
				</div> 
			</div>
            </div>
        </div>
        @endif
        @endforeach
        @endif




    </div>
</div>
<!-- End Marketing Fulfillment Section --> 
@include('footer')