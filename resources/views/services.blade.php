@include('header')	
	<!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.@$setting->header)}});">
    	<div class="auto-container">
        	<h2>Offered Services</h2>
			<div class="separater"></div>
        </div>
    </section>
    
    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('/')}}">Home</a> <span>/</span></li>
                <li>Services</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->
	
	<section class="services-section page-view red">
		<div class="auto-container">
			<div class="row clearfix">
				<!-- Services Block Two -->
				@if(@$data !=='')
               @foreach(@$data as $key=>$value)
				<div class="services-block-two col-lg-4 col-md-6 col-sm-12" id="{{@$value->page_url}}">
					<div class="inner-box">
						<div class="icon-box">
							<span class="icon {{@$value->class_name}}" style="font-size: 37px;"></span>
						</div>
						<h2><a href="{{url(@$value->page_url)}}">{{@$value->name}}</a></h2>
						<div class="text" style="text-align: justify; padding-bottom: 28%;"> {!!@$value->descriptions!!}</div>
						<a href="{{url(@$value->page_url)}}" class="read-more">Read more</a>
					</div>
				</div>
				@endforeach
				@endif

				
				
			</div>
		</div>
	</section>
@include('footer')