@include('header')

<!--End Main Header -->

	

	

	<!--Page Title-->

    <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.$setting->header)}});">

    	<div class="auto-container">

        	<h2>{!!@$data->name!!}</h2>

			<div class="separater"></div>

        </div>

    </section>

    

    <!--Breadcrumb-->

    <div class="breadcrumb-outer">

    	<div class="auto-container">

        	<ul class="bread-crumb text-center">

            	<li><a href="{{url('services')}}">Services</a> <span>/</span></li>

                <li>{!!@$data->name!!}</li>

            </ul>

        </div>

    </div>

    <!--End Page Title-->



@if(@$data->getSubCategory !=='')

    @foreach(@$data->getSubCategory as $key=>$val)

       @if($key<3)

        @if($key==0)

	<!-- Linked Same Day -->

    <span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>

	<section class="domestic-services-section gap" id="linkedsameday">

		<div class="auto-container">

		<div class="sec-title centered">

			<!-- <h3 style="font-family: Raleway;">Linked<span> Same Day</span></h3> -->

			<h3 style="font-family: Raleway;">{!!@$val->name!!}</h3>

			<div class="separater"></div> 

            </div>

		</div>

		<div class="tem-sec">

			<div class="row">



               @if(@$val->childsubcategory!=='')

               @foreach(@$val->childsubcategory as $key=>$value)

               <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>

                <div class="col-lg-3 col-md-6">

                    <div class="service-icon-box">

                        <div class="icon"><span class="flaticon {{@$value->class_name}}" style="color: #eb0028; font-size:60px; padding-bottom:45px;"></div><br>

							<h5 style="font-weight: 600; color:#eb0028;">{!!@$value->name!!}</h5>

                        <div class="des"> {!! @$value->descriptions !!}

                        </div>

                    </div>

                </div>

                @endforeach

                @endif

                

            </div>

		</div>

	</div>

</section>

<!-- End Linked Same Day -->

@endif

@if($key==1)

<!-- Linked Overnight -->

<span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>

<section class="nile-about-section" id="linkedovernight">

    <div class="auto-container">

        <div class="row clearfix">

                <div class="content-column col-lg-6 col-md-12 col-sm-12" style="margin-bottom: 2%;">

					<div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">

						<div class="sec-title-two sec-title">

							<!-- <h2>Linked <span>Overnight </span></h2> -->

							<h2>{!!@$val->name!!}</h2>

							<div class="separater"></div>

						</div></div>

                <div class="about-text margin-tb-25px">

                    <h5 style="padding-bottom: 2%;">{!!@$val->text!!} </h5><br>

                    {!!@$val->descriptions!!}

                </div>

            </div>

            <div class="col-lg-6">

				<div class="image-domestic-linked-overnight">

                <img src="images/resource/domesticservice-linked-overnight.png" alt=""></div>

            </div>

        </div>

    </div>

</div>

<!-- End Linked Overnight Section -->

@endif

@if($key==2)

<!-- Linked Economy -->

<span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>

<section class="nile-about-section" id="linkedeconomy" style="margin-top: 5%;">

    <div class="auto-container">

        <div class="row clearfix"> 

            <div class="col-lg-6">

				<div class="image-domestic-linked-economy">

                <img src="images/resource/domesticservice-linked-economy.png" alt="">

            </div></div>



            <div class="content-column col-lg-6 col-md-12 col-sm-12" style="margin-bottom: 14%;">

                <div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">

                    <div class="sec-title-two sec-title">

                        <!-- <h2>Linked <span>Economy </span></h2> -->

                        <h2>{!!@$val->name!!}</h2>

                        <div class="separater"></div>

                    </div></div>

            <div class="about-text margin-tb-25px">

                <h5 style="padding-bottom: 2%;"> {!!@$val->text!!} </h5><br>{!!@$val->descriptions!!}

            </div>

            <!-- <a href="#" class="nile-bottom sm" style="margin-top:8%;">Read More</a>  -->

        </div>

    </div>

</div>

<!-- End Linked Economy -->

@endif

 @endif

  @endforeach

   @endif







@if(@$data->getSubCategory !=='')

    @foreach(@$data->getSubCategory as $key=>$val)

       @if($key==3)

     <!-- Transportion Section -->

     <span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>

	 <section class="transport-section" style="background-image:url(images/background/domesticservice-ltl-ftl.png)" id="transportation">

		<div class="auto-container">



			<div class="sec-title-two centered light sec-title">

				<h2>{!! @$val->name !!}</h2>

				<div class="separater"></div>

				<div class="text" style="color: #ffffff; text-align: justify;padding-left: 5%; padding-right: 5%; padding-top: 2%;">

					{!! @$val->descriptions !!}

				</div>

			</div> 

			<div class="clearfix"> 

				<!-- Transport Block -->





            @if(@$val->childsubcategory!=='')

               @foreach(@$val->childsubcategory as $key=>$value)

               <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>

				<div class="transport-block col-lg-5 col-md-6 col-sm-12">

					<div class="inner-box wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">

                        @if($loop->odd)

						<div class="{{@$value->class_name}}" style="font-size:50px; padding-top:10%; padding-bottom: 6%; color:#eb0028"></div>

						<div class="title-first" style="padding-left: 20%; padding-right: 20%;">

                        @else

                        <div class="flaticon-moving-truck" style="font-size:60px; padding-top:10%; padding-bottom: 6%; color:#eb0028"></div>

                        <div class="title-first"  style="padding-left: 12%; padding-right: 12%;">

                        @endif

						  {!!@$value->name!!} 

							<div align="center"><hr width="90px;" color="#eb0028" size="10;"></div>

						</div>

						<!-- <div class="price-box">

							<div class="price">$50.0</div>

							<div class="months">/KM Distance</div>

						</div> -->

						<div class="transport-list-first"> 

							<span class="tech-content" style="font-size: 15px;">

								{!! @$value->descriptions !!} 

						</div>

						<!-- <div class="btn-box">

							<a href="#" class="theme-btn choose-btn">Choose Now</a>

						</div> -->

					</div>

				</div>

				@endforeach

				@endif

				

				

					</div>

				</div>

			</div>

		</div>

	</section>

	<!-- End Transportation Section -->

	@endif

  @endforeach

@endif









@if(@$data->getSubCategory !=='')

    @foreach(@$data->getSubCategory as $key=>$val)

       @if($key >3)

       @if($loop->odd)

	<!-- Home Deliveries & e-Commerces -->

    <span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>

<section class="nile-about-section" id="homedeliveries" style="margin-top: 6%;">

    <div class="auto-container">

        <div class="row clearfix">



                <div class="content-column col-lg-6 col-md-12 col-sm-12" style="margin-bottom: 2%;">

					<div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">

						<div class="sec-title-two sec-title">

							<!-- <h2>Home Deliveries <span><br>& e-Commerces</span></h2> -->

							<h2>{!!@$val->name!!}</h2>

							<div class="separater"></div>

						</div></div>

                <div class="about-text margin-tb-25px">

                    <h5 style="padding-bottom: 2%;">{!!@$val->text!!}</h5><br>

                    {!!@$val->descriptions!!}

                </div>

                <!-- <a href="#" class="nile-bottom sm">Read More</a> -->



            </div>

            <div class="col-lg-6">

                <img src="{{asset('storage/app/public/images/services/'.@$val->image)}}" alt="{{@$val->image_name}}" style="padding-left: 7%;">

            </div>

        </div>

    </div>

</div>

<!-- End Home Deliveries & e-Commerces -->

@else

<!-- Retail Merchandising -->

<span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>

<section class="nile-about-section" id="retailmerchandising" style="margin-top: 3%;">

    <div class="auto-container"> 

        <div class="row clearfix"> 

            <div class="col-lg-6">

                <img src="{{asset('storage/app/public/images/services/'.@$val->image)}}" alt="{{@$val->image_name}}" style="padding-right: 7%;">

            </div>



            <div class="content-column col-lg-6 col-md-12 col-sm-12" style="margin-bottom: 2%;">

                <div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">

                    <div class="sec-title-two sec-title">

                       <!--  <h2>Retail<span> Merchandising</span></h2> -->

                        <h2>{!!@$val->name!!}</h2>

                        <div class="separater"></div>

                    </div></div>

            <div class="about-text margin-tb-25px" style="margin-bottom: 10%;">

                {!!@$val->text!!}</h5>

                {!!@$val->descriptions!!}

            </div>

            <!-- <a href="#" class="nile-bottom sm" style="margin-top:2%;">Read More</a>  -->

        </div>

    </div>

</div>

</section>

@endif

@endif

@endforeach

@endif



	<!--End Sponsors Section-->

@include('footer')