@include('header')
<!--End Main Header -->


	<!--Page Title-->
    <section class="page-title" style="background-image:url('{{asset('images/background/about-bg.png')}}')">
    	<div class="auto-container">
        	<h2>{!!ucfirst(@$data->name)!!}</h2>
			<div class="separater"></div>
        </div>
    </section>

    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('solutions')}}">Solutions</a> <span>/</span></li>
                <li>{!!ucfirst(@$data->name)!!}</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->

    <!-- Cross Border Express Section -->
	<section class="testimonial-section gap" id="globalsameday" style="margin-top: 3%; margin-bottom: 3%;">
		<div class="auto-container">

            <div class="row">
                <div class="col-lg-4">
                    <div class="text-center sm-mb-55px">
                        <img src="{{asset('storage/app/public/images/solutions/'.$data->image)}}" alt="" style="margin-top: -5%;">
                    </div> 
                </div>
                <div class="col-lg-8">
                    <div class="inner-column">
                        <div class="sec-title">
                            <h3 style="font-family: Raleway;">{!!@$manufacture->name!!}</h3>
                            <div class="separater"></div>
                        <h5 style="font-weight: 500; padding-top: 2%; text-align: justify; padding-right: 5%;font-size: 17px;">{!!@$data->descriptions!!}</h5>
                </div>
            </div>
            <!-- <div class="text-center margin-top-35px">
                <a href="#" class="nile-bottom md">Show all <i class="fa fa-arrow-right"></i> </a>
            </div> -->
        </div>
	</div>
    </div>
</section> 

<!-- Key Benefits & What We Offer Accordion -->
<section class="services-section-two" style="margin-top: 4%; padding-right: 2.5%;">
    <div class="auto-container">
		<div class="sec-title centered"> 
			<h4 style="font-family:Raleway; font-weight: 600; color: #eb0028; font-size: 26px;">That is why we have developed a range of Manufacturing Logistics solutions</h4>
        <!-- <div class="row clearfix">  --> 
            </div> 
			<div class="row">

				<div class="col-lg-5">
				 @if(@$manufacture->getSubCategory['0']['childsubcategory'] !=='')
                   @foreach(@$manufacture->getSubCategory['0']['childsubcategory'] as $key=>$val)
                   @if($val->position=='left')
                   <span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>
				<div id="accordion" class="nile-accordion margin-top-30px sm-mb-45px">
					<div class="card">
						<div class="card-header" id="headingOne">
							<h5 class="mb-0">
							<button class="btn btn-block btn-link" style="font-size: 17px; font-weight:600;"><i class="fa {{$val->class_name}}" style="padding-right:2%;"></i>{!!@$val->name!!}</button>
							</h5>
						</div>

						<div>
							<div class="card-body">
							{!! @$val->descriptions !!}
							</div>
						</div>
					</div>
				</div>
				@endif
				@endforeach
				@endif
			</div> 
		<div class="col-lg-5" style="margin-top:5%;">
			@if(@$manufacture->getSubCategory['0']['childsubcategory'] !=='')
                   @foreach(@$manufacture->getSubCategory['0']['childsubcategory'] as $key=>$val)
                   @if($val->position=='right')
                   <span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>
			<div id="accordion" class="nile-accordion margin-top-30px sm-mb-45px">
				<div class="card">
					<div class="card-header" id="headingTwo">
						<h5 class="mb-0">
							<button class="btn btn-block btn-link" style="font-size: 17px; font-weight:600;"><i class="fa fa-plane" style="padding-right:2%;"></i>{!!@$val->name!!}</button>
						</h5>
					</div>
					<div>
						<div class="card-body"  style="font-size:16px;">
							{!! @$val->descriptions !!}
						</div>
					</div>
				</div>
			</div>
			@endif
			@endforeach
			@endif
		</div>
		</div> 
	</div> 
<div style="margin-bottom: 1%;"></div> 
</section>
<!-- End Key Benefits & What We Offer Section --> 

<!--Main Footer-->
@include('footer')