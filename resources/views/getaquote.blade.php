@include('header')
@include('sweetalert::alert')
<!--End Main Header -->
	
	<!--Page Title-->
   <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.@$setting->header)}});">
    	<div class="auto-container">
        	<h2>Quote Request</h2>
			<div class="separater"></div>
        </div>
    </section>
    
    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="index.html">Home</a> <span>/</span></li>
                <li>Request a Quote</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->
	
	<!-- Quote Section -->
	<section class="quote-section">
		<div class="auto-container"> 

			<div class="quotation-form-header">
				<div class="h3"><span style="color: #eb0028;">Step 1 - </span>Complete Information About Applicant</div> 
				<div align="center" style="padding-bottom: 1%;">
				<hr width="120px;" color="#eb0028" size="10;" style="padding-bottom: 2px;">
				</div>
			</div>
			<div class="quote-form-box">
				
				<!--Login Form-->
                <form id="quote_form" method="post" action="{{url('getaquote')}}" enctype="multipart/form-data">
                	@csrf
					<div class="row clearfix">
						
						<div class="form-group col-lg-4 col-md-6 col-sm-12">
							<label>Company Name</label>
							<input type="text" name="user_companyname" placeholder="Company Name">
						</div>
						
						<div class="form-group col-lg-4 col-md-6 col-sm-12">
							<label>Title</label>
							<input type="text" name="user_title" placeholder="Title">
						</div>
						
						<div class="form-group col-lg-4 col-md-6 col-sm-12">
							<label>Email</label>
							<input type="email" name="user_email" placeholder="Email">
						</div>
						
						<div class="form-group col-lg-4 col-md-6 col-sm-12">
							<label>Telephone #</label>
							<input type="text" name="user_phone" placeholder="Telephone" maxlength="12" pattern="^(\+?6?01)[0-46-9]-*[0-9]{7,8}$">
						</div>
            
            			<div class="form-group col-lg-4 col-md-6 col-sm-12">
							<label>Ext</label>
							<input type="text" name="user_ext" placeholder="Ext">
						</div>
            
            			<div class="form-group col-lg-4 col-md-6 col-sm-12">
							<label>Fax #</label>
							<input type="text" name="user_fax" placeholder="Fax #">
						</div>
            
            			<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<label>Your Name</label>
							<input type="text" name="user_name" placeholder="Your Name">
						</div>

						<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<label>Account Payable Name:</label>
							<input type="text" name="user_payablename" placeholder="Account Payable Name">
						</div>
            
            			<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<label>Mobile / Direct Line #</label>
							<input type="text" name="user_mobiledirect" placeholder="Mobile / Direct Line #" maxlength="12" pattern="^(\+?6?01)[0-46-9]-*[0-9]{7,8}$">
						</div> 
            
            			<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<label>Fax # for P.O.D</label>
							<input type="text" name="user_faxpod" placeholder="Fax # for P.O.D">
						</div>

            
			<!-- Physical Address -->
				<div class="quotation-form-header" style="padding-top: 3%;">
					<div class="h3-address">Physical Address</div> 
					<div align="center" style="padding-bottom: 1%;">
					<hr width="120px;" color="#eb0028" size="10;" style="padding-bottom: 2px;">
					</div>
				</div>

            			<div class="form-group col-lg-8 col-md-6 col-sm-12">
							<label>Address</label>
							<input type="text" name="p_address" placeholder="Physical Address" >
						</div>
            
            			<div class="form-group col-lg-4 col-md-6 col-sm-12">
							<label>City</label>
							<input type="text" name="p_city" placeholder="Physical City" >
						</div>
            
            			<!-- <div class="form-group col-lg-4 col-md-6 col-sm-12">
							<label></label>
							<input type="text" name="address" placeholder="" >
						</div> -->
            
            			<div class="form-group col-lg-4 col-md-6 col-sm-12">
							<label>Postal / Zip Code</label>
							<input type="text" name="p_postalzip" placeholder="Physical Postal / Zip Code" >
						</div>
            
            			<div class="form-group col-lg-4 col-md-6 col-sm-12">
							<label>State</label>
							<input type="text" name="p_state" placeholder="state" >
						</div>
            
            			<!-- <div class="form-group col-lg-4 col-md-6 col-sm-12">
							<label>Country</label>
							<input type="text" name="p_country" placeholder="Physical Country" >
						</div> -->

						<div class="form-group col-lg-4 col-md-6 col-sm-12">
							<label>Country</label>
							<select type="country" id="p_country" name="p_country" class="form-control">
								<option value="">Select Country</option>
								 @if(@$country!=='')
                                  @foreach($country as $key=>$value)
								   <option value="{{$value->nicename}}">{{$value->nicename}}</option>
								  @endforeach
								 @endif
							</select>
						</div>

			<!-- Billing Address -->
				<div class="quotation-form-header" style="padding-top: 3%;">
					<div class="h3-address">Billing Address <span style="font-weight: 500;"></span></div> 
					<div align="center" style="padding-bottom: 1%;">
					<hr width="120px;" color="#eb0028" size="10;" style="padding-bottom: 2px;">
					<input type="checkbox" id="billing_information" name="billing_information" value="yes"> <label style="margin-right: 11px;"> <span>My physical information is the same as my billing information.</span> </label> 
					</div>

			</div>

				<div class="form-group col-lg-8 col-md-6 col-sm-12">
					<label>Address</label>
					<input type="text" name="b_address" placeholder="Billing Address" >
				</div>

				<div class="form-group col-lg-4 col-md-6 col-sm-12">
					<label>City</label>
					<input type="text" name="b_city" placeholder="Billing City" >
				</div>

				<div class="form-group col-lg-4 col-md-6 col-sm-12">
					<label>Postal / Zip Code</label>
					<input type="text" name="b_postalzip" placeholder="Billing Postal/Zip Code" >
				</div>

				<div class="form-group col-lg-4 col-md-6 col-sm-12">
					<label>State</label>
					<input type="text" name="b_state" placeholder="Billing State" >
				</div>

				<!-- <div class="form-group col-lg-4 col-md-6 col-sm-12">
					<label>Country</label>
					<input type="text" name="b_country" placeholder="Billing Country" >
				</div> -->  
				<div class="form-group col-lg-4 col-md-6 col-sm-12">
							<label>Country</label>
							<select type="country" id="b_country" name="b_country" class="form-control">
								<option value="">Select Country</option>
								 @if(@$country!=='')
                                  @foreach($country as $key=>$value)
								   <option value="{{$value->nicename}}">{{$value->nicename}}</option>
								  @endforeach
								 @endif
							</select>
				</div>

				<div class="form-group col-lg-7 col-md-12 col-sm-12">
					<br>
					<label>Does the firm have offices in multiple cities that require our services?</label>
					</div>
				  <div class="form-group col-lg-5 col-md-12 col-sm-12" style="margin-top: 2%;">
					<input type="radio" id="services" name="b_our_services" value="Yes" >
					<label for="yes"><b>Yes&nbsp;&nbsp;</b></label>
					<input type="radio" id="services" name="b_our_services" value="No" >
					<label for="no"><b>No</b></label><span id="errorToShow3"></span><br>
				</div> 
				
				<div class="form-group col-lg-7 col-md-12 col-sm-12">
					<br>
					<label>If multiple cities require our services, please enter the number of locations</label>
					</div>
				  <div class="form-group col-lg-5 col-md-12 col-sm-12" style="margin-top: 1%;">
					<input type="text" name="b_numberlocation" placeholder="Please Enter">
				</div> 

				<div class="form-group col-lg-12 col-md-12 col-sm-12">
					<br>
					<label><b>* Type of Business</b></label>
					</div>

				<div class="form-group col-lg-12 col-md-12 col-sm-12">
					<textarea name="b_typeofbusiness" placeholder="Type of Business"></textarea>
				</div> 

				
				<!-- Name of Owners, Partners or Officers -->
				<div class="quotation-form-header" style="padding-top: 3%;">
					<div class="h3-address">Name of Owners, Partners or Officers</div> 
					<div align="center" style="padding-bottom: 1%;">
					<hr width="120px;" color="#eb0028" size="10;" style="padding-bottom: 2px;">
					</div>
				</div>

				<div class="form-group col-lg-6 col-md-6 col-sm-12">
					<label>* Name</label>
					<input type="text" name="o_nameone" placeholder="Name">
				</div>
	
				<div class="form-group col-lg-6 col-md-6 col-sm-12">
					<label>* Title</label>
					<input type="text" name="o_titleone" placeholder="Title">
				</div>
				
				<div class="form-group col-lg-6 col-md-6 col-sm-12">
					<label>Name <span style="font-style: italic;">(optional)</span></label>
					<input type="text" name="o_nametwo" placeholder="Name">
				</div>
	
				<div class="form-group col-lg-6 col-md-6 col-sm-12">
					<label>Title <span style="font-style: italic;">(optional)</span></label>
					<input type="text" name="o_titletwo" placeholder="Title">
				</div>
				
				<div class="form-group col-lg-6 col-md-6 col-sm-12">
					<label>Name <span style="font-style: italic;">(optional)</span></label>
					<input type="text" name="o_namethree" placeholder="Name">
				</div>
	
				<div class="form-group col-lg-6 col-md-6 col-sm-12">
					<label>Title <span style="font-style: italic;">(optional)</span></label>
					<input type="text" name="o_titlethree" placeholder="Title">
				</div>
	
				<div class="quotation-form-header" style="padding-top: 5%;">
					<div class="h3"><span style="color: #eb0028;">Step 2 - </span>Business Registration Documents</div> 
					<div align="center" style="padding-bottom: 1%;">
					<hr width="120px;" color="#eb0028" size="10;" style="padding-bottom: 2px;">
					</div>
				</div>

				<!-- Company, Firm, Businesses Registration Documents -->
				<div class="quotation-form-header" style="padding-top: 3%;">
					<div class="h3-documents">Company, Firm, Businesses Registration Documents
					</div> 
				</div>

            	<!-- <div class="form-group col-lg-12 col-md-6 col-sm-12"> 
					<label for="myfile">Attach Documents:</label> 
					<input type="file" id="myfile" name="myfile" multiple><br><br>
				</div> -->

				<div class="form-group col-lg-2 col-md-12 col-sm-12">
					<br>
					<label for="myfile">Attach Documents:</label> 
					</div>
				  <div class="form-group col-lg-10 col-md-12 col-sm-12" style="margin-top: 2%;">
					<input type="file" id="myfile" name="myfile" multiple><br><br>
				</div> 

				<div class="form-group col-lg-12 col-md-12 col-sm-12">
					
					<label>* Legal Classification: </label>
					</div>
				  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="margin-top: 0%;">

					<input type="radio" id="classify_one" name="classify" value="individual" >
					<label for="individual">Individual&nbsp;&nbsp;</label> 
					
					<input type="radio" id="classify_two" name="classify" value="partnership" >
					<label for="partnership">Partnership&nbsp;&nbsp;</label> 
				
					<input type="radio" id="classify_three" name="classify" value="corporation" >
					<label for="corporation">Corporation&nbsp;&nbsp;</label>
					<span id="errorToShow1"></span>
				</div>
            
				<!-- Company, Firm, Businesses Registration Documents -->
				<div class="quotation-form-header">
					<div class="h3-documents-ending"><b>CONFIDENTIALITY STATEMENT:</b> The following information is required to process this application. It is understood that this information will be held in confidence and used only by our credit department.
					</div></div> 

				<div class="quotation-form-header" style="padding-top: 5%;">
					<div class="h3"><span style="color: #eb0028;">Step 3 - </span>Bank Reference</div> 
					<div align="center" style="padding-bottom: 1%;">
					<hr width="120px;" color="#eb0028" size="10;" style="padding-bottom: 2px;">
					</div>
				</div>

				<div class="form-group col-lg-6 col-md-6 col-sm-12">
					<label>Principal Bank</label>
					<input type="text" name="bank_principalbank" placeholder="Principal Bank" >
				</div>
	
				<div class="form-group col-lg-6 col-md-6 col-sm-12">
					<label>Branch or Location</label>
					<input type="text" name="bank_branchlocation" placeholder="Branch or Location" >
				</div>
				
				<div class="form-group col-lg-6 col-md-6 col-sm-12">
					<label>Contact</label>
					<input type="text" name="bank_contactbankreference" placeholder="Contact" maxlength="12" pattern="^(\+?6?01)[0-46-9]-*[0-9]{7,8}$">
				</div>
	
				<div class="form-group col-lg-6 col-md-6 col-sm-12">
					<label>Telephone #</label>
					<input type="text" name="bank_telephonebankreference" placeholder="Telephone" maxlength="12" pattern="^(\+?6?01)[0-46-9]-*[0-9]{7,8}$" >
				</div>
				
				<div class="form-group col-lg-6 col-md-6 col-sm-12">
					<label>Ext</label>
					<input type="text" name="bank_extbankreference" placeholder="Ext" >
				</div>
	
				<div class="form-group col-lg-6 col-md-6 col-sm-12">
					<label>Account</label>
					<input type="text" name="bank_accountbankreference" placeholder="Account" >
				</div> 

				<div class="form-group col-lg-10 col-md-12 col-sm-12">
					<br>
					<label>Once your application has been approved, would you like BUMILINK Global Logistics Services to open a free online account that will enable you to ship packages online, manage your online user accounts, generate reports, and view shipping information?</label>
					</div>
				  <div class="form-group col-lg-2 col-md-12 col-sm-12" style="margin-top: 2%;">
					<input type="radio" id="bankreference_one" name="bank_approve" value="Yes" >
					<label for="approve_yes"><b>Yes&nbsp;&nbsp;</b></label>
					<input type="radio" id="bankreference_two" name="bank_approve" value="approve_no" >
					<label for="approve_no"><b>No</b></label><span id="errorToShow2"></span><br>
				</div> 
			
						<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<button type="submit" class="theme-btn btn-style-three">Request a Quote</button>
						</div> 
					</div>
				</form>
			</div>
			</div>
		</div>
	</section>
	<!-- End Quote Section --> 
	@include('footer')