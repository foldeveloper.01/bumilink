@include('header')
	<!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.@$setting->header)}});">
    	<div class="auto-container">
        	<h2>Custom Brokerage</h2>
			<div class="separater"></div>
        </div>
    </section>
    
    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('international')}}">Our Services</a> <span>/</span></li>
                <li>{!!@$data->name!!}</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->
	
	<!-- Custom Brokerage -->
	 <span id="{{preg_replace('/\s+/','',@$data->getSubCategory[0]['name'])}}"></span>
	<section class="domestic-services-section gap">
		<div class="auto-container">
		<div class="sec-title centered">
			<!-- <h3 style="font-family: Raleway;">Custom<span> Declaration</span></h3> -->
            <h3 style="font-family: Raleway;">{!!@$data->getSubCategory[0]['name']!!}</h3>
			<div class="separater"></div> 
            </div>
		</div>
		<div class="tem-sec">
			<div class="row">

            @if(@$data->getSubCategory[0] && @$data->getSubCategory[0] !=='')
            @foreach(@$data->getSubCategory[0]->childsubcategory as $key=>$val)
               <div class="col-lg-4 col-md-6">
                    <div class="service-icon-box">
                        <div class="icon"><span class="{{@$val->class_name}}" style="color: #eb0028; font-size:55px; padding-bottom:15px;"></div><br>
							<!-- <h5 style="font-weight: 600; color:#eb0028;">Customs Brokerage & Compliance</h5> -->
                            <h5 style="font-weight: 600; color:#eb0028;">{!!@$val->name!!}</h5>
                        <div class="des">{!!@$val->descriptions!!}
                        </div>
                    </div>
                </div> 
            @endforeach
            @endif

            </div>
		</div>
	</div>
</section>
<!-- End Custom Brokerage --> 

<!-- Key Services --> 
<section class="transport-section" style="background-image:url(images/background/custombrokerage-bg.png)" id="transportation">
    <div class="auto-container"> 
        <div class="clearfix"> 

            @if(@$data->getSubCategory[0] !=='')
            @foreach(@$data->getSubCategory as $key=>$val)
            @if($key>0)
            <span id="{{preg_replace('/\s+/','',@$val->name)}}"></span>
                <div class="transport-block-key-services col-lg-5 col-md-12 col-sm-12">
					<div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="sec-title-two light sec-title">
							<!-- <h2>Key <span>Services </span></h2> -->
                            <h2>{!!@$val->name!!}</h2>

							<div class="separater"></div>
						</div></div>
                <div class="about-text margin-tb-25px" style="color: white;">
                    <h5></h5><br>
                    {!!@$val->descriptions!!}
                </div>
                <img src="{{asset('storage/app/public/images/services/'.$val->image)}}" alt="{{@$val->image_name}}" style=>
                <!-- <a href="#" class="nile-bottom sm">Read More</a> --> 
            </div>
            @endif
            @endforeach
            @endif
            
            
        </div>
    </div>
</div></section>
<!-- End Key Services Section -->
	
@include('footer')