@include('header')
<!--End Main Header -->
	
	<!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.@$setting->header)}});">
    	<div class="auto-container">
        	<h2>Contact Us</h2>
			<div class="separater"></div>
        </div>
    </section>
    
    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="index.html">Home</a> <span>/</span></li>
                <li>Contact Us</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->
	
	<!-- Map Section -->
    <div class="auto-container">
		<div class="sec-title centered" style="padding-top: 3%;">
			<h3><span>Bumilink Global Logistic Sdn Bhd</span></h3>
				<div class="separater"></div>
		</div>
        <div class="google-map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3984.2705666322568!2d101.54079351573264!3d3.0218297487788757!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cdb2b8e12fb3c7%3A0xb993219529b787ef!2sNo%2040%20%26%2040A%2C%20Jalan%20Tabla%2033%2F21%2C%20Seksyen%2033%2C%2040400%20Shah%20Alam%2C%20Selangor!5e0!3m2!1sen!2smy!4v1666598065562!5m2!1sen!2smy" width="100%" height="550" frameborder="0;" style="border:0; padding-top: 1%;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
		</div>
    </div>		
	<!-- Map Section -->

	<!-- Contact details Section -->
	<section class="team-section gap">
        <div class="auto-container">
		<!-- <div class="sec-title centered">
			<h3>Vision and<span> Mission</span></h3>
				<div class="separater"></div>
			</div> -->
			<div class="tem-sec">
				<div class="row">
					<div class="col-lg-5 col-md-12 col-sm-12">
						<div class="tm-bx">
							<div class="tm-thmb">
								<div class="contact-inf wowallow fadeInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
									<br>
									<span class="icon flaticon-map-1" style="font-size:45px; color:#eb0028"></span>
									<h3 span style="color:#eb0028; font-weight: 700; line-height:55px;">Our Address</h3>
									<span class="designation" style="text-align: center; font-size: 15px;">{{@$setting->address}}</span>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-lg-5 col-md-12 col-sm-12">
						<div class="tm-bx">
							<div class="tm-thmb">
								<div class="contact-inf wowallow fadeInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
									<br>
									<span class="icon flaticon-e-mail-envelope" style="font-size:45px; color:#eb0028"></span>
									<h3 span style="color:#eb0028; font-weight: 700; line-height:55px;">Contact Us</h3>
									<span class="designation" style="text-align: center; font-size: 15px;">Tel: @if(@$setting->phone) {{@$setting->phone}}@endif <br>Fax: @if(@$setting->fax) {{@$setting->fax}}@endif</span>
									<span class="designation-email" style="text-align: center; font-size: 15px;">Email: @if(@$setting->email) {{@$setting->email}}@endif</a></span>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div><!-- Team Sec -->
		</div>
    </section>

	<!-- Contact Section -->
	<section class="quote-section">
		<div class="auto-container">
			<div class="quote-form-box contact-page">
				<div class="sec-title centered">
					<h3>Give Us <span>Your Feedback</span></h3>
					<div class="separater"></div>
					<div class="text">We Value Your Feedback Comments & Queries</div>
				</div>
				<!-- Form-->
                <form id="userfeedbackform" method="post" action="{{url('savefeedback')}}">
                	@csrf
					<div class="row clearfix">
						<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<input type="text" name="firstname" placeholder="First Name">
						</div>
						
						<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<input type="text" name="lastname" placeholder="Last Name">
						</div>
		
						<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<input type="text" name="company" placeholder="Your Company">
						</div>
						
						<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<input type="text" name="title" placeholder="Your Title">
						</div>
						
						<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<input type="text" name="phone" placeholder="Telephone">
						</div>
						
						<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<input type="text" name="fax" placeholder="Fax">
						</div>
						
						<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<input type="text" name="mobile" placeholder="Mobile / Direct Line">
						</div>
						
						<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<input type="email" name="email" placeholder="Your Email">
						</div>
						
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-lg-12">
							<textarea name="address" placeholder="Address"></textarea>
						</div>
						
						<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<input type="text" name="postal" placeholder="Postal / Zip Code">
						</div>
						
						<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<input type="text" name="city" placeholder="City">
						</div>
						
						<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<input type="text" name="state" placeholder="State">
						</div>
						
						<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<select type="country" name="country" class="form-control">
								<option value="">Select Country</option>
								 @if(@$country!=='')
                                  @foreach($country as $key=>$value)
								   <option value="{{$value->nicename}}">{{$value->nicename}}</option>
								  @endforeach
								 @endif
							</select>
						</div>

						<!-- <div class="form-group col-lg-6 col-md-6 col-sm-12">
							<input type="text" name="firstname" placeholder="First Name" required>
						</div> -->
						
						<div class="form-group col-lg-12 col-md-12 col-sm-12">
							<h5 style="color:#2d2d2d">I am interested in the following services:</h5>
							<span id="errorToShow"></span>
						</div>
                        @if(@$following_services!=='')
                        @foreach($following_services as $key=>$value)
						  <div class="form-group col-lg-6 col-md-6 col-sm-12">
							<input class="form-check-input" name="followingservice[]" type="checkbox" id="inlineCheckbox{{$key}}" value="{{$value->id}}" style="margin-left:1%">
							<label class="form-check-label" for="inlineCheckbox{{$key}}" >{{$value->name}}</label><span></span>
						  </div>
						  @endforeach
						  @endif

						  <div class="form-group col-lg-12 col-md-12 col-sm-12">
							<h5 style="color:#2d2d2d">Please contact me by:</h5> 
							
							</div>
						 @if(@$contactby!=='')
                         @foreach($contactby as $key=>$value)
						  <div class="form-group col-lg-6 col-md-6 col-sm-12">
						  	<span id="errorToShow1"></span>
							<input class="form-check-input" name="contactby[]" type="checkbox" id="inlineCheckbox{{$value->name}}" value="{{$value->id}}" style="margin-left:1%">
							<label class="form-check-label" for="inlineCheckbox_{{$key}}" >{{$value->name}}</label>
						  </div>

						  @endforeach
						  @endif


						  

						  <div class="form-group col-lg-3 col-md-12 col-sm-12">
							<br>
							<h5 style="color:#2d2d2d">I heard about Bumilink from:</h5>
							</div>
						  <div class="form-group col-lg-3 col-md-12 col-sm-12" style="margin-top: 1%;">
							<select type="social" name="heardabout" class="form-control">
								<option value="">Please Select</option>
                                 @if(@$heredabout!=='')
                                 @foreach($heredabout as $key=>$value)
								 <option value="{{$value->name}}">{{$value->name}}</option>
								 @endforeach
								@endif

							</select></div>
							<div class="form-group col-lg-6 col-md-12 col-sm-12">
								<br>
								<h5 style="color:#2d2d2d00">I heard about Bumilink from:</h5>
								</div>
						</div>

						<div class="form-group col-lg-12 col-md-12 col-sm-12">
							<br>
							<h5 style="color:#2d2d2d">Message</h5>
							</div>

						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-lg-12">
							<textarea name="message" placeholder="Your Message"></textarea>
						</div>

						<div class="form-group col-lg-6 col-md-6 col-sm-12">
							<button type="submit" class="theme-btn btn-style-three">Send A Message / Give Feedback</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
	<!-- End contact Section -->

	 @include('footer')
