<!--Main Footer-->
    <footer class="main-footer style-two" style="background-image:url('{{asset('images/background/7.png')}}')">
    	<div class="auto-container"> 
        	<!--Widgets Section-->
            <div class="widgets-section">
            	<div class="row clearfix">

                    <!--Column-->
                    <div class="big-column col-lg-6 col-md-12 col-sm-12">
						<div class="row clearfix">

                        	<!--Footer Column-->
                            <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                                <div class="footer-widget logo-widget">
									<h2><span class="icon fa fa-thumbs-o-up" style="padding-left:6%;"></span>Useful Links</h2>

									<div class="row clearfix">
										<div class="column col-lg-12 col-md-6 col-sm-12">
											<ul class="footer-list">
												@if(count($usefullink)!=0 && @$usefullink !==null)
												@foreach($usefullink as $key=>$value)
												@if($key<5)
												<li><a href="{{@$value->link}}" target="_blank" rel="noopener noreferrer">{{@$value->name}}</a></li>
												@endif
												@endforeach
												@endif
											</ul>
										</div>
									</div>

								</div>
							</div>

							<!--Footer Column-->
                            <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                                <div class="footer-widget news-widget">
									<!-- <div class="footer-widget news-widget" style="margin-left:-20%; padding-right:15%;"></div> -->
									<h2 style="color: #eb002700;"><span class="icon fa fa-bullhorn" style="color:#eb002700"></span>Recent News</h2>

									<!--News Widget Block-->
									<div class="column col-lg-12 col-md-6 col-sm-12">
										<div class="news-widget-block">
										<ul class="footer-list"> 
											@if(count($usefullink)!=0 && @$usefullink !==null)
												@foreach($usefullink as $key=>$value)
												@if($key>=5)
												<li><a href="{{@$value->link}}" target="_blank" rel="noopener noreferrer">{{@$value->name}}</a></li>
												@endif
												@endforeach
										    @endif											
										</ul>
									</div>
									</div>
                                    <!-- <div class="news-widget-block">
                                        <div class="widget-inner">
                                            <div class="image">
                                                <img src="images/resource/news-image-1.jpg" alt="" />
                                            </div>
                                            <h3><a href="blog-detail.html">Lorem ipsum dolor sitam et, mandamus his ad</a></h3>
                                            <div class="post-date">Sept 26, 2018</div>
                                        </div>
                                    </div> -->

									<!--News Widget Block-->
                                    <!-- <div class="news-widget-block">
                                        <div class="widget-inner">
                                            <div class="image">
                                                <img src="images/resource/news-image-2.jpg" alt="" />
                                            </div>
                                            <h3><a href="blog-detail.html">Lorem ipsum dolor sitam et, mandamus his ad</a></h3>
                                            <div class="post-date">Oct 29, 2018</div>
                                        </div>
                                    </div> -->

									<!--News Widget Block-->
                                    <!-- <div class="news-widget-block">
                                        <div class="widget-inner">
                                            <div class="image">
                                                <img src="images/resource/news-image-3.jpg" alt="" />
                                            </div>
                                            <h3><a href="blog-detail.html">Lorem ipsum dolor sitam et, mandamus his ad</a></h3>
                                            <div class="post-date">Jan 26, 2020</div>
                                        </div>
                                    </div> -->

								</div>
							</div>

						</div>
					</div>

					<!--Column-->
                    <div class="big-column col-lg-6 col-md-12 col-sm-12">
						<div class="row clearfix"> 

							<!--Footer Column-->
                            <div class="footer-column col-lg-6 col-md-6 col-sm-12" >
                                <div class="footer-widget about-widget">
									<!-- <div class="footer-widget about-widget"style="margin-left:-7%; padding-right:5%"></div> -->
									<h2><span class="icon fa fa-user"></span>About Us</h2>
									<div class="column col-lg-12 col-md-6 col-sm-12">
										<ul class="footer-list"> 
											<!-- <ul class="footer-list" style="margin-left: -6%;">  -->
												<li><a href="{{url('domesticservices')}}"  rel="noopener noreferrer">Domestic Services</a></li>
												<li><a href="{{url('international')}}"  rel="noopener noreferrer">International</a></li>
												<li><a href="{{url('globallogistic')}}"  rel="noopener noreferrer">Global Logistics</a></li>
												<li><a href="{{url('custombrokerage')}}" rel="noopener noreferrer">Customs Brokerage</a></li>
												<li><a href="{{url('cargoinsurance')}}" rel="noopener noreferrer">Cargo Insurance</a></li>
										</ul>
									</div>
									<!-- <div class="text">Lorem ipsum dolor sit amet, conec tetur adipisicing elit, sed do eiusd tempor incididunt ut labore dolore magna aliqua tempor.</div>
									<div class="phone-number">0044 123 456</div>
									<div class="about-email">carga@example.com</div>-->
									
									<!--Social Box-->
									<!-- <ul class="social-icon-one">
										<li><a href="#"><span class="fa fa-twitter"></span></a></li>
										<li><a href="#"><span class="fa fa-facebook"></span></a></li>
										<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
										<li><a href="#"><span class="fa fa-google-plus"></span></a></li>
										<li><a href="#"><span class="fa fa-instagram"></span></a></li>
										<li><a href="#"><span class="fa fa-youtube"></span></a></li>
									</ul> -->
								</div>
							</div>

							<!--Footer Column-->
                            <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                                <div class="footer-widget mail-widget" >
									<!-- <div class="footer-widget mail-widget" style="margin-left:-8%; padding-right:10%;"></div> -->
									<h2><span class="icon fa fa-envelope-o"></span>Contact Us</h2>
									<div class="text">Don't hesitate to contact us now!</div>
									<div class="phone-number"><a href="tel:@if(@$setting->phone) {{@$setting->phone}}@endif" style="color:#eb0028">@if(@$setting->phone) {{@$setting->phone}}@endif </a></div>
									<!-- <div class="phone-number-fax"><a href="tel:@if(@$setting->fax) {{@$setting->fax}}@endif" style="color:#555555;">@if(@$setting->fax) {{@$setting->fax}}@endif </a></div> -->
									<div class="about-emails" style="position: relative;color: #555555;font-size: 17px;/* margin-left: 35px; */margin-top: 10px; margin-bottom: 35px;"><span class="icon flaticon-e-mail-envelope" style="font-size:25px; color:#eb0028"></span><a href="mailto:@if(@$setting->email) {{@$setting->email}}@endif" style="color:#eb0028">&nbsp;&nbsp;&nbsp;@if(@$setting->email) {{@$setting->email}}@endif</a></div>
									<!--Social Box-->
									
									<ul class="social-icon-one">
                                        @foreach($sociallink as $key=>$social) 
                                          @if($social->name=="instagram")
                                             <li><a target="_blank" href="{{$social->link}}"><span class="fa fa-instagram"></span></a></li>
                                          @endif
                                          @if($social->name=="facebook")
                                             <li><a target="_blank" href="{{$social->link}}"><span class="fa fa-facebook"></span></a></li>
                                          @endif
                                           @if($social->name=="twitter")
                                             <li><a target="_blank" href="{{$social->link}}"><span class="fa fa-twitter"></span></a></li>
                                          @endif
                                           @if($social->name=="youtube")
                                             <li><a target="_blank" href="{{$social->link}}"><span class="fa fa-youtube"></span></a></li>
                                          @endif
                                        @endforeach
                                        <!-- <li><a target="_blank" href=""><span class="fa fa-instagram"></span></a></li>						
										<li><a target="_blank" href=""><span class="fa fa-twitter"></span></a></li>
                                        <li><a target="_blank" href=""><span class="fa fa-youtube"></span></a></li>
										<li><a target="_blank" href=""><span class="fa fa-linkedin"></span></a></li>
										<li><a target="_blank" href=""><span class="fa fa-google-plus"></span></a></li> -->
										
										
									</ul>
									<!-- Email Form -->
									<!-- <div class="email-form">
										<form method="post" action="quote.html">
											<div class="form-group clearfix">
												<input type="email" name="email" value="" placeholder="Email address">
												<button type="submit" class="theme-btn submit-btn"><span class="icon fa fa-check"></span></button>
											</div>
										</form>
									</div> -->
									<!-- <div class="text wid-notice">(No Spam, We respect your privacy)</div> -->
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<!-- Footer Bottom -->
		<div class="footer-bottom">
			<div class="privacy">
				<a href="privacy.html">Privacy</a> | Terms Of Use | Resources</div>
			<div class="copyright">All Rights Reserved &copy; 2010 - {{date('Y')}} / {{@$setting->site_name}} | Powered by First Online (M) Sdn. Bhd.</div>
		</div>

	</footer>

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>
<script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script>

<script src="{{ URL::asset('js/popper.min.js') }}"></script>  
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script> <!-- 
<script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script> -->
<script type="text/javascript" src="{{ URL::asset('js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.fancybox.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/appear.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/owl.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/wow.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery-ui.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/script.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/sweetalert2.all.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/toastr.js') }}"></script>

    <script>
      $(document).ready(function() {
          
           $('#billing_information').on('change', function (e) {
                if($('#billing_information').is(":checked")==true){
                   var p_address=$("input[name=p_address]").val();
                    var p_city=$("input[name=p_city]").val();
                    var p_postalzip=$("input[name=p_postalzip]").val();
                    var p_state=$("input[name=p_state]").val();
                    var p_country=$("#p_country").val();$('#b_country-error').remove();
                    $("input[name=b_address]").val(p_address);$('#b_address-error').remove();
                    $("input[name=b_city]").val(p_city);$('#b_city-error').remove();
                    $("input[name=b_postalzip]").val(p_postalzip);$('#b_postalzip-error').remove();
                    $("input[name=b_state]").val(p_state);$('#b_state-error').remove();
                    //alert(p_country);
                    $('#b_country option[value="'+p_country+'"]').attr("selected","selected");
                }else{
                    $("input[name=b_address]").val('');
                    $("input[name=b_city]").val('');
                    $("input[name=b_postalzip]").val('');
                    $("input[name=b_state]").val('');
                    $("#b_country option[value='']").attr('selected', true);
                    $("#b_country").find('option').removeAttr("selected");
                }                
            });

             @if(Session::has('form_success'))
                   Swal.fire({
                      //position: 'top-end',
                      icon: 'success',
                      title: '{{ session('form_success') }}',
                      showConfirmButton: false,
                      timer: 1500
                    });
            @endif

        	$(document).ready(function() {
        	//quote form
            $("#quote_form").validate({
                rules: {
                    user_companyname: {
                        required: true,
                        maxlength: 35,
                    },user_title:{
                        required: true,
                        maxlength: 35,
                    },user_email: {
                        required: true,
                        email: true,
                        maxlength: 50
                    },user_phone:{
                        required: true
                    }/*,user_ext:{
                        required: true
                    }*//*,user_fax:{
                        required: true
                    }*/,user_name:{
                        required: true,
                        maxlength: 100,
                    }/*,user_payablename: {
                        required: true,
                    }*/,user_mobiledirect: {
                        required: true
                    }/*,user_faxpod: {
                        required: true
                    }*/

                    ,p_address: {
                        required: true
                    },p_city: {
                        required: true
                    },p_postalzip: {
                        required: true
                    },p_state: {
                        required: true
                    },p_country: {
                        required: true
                    }

                    ,b_address: {
                        required: true
                    },b_city: {
                        required: true
                    },b_postalzip: {
                        required: true
                    },b_state: {
                        required: true
                    },b_country: {
                        required: true
                    },b_our_services: {
                        required: true
                    },b_numberlocation: {
                        required: true
                    },b_typeofbusiness: {
                        required: true
                    }

                    ,o_nameone: {
                        required: true
                    },o_titleone: {
                        required: true
                    }

                    ,myfile: {
                        required: true,
                        filesize: 2,
                    },classify: {
                        required: true
                    }

                    ,bank_principalbank: {
                        required: true
                    },bank_branchlocation: {
                        required: true
                    },bank_contactbankreference: {
                        required: true
                    }/*,bank_telephonebankreference: {
                        required: true
                    }*//*,bank_extbankreference: {
                        required: true
                    }*/,bank_accountbankreference: {
                        required: true
                    },bank_approve: {
                        required: true
                    }

                },
                messages: {
                	bank_principalbank: {
                        required: "Enter Principal Bank"
                    },bank_branchlocation: {
                        required: "Enter Branch"
                    },bank_contactbankreference: {
                        required: "Enter contact number"
                    }/*,bank_telephonebankreference: {
                        required: "Enter telephone number"
                    }*//*,bank_extbankreference: {
                        required: "Enter Ext number"
                    }*/,bank_accountbankreference: {
                        required: "Enter Account number"
                    },bank_approve: {
                        required: "This field is required"
                    },

                	myfile: {
                        required: "Please upload file"
                    },classify: {
                        required: "Please select one"
                    },

                    o_nameone: {
                        required: "Name is required"
                    },o_titleone: {
                        required: "Title is required"
                    },

                	b_address: {
                        required: "Billing address is required"
                    },b_city: {
                        required: "Billing city is required"
                    },b_postalzip: {
                        required: "Billing postal is required"
                    },b_state: {
                        required: "Billing state is required"
                    },b_country: {
                        required: "Billing country is required"
                    },b_our_services: {
                        required: "Select one service"
                    },b_numberlocation: {
                        required: "Number of locations is required"
                    },b_typeofbusiness: {
                        required: "Billing business is required"
                    },

                    user_companyname: {
                        required: "Company name is required",
                        maxlength: "Company name cannot be more than 35 characters"
                    },user_title: {
                        required: "User Title is required",
                        maxlength: "User Title cannot be more than 35 characters"
                    },user_email: {
                        required: "Email is required",
                        email: "Email must be a valid email address",
                        maxlength: "Email cannot be more than 50 characters",
                    },user_phone: {
                        required: "Telephone number is required"
                    }/*,user_ext: {
                        required:  "Ext is required",
                    }*//*,user_fax: {
                        required: "Fax is required"
                    }*/,user_name: {
                        required: "Name is required",
                        maxlength: "Name cannot not be more than 250 characters"
                    }/*,user_payablename: {
                        required: "Account Payable Name is required"                        
                    }*/,user_mobiledirect: {
                        required: "Mobile / Direct Line is required"
                    }/*,user_faxpod: {
                        required: "Fax # for P.O.D is required"
                    }*/

                    ,p_address: {
                        required: "Physical Address is required"
                    },p_city: {
                        required: "Physical City is required"
                    },p_postalzip: {
                        required: "Physical Postal is required"
                    },p_state: {
                        required: "Physical State is required"
                    },p_country: {
                        required: "Physical Country is required"
                    }
                },
                errorPlacement: function(error, element) {
                if (element.attr("name") == "classify") {
                    error.appendTo("#errorToShow1").css('color','#dc3545').css("fontSize", "14px").css('float','center');
                }else if (element.attr("name") == "bank_approve") {
                    error.appendTo("#errorToShow2").css('color','#dc3545').css("fontSize", "14px").css('float','center');
                }else if (element.attr("name") == "b_our_services") {
                    error.appendTo("#errorToShow3").css('color','#dc3545').css("fontSize", "14px").css('float','center');
                }else {
                    error.insertAfter(element);
                }                
               }

            });
        });




            //user contact form
            $("#userfeedbackform").validate({
                rules: {
                    firstname: {
                        required: true,
                        maxlength: 35,
                    },lastname:{
                        required: true,
                        maxlength: 35,
                    },"followingservice[]":{
                        required: true
                    },"contactby[]":{
                        required: true
                    },heardabout:{
                        required: true
                    },company:{
                        required: true,
                        maxlength: 100,
                    },mobile: {
                        required: true,
                    },title: {
                        required: true
                    },email: {
                        required: true,
                        email: true,
                        maxlength: 50
                    },country: {
                        required: true
                    },address: {
                        required: true,
                        maxlength: 250
                    },city: {
                        required: true,
                        maxlength: 100
                    },state: {
                        required: true,
                        maxlength: 80
                    },postal: {
                        required: true,
                        number:true
                    },message: {
                        required: true,
                    }
                },
                messages: {
                    firstname: {
                        required: "First name is required",
                        maxlength: "First name cannot be more than 35 characters"
                    },lastname: {
                        required: "Last name is required",
                        maxlength: "Last name cannot be more than 35 characters"
                    },"followingservice[]": {
                        required: "Please select one following services"
                    },"contactby[]": {
                        required: "Please select one Contact me"
                    },company: {
                        required: "Company name is required",
                        maxlength: "Company name cannot be more than 100 characters"
                    },email: {
                        required: "Email is required",
                        email: "Email must be a valid email address",
                        maxlength: "Email cannot be more than 50 characters",
                    },mobile: {
                        required: "Mobile number is required",
                       // minlength: "Phone number must be of 10 digits"
                    },title: {
                        required:  "Title is required",
                    },country: {
                        required: "Country is required"
                    },heardabout: {
                        required: "Please select one"
                    },address: {
                        required: "Address is required",
                        maxlength: "Address cannot not be more than 250 characters"
                    },city: {
                        required: "City is required",
                        maxlength: "City cannot be more than 100 characters"
                    },state: {
                        required: "State is required",
                        maxlength: "City cannot be more than 80 characters"
                    },postal: {
                        required: "Zipcode is required",
                    },message: {
                        required: "Message is required"
                    } 
                },
                errorPlacement: function(error, element) {
                if (element.attr("name") == "contactby[]") {
                    error.appendTo("#errorToShow1").css('color','#dc3545').css("fontSize", "14px").css('float','right');
                } else {
                    error.insertAfter(element);
                }
                if (element.attr("name") == "followingservice[]") {
                    error.appendTo("#errorToShow").css('color','#dc3545').css("fontSize", "14px");
                } else {
                    error.insertAfter(element);
                }
               }
            });
        });
    </script>

</body>
</html>