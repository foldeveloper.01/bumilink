@extends('admin.layouts.app')

    @section('styles')

    @endsection

        @section('content')

                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Dashboard</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 -->
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
                                <div class="row">
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xl-3" >
                                        <div class="card overflow-hidden">
                                            <div class="card-body" style="background-color: #EA9999;">
                                                <div class="d-flex">
                                                    <div class="mt-2">
                                                        <h6 class="" style="font-family: Raleway; font-weight: 600; color: black;">Today Feedback</h6>
                                                        <h2 class="mb-0 number-font">{{@$todayfeedback}}</h2>
                                                    </div>
                                                    <!-- <div class="ms-auto">
                                                        <div class="chart-wrapper mt-1">
                                                            <canvas id="saleschart"
                                                                class="h-8 w-9 chart-dropshadow"></canvas>
                                                        </div>
                                                    </div> -->
                                                </div>
                                                <!-- <span class="text-muted fs-12"><span class="text-secondary"><i
                                                            class="fe fe-arrow-up-circle  text-secondary"></i> 5%</span>
                                                    Last week</span> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xl-3">
                                        <div class="card overflow-hidden">
                                            <div class="card-body" style="background-color: #F4CCCC;">
                                                <div class="d-flex">
                                                    <div class="mt-2">
                                                        <h6 class="" style="font-family: Raleway; font-weight: 600; color: black;">Total Feedback</h6>
                                                        <h2 class="mb-0 number-font">{{count(@$totalFeedback)}}</h2>
                                                    </div>
                                                    <!-- <div class="ms-auto">
                                                        <div class="chart-wrapper mt-1">
                                                            <canvas id="leadschart"
                                                                class="h-8 w-9 chart-dropshadow"></canvas>
                                                        </div>
                                                    </div> -->
                                                </div>
                                                <!-- <span class="text-muted fs-12"><span class="text-pink"><i
                                                            class="fe fe-arrow-down-circle text-pink"></i> 0.75%</span>
                                                    Last 6 days</span> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xl-3">
                                        <div class="card overflow-hidden">
                                            <div class="card-body" style="background-color: #9FC5E8;">
                                                <div class="d-flex">
                                                    <div class="mt-2">
                                                        <h6 class="" style="font-family: Raleway; font-weight: 600; color: black;">Today Enquiry</h6>
                                                        <h2 class="mb-0 number-font">{{@$todayquote}}</h2>
                                                    </div>
                                                   <!--  <div class="ms-auto">
                                                        <div class="chart-wrapper mt-1">
                                                            <canvas id="profitchart"
                                                                class="h-8 w-9 chart-dropshadow"></canvas>
                                                        </div>
                                                    </div> -->
                                                </div>
                                                <!-- <span class="text-muted fs-12"><span class="text-green"><i
                                                            class="fe fe-arrow-up-circle text-green"></i> 0.9%</span>
                                                    Last 9 days</span> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xl-3">
                                        <div class="card overflow-hidden">
                                            <div class="card-body" style="background-color: #CFE2F3;">
                                                <div class="d-flex">
                                                    <div class="mt-2">
                                                        <h6 class="" style="font-family: Raleway; font-weight: 600; color: black;">Total Enquiry</h6>
                                                        <h2 class="mb-0 number-font">{{count(@$totalquote)}}</h2>
                                                    </div> 
                                                    <!-- <div class="ms-auto">
                                                        <div class="chart-wrapper mt-1">
                                                            <canvas id="costchart"
                                                                class="h-8 w-9 chart-dropshadow"></canvas>
                                                        </div>
                                                    </div> -->
                                                </div>
                                                <!-- <span class="text-muted fs-12"><span class="text-warning"><i
                                                            class="fe fe-arrow-up-circle text-warning"></i> 0.6%</span>
                                                    Last year</span> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ROW-1 END -->

                        <!-- ROW-2 -->
                        <div class="row">




                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Feedback list</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered text-nowrap border-bottom" id="responsive-datatable">
                                                <thead>
                                                    <tr>                                                        
                                                        <th class="wd-15p border-bottom-0">Name</th>
                                                        <th class="wd-15p border-bottom-0">Email</th>
                                                        <th class="wd-15p border-bottom-0">Phone</th>
                                                        <th class="wd-15p border-bottom-0">Type</th>
                                                        <th class="wd-10p border-bottom-0">Created Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   
                                                     @if(@$totalFeedback)
                                                    @foreach(@$totalFeedback as $key=>$data)                                                    
                                                    <tr>
                                                        <td>{{$data->firstname}}</td>
                                                        <td>{{$data->email}}</td>
                                                        <td>{{$data->phone}}</td>
                                                        <td>@if($data->message_type=='Unread')
                                                            <span class="badge bg-success ms-3 px-2 pb-1 mb-1">{{$data->message_type}}</span>
                                                            @else
                                                            <span class="badge bg-secondary ms-3 px-2 pb-1 mb-1">{{$data->message_type}}</span>
                                                            @endif                                                            
                                                        </td>
                                                        <td>{{ \Carbon\Carbon::parse($data->created_at)->format('d/m/Y h:i')}}</td>
                                                    </tr>
                                                    @endforeach
                                                    @endif   

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- COL END -->





                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Quote list</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered text-nowrap border-bottom" id="responsive-datatable1">
                                                <thead>
                                                    <tr>                                                        
                                                        <th class="wd-15p border-bottom-0">Company Name</th>
                                                        <th class="wd-15p border-bottom-0">Company Email</th>
                                                        <th class="wd-15p border-bottom-0">User Name</th>
                                                        <th class="wd-15p border-bottom-0">Type</th>
                                                        <th class="wd-10p border-bottom-0">Created Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   
                                                     @if(@$totalquote)
                                                    @foreach(@$totalquote as $key=>$data)                                                    
                                                    <tr>
                                                        <td>{{$data->company_name}}</td>
                                                        <td>{{$data->company_email}}</td>
                                                        <td>{{$data->user_name}}</td>
                                                        <td>
                                                        @if($data->quote_type=='Unread')
                                                            <span class="badge bg-success ms-3 px-2 pb-1 mb-1">{{$data->quote_type}}</span>
                                                            @else
                                                            <span class="badge bg-secondary ms-3 px-2 pb-1 mb-1">{{$data->quote_type}}</span>
                                                            @endif
                                                        </td>
                                                        <td>{{ \Carbon\Carbon::parse($data->created_at)->format('d/m/Y h:i')}}</td> 
                                                    </tr>
                                                    @endforeach
                                                    @endif   

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="card overflow-hidden">
                                    <div class="card-body pb-0 bg-recentorder">
                                        <h3 class="card-title text-white">Quote List</h3>                                        
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered text-nowrap border-bottom" id="responsive-datatable1">
                                                <thead>
                                                    <tr>                                                        
                                                        <th class="wd-15p border-bottom-0">First Name</th>
                                                        <th class="wd-15p border-bottom-0">Email</th>
                                                        <th class="wd-15p border-bottom-0">Phone</th>
                                                        <th class="wd-15p border-bottom-0">Type</th>
                                                        <th class="wd-10p border-bottom-0">Created Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   
                                                     @if(@$totalFeedback)
                                                    @foreach(@$totalFeedback as $key=>$data)                                                    
                                                    <tr>
                                                        <td>{{$data->firstname}}</td>
                                                        <td>{{$data->email}}</td>
                                                        <td>{{$data->phone}}</td>
                                                        <td>@if($data->message_type=='Unread')
                                                            <span class="badge bg-success ms-3 px-2 pb-1 mb-1">{{$data->message_type}}</span>
                                                            @else
                                                            <span class="badge bg-secondary ms-3 px-2 pb-1 mb-1">{{$data->message_type}}</span>
                                                            @endif                                                            
                                                        </td>
                                                        <td>{{ \Carbon\Carbon::parse($data->created_at)->format('d/m/Y h:i')}}</td>
                                                    </tr>
                                                    @endforeach
                                                    @endif   

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div> -->
                            </div>




                            <!-- COL END -->
                        </div>
                        <!-- ROW-2 END -->

                        <!-- ROW-3 -->
                        <!-- <div class="row">
                            <div class="col-xl-4 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title fw-semibold">Daily Activity</h4>
                                    </div>
                                    <div class="card-body pb-0">
                                        <div class="table-responsive">
                                            <table class="table table-bordered text-nowrap border-bottom" id="responsive-datatable">
                                                <thead>
                                                    <tr>                                                        
                                                        <th class="wd-15p border-bottom-0">First Name</th>
                                                        <th class="wd-15p border-bottom-0">Email</th>
                                                        <th class="wd-15p border-bottom-0">Phone</th>
                                                        <th class="wd-15p border-bottom-0">Type</th>
                                                        <th class="wd-10p border-bottom-0">Created Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>                                                     
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                           <!--  <div class="col-xl-4 col-lg-6 col-md-12">
                                <div class="card overflow-hidden">
                                    <div class="card-header">
                                        <div>
                                            <h3 class="card-title">Sales Report by Locations with Devices</h3>
                                        </div>
                                    </div>
                                    <div class="card-body p-0 mt-2">
                                        <div class="table-responsive mt-2 text-center">
                                        <div class="table-responsive">
                                            <table class="table table-bordered text-nowrap border-bottom" id="responsive-datatable">
                                                <thead>
                                                    <tr>                               
                                                        <th class="wd-15p border-bottom-0">First Name</th>
                                                        <th class="wd-15p border-bottom-0">Email</th>
                                                        <th class="wd-15p border-bottom-0">Phone</th>
                                                        <th class="wd-15p border-bottom-0">Type</th>
                                                        <th class="wd-10p border-bottom-0">Created Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="col-xl-4 col-lg-6 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title fw-semibold">Browser Usage</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="browser-stats">
                                            <div class="row mb-4">
                                                <div class="col-sm-2 col-lg-3 col-xl-3 col-xxl-2 mb-sm-0 mb-3">
                                                    <img src="{{asset('assets/images/browsers/chrome.svg')}}" class="img-fluid"
                                                        alt="img">
                                                </div>
                                                <div class="col-sm-10 col-lg-9 col-xl-9 col-xxl-10 ps-sm-0">
                                                    <div class="d-flex align-items-end justify-content-between mb-1">
                                                        <h6 class="mb-1">Chrome</h6>
                                                        <h6 class="fw-semibold mb-1">35,502 <span
                                                                class="text-success fs-11">(<i
                                                                    class="fe fe-arrow-up"></i>12.75%)</span></h6>
                                                    </div>
                                                    <div class="progress h-2 mb-3">
                                                        <div class="progress-bar bg-primary" style="width: 70%;"
                                                            role="progressbar"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-sm-2 col-lg-3 col-xl-3 col-xxl-2 mb-sm-0 mb-3">
                                                    <img src="{{asset('assets/images/browsers/opera.svg')}}" class="img-fluid"
                                                        alt="img">
                                                </div>
                                                <div class="col-sm-10 col-lg-9 col-xl-9 col-xxl-10 ps-sm-0">
                                                    <div class="d-flex align-items-end justify-content-between mb-1">
                                                        <h6 class="mb-1">Opera</h6>
                                                        <h6 class="fw-semibold mb-1">12,563 <span
                                                                class="text-danger fs-11">(<i
                                                                    class="fe fe-arrow-down"></i>15.12%)</span></h6>
                                                    </div>
                                                    <div class="progress h-2 mb-3">
                                                        <div class="progress-bar bg-secondary" style="width: 40%;"
                                                            role="progressbar"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-sm-2 col-lg-3 col-xl-3 col-xxl-2 mb-sm-0 mb-3">
                                                    <img src="{{asset('assets/images/browsers/ie.svg')}}" class="img-fluid"
                                                        alt="img">
                                                </div>
                                                <div class="col-sm-10 col-lg-9 col-xl-9 col-xxl-10 ps-sm-0">
                                                    <div class="d-flex align-items-end justify-content-between mb-1">
                                                        <h6 class="mb-1">IE</h6>
                                                        <h6 class="fw-semibold mb-1">25,364 <span
                                                                class="text-success fs-11">(<i
                                                                    class="fe fe-arrow-down"></i>24.37%)</span></h6>
                                                    </div>
                                                    <div class="progress h-2 mb-3">
                                                        <div class="progress-bar bg-success" style="width: 50%;"
                                                            role="progressbar"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-sm-2 col-lg-3 col-xl-3 col-xxl-2 mb-sm-0 mb-3">
                                                    <img src="{{asset('assets/images/browsers/firefox.svg')}}" class="img-fluid"
                                                        alt="img">
                                                </div>
                                                <div class="col-sm-10 col-lg-9 col-xl-9 col-xxl-10 ps-sm-0">
                                                    <div class="d-flex align-items-end justify-content-between mb-1">
                                                        <h6 class="mb-1">Firefox</h6>
                                                        <h6 class="fw-semibold mb-1">14,635 <span
                                                                class="text-success fs-11">(<i
                                                                    class="fe fe-arrow-down"></i>15.63%)</span></h6>
                                                    </div>
                                                    <div class="progress h-2 mb-3">
                                                        <div class="progress-bar bg-danger" style="width: 50%;"
                                                            role="progressbar"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-sm-2 col-lg-3 col-xl-3 col-xxl-2 mb-sm-0 mb-3">
                                                    <img src="{{asset('assets/images/browsers/edge.svg')}}" class="img-fluid"
                                                        alt="img">
                                                </div>
                                                <div class="col-sm-10 col-lg-9 col-xl-9 col-xxl-10 ps-sm-0">
                                                    <div class="d-flex align-items-end justify-content-between mb-1">
                                                        <h6 class="mb-1">Edge</h6>
                                                        <h6 class="fw-semibold mb-1">15,453 <span
                                                                class="text-danger fs-11">(<i
                                                                    class="fe fe-arrow-down"></i>23.70%)</span></h6>
                                                    </div>
                                                    <div class="progress h-2 mb-3">
                                                        <div class="progress-bar bg-warning" style="width: 10%;"
                                                            role="progressbar"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-sm-2 col-lg-3 col-xl-3 col-xxl-2 mb-sm-0 mb-3">
                                                    <img src="{{asset('assets/images/browsers/safari.svg')}}" class="img-fluid"
                                                        alt="img">
                                                </div>
                                                <div class="col-sm-10 col-lg-9 col-xl-9 col-xxl-10 ps-sm-0">
                                                    <div class="d-flex align-items-end justify-content-between mb-1">
                                                        <h6 class="mb-1">Safari</h6>
                                                        <h6 class="fw-semibold mb-1">10,054 <span
                                                                class="text-success fs-11">(<i
                                                                    class="fe fe-arrow-up"></i>11.04%)</span></h6>
                                                    </div>
                                                    <div class="progress h-2 mb-3">
                                                        <div class="progress-bar bg-info" style="width: 40%;"
                                                            role="progressbar"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2 col-lg-3 col-xl-3 col-xxl-2 mb-sm-0 mb-3">
                                                    <img src="{{asset('assets/images/browsers/netscape.svg')}}" class="img-fluid"
                                                        alt="img">
                                                </div>
                                                <div class="col-sm-10 col-lg-9 col-xl-9 col-xxl-10 ps-sm-0">
                                                    <div class="d-flex align-items-end justify-content-between mb-1">
                                                        <h6 class="mb-1">Netscape</h6>
                                                        <h6 class="fw-semibold mb-1">35,502 <span
                                                                class="text-success fs-11">(<i
                                                                    class="fe fe-arrow-up"></i>12.75%)</span></h6>
                                                    </div>
                                                    <div class="progress h-2 mb-3">
                                                        <div class="progress-bar bg-green" style="width: 30%;"
                                                            role="progressbar"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <!-- ROW-3 END -->

                        <!-- ROW-4 -->
                        
                        <!-- ROW-4 END -->

        @endsection

    @section('scripts')

    <!-- SPARKLINE JS-->
    <script src="{{asset('assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>

    <!-- CHART-CIRCLE JS-->
    <script src="{{asset('assets/plugins/circle-progress/circle-progress.min.js')}}"></script>

    <!-- INTERNAL SELECT2 JS -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>

    <!-- PIETY CHART JS-->
    <script src="{{asset('assets/plugins/peitychart/jquery.peity.min.js')}}"></script>
    <script src="{{asset('assets/plugins/peitychart/peitychart.init.js')}}"></script>

    <!-- INTERNAL CHARTJS CHART JS-->
    <script src="{{asset('assets/plugins/chart/Chart.bundle.js')}}"></script>
    <script src="{{asset('assets/plugins/chart/rounded-barchart.js')}}"></script>
    <script src="{{asset('assets/plugins/chart/utils.js')}}"></script>

    <!-- INTERNAL SELECT2 JS -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>

    <!-- INTERNAL Data tables js-->
    <script src="{{asset('assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/dataTables.bootstrap5.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/js/table-data.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/responsive.bootstrap5.min.js')}}"></script>
        <script src="{{asset('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.bootstrap5.min.js')}}"></script>
       <script src="{{asset('assets/plugins/datatable/js/jszip.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>

    <!-- INTERNAL APEXCHART JS -->
    <script src="{{asset('assets/js/apexcharts.js')}}"></script>
    <script src="{{asset('assets/plugins/apexchart/irregular-data-series.js')}}"></script>

    <!-- C3 CHART JS -->
    <script src="{{asset('assets/plugins/charts-c3/d3.v5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/charts-c3/c3-chart.js')}}"></script>

    <!-- CHART-DONUT JS -->
    <script src="{{asset('assets/js/charts.js')}}"></script>

    <!-- INTERNAL Flot JS -->
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.fillbetween.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/chart.flot.sampledata.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/dashboard.sampledata.js')}}"></script>

    <!-- INTERNAL Vector js -->
    <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

    <!-- INTERNAL INDEX JS -->
    <script src="{{asset('assets/js/index.js')}}"></script>
    <script src="{{asset('assets/js/index1.js')}}"></script>
    <script type="text/javascript">
        $("#responsive-datatable1").DataTable({language:{searchPlaceholder:"Search...",scrollX:"100%",sSearch:""}});
    </script>

    @endsection
