@extends('admin.layouts.app')

@section('styles')

@endsection

@section('content')

<!-- PAGE-HEADER -->
<div class="page-header">
    <h1 class="page-title">OurTechnology</h1>
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page"><a href="{{url('admin')}}">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="{{url('admin/ourtechnology')}}">OurTechnology</a></li>
            <li class="breadcrumb-item active" aria-current="page">Update</li>
        </ol>
    </div>
</div>
<!-- PAGE-HEADER END -->

<!-- ROW-1 OPEN -->
<div class="row">                          
    <form id="contactby" action="{{url('admin/ourtechnology/update/'.Request::segment(4))}}" method="post" enctype="multipart/form-data">
       @csrf
       <div class="col-xl-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Update OurTechnology</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="form-group">
                          <label for="exampleInputname">Title</label>
                          <input type="text" value="{{@$data->title}}"  name="title" class="form-control" id="exampleInputname" placeholder="Title">
                          @if($errors->has('title'))
                          <div class="error">{{ $errors->first('title') }}</div>
                          @endif
                      </div>
                  </div>                                            
              </div>
              <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="form-group">
                      <label for="exampleInputname">Class Name</label>
                      <input type="text" value="{{@$data->class_name}}"  name="class_name" class="form-control" id="class_name" placeholder="Class name">
                      @if($errors->has('class_name'))
                      <div class="error">{{ $errors->first('class_name') }}</div>
                      @endif
                  </div>
              </div>                                            
          </div>
          <div class="row">

            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="form-group">
                      <label for="exampleInputname">Description</label>
                      <textarea id="example" class="summernote" name="example">{{@$data->description}}</textarea>
                  </div>
              </div>                                            
          </div>



          <div class="row">

            <div class="col-lg-6 col-md-12">
                <div class="form-group">
                    <label for="exampleInputname1">Status</label>
                    <select name="status" class="form-control select2 form-select">
                        <option value="">Please select one</option>
                        <option value="0" @if(@$data->status==0) selected @endif>Active</option>   
                        <option value="1" @if(@$data->status==1) selected @endif>Suspend</option>   
                    </select>
                    @if($errors->has('status'))
                    <div class="error">{{ $errors->first('status') }}</div>
                    @endif
                </div>
            </div> 
        </div> 


    </div>
    <div class="card-footer text-end">
        <a href="{{url('admin/ourtechnology')}}" class="btn btn-danger my-1">Cancel</a>
        <button class="btn btn-success my-1" value="submit">Save</button>
    </div>
</div>
</div>
</form>
</div> 
<!-- ROW-1 CLOSED -->

@endsection

@section('scripts')

<!-- INTERNAL SELECT2 JS -->
<script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{asset('assets/js/select2.js')}}"></script>
<script src="{{asset('assets/js/generate-otp.js')}}"></script>
<script src="{{asset('assets/plugins/summernote/summernote1.js')}}"></script>
<script src="{{asset('assets/js/summernote.js')}}"></script>
 <!-- INTERNAL WYSIWYG Editor JS -->
<script src="{{asset('assets/plugins/wysiwyag/jquery.richtext.js')}}"></script>
<script src="{{asset('assets/plugins/wysiwyag/wysiwyag.js')}}"></script>
<script type="text/javascript">
   $(document).ready(function() {
            //page url
    $("#exampleInputname").keyup(function(){
        var values = $('#exampleInputname').val().replace(/[^A-Z]/gi,'');
        $("#page_url").val(values).css("text-transform","lowercase");
    });
            //page url
    $('.summernote').summernote();
});
        // just for the demos, avoids form submit
   jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
  });
   $( "#contactby" ).validate({
    submitHandler : function(form) {
        form.submit();
    },
    rules: {
        title: {
          required: true
      },
      status: {
          required: true
      },
      description: {
          required: true
      },
      class_name: {
          required: true
      }
  },
  messages: {
    title: {
        required: "Please enter title",
    },
    status: {
        required: "Please select status"
    },
    description: {
          required: "Please enter description",
      },
    class_name: {
          required: "Please enter class name",
      }
},
});
</script>

@endsection
