@extends('admin.layouts.app')

    @section('styles')

    @endsection

        @section('content')

                           <!-- PAGE-HEADER -->
                           <div class="page-header">
                            <h1 class="page-title">Services</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active" aria-current="page"><a href="{{url('admin')}}">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Services</li>
                                </ol>
                            </div>
                        </div>                                               
                        <div class="row row-sm">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Services List</h3>
                                    </div>
                                    <div class="card-body">
                                      <!--   <a href="{{url('admin/services/create')}}"> <button id="table2-new-row-button" class="btn btn-primary mb-4"> Add New</button></a> -->
                                        <div class="table-responsive">
                                            <table class="table table-bordered text-nowrap border-bottom" id="responsive-datatable">
                                                <thead>
                                                    <tr>
                                                        <th class="wd-15p border-bottom-0">#</th>
                                                        <th class="wd-15p border-bottom-0">Name</th>
                                                        <!-- <th class="wd-15p border-bottom-0">Description</th> -->
                                                        <th class="wd-10p border-bottom-0">Image</th>
                                                        <th class="wd-10p border-bottom-0">Status</th>
                                                        <th class="wd-10p border-bottom-0">Created Date</th>
                                                        <th class="wd-25p border-bottom-0">Action</th>
                                                        <th class="wd-25p border-bottom-0">Page Link</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   
                                                    @if(@$list)
                                                    @foreach(@$list as $key=>$data)                                                    
                                                    <tr>                                                        
                                                        <td>{{@$key+1}}</td>
                                                        <td>{{ Illuminate\Support\Str::limit(@$data->name,20) }}</td>
                                                        <!-- <td>{{ Illuminate\Support\Str::limit(strip_tags(@$data->descriptions), 25) }}</td>  -->
                                                        <td>@if(@$data->image!='' && @$data->image!=null)
                                                    <img class="avatar avatar-md br-7"  src="{{asset('storage/app/public/images/services/'.$data->image)}}"/>
                                                    <a target="_blank" href="{{asset('storage/app/public/images/services/'.$data->image)}}"> <i class="fe fe-eye ">  </i> </a>
                                                  
                                                    @else
                                                    <img class="avatar avatar-md br-7"  src="{{asset('storage/app/public/images/no_image.png')}}"/>
                                                        <a target="_blank" href="{{asset('storage/app/public/images/no_image.png')}}"> <i class="fe fe-eye ">  </i> </a>
                                                    @endif</td>
                                                        <td>@if(@$data->status==0) <span class="badge bg-success-transparent rounded-pill text-success p-2 px-3">Active </span> @else <span class="badge bg-danger-transparent rounded-pill text-danger p-2 px-3">Suspend </span> @endif</td>
                                                        <td>{{ \Carbon\Carbon::parse(@$data->created_at)->format('d/m/Y h:i')}}</td>                                 
                                                        <td class="text-center align-middle">
                                                        <a href="{{url('admin/services/update/'.$data->id)}}" class="bg-success text-white border-success border"><i class="fe fe-edit"></i></a>
                                                        </td>
                                                        
                                                        <td class="text-center align-middle">
                                                        <a  target="_blank" href="{{url('services#').$data->page_url}}" class="bg-primary text-white border-primary border"><i class="fe fe-eye"></i></a>
                                                            <a target="_blank" href="{{url($data->page_url).'#'}}" class="bg-primary text-white border-primary border"><i class="fe fe-eye"></i></a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @endif    

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Row -->
                        <!-- End Row -->

        @endsection

    @section('scripts')

    <!-- Select2 js-->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>

    <!-- DATA TABLE JS-->
    <script src="{{asset('assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/dataTables.bootstrap5.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.bootstrap5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/jszip.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/responsive.bootstrap5.min.js')}}"></script>
    <script src="{{asset('assets/js/table-data.js')}}"></script>

    <!-- INTERNAL Edit-Table JS -->
   <!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script>
    <script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
    <script type="text/javascript">
    </script>
    @endsection
