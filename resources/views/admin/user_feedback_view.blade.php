@extends('admin.layouts.app')

@section('styles')

@endsection

@section('content')

<!-- PAGE-HEADER -->
<div class="page-header">
    <h1 class="page-title">Feedback Details</h1>
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('admin')}}">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page">Feedback Details</li>
        </ol>
    </div>
</div>
<!-- PAGE-HEADER END -->

<!-- ROW-1 OPEN -->
<div class="row">                            
    <div class="col-xl-12 col-md-12">
        <div class="card productdesc">
            <div class="card-body">
                <div class="panel panel-primary">
                    <div class=" tab-menu-heading">
                        <div class="tabs-menu1">
                            <!-- Tabs -->
                            <ul class="nav panel-tabs">
                                <li><a href="#tab5" class="active" data-bs-toggle="tab">Feedback Details</a></li>
                                <li><a href="#tab6" data-bs-toggle="tab">I am interested in the following services</a></li>
                                <li><a href="#tab7" data-bs-toggle="tab">Contact me by</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body tabs-menu-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab5" style="width: 70%;">
                                <h4 class="mb-5 mt-3 fw-bold">Feedback Details :</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="feedbacktable" style="width: 70%;">
                                        <tr>
                                            <td class="fw-bold">First Name</td>
                                            <td> {{@$data->firstname}}</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Last Name</td>
                                            <td> {{@$data->lastname}}</td>
                                        </tr> 
                                        <tr>
                                            <td class="fw-bold">Company Name</td>
                                            <td> {{@$data->company_name}}</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Company Title</td>
                                            <td>{{@$data->company_title}}</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Phone</td>
                                            <td> {{@$data->phone}}</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Fax</td>
                                            <td> {{@$data->fax}}</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Mobile</td>
                                            <td> {{@$data->mobile}}</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Email</td>
                                            <td> {{@$data->email}}</td>
                                        </tr>  <tr>
                                            <td class="fw-bold">Address</td>
                                            <td> {{@$data->address}}</td>
                                        </tr>  <tr>
                                            <td class="fw-bold">Postal</td>
                                            <td> {{@$data->postal}}</td>
                                        </tr>   <tr>
                                            <td class="fw-bold">City</td>
                                            <td> {{@$data->city}}</td>
                                        </tr>   <tr>
                                            <td class="fw-bold">State</td>
                                            <td> {{@$data->state}}</td>
                                        </tr>    <tr>
                                            <td class="fw-bold">Country</td>
                                            <td> {{@$data->country}}</td>
                                        </tr> <tr>
                                            <td class="fw-bold">I heard about Bumilink from</td>
                                            <td> {{@$data->heardabout}}</td>
                                        </tr><tr>
                                            <td class="fw-bold">Message</td>
                                            <td> {{@$data->message}}</td>
                                        </tr>                                                                
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane pt-5" id="tab6">
                                <div class="table-responsive">
                                    <ul class="p-5">
                                        @foreach(@$data->getFeedbackFollowings($data->id) as $key=>$value)
                                        <li><i class="fa fa-check me-3 text-success mb-5"></i>{{@$value->getFollowingService(@$value->following_services_id)->name}}</li>
                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab7">
                                <ul class="p-5">

                                   @foreach(@$data->getFeedbackContactBy($data->id) as $key=>$value)
                                   <li><i class="fa fa-check me-3 text-success mb-5"></i>{{@$value->getFeedbackContactBy(@$value->contact_by_id)->name}}</li>
                                   @endforeach
                                   
                               </ul>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
   
   
   
</div>
<!-- ROW-1 CLOSED -->

@endsection

@section('scripts')

<!-- INTERNAL SELECT2 JS -->
<script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{asset('assets/js/select2.js')}}"></script>

@endsection
