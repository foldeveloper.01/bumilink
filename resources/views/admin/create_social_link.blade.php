@extends('admin.layouts.app')

    @section('styles')

    @endsection

        @section('content')

                           <!-- PAGE-HEADER -->
                           <div class="page-header">
                            <h1 class="page-title">SocialLink</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin')}}">Admin</a></li>
                                    <li class="breadcrumb-item"><a href="{{url('admin/sociallink')}}">SocialLink</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Create</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">                          
                            <form id="sociallink" action="{{url('admin/sociallink/save')}}" method="post">
                                 @csrf
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Create SocialLink</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                  <label for="exampleInputname">Name</label>
                                                  <select name="name" class="form-control select2 form-select">
                                                            <option value="">Please select one</option>
                                                              <option value="facebook">Facebook</option>   
                                                              <option value="twitter">Twitter</option>
                                                              <option value="instagram">Instagram</option>   
                                                              <option value="google">Google+</option>   
                                                              <option value="linkedin">LinkedIn</option>   
                                                              <option value="youtube">Youtube</option>   
                                                    </select>
                                                    @if($errors->has('name'))
                                                        <div class="error">{{ $errors->first('name') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname">Link</label>
                                                  <input type="text" value="#"  name="link" class="form-control" id="exampleInputlink" placeholder="Link">
                                                  @if($errors->has('link'))
                                                        <div class="error">{{ $errors->first('link') }}</div>
                                                  @endif
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                  <label for="exampleInputname">Hover</label>
                                                  <input type="text" value="{{old('hover')}}"  name="hover" class="form-control" id="exampleInputh" placeholder="Hover">
                                                  @if($errors->has('hover'))
                                                        <div class="error">{{ $errors->first('hover') }}</div>
                                                  @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname1">Position</label>
                                                    <select name="position" class="form-control select2 form-select">
                                                        @for($x = 1; $x <= 10; $x++)
                                                              <option value="{{$x}}">{{$x}}</option>
                                                        @endfor
                                                    </select>
                                                    @if($errors->has('positions'))
                                                        <div class="error">{{ $errors->first('positions') }}</div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname1">Status</label>
                                                    <select name="status" class="form-control select2 form-select">
                                                            <option value="">Please select one</option>
                                                              <option value="0" selected>Active</option>   
                                                              <option value="1">Suspend</option>   
                                                    </select>
                                                    @if($errors->has('status'))
                                                        <div class="error">{{ $errors->first('status') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>                                       
                                    </div>
                                     <div class="card-footer text-end">
                                        <a href="{{url('admin/sociallink')}}" class="btn btn-danger my-1">Cancel</a>
                                        <button class="btn btn-success my-1" value="submit">Save</button>
                                        
                                    </div>    
                                    </div>
                                   
                                </div>
                            </div>
                           </form>
                        </div> 
                    <!-- ROW-1 CLOSED -->

        @endsection

    @section('scripts')

    <!-- INTERNAL SELECT2 JS -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>
    <script src="{{asset('assets/js/generate-otp.js')}}"></script>
    <script type="text/javascript">
        // just for the demos, avoids form submit
        jQuery.validator.setDefaults({
          debug: true,
          success: "valid"
        });
        $( "#sociallink" ).validate({
            submitHandler : function(form) {
            form.submit();
        },
          rules: {
            name: {
              required: true
            },
            link: {
              required: true
            },
            status: {
              required: true
            }
          },
            messages: {        
                name: {
                    required: "Please enter name",
                },
                link: {
                    required: "Please enter link",
                },
                status: {
                    required: "Please select status",
                }
            },
        });
    </script>

    @endsection
