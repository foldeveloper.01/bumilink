@extends('admin.layouts.app')

    @section('styles')

    @endsection

        @section('content')

                           <!-- PAGE-HEADER -->
                           <div class="page-header">
                            <h1 class="page-title">SocialLink</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin')}}">Admin</a></li>
                                    <li class="breadcrumb-item"><a href="{{url('admin/sociallink')}}">SocialLink</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">update</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">                          
                            <form id="sociallink" action="{{url('admin/sociallink/update/'.Request::segment(4))}}" method="post">
                                 @csrf
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">update SocialLink</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                     <label for="exampleInputname">Name</label>
                                                    <select name="name" class="form-control select2 form-select">
                                                              <option value="">Please select one</option>
                                                              <option value="facebook" @if(@$data->name=='facebook') selected @endif >Facebook</option>   
                                                              <option value="twitter" @if(@$data->name=='twitter') selected @endif >Twitter</option>   
                                                              <option value="instagram" @if(@$data->name=='instagram') selected @endif >Instagram</option>   
                                                              <option value="google" @if(@$data->name=='google') selected @endif >Google+</option>   
                                                              <option value="youtube" @if(@$data->name=='youtube') selected @endif >Youtube</option>
                                                              <option value="linkedin" @if(@$data->name=='linkedin') selected @endif>LinkedIn</option>   
                                                    </select>
                                                    @if($errors->has('name'))
                                                        <div class="error">{{ $errors->first('name') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                             <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname">Link</label>
                                                    <input type="text" value="{{@$data->link}}"  name="link" class="form-control" id="exampleInputlink" placeholder="link">
                                                    @if($errors->has('link'))
                                                        <div class="error">{{ $errors->first('link') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname">Hover</label>
                                                    <input type="text" value="{{@$data->hover}}"  name="hover" class="form-control" id="exampleInputhover" placeholder="Hover">
                                                    @if($errors->has('hover'))
                                                        <div class="error">{{ $errors->first('hover') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname1">Position</label>
                                                    <select name="position" class="form-control select2 form-select">
                                                        @for($x = 1; $x <= 10; $x++)
                                                              <option value="{{$x}}" @if(@$data->position==$x) selected @endif>{{$x}}</option>
                                                        @endfor
                                                    </select>
                                                    @if($errors->has('positions'))
                                                        <div class="error">{{ $errors->first('positions') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                             <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname1">Status</label>
                                                    <select name="status" class="form-control select2 form-select">
                                                            <option value="">Please select one</option>
                                                              <option value="0" @if(@$data->status==0) selected @endif >Active</option>   
                                                              <option value="1" @if(@$data->status==1) selected @endif >Suspend</option>   
                                                    </select>
                                                    @if($errors->has('status'))
                                                        <div class="error">{{ $errors->first('status') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer text-end">
                                        <a href="{{url('admin/sociallink')}}" class="btn btn-danger my-1">Cancel</a>
                                        <button class="btn btn-success my-1" value="submit">Save</button>
                                        
                                    </div>
                                </div>
                            </div>
                           </form>
                        </div>
                        <!-- ROW-1 CLOSED -->

        @endsection

    @section('scripts')

    <!-- INTERNAL SELECT2 JS -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>
     <script type="text/javascript">
        // just for the demos, avoids form submit
        jQuery.validator.setDefaults({
          debug: true,
          success: "valid"
        });
        $( "#sociallink" ).validate({
            submitHandler : function(form) {
            form.submit();
        },
          rules: {
            name: {
              required: true
            },
            status: {
              required: true
            },
            link: {
              required: true
            }
          },
            messages: {        
                name: {
                    required: "Please enter name",
                },
                link: {
                    required: "Please enter link",
                },
                 status: {
                    required: "Please select status"
                },
            },
        });
    </script>

    @endsection
