@extends('admin.layouts.app')

    @section('styles')

    @endsection

        @section('content')

                           <!-- PAGE-HEADER -->
                           <div class="page-header">
                            <h1 class="page-title">Following Service</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin')}}">Admin</a></li>
                                    <li class="breadcrumb-item"><a href="{{url('admin/followingservices')}}">Following Service</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">update</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">                          
                           
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Update Following Service</h3>
                                    </div>
                                    <div class="card-body">
                                        <form id="followingservice" action="{{url('admin/followingservices/update/'.Request::segment(4))}}" method="post">
                                        @csrf
                                            <div class="">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1" class="form-label">Name</label>
                                                    <input type="text" value="{{@$data->name}}"  name="name" class="form-control" id="exampleInputname" placeholder="Name">
                                                  @if($errors->has('name'))
                                                        <div class="error">{{ $errors->first('name') }}</div>
                                                  @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputname1">Status</label>
                                                    <select name="status" class="form-control select2 form-select">
                                                            <option value="">Please select one</option>
                                                              <option value="0" @if(@$data->status==0) selected @endif >Active</option>   
                                                              <option value="1" @if(@$data->status==1) selected @endif >Suspend</option>  
                                                    </select>
                                                    @if($errors->has('status'))
                                                        <div class="error">{{ $errors->first('status') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <a href="{{url('admin/followingservices')}}" class="btn btn-danger my-1">Cancel</a>
                                            <button class="btn btn-success my-1" value="submit">Save</button>
                                            
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                           </form>
                        </div>
                        <!-- ROW-1 CLOSED -->

        @endsection

    @section('scripts')

    <!-- INTERNAL SELECT2 JS -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>
     <script type="text/javascript">
        // just for the demos, avoids form submit
        jQuery.validator.setDefaults({
          debug: true,
          success: "valid"
        });
        $( "#followingservice" ).validate({
            submitHandler : function(form) {
            form.submit();
        },
          rules: {
            name: {
              required: true,
              minlength: 3
            },
            status: {
              required: true
            }
          },
            messages: {        
                name: {
                    required: "Please enter name",
                },
                 status: {
                    required: "Please select status"
                },
            },
        });
    </script>

    @endsection
