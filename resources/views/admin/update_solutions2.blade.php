@extends('admin.layouts.app')

    @section('styles')

    @endsection

        @section('content')

                           <!-- PAGE-HEADER -->
                           <div class="page-header">
                            <h1 class="page-title">Subcategory</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin')}}">Admin</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/solutions')}}">Solutions</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('admin/solutions/category1')}}">Category</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="{{url('admin/solutions/category1/subcategory1')}}">SubCategory</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Update</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">                          
                            <form id="contactby" action="{{url('admin/solutions/category1/subcategory1/update/'.Request::segment(6))}}" method="post" enctype="multipart/form-data">
                                 @csrf
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Update Subcategory</h3>
                                    </div>
                                    <div class="card-body">

                                        <div class="row">
                                         <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname1">Category</label>
                                                    <select id="category" name="category" class="form-control select2 form-select">
                                                            <option value="">Please select one</option>
                                                              @if(@$category!=='')
                                                              @foreach(@$category as $key=>$value)
                                                              <option value="{{@$value->id}}" @if(@$data->category_id==@$value->id) selected @endif>{{@$value->name}}</option>
                                                              @endforeach
                                                              @endif
                                                    </select>
                                                    @if($errors->has('category'))
                                                        <div class="error">{{ $errors->first('category') }}</div>
                                                    @endif
                                                </div>
                                         </div> 
                                        </div>

                                        <div class="row">
                                         <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname1">Sub Category</label>
                                                    <select id="subcategory" name="subcategory" class="form-control select2 form-select">
                                                            <option value="">Please select one</option>
                                                             @if(@$subcategory!=='')
                                                              @foreach(@$subcategory as $key=>$val)
                                                              <option value="{{@$val->id}}" @if(@$data->subcategory_id==@$val->id) selected @endif>{{@$val->name}}</option>
                                                              @endforeach
                                                              @endif
                                                    </select>
                                                </div>
                                         </div> 
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                  <label for="exampleInputname">Name</label>
                                                  <input type="text" value="{{@$data->name}}"  name="name" class="form-control" id="exampleInputname" placeholder="Name">
                                                  @if($errors->has('name'))
                                                        <div class="error">{{ $errors->first('name') }}</div>
                                                  @endif
                                                </div>
                                            </div>                                            
                                        </div> 

                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                  <label for="exampleInputname">Class Name</label>
                                                  <input type="text" value="{{@$data->class_name}}"  name="class_name" class="form-control" id="class_name" placeholder="Class Name">
                                                  @if($errors->has('class_name'))
                                                        <div class="error">{{ $errors->first('class_name') }}</div>
                                                  @endif
                                                </div>
                                            </div>                                            
                                        </div>

                                        <div class="row">

                                            <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                  <label for="exampleInputname">Description</label>
                                                  <textarea id="summernote" class="summernote" name="description">{{@$data->descriptions}}</textarea>
                                                </div>
                                            </div>                                            
                                        </div>
                                        <div class="row">

                                            <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                  <label for="exampleInputname">File Upload</label>
                                                  <input class="form-control" accept="image/*" name="file" type="file">
                                                </div>
                                            </div>                                            
                                        </div>

                                        <div class="row">
                                        <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname1">Position</label>
                                                    <select name="position" class="form-control select2 form-select">
                                                            <option value="">Please select one</option>
                                                              <option value="none" @if(@$data->position=='none') selected @endif>None</option>   
                                                              <option value="right" @if(@$data->position=='right') selected @endif>Right</option>   
                                                              <option value="left" @if(@$data->position=='left') selected @endif>Left</option>
                                                              <option value="other" @if(@$data->position=='other') selected @endif>Other</option>
                                                    </select>
                                                    @if($errors->has('position'))
                                                        <div class="error">{{ $errors->first('position') }}</div>
                                                    @endif
                                                </div>
                                            </div> 
                                        </div>

                                        <div class="row">

                                        <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname1">Status</label>
                                                    <select name="status" class="form-control select2 form-select">
                                                            <option value="">Please select one</option>
                                                            <option value="0" @if(@$data->status==0) selected @endif >Active</option>   
                                                            <option value="1" @if(@$data->status==1) selected @endif >Suspend</option> 
                                                    </select>
                                                    @if($errors->has('status'))
                                                        <div class="error">{{ $errors->first('status') }}</div>
                                                    @endif
                                                </div>
                                            </div> 
                                        </div> 


                                    </div>     
                                    </div>
                                    <div class="card-footer text-end">
                                         <a href="{{url('admin/solutions/category1/subcategory1')}}" class="btn btn-danger my-1">Cancel</a>
                                        <button class="btn btn-success my-1" value="submit">Save</button>
                                       
                                    </div>
                                </div>
                            </div>
                           </form>
                        </div> 
                    <!-- ROW-1 CLOSED -->

        @endsection

    @section('scripts')

    <!-- INTERNAL SELECT2 JS -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>
    <script src="{{asset('assets/js/generate-otp.js')}}"></script>
    <script src="{{asset('assets/plugins/summernote/summernote1.js')}}"></script>
    <script src="{{asset('assets/js/summernote.js')}}"></script>
    <script type="text/javascript">
         $(document).ready(function() {
          $('.summernote').summernote();
         });
        // just for the demos, avoids form submit
        jQuery.validator.setDefaults({
          debug: true,
          success: "valid"
        });
        $( "#contactby" ).validate({
            submitHandler : function(form) {
            form.submit();
        },
          rules: {
            name: {
              required: true,
              minlength: 3
            },category: {
              required: true,
            },subcategory: {
              required: true,
            },
            status: {
              required: true
            }
          },
            messages: {        
                name: {
                    required: "Please enter name",
                },category: {
                    required: "Please select category",
                },subcategory: {
                    required: "Please select sub category",
                },
                 status: {
                    required: "Please select status"
                },
            },
        });

$(document).ready(function() {
         $(document).ready(function(){

        // Department Change
        $('#category').change(function(){

             // Department id
             var id = $(this).val();

             // Empty the dropdown
             $('#subcategory').find('option').not(':first').remove();
             var site_url = "{{url('admin/solutions/category/subcategory/getsolutionsubcategory/')}}";
             var site_url = site_url+"/"+id;
             // AJAX request 
             $.ajax({
                 url: site_url,
                 type: 'get',
                 dataType: 'json',
                 success: function(response){

                     var len = 0;
                     if(response['data'] != null){
                          len = response['data'].length;
                     }

                     if(len > 0){
                          // Read data and create <option >
                          for(var i=0; i<len; i++){

                               var id = response['data'][i].id;
                               var name = response['data'][i].name;

                               var option = "<option value='"+id+"'>"+name+"</option>";
                               $("#subcategory").append(option); 
                          }
                     }

                 }
             });
        });
    });


    });

    </script>

    @endsection
