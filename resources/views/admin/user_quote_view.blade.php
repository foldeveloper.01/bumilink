@extends('admin.layouts.app')

@section('styles')

@endsection

@section('content')

<!-- PAGE-HEADER -->
<div class="page-header">
    <h1 class="page-title">Quote Details</h1>
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('admin')}}">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page">Quote Details</li>
        </ol>
    </div>
</div>
<!-- PAGE-HEADER END -->

<!-- ROW-1 OPEN -->
<div class="row">                            
    <div class="col-xl-12 col-md-12">
        <div class="card productdesc">
            <div class="card-body">
                <div class="panel panel-primary">
                    <div class=" tab-menu-heading">
                        <div class="tabs-menu1">
                            <!-- Tabs -->
                            <ul class="nav panel-tabs">
                                <li><a href="#tab5" class="active" data-bs-toggle="tab">Applicant Information</a></li>
                                <li><a href="#tab6" data-bs-toggle="tab">Physical Address</a></li>
                                <li><a href="#tab7" data-bs-toggle="tab">Billing Address</a></li>
                                <li><a href="#tab8" data-bs-toggle="tab">Name of Owners, Partners or Officers</a></li>
                                <li><a href="#tab9" data-bs-toggle="tab">Business Registration Documents</a></li>
                                <li><a href="#tab10" data-bs-toggle="tab">Bank Reference</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body tabs-menu-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab5" style="width: 70%;">
                                <h4 class="mb-5 mt-3 fw-bold">Applicant Information :</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="feedbacktable" style="width: 70%;">
                                        <tr>
                                            <td class="fw-bold">Company Name</td>
                                            <td> {{@$moreinformation->user_companyname}}</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Title</td>
                                            <td> {{@$moreinformation->user_title}}</td>
                                        </tr> 
                                        <tr>
                                            <td class="fw-bold">Email</td>
                                            <td> {{@$moreinformation->user_email}}</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Telephone</td>
                                            <td>{{@$moreinformation->user_phone}}</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Ext</td>
                                            <td> {{@$moreinformation->user_ext}}</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Fax</td>
                                            <td> {{@$moreinformation->user_fax}}</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Your Name</td>
                                            <td> {{@$moreinformation->user_name}}</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Account Payable Name</td>
                                            <td> {{@$moreinformation->user_payablename}}</td>
                                        </tr>  <tr>
                                            <td class="fw-bold">Mobile / Direct Line</td>
                                            <td> {{@$moreinformation->user_mobiledirect}}</td>
                                        </tr>  <tr>
                                            <td class="fw-bold">Fax # for P.O.D</td>
                                            <td> {{@$moreinformation->user_faxpod}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane pt-5" id="tab6">
                                <div class="table-responsive">
                                        <table class="table table-bordered" id="feedbacktable1" style="width: 70%;">
                                         <tr>
                                            <td class="fw-bold">Address</td>
                                            <td> {{@$moreinformation->p_address}}</td>
                                         </tr><tr>
                                            <td class="fw-bold">City</td>
                                            <td> {{@$moreinformation->p_city}}</td>
                                         </tr><tr>
                                            <td class="fw-bold">Postal / Zip Code</td>
                                            <td> {{@$moreinformation->p_postalzip}}</td>
                                         </tr><tr>
                                            <td class="fw-bold">State</td>
                                            <td> {{@$moreinformation->p_state}}</td>
                                         </tr><tr>
                                            <td class="fw-bold">Country</td>
                                            <td> {{@$moreinformation->p_country}}</td>
                                         </tr>
                                       </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab7">
                                <table class="table table-bordered" id="feedbacktable2" style="width: 70%;">
                                         <tr>
                                            <td class="fw-bold">Address</td>
                                            <td> {{@$moreinformation->b_address}}</td>
                                         </tr><tr>
                                            <td class="fw-bold">City</td>
                                            <td> {{@$moreinformation->b_city}}</td>
                                         </tr><tr>
                                            <td class="fw-bold">Postal / Zip Code</td>
                                            <td> {{@$moreinformation->b_postalzip}}</td>
                                         </tr><tr>
                                            <td class="fw-bold">State</td>
                                            <td> {{@$moreinformation->b_state}}</td>
                                         </tr><tr>
                                            <td class="fw-bold">Country</td>
                                            <td> {{@$moreinformation->b_country}}</td>
                                         </tr><tr>
                                            <td class="fw-bold">Does the firm have offices in multiple cities that require our services?</td>
                                            <td> {{@$moreinformation->b_our_services}}</td>
                                         </tr><tr>
                                            <td class="fw-bold">If multiple cities require our services, please enter the number of locations</td>
                                            <td> {{@$moreinformation->b_numberlocation}}</td>
                                         </tr><tr>
                                            <td class="fw-bold">Type of Business</td>
                                            <td> {{@$moreinformation->b_typeofbusiness}}</td>
                                         </tr>
                                </table>
                           </div>
                           <div class="tab-pane" id="tab8">
                                <table class="table table-bordered" id="feedbacktable2" style="width: 70%;">
                                    <tr>
                                        <td class="fw-bold">Name One</td>
                                        <td> {{@$moreinformation->o_nameone}}</td>
                                    </tr><tr>
                                        <td class="fw-bold">Title One</td>
                                        <td> {{@$moreinformation->o_titleone}}</td>
                                    </tr><tr>
                                        <td class="fw-bold">Name Two</td>
                                        <td> {{@$moreinformation->o_nametwo}}</td>
                                    </tr><tr>
                                        <td class="fw-bold">Title Two</td>
                                        <td> {{@$moreinformation->o_titletwo}}</td>
                                    </tr><tr>
                                        <td class="fw-bold">Name Three</td>
                                        <td> {{@$moreinformation->o_namethree}}</td>
                                    </tr><tr>
                                        <td class="fw-bold">Title Three</td>
                                        <td> {{@$moreinformation->o_titlethree}}</td>
                                    </tr>
                                </table>
                           </div>
                           <div class="tab-pane" id="tab9">
                                <table class="table table-bordered" id="feedbacktable3" style="width: 70%;">
                                    <tr>
                                        <td class="fw-bold">Attach Documents</td>
                                        <td> <iframe src="{{asset('storage/app/public/images/getaquote/'.$data->file)}}" style="width:600px; height:500px;" frameborder="0"></iframe> 
                                            <a href="{{asset('storage/app/public/images/getaquote/'.$data->file)}}" target="_blank">View</a>
                                        </td>
                                    </tr><tr>
                                        <td class="fw-bold">* Legal Classification</td>
                                        <td> {{@$moreinformation->classify}}</td>
                                    </tr>
                                </table>
                           </div>
                           <div class="tab-pane" id="tab10">
                                <table class="table table-bordered" id="feedbacktable3" style="width: 70%;">
                                    <tr>
                                        <td class="fw-bold">Principal Bank</td>
                                        <td> {{@$moreinformation->bank_principalbank}}</td>
                                    </tr><tr>
                                        <td class="fw-bold">Branch or Location</td>
                                        <td> {{@$moreinformation->bank_branchlocation}}</td>
                                    </tr><tr>
                                        <td class="fw-bold">Contact</td>
                                        <td> {{@$moreinformation->bank_contactbankreference}}</td>
                                    </tr><tr>
                                        <td class="fw-bold">Telephone</td>
                                        <td> {{@$moreinformation->bank_telephonebankreference}}</td>
                                    </tr><tr>
                                        <td class="fw-bold">Ext</td>
                                        <td> {{@$moreinformation->bank_extbankreference}}</td>
                                    </tr><tr>
                                        <td class="fw-bold">Account</td>
                                        <td> {{@$moreinformation->bank_accountbankreference}}</td>
                                    </tr><tr>
                                        <td class="fw-bold">Once your application has been approved, would you like BUMILINK Global Logistics Services to open a free online account that will enable you to ship packages online, manage your online user accounts, generate reports, and view shipping information?</td>
                                        <td> {{@$moreinformation->bank_approve}}</td>
                                    </tr>
                                </table>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
   
   
   
</div>
<!-- ROW-1 CLOSED -->

@endsection

@section('scripts')

<!-- INTERNAL SELECT2 JS -->
<script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{asset('assets/js/select2.js')}}"></script>

@endsection
