    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/brand/favicon.ico')}}" />

    <!-- BOOTSTRAP CSS -->
    <link id="style" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />

    <!-- STYLE CSS -->
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/css/dark-style.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/css/transparent-style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/skin-modes.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/css/sweetalert.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/css/toastr.min.css')}}" rel="stylesheet" />
    <style type="text/css">
    .error{
     color: #FF0000; 
    }.alert-error{
     color: #FF0000; 
    }
    </style>

    @yield('styles')

    <!--- FONT-ICONS CSS -->
    <link href="{{asset('assets/plugins/icons/icons.css')}}" rel="stylesheet" />

    <!-- COLOR SKIN CSS -->
    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{asset('assets/css/color1.css')}}" />
