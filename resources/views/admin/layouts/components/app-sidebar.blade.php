            <!--app-sidebar-->
            <div class="sticky">
                <div class="app-sidebar__overlay" data-bs-toggle="sidebar"></div>
                <div class="app-sidebar">
                    <div class="side-header">
                        <a class="header-brand1" href="{{url('admin')}}"><!-- 
                            <img src="../assets/images/brand/logo.png" class="header-brand-img desktop-logo" alt="logo">
                            <img src="../assets/images/brand/logo-1.png" class="header-brand-img toggle-logo" alt="logo">
                            <img src="../assets/images/brand/logo-2.png" class="header-brand-img light-logo" alt="logo"> -->
                            <img  class="header-brand-img light-logo1" alt="logo"  src="{{asset('storage/app/public/images/settings/'.@$setting->logo)}}"/>
                        </a>
                        <!-- LOGO -->
                    </div>
                    <div class="main-sidemenu">
                        <div class="slide-left disabled" id="slide-left"><svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191" width="24" height="24" viewBox="0 0 24 24"><path d="M13.293 6.293 7.586 12l5.707 5.707 1.414-1.414L10.414 12l4.293-4.293z"/></svg></div>
                        <ul class="side-menu">
                            <li class="sub-category">
                                <h3>Dashboard</h3>
                            </li>
                            <li class="slide">
                                <a class="side-menu__item @if(Request::segment(1)=='admin' && Request::segment(2)=='') active @endif" data-bs-toggle="slide" href="{{url('admin')}}"><i class="side-menu__icon fe fe-home"></i><span class="side-menu__label">Dashboard</span></a>
                            </li>

                            
                            <li class="@if(Request::segment(2)=='services') slide is-expanded @else slide @endif">
                                <a class="@if(Request::segment(2)=='services') side-menu__item active @else side-menu__item @endif" data-bs-toggle="slide" href="javascript:void(0)">
                                    <i class="side-menu__icon fe fe-server"></i>
                                    <span class="side-menu__label">Services</span><i class="angle fe fe-chevron-right"></i></a>
                                <ul class="@if(Request::segment(2)=='services') slide-menu open @else slide-menu @endif">                                    
                                    <li><a href="{{url('admin/services')}}" class="@if(Request::segment(2)=='services' && Request::segment(3)!='category') slide-item active @else slide-item @endif">Services</a></li>
                                    
                                    <li><a href="{{url('admin/services/category')}}" class="@if(Request::segment(2)=='services' && Request::segment(3)=='category' && Request::segment(4)!='subcategory') slide-item active @else slide-item @endif">Category</a></li>

                                    <li><a href="{{url('admin/services/category/subcategory')}}" 
                                        class="@if(Request::segment(2)=='services' && Request::segment(4)=='subcategory') slide-item active @else slide-item @endif">SubCategory</a></li>
                                </ul>
                            </li>

                        <li class="@if(Request::segment(2)=='solutions') slide is-expanded @else slide @endif">
                                <a class="@if(Request::segment(2)=='solutions') side-menu__item active @else side-menu__item @endif" data-bs-toggle="slide" href="javascript:void(0)">
                                    <i class="side-menu__icon fe fe-codepen"></i>
                                    <span class="side-menu__label">Solutions</span><i class="angle fe fe-chevron-right"></i></a>
                                <ul class="@if(Request::segment(2)=='solutions') slide-menu open @else slide-menu @endif">                                    
                                    <li><a href="{{url('admin/solutions')}}" class="@if(Request::segment(2)=='solutions' && Request::segment(3)!='category1') slide-item active @else slide-item @endif">Solutions</a></li>
                                    
                                    <li><a href="{{url('admin/solutions/category1')}}" class="@if(Request::segment(2)=='solutions' && Request::segment(3)=='category1' && Request::segment(4)!='subcategory1') slide-item active @else slide-item @endif">Category</a></li>

                                    <li><a href="{{url('admin/solutions/category1/subcategory1')}}" 
                                        class="@if(Request::segment(2)=='solutions' && Request::segment(4)=='subcategory1') slide-item active @else slide-item @endif">SubCategory</a></li>
                                </ul>
                            </li>

                             

                            <li class="slide">
                                <a class="side-menu__item @if(Request::segment(2)=='ourtechnology') active @endif" data-bs-toggle="slide" href="{{url('admin/ourtechnology')}}"><i class="side-menu__icon fe fe-aperture"></i><span class="side-menu__label">Our Technology</span></a>
                            </li>

                            <li class="slide">
                                <a class="side-menu__item @if(Request::segment(2)=='ourbusiness') active @endif" data-bs-toggle="slide" href="{{url('admin/ourbusiness')}}"><i class="side-menu__icon fe fe-package"></i><span class="side-menu__label">Our Business</span></a>
                            </li>

                            <li class="slide @if(Request::segment(2)=='subpages' || Request::segment(2)=='pages') is-expanded @endif">
                                <a class="side-menu__item @if(Request::segment(2)=='subpages' || Request::segment(2)=='pages') active @endif " data-bs-toggle="slide" href="javascript:void(0)"><i class="side-menu__icon fe fe-external-link"></i><span class="side-menu__label">Pages</span><i class="angle fe fe-chevron-right"></i></a>
                                <ul class="slide-menu @if(Request::segment(2)=='subpages' || Request::segment(2)=='pages') open @endif">
                                    <li class="side-menu-label1"><a href="javascript:void(0)">Pages</a></li>
                                    <li><a href="{{url('admin/pages')}}" class="slide-item @if(Request::segment(2)=='pages') active @endif"> Pages</a></li>

                                    <li class="slide"><a href="{{url('admin/subpages')}}" class="slide-item @if(Request::segment(2)=='subpages') active @endif">Subcategory Page</a></li>
                                </ul>
                            </li>

                            <li class="slide @if(Request::segment(2)=='followingservices' || Request::segment(2)=='contactby' || Request::segment(2)=='heardabout' ) is-expanded @endif ">    
                                <a class="side-menu__item @if(Request::segment(2)=='followingservices' || Request::segment(2)=='contactby' || Request::segment(2)=='heardabout') active @endif " data-bs-toggle="slide" href="javascript:void(0)"><i class="side-menu__icon fe fe-airplay"></i><span class="side-menu__label">Contact Page</span><i class="angle fe fe-chevron-right"></i></a>
                                <ul class="slide-menu @if(Request::segment(2)=='followingservices' || Request::segment(2)=='contactby' || Request::segment(2)=='heardabout') open @endif">
                                    <li class="side-menu-label1"><a href="javascript:void(0)">Contact Page</a></li>
                                    <li><a href="{{url('admin/followingservices')}}" class="slide-item @if(Request::segment(2)=='followingservices') active @endif "> Following Service</a></li>
                                    <li><a href="{{url('admin/contactby')}}" class="slide-item @if(Request::segment(2)=='contactby') active @endif">ContactBy</a></li>
                                    <li><a href="{{url('admin/heardabout')}}" class="slide-item @if(Request::segment(2)=='heardabout') active @endif">I heard about Bumilink from</a></li>
                                </ul>
                            </li>

                            <li class="slide">
                                <a class="side-menu__item  @if(Request::segment(2)=='userfeedback' || Request::segment(2)=='userfeedbackview') active @endif" data-bs-toggle="slide" href="{{url('admin/userfeedback')}}"><i class="side-menu__icon fe fe-mail"></i><span class="side-menu__label">User FeedBack</span></a>
                            </li>

                            <li class="slide">
                                <a class="side-menu__item @if(Request::segment(2)=='quote'|| Request::segment(2)=='quoteview') active @endif" data-bs-toggle="slide" href="{{url('admin/quote')}}"><i class="side-menu__icon fe fe-phone-incoming"></i><span class="side-menu__label">User Quote</span></a>
                            </li>

                            <li class="slide @if(Request::segment(2)=='sociallink' || Request::segment(2)=='usefullink') is-expanded @endif">
                                <a class="side-menu__item @if(Request::segment(2)=='sociallink' || Request::segment(2)=='usefullink') active @endif " data-bs-toggle="slide" href="javascript:void(0)"><i class="side-menu__icon fe fe-link-2"></i><span class="side-menu__label">Links</span><i class="angle fe fe-chevron-right"></i></a>
                                <ul class="slide-menu @if(Request::segment(2)=='sociallink' || Request::segment(2)=='usefullink') open @endif">
                                    <li class="side-menu-label1"><a href="javascript:void(0)">Apps</a></li>
                                    <li><a href="{{url('admin/sociallink')}}" class="slide-item @if(Request::segment(2)=='sociallink') active @endif"> Social Links</a></li>

                                    <li><a href="{{url('admin/usefullink')}}" class="slide-item @if(Request::segment(2)=='usefullink') active @endif">Useful Links</a></li>
                                </ul>
                            </li>

                           <!--  <li class="slide">
                                <a class="side-menu__item" data-bs-toggle="slide" href="{{url('admin/settings')}}"><i class="side-menu__icon fe fe fe-settings fa-spin "></i><span class="side-menu__label">Settings</span></a>
                            </li> -->

                             <li class="slide @if(Request::segment(2)=='settings') is-expanded @endif">
                                <a class="side-menu__item @if(Request::segment(2)=='settings') active @endif " data-bs-toggle="slide" href="javascript:void(0)"><i class="side-menu__icon fe fe-settings fa-spin"></i><span class="side-menu__label">Settings</span><i class="angle fe fe-chevron-right"></i></a>
                                <ul class="slide-menu @if(Request::segment(2)=='admins') open @endif">
                                    <li class="side-menu-label1"><a href="javascript:void(0)">Settings</a></li>
                                    <li><a href="{{url('admin/settings')}}" class="slide-item @if(Request::segment(2)=='settings') active @endif">Site Settings</a></li>
                                    <li><a href="{{url('admin/smtpsettings')}}" class="slide-item @if(Request::segment(2)=='settingss') active @endif">SMTP Setting</a></li>
                                </ul>
                            </li>
                           

                            <li class="slide @if(Request::segment(2)=='admins') is-expanded @endif">
                                <a class="side-menu__item @if(Request::segment(2)=='admins') active @endif " data-bs-toggle="slide" href="javascript:void(0)"><i class="side-menu__icon fe fe-users"></i><span class="side-menu__label">Admin</span><i class="angle fe fe-chevron-right"></i></a>
                                <ul class="slide-menu @if(Request::segment(2)=='admins') open @endif">
                                    <li class="side-menu-label1"><a href="javascript:void(0)">Admin</a></li>
                                    <li><a href="{{url('admin/admins')}}" class="slide-item @if(Request::segment(2)=='admins') active @endif"> Admin Listing</a></li>
                                    @if(Auth()->guard('admin')->user()->admin_type_id=='1')
                                    <li><a href="{{url('admin/useractivity')}}" class="slide-item @if(Request::segment(2)=='useractivity') active @endif">Audit Trail</a></li>
                                    <li><a href="{{url('admin/loginhistroy')}}" class="slide-item @if(Request::segment(2)=='loginhistroy') active @endif">Login History</a></li>
                                    <li><a href="{{url('admin/accesslevel')}}" class="slide-item @if(Request::segment(2)=='accesslevel') active @endif">Admin Access Level</a></li>
                                    @endif

                                </ul>
                            </li>

                         
                        </ul>
                        <div class="slide-right" id="slide-right"><svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191" width="24" height="24" viewBox="0 0 24 24"><path d="M10.707 17.707 16.414 12l-5.707-5.707-1.414 1.414L13.586 12l-4.293 4.293z"/></svg></div>
                    </div>
                </div>
                <!--/APP-SIDEBAR-->
            </div>
            <!--app-sidebar-->
