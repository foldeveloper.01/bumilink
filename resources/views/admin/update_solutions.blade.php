@extends('admin.layouts.app')

    @section('styles')

    @endsection

        @section('content')

                           <!-- PAGE-HEADER -->
                           <div class="page-header">
                            <h1 class="page-title">Solutions</h1>
                            <div>
                                <ol class="breadcrumb">
                                     <li class="breadcrumb-item active" aria-current="page"><a href="{{url('admin')}}">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="{{url('admin/solutions')}}">Solutions</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Update</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">                          
                            <form id="contactby" action="{{url('admin/solutions/update/'.Request::segment(4))}}" method="post" enctype="multipart/form-data">
                                 @csrf
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Update Solutions</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                  <label for="exampleInputname">Name</label>                                                  
                                                  <input type="text" value="{{@$data->name}}"  name="name" class="form-control" id="exampleInputname" placeholder="Name">
                                                  @if($errors->has('name'))
                                                        <div class="error">{{ $errors->first('name') }}</div>
                                                  @endif
                                                </div>
                                            </div>                                            
                                        </div>
                                         <input type="hidden" value="{{@$data->page_url}}"  name="page_url" class="form-control" id="page_url" placeholder="Page url">
                                        <!-- <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                  <label for="exampleInputname">Page_url</label>
                                                  <input type="text" value="{{@$data->page_url}}"  name="page_url" class="form-control" id="page_url" placeholder="Page url" readonly>
                                                  @if($errors->has('page_url'))
                                                        <div class="error">{{ $errors->first('page_url') }}</div>
                                                  @endif
                                                </div>
                                            </div>                                            
                                        </div> -->
                                        <div class="row">

                                            <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                  <label for="exampleInputname">Description</label>
                                                  <textarea id="summernote" class="summernote" name="description">{{@$data->descriptions}}</textarea>
                                                </div>
                                            </div>                                            
                                        </div>
                                        <div class="row">

                                            <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                  <label for="exampleInputname">File Upload</label>
                                                  <input class="form-control" accept="image/*" name="file" type="file"><br>
                                                  @if($data->image!='' && $data->image!=null)
                                                    <img class="avatar avatar-md br-7"  src="{{asset('storage/app/public/images/solutions/'.$data->image)}}"/>
                                                    <a target="_blank" href="{{asset('storage/app/public/images/solutions/'.$data->image)}}"> <i class="fe fe-eye ">  </i> </a>
                                                  
                                                    @else
                                                    <img class="avatar avatar-md br-7"  src="{{asset('storage/app/public/images/no_image.png')}}"/>
                                                        <a target="_blank" href="{{asset('storage/app/public/images/no_image.png')}}"> <i class="fe fe-eye ">  </i> </a>
                                                    @endif
                                                </div>
                                            </div>                                            
                                        </div>
                                        
                                        <div class="row">

                                        <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname1">Status</label>
                                                    <select name="status" class="form-control select2 form-select">
                                                            <option value="">Please select one</option>
                                                              <option value="0" @if(@$data->status==0) selected @endif >Active</option>   
                                                              <option value="1" @if(@$data->status==1) selected @endif >Suspend</option> 
                                                    </select>
                                                    @if($errors->has('status'))
                                                        <div class="error">{{ $errors->first('status') }}</div>
                                                    @endif
                                                </div>
                                            </div> 
                                        </div> 


                                    </div>     
                                    </div>
                                    <div class="card-footer text-end">
                                        <a href="{{url('admin/solutions')}}" class="btn btn-danger my-1">Cancel</a>
                                        <button class="btn btn-success my-1" value="submit">Save</button>
                                        
                                    </div>
                                </div>
                            </div>
                           </form>
                        </div> 
                    <!-- ROW-1 CLOSED -->

        @endsection

    @section('scripts')

    <!-- INTERNAL SELECT2 JS -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>
    <script src="{{asset('assets/js/generate-otp.js')}}"></script>
    <script src="{{asset('assets/plugins/summernote/summernote1.js')}}"></script>
    <script src="{{asset('assets/js/summernote.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){ 
            //page url
            $("#exampleInputname").keyup(function(){
                var values = $('#exampleInputname').val().replace(/[^A-Z]/gi,'');
                $("#page_url").val(values).css("text-transform","lowercase");
             });
            //page url
                  //var markupStr = $("#edit_description").val();
            //summernote
                 $('.summernote').summernote('code');
            //summernote
        });
        // just for the demos, avoids form submit
        jQuery.validator.setDefaults({
          debug: true,
          success: "valid"
        });
        $( "#contactby" ).validate({
            submitHandler : function(form) {
            form.submit();
        },
          rules: {
            name: {
              required: true,
              minlength: 3
            },
            status: {
              required: true
            }
          },
            messages: {        
                name: {
                    required: "Please enter name",
                },
                 status: {
                    required: "Please select status"
                },
            },
        });
    </script>

    @endsection
