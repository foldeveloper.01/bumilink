@extends('admin.layouts.app')

    @section('styles')

    @endsection

        @section('content')

                           <!-- PAGE-HEADER -->
                           <div class="page-header">
                            <h1 class="page-title">Following Service</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin')}}">Admin</a></li>
                                    <li class="breadcrumb-item"><a href="{{url('admin/followingservices')}}">Following Service</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">List</li>
                                </ol>
                            </div>
                        </div>

                        <!-- Row -->
                        <div class="row row-sm">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Following Service List</h3>
                                    </div>
                                    <div class="card-body">
                                        <a href="{{url('admin/followingservices/create')}}"> <button id="table2-new-row-button" class="btn btn-primary mb-4"> Add New</button></a>
                                        <div class="table-responsive">
                                            <table class="table table-bordered text-nowrap border-bottom" id="responsive-datatable">
                                                <thead>
                                                    <tr>
                                                        <th class="wd-15p border-bottom-0">#</th>
                                                        <th class="wd-15p border-bottom-0">Name</th>
                                                        <th class="wd-15p border-bottom-0">Status</th>
                                                        <th class="wd-10p border-bottom-0">Created Date</th>
                                                        <th class="wd-25p border-bottom-0">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   
                                                     @if(@$list)
                                                    @foreach(@$list as $key=>$data)                                                    
                                                    <tr>                                                        
                                                        <td>{{$key+1}}</td>
                                                        <td>{{$data->name}}</td>
                                                        <td>@if($data->status==0) <span class="badge bg-success-transparent rounded-pill text-success p-2 px-3">Active </span> @else <span class="badge bg-danger-transparent rounded-pill text-danger p-2 px-3">Suspend </span> @endif</td>
                                                        <td>{{ \Carbon\Carbon::parse($data->created_at)->format('d/m/Y h:i')}}</td>                                 
                                                        <td class="text-center align-middle">                                                      
                                                            
                                                             <a href="{{url('admin/followingservices/update/'.$data->id)}}" class="bg-success text-white border-success border"><i class="fe fe-edit"></i></a>
                                                           <!--  <input type='button' class="btn btn-warning mt-2" value='Warning alert' id='click1'> -->
                                                            
                                                            <a  onclick="deletefunction(event,{{$data->id}});" href="" class="bg-danger text-white border-danger border"><i class="fe fe-x"></i></a>
                                                        <form id="delete_form{{$data->id}}" method="POST" action="{{ url('admin/followingservice/delete', $data->id) }}">
                                                            @csrf
                                                            <input name="_method" type="hidden" value="DELETE">
                                                        </form>
                                                        </td>   
                                                    </tr>
                                                    @endforeach
                                                    @endif   

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Row -->

        @endsection

    @section('scripts')

    <!-- Select2 js-->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>

    <!-- DATA TABLE JS-->
    <script src="{{asset('assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/dataTables.bootstrap5.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.bootstrap5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/jszip.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/responsive.bootstrap5.min.js')}}"></script>
    <script src="{{asset('assets/js/table-data.js')}}"></script>

    <!-- INTERNAL Edit-Table JS -->
   <!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script>
    <script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
    <script type="text/javascript">

        function deletefunction(event, id) {
              event.preventDefault();
                swal({
                    title: "Are you sure you want to delete this record?",
                    text: "If you delete this, it will be gone forever.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Delete',
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                       /* swal({
                            title: 'Deleted!',
                            text: 'The data has been successfully removed!',
                            type: 'success'
                        }, function() {*/
                            $("#delete_form"+id).submit();
                        //});
                    } else {
                        swal("Cancel", "Your data has not been removed", "error");
                    }
                });
            }
        
    </script>
    @endsection
