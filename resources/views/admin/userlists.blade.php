@extends('admin.layouts.app')

@section('styles')

@endsection

@section('content')

<!-- PAGE-HEADER -->
<div class="page-header">
    <h1 class="page-title">Admin</h1>
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('admin')}}">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page">List</li>
        </ol>
    </div>
</div>
<!-- PAGE-HEADER END -->

                        <!-- Row -->
                        <div class="row row-sm">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Admin List</h3>
                                    </div>
                                    <div class="card-body">
                                       <a href="{{url('admin/admins/create')}}"> <button id="table2-new-row-button" class="btn btn-primary mb-4"> Add New</button></a>
                                       <div class="table-responsive">
                                        <table class="table table-bordered text-nowrap border-bottom" id="responsive-datatable">
                                            <thead>
                                                <tr> 
                                                <th class="wd-15p border-bottom-0">#</th>
                                                <th class="wd-15p border-bottom-0">Name</th>
                                                <th class="wd-20p border-bottom-0">Email</th>
                                                <th class="wd-15p border-bottom-0">Contact Number</th>
                                                <th class="wd-15p border-bottom-0">Type</th>
                                                <th class="wd-25p border-bottom-0">Status</th>
                                                <th class="wd-25p border-bottom-0">Created date</th>
                                                <th class="wd-25p border-bottom-0">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @if(@$userlist)
                                                @foreach(@$userlist as $key=>$data)
                                                @php 
                                                $value = \App\Models\Admintypes::find($data->admin_type_id);
                                                @endphp 
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td>{{$data->firstname}}</td>
                                                    <td>{{$data->email}}</td>
                                                    <td>{{$data->number}}</td>
                                                    <td>{{@$data->AdminTyps->type}}</td>
                                                    <td>@if($data->status==0) <span class="badge bg-success-transparent rounded-pill text-success p-2 px-3">Active </span> @else <span class="badge bg-danger-transparent rounded-pill text-danger p-2 px-3">Suspend </span> @endif</td>
                                                    <td>{{ \Carbon\Carbon::parse($data->created_at)->format('d/m/Y h:i')}}</td>     
                                                    <td class="text-center align-middle">
                                                        <form id="send_email{{$data->id}}" method="POST" action="{{ url('admin/admins/emailSend', $data->id) }}"> @csrf
                                                            <input name="_method" type="hidden" value="DELETE">
                                                        </form>
                                                        <a onclick="send_email(event,{{$data->id}},'{{$data->email}}');" class="bg-success text-white border-success border" data-bs-toggle="tooltip"><i class="fa fa-expeditedssl"></i></a>
                                                        

                                                        <a href="{{url('admin/admins/update/'.$data->id)}}" class="bg-success text-white border-success border"><i class="fe fe-edit"></i></a>
                                                        
                                                        <a  onclick="deletefunction(event,{{$data->id}});" href="" class="bg-danger text-white border-danger border"><i class="fe fe-x"></i></a>
                                                        <form id="delete_form{{$data->id}}" method="POST" action="{{ url('admin/admins/delete', $data->id) }}">
                                                            @csrf
                                                            <input name="_method" type="hidden" value="DELETE">
                                                        </form>

                                                    </td>  
                                                </tr>
                                                @endforeach
                                                @endif     

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Row -->



                    @endsection

                    @section('scripts')

                    <!-- Select2 js-->
                    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
                    <script src="{{asset('assets/js/select2.js')}}"></script>
                    <!-- DATA TABLE JS-->
                    <script src="{{asset('assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
                    <script src="{{asset('assets/plugins/datatable/js/dataTables.bootstrap5.js')}}"></script>
                    <script src="{{asset('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
                    <script src="{{asset('assets/plugins/datatable/js/buttons.bootstrap5.min.js')}}"></script>
                    <script src="{{asset('assets/plugins/datatable/js/jszip.min.js')}}"></script>
                    <script src="{{asset('assets/plugins/datatable/pdfmake/pdfmake.min.js')}}"></script>
                    <script src="{{asset('assets/plugins/datatable/pdfmake/vfs_fonts.js')}}"></script>
                    <script src="{{asset('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
                    <script src="{{asset('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
                    <script src="{{asset('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
                    <script src="{{asset('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
                    <script src="{{asset('assets/plugins/datatable/responsive.bootstrap5.min.js')}}"></script>
                    <script src="{{asset('assets/js/table-data.js')}}"></script>


                    <!-- INTERNAL Edit-Table JS -->
   <!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script>
    <script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
    <script type="text/javascript">
        function send_email(event, id, email) {
              event.preventDefault();
                swal({
                    title: "Password will be sent to "+email+". Is the email correct?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Send',
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                            $("#send_email"+id).submit();                       
                    } else {
                        swal("Cancel", "You Canceled this one", "error");
                    }
                });
            }
            function deletefunction(event, id) {
              event.preventDefault();
                swal({
                    title: "Are you sure you want to delete this record?",
                    text: "If you delete this, it will be gone forever.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Delete',
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                            $("#delete_form"+id).submit();                       
                    } else {
                        swal("Cancel", "Your data has not been removed", "error");
                    }
                });
            }         
    </script>
    @endsection
