@extends('admin.layouts.app')

    @section('styles')

    @endsection

        @section('content')

                           <!-- PAGE-HEADER -->
                           <div class="page-header">
                            <h1 class="page-title">Site Setting</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin')}}">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Setting</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">
                          
                            <form id="create_user" action="{{url('admin/settings')}}" method="post" enctype="multipart/form-data">
                                 @csrf
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Site Setting</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname">Site Name</label>
                                                    <input type="text" value="@if(@$settingData->site_name!==''){{@$settingData->site_name}}@endif"  name="sitename" class="form-control" id="exampleInputname" placeholder="Site Name">
                                                    @if($errors->has('sitename'))
                                                        <div class="error">{{ $errors->first('sitename') }}</div>
                                                   @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group"> 
                                                    <label for="exampleInputname1">Site link</label>
                                                    <input type="text" value="@if(@$settingData->site_link!==''){{@$settingData->site_link}}@endif" name="sitelink" class="form-control" id="exampleInputname1" placeholder="Site Link">
                                                    @if($errors->has('sitelink'))
                                                        <div class="error">{{ $errors->first('sitelink') }}</div>
                                                  @endif
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row">
                                        <div class="col-lg-6 col-md-12">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" name="email" value="@if(@$settingData->email!==''){{@$settingData->email}}@endif" class="form-control" id="exampleInputEmail1" placeholder="Email address">
                                            @if($errors->has('email'))
                                                        <div class="error">{{ $errors->first('email') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <label for="exampleInputnumber">Phone</label>
                                            <input type="text" name="number" value="@if(@$settingData->phone!==''){{@$settingData->phone}}@endif" class="form-control" id="exampleInputnumber" placeholder="phone">
                                            @if($errors->has('number'))
                                                        <div class="error">{{ $errors->first('number') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <label for="exampleInputnumber">Fax</label>
                                            <input type="text" name="fax" value="@if(@$settingData->fax!==''){{@$settingData->fax}}@endif" class="form-control" id="exampleInputnumber" placeholder="Fax">
                                            @if($errors->has('fax'))
                                                        <div class="error">{{ $errors->first('fax') }}</div>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Address</label>
                                            <textarea name="address" value="@if(@$settingData->address!==''){{@$settingData->address}}@endif" class="form-control" rows="6"placeholder="Enter Address">@if(@$settingData->address!==''){{@$settingData->address}}@endif</textarea>
                                             @if($errors->has('address'))
                                                        <div class="error">{{ $errors->first('address') }}</div>
                                             @endif
                                        </div>

                                        <div class="col-lg-6 col-md-12">
                                            <label for="exampleInputnumber">Logo</label>
                                            <input class="form-control" accept="image/*" name="logo" type="file">
                                        </div>
                                        <br>
                                        @if(@$settingData->logo!=null && @$settingData->logo!='')
                                        <div class="col-lg-6 col-md-12">
                                        <img height="150px;" width="150px;" class="avatar avatar-md br-7"  src="{{asset('storage/app/public/images/settings/'.@$settingData->logo)}}"/>
                                                    <a target="_blank" href="{{asset('storage/app/public/images/settings/'.@$settingData->logo)}}"> <i class="fe fe-eye ">  </i> </a>
                                        </div>
                                        @endif

                                        <div class="col-lg-6 col-md-12">
                                            <label for="exampleInputnumber">Header Image</label>
                                            <input class="form-control" accept="image/*" name="header" type="file">
                                        </div><br>
                                        @if(@$settingData->header!=null && @$settingData->header!='')
                                        <div class="col-lg-6 col-md-12">
                                        <img height="150px;" width="150px;" class="avatar avatar-md br-7"  src="{{asset('storage/app/public/images/settings/'.@$settingData->header)}}"/>
                                                    <a target="_blank" href="{{asset('storage/app/public/images/settings/'.@$settingData->header)}}"> <i class="fe fe-eye ">  </i> </a>
                                        </div>
                                        @endif

                                        <div class="col-lg-6 col-md-12">
                                            <label for="exampleInputnumber">Favicon Image</label>
                                            <input class="form-control" accept="image/*" name="favicon" type="file">
                                        </div> <br>
                                        @if(@$settingData->favicon!=null && @$settingData->favicon!='')
                                        <div class="col-lg-6 col-md-12">
                                        <img height="150px;" width="150px;" class="avatar avatar-md br-7"  src="{{asset('storage/app/public/images/settings/'.@$settingData->favicon)}}"/>
                                                    <a target="_blank" href="{{asset('storage/app/public/images/settings/'.@$settingData->favicon)}}"> <i class="fe fe-eye ">  </i> </a>
                                        </div>
                                        @endif
                                        <div class="col-lg-6 col-md-12">
                                            <label for="exampleInputnumber">Meta Author</label>
                                            <input type="text" name="meta_author" value="@if(@$settingData->meta_author!==''){{@$settingData->meta_author}}@endif" class="form-control" id="exampleInputnumber5" placeholder="Meta Author">
                                            @if($errors->has('meta_author'))
                                                        <div class="error">{{ $errors->first('meta_author') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <label for="exampleInputnumber">Meta kewords</label>
                                            <input type="text" name="meta_keywords" value="@if(@$settingData->meta_keywords!==''){{@$settingData->meta_keywords}}@endif" class="form-control" id="exampleInputnumber5" placeholder="Meta keywords">
                                            @if($errors->has('meta_keywords'))
                                                        <div class="error">{{ $errors->first('meta_keywords') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <label for="exampleInputnumber">Meta Description</label>
                                            <input type="text" name="meta_descriptions" value="@if(@$settingData->meta_descriptions!==''){{@$settingData->meta_descriptions}}@endif" class="form-control" id="exampleInputnumber6" placeholder="Meta Descriptions">
                                            @if($errors->has('meta_descriptions'))
                                                        <div class="error">{{ $errors->first('meta_descriptions') }}</div>
                                            @endif
                                        </div>

                                    </div>
                                        
                                    </div>
                                    <div class="card-footer text-end">
                                        <a href="{{url('admin')}}" class="btn btn-danger my-1">Back</a>
                                        <button class="btn btn-success my-1" value="submit">Save</button>
                                        
                                    </div>
                                </div>                              
                            </div>
                           </form>
                        </div>
                        <!-- ROW-1 CLOSED -->

        @endsection

    @section('scripts')

    <!-- INTERNAL SELECT2 JS -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>

    @endsection
