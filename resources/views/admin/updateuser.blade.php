@extends('admin.layouts.app')

    @section('styles')

    @endsection

        @section('content')

                           <!-- PAGE-HEADER -->
                           <div class="page-header">
                            <h1 class="page-title">Admin</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin/admins')}}">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">update</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">                           
                            <form id="update_user" action="{{url('admin/admins/update/'.Request::segment(4))}}" method="post" enctype="multipart/form-data">
                                 @csrf
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Update Admin</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname">Name</label>
                                                    <input type="text" value="{{@$getuserData->firstname}}"  name="firstname" class="form-control" id="exampleInputname" placeholder="Name">
                                                </div>
                                            </div>
                                           <!--  <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname1">Last Name</label>
                                                    <input type="text" value="{{@$getuserData->lastname}}" name="lastname" class="form-control" id="exampleInputname1" placeholder="Last Name">
                                                </div>
                                            </div> -->
                                        
                                         
                                        <div class="col-lg-6 col-md-12">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" name="email" value="{{@$getuserData->email}}" class="form-control" id="exampleInputEmail1" placeholder="Email address">
                                            @if($errors->has('email'))
                                                        <div class="error">{{ $errors->first('email') }}</div>
                                            @endif
                                        </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-lg-6 col-md-12">
                                            <label for="exampleInputnumber">Contact Number</label>
                                            <input type="number" name="number" value="{{@$getuserData->number}}" class="form-control" id="exampleInputnumber" placeholder="Contact number" maxlength="12" pattern="^(\+?6?01)[0-46-9]-*[0-9]{7,8}$" >
                                        </div>

                                         <div class="col-lg-6 col-md-12">
                                          <label class="form-label">User Type</label>
                                          <select name="usertype" class="form-control select2 form-select">
                                                            <option value="">Please select one</option>
                                                            @if(@$admintypes)
                                                              @foreach(@$admintypes as $key=>$value)
                                                              <option value="{{@$value->id}}" @if(@$value->id==@$getuserData->admin_type_id) selected="selected" @endif>{{@$value->type}}</option>
                                                              @endforeach
                                                            @endif                                                            
                                                    </select>
                                          </div>

                                          <div class="col-lg-6 col-md-12">
                                            <label class="form-label">Status</label>
                                              <select name="status" class="form-control select2 form-select">
                                                            <option value="0" @if(@$getuserData->status==0) selected="selected" @endif>Active</option>
                                                            <option value="1" @if(@$getuserData->status==1) selected="selected" @endif>Suspend</option>                                                            
                                                    </select>
                                          </div>

                                    </div>
                                        
                                        
                                        <!-- <div class="form-group">
                                            <label class="form-label">Password</label>
                                            <div class="wrap-input100 validate-input input-group col-md-4 mb-2" id="Password-toggle">
                                                <input name="password" id="password" value="{{old('password')}}"  class="input100 form-control" type="password" placeholder="Password">
                                                <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                    <i class="zmdi zmdi-eye text-muted toggle-password" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Confirm Password</label>
                                            <div class="wrap-input100 validate-input input-group col-md-4 mb-2" id="Password-toggle">
                                                
                                                <input id="password_confirmation" value="{{old('password_confirmation')}}" name="password_confirmation" class="input100 form-control" type="password" placeholder="Confirm Password">
                                                <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                    <i class="zmdi zmdi-eye text-muted toggle-password1" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                            <div id="divCheckPasswordMatch"></div>
                                        </div> -->
                                   
                                    <div class="card-footer text-end">
                                        <a href="{{url('admin/admins')}}" class="btn btn-danger my-1">Cancel</a>
                                        <button class="btn btn-primary my-1" value="submit">Save</button>
                                        
                                    </div>
                                </div>
                               
                            </div>
                           </form>
                        </div>
                        <!-- ROW-1 CLOSED -->

        @endsection

    @section('scripts')

    <!-- INTERNAL SELECT2 JS -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
           $("#password, #password_confirmation").keyup(checkPasswordMatch);
         });

           $("body").on('click', '.toggle-password', function() {
              $(this).toggleClass("fa-eye fa-eye-slash");
              var input = $("#password");
              if (input.attr("type") === "password") {
                input.attr("type", "text");
              } else {
                input.attr("type", "password");
              }
            });
          $("body").on('click', '.toggle-password1', function() {
              $(this).toggleClass("fa-eye fa-eye-slash");
              var input = $("#password_confirmation");
              if (input.attr("type") === "password") {
                input.attr("type", "text");
              } else {
                input.attr("type", "password");
              }
          });

        function checkPasswordMatch() {
            var password = $("#password").val();
            if(password == $(this).val()){
                //$("#divCheckPasswordMatch").html('Passwords match.');
                $('button').prop('disabled', false);
                $("#divCheckPasswordMatch").html('');
            }else{
                $("#divCheckPasswordMatch").html('Passwords do not match!').css('color', 'red');
                $('button').prop('disabled', true);
            }
        }        
        // just for the demos, avoids form submit
        jQuery.validator.setDefaults({
          debug: true,
          success: "valid"
        });
        $( "#update_user" ).validate({
            submitHandler : function(form) {
            form.submit();
        },
          rules: {
            firstname: {
              required: true,
              minlength: 3
            },
            email: {
              required: true,
              email:true,
            },
            usertype: {
              required: true
            }
          },
            messages: {        
                firstname: {
                    required: "Please enter name",
                },
                email: {
                    required: "Please enter email",
                },
                usertype: {
                    required: "Please select usertype",
                }
            },
        });
    </script>
    @endsection
