 	<!-- Main Header / Header Style Two -->
      @include('header')
    <!--End Main Header -->

	<!-- Banner Section Two -->
    <section class="banner-section-two" style="background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('images/background/home-banner.png');">
		<div class="auto-container">

			<!-- Content Column -->
			<div class="content-column">
				<div class="inner-column wow slideInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
					<h1>YOUR FULLFILMENT PARTNER</h1>
					<div class="text">Join the millions getting bargain deals offers ocean, air, and land freight <br>along with integrated supply chain services in more than 100 countries.</div>

					<!-- Banner Form -->
					<div class="banner-form-two wow slideInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
						<form method="post" action="index.html">
							<div class="row clearfix">

								<!-- Form Group -->
								<div class="form-group col-lg-4 col-md-6 col-sm-12">
									<input type="text" name="emailid" placeholder="Enter your Email ID" required>
								</div>

								<!-- Form Group -->
								<div class="form-group col-lg-4 col-md-6 col-sm-12">
									<input type="text" name="trackid" placeholder="Enter Your Track ID" required>
								</div>

								<!-- Form Group -->
								<div class="form-group col-lg-4 col-md-12 col-sm-12">
									<button type="submit" class="theme-btn btn-style-three">Search Now</button>
								</div>

							</div>
						</form>
					</div>
					<!-- End Banner Form -->

				</div>
			</div>

		</div>
	</section>
	<!-- End Banner Section Two -->

	<!-- Featured Section -->
	<section class="featured-section">
		<div class="outer-container">
			<div class="clearfix">

				<!-- Feature Block -->
				<div class="feature-block">
					<div class="inner-box">
						<div class="icon-box">
							<span class="icon flaticon-air-freight"></span>
						</div>
						<h3><a href="#">Air Freight</a></h3>
						<div class="text">Property Carried in Aircraft</div>
					</div>
				</div>

				<!-- Feature Block -->
				<div class="feature-block">
					<div class="inner-box">
						<div class="icon-box">
							<span class="icon flaticon-moving-truck"></span>
						</div>
						<h3><a href="#">Road Freight</a></h3>
						<div class="text">Motor Vehicles Transport Cargo</div>
					</div>
				</div>

				<!-- Feature Block -->
				<div class="feature-block">
					<div class="inner-box">
						<div class="icon-box">
							<span class="icon flaticon-boat"></span>
						</div>
						<h3><a href="#">Sea Freight</a></h3>
						<div class="text">Goods Carrier Ships</div>
					</div>
				</div>

				<!-- Feature Block -->
				<div class="feature-block">
					<div class="inner-box">
						<div class="icon-box">
							<span class="icon flaticon-planet-earth"></span>
						</div>
						<h3><a href="#">Insurance</a></h3>
						<div class="text">Protect Your Cargo</div>
					</div>
				</div>

			</div>
		</div>
	</section>
	<!-- End Featured Section -->

	<!-- Welcome Section -->
	<section class="welcome-section">
		<div class="auto-container">
			<div class="row clearfix">

				<!-- Content Column -->
				<div class="content-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="sec-title-two sec-title">
							<?php if(str_word_count(@$category1->name)==1){ ?>
                                        <?php
                                        $output=substr(@$category1->name, 2, strlen(@$category1->name));
                                        $output1=substr(@$category1->name, 0, 2);
                                        ?>
                                       <h3><?php  echo @$output1;?><span> <?php echo @$output;?> </span> </h3>
                                    <?php }else{ ?>
                                        <?php $output2=strtok(@$category1->name, " ");
                                        $output3=substr(strstr(@$category1->name," "), 1);
                                        ?>
                                        <h3>
                                    <?php echo @$output2; ?><span> <?php echo @$output3;?> </span>
                                    <?php } ?></h3>
							<div class="separater"></div>
						</div>
						@if(@$welcome_bumilink!=='')
						@foreach(@$welcome_bumilink as $key=>$value)
						<ul class="list-style-one">
							<li><span>{!!@$value->name!!}</span> {!!@$value->descriptions!!} </li>							
						</ul>
						@endforeach
						@endif
						<a href="{{url('about')}}" class="theme-btn btn-style-four">Read More</a>
					</div>
				</div>

				<!-- Image Column -->
				<div class="image-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column wowallow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image">
							@if(@$category1->image!=='' && @$category1->image!==null)
							<img src="{{asset('storage/app/public/images/pages/'.@$category1->image)}}" alt="{{@$category1->image_name}}"/>
							@endif
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
	<!-- End Welcome Section -->

	<!-- Services Section Two -->
	<section class="services-section-two">
		<div class="auto-container">
			<div class="sec-title centered">
				<h3 style="font-family: Raleway;">Our <span> Services</span></h3>
				<div class="separater"></div>
			</div>
			<div class="row clearfix">
				<!-- Services Block -->
				@if(@$services!=='')
				@foreach(@$services as $key=>$value)
				<div class="services-block-three col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image"> 
							<a href="{{url(@$value->page_url)}}">
							<img src="{{asset('storage/app/public/images/services/'.@$value->image)}}" alt="{{@$value->image_name}}"/>
							<h4>{{@$value->name}}</h4></a>
						</div>
					</div>
				</div>
				@endforeach
				@endif
							
			</div>
		</div>
	</section>
	<!-- End Services Section Two -->

	<!-- Counter Section -->
	<section class="counter-section" style="background-image:url(images/background/home-banner-counter.png)">
		<div class="auto-container">
			<div class="row clearfix">

				<!-- Order Column -->
				<div class="order-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column" style="background-image:url(images/background/4.png);">
						<div class="icon-box">
							<span class="flaticon flaticon-fast-delivery"></span>
						</div>
						<h2>Track Your Order</h2>
						<div class="text">Enter your Track Id For Instant Search</div>

						<!--Track Form-->
						<div class="track-form">
                            <form method="post" action="contact.html">
                                <div class="form-group">
                                    <input type="text" name="text" value="" placeholder="Tracking ID" required="">
                                    <button type="submit" class="theme-btn"><span class="fa fa-search"></span></button>
                                </div>
                            </form>
                        </div>

						<!-- Social Box -->
						<div class="social-box">
							<a href="#" class="fa fa-twitter"></a>
							<a href="#" class="fa fa-facebook"></a>
							<a href="#" class="fa fa-linkedin"></a>
							<a href="#" class="fa fa-google-plus"></a>
						</div>

					</div>
				</div>

				<!-- Counter Column -->
				<div class="counter-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column">

						<div class="fact-counter">
							<div class="clearfix">
								<!--Column-->
								<div class="column counter-column col-lg-6 col-md-6 col-sm-12">
									<div class="inner">
										<div class="content">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2000" data-stop="45">0</span>
											</div>
											<h4 class="counter-title">Years Experience</h4>
										</div>
									</div>
								</div>

								<!--Column-->
								<div class="column counter-column col-lg-6 col-md-6 col-sm-12">
									<div class="inner">
										<div class="content">
											<div class="count-outer count-box alternate">
												<span class="count-text" data-speed="2800" data-stop="2500">0</span>+
											</div>
											<h4 class="counter-title">Professional Workers</h4>
										</div>
									</div>
								</div>

								<!--Column-->
								<div class="column counter-column col-lg-6 col-md-6 col-sm-12">
									<div class="inner">
										<div class="content">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2500" data-stop="90">0</span>%
											</div>
											<h4 class="counter-title">Areas Covered</h4>
										</div>
									</div>
								</div>

								<!--Column-->
								<div class="column counter-column col-lg-6 col-md-6 col-sm-12">
									<div class="inner">
										<div class="content">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2000" data-stop="180">0</span>+
											</div>
											<h4 class="counter-title">Countries Covered</h4>
										</div>
									</div>
								</div>

								<!--Column-->
								<div class="column counter-column col-lg-6 col-md-6 col-sm-12">
									<div class="inner">
										<div class="content">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2000" data-stop="270">0</span>+
											</div>
											<h4 class="counter-title">Corporate Clients</h4>
										</div>
									</div>
								</div>

								<!--Column-->
								<div class="column counter-column col-lg-6 col-md-6 col-sm-12">
									<div class="inner">
										<div class="content">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2000" data-stop="4500">0</span>+
											</div>
											<h4 class="counter-title">Owned Vehicles</h4>
										</div>
									</div>
								</div>

							</div>
						</div>


					</div>
				</div>

			</div>
		</div>
	</section>

	<section class="team-section gap">
            <div class="auto-container">
			<div class="sec-title centered">
				<?php if(str_word_count(@$category->name)==1){ ?>
                                        <?php
                                        $output=substr(@$category->name, 2, strlen(@$category->name));
                                        $output1=substr(@$category->name, 0, 2);
                                        ?>
                                       <h3><?php  echo @$output1;?><span> <?php echo @$output;?> </span> </h3>
                                    <?php }else{ ?>
                                        <?php $output2=strtok(@$category->name, " ");
                                        $output3=substr(strstr(@$category->name," "), 1);
                                        ?>
                                        <h3>
                                    <?php echo @$output2; ?><span> <?php echo @$output3;?> </span>
                                    <?php } ?></h3>
				<div class="separater"></div> 
			</div>
			<div class="tem-sec">
				<div class="row">
					<div class="col-lg-6 col-md-12 col-sm-12">
						<div class="tm-bx">
							<div class="tm-thmb">
								@if(@$category->image!=='' && @$category->image!==null )
								<img src="{{asset('storage/app/public/images/pages/'.@$category->image)}}" style="padding-bottom: 5%;"alt="{{@$category->image_name}}"></img>
								@endif
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-12 col-sm-12">
						<div class="tm-bx">
							<div class="tm-thmb">
                             
                             @if(@$vission_data!=='' && @$vission_data!==null)
                               @foreach(@$vission_data as $key=>$value)
                            @if(@$key==0)
							<div class="tm-inf wowallow fadeInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
									<br>
									<span class="icon {{@$value->class_name}}" style="font-size:60px; color:#eb0028"></span>
									<h3 span style="color:#eb0028; font-weight: 700; line-height:60px;">{{@$value->name}}</h3>
									<span class="designation" style="text-align: justify; font-size: 12px;"> {!! @$value->descriptions!!}</span>
							</div>
							@endif
                            @if(@$key==1)
							 <div class="tm-bx">
								<div class="tm-thmb">
									<div class="tm-inff wowallow fadeInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
										<br>
										<span class="icon {{@$value->class_name}}" style="font-size:63px; color:#eb0028;text-align: center; padding-left: 44%; padding-right: 44%;"></span>
										<h3 span style="color:#eb0028; font-weight: 700; line-height:60px; text-align: center;">{{@$value->name}}</h3>
										<span class="designation" style="text-align: justify; font-size: 12px;">{!! @$value->descriptions!!}</span>									
									</div>
								</div>
							</div>
							@endif
							@endforeach
							@endif


					</div> 
				</div>
			</div><!-- Team Sec -->
		</div>
	</div>
</div>
    </section>

	<!-- Our technology Section -->
	<section class="price-section" style="background-image:url(images/background/home-tech-banner.png)">
		<div class="auto-container maxwidth-1400">

			<div class="sec-title-two centered light sec-title">
				<h2>Our <span>Technology</span></h2>
				<div class="separater"></div>
			</div>

			<div class="clearfix">

				<!-- Price Block -->
				@if(@$ourtechnology!=='' && @$ourtechnology!==null)
				@foreach(@$ourtechnology as $key=>$value)
				<div class="price-block col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms"> 
						<div class="{{@$value->class_name}}" style="font-size:60px; padding:12%; color:#eb0028"></div>
						<div class="@if($key==0) title-first @endif @if($key==1) title @endif @if($key==2) title @endif">{{@$value->title}}</div>
						<div class="@if($key==0) price-list-first @endif @if($key==1) price-list @endif @if($key==2) price-list-last @endif"> 
							<span class="tech-content para-content">{!!@$value->description!!}</span>
						</div>
					</div>
				</div>
				@endforeach
				@endif 				

			</div>
		</div>
	</section>
	<!-- End Our technology Section -->

	<!-- Our way of business Section -->
	<section class="testimonial-section gap">
		<div class="auto-container">
			<div class="sec-title centered">
				<h3>Our <span>Way Of</span> Business</h3>
				<div class="separater"></div>
			</div>
			<div class="two-item-carousel owl-carousel owl-theme">

              @if(@$ourbuiness!=='' && @$ourbuiness!==null)
				@foreach(@$ourbuiness as $key=>$val)
				<!-- Testimonial Block -->
				<div class="testimonial-block">
					<div class="inner-box">
						<div class="quote-icon {{@$val->class_name}}" style="color:#eb0028; font-size: 35px;"></div> 
						<!-- <h3 style="color:black; font-weight: 600; line-height:50px;"><span style="color:#eb0028; font-weight: 600; line-height:50px;">Operational</span> Excellence</h3> -->
						
							<?php if(str_word_count(@$val->title)==1){ ?>
								<?php 
								$output=substr(@$val->title, 2, strlen(@$val->title));
								$output1=substr(@$val->title, 0, 2);
								?>
                               <h3 style="color:black; font-weight: 600; line-height:50px;"><span style="color:#eb0028; font-weight: 600; line-height:50px;"><?php  echo @$output1;?></span><?php echo @$output;?></h3>
							<?php }else{ ?>
								<?php $output2=strtok(@$val->title, " ");
								$output3=substr(strstr(@$val->title," "), 1);
								?>
								<h3 style="color:black; font-weight: 600; line-height:50px;"><span style="color:#eb0028; font-weight: 600; line-height:50px;">
						    <?php echo @$output2; ?> </span> <?php echo @$output3;?>

						    <?php } ?>

                           </h3>
						<div class="text-one{{$key}}"> {!!@$val->description!!}					
					  
					   </div>						
					</div> 
				</div>
				@endforeach
				@endif



				<!-- Testimonial Block -->

				<!-- <div class="testimonial-block">
					<div class="inner-box">
						<div class="quote-icon flaticon-hand-shake-1" style="color:#eb0028; font-size: 35px;"></div> 
						<h3 style="color:black; font-weight: 600; line-height:50px;"><span style="color:#eb0028; font-weight: 600; line-height:50px;">Customer</span> Relations</h3>
						<div class="text-two">Understanding a company's requirements is the first step in creating a tailor-made solution. We start by listening and challenging. We require all our staff to understand and develop their relationship with customers whether operational, administrative or commercial. Our management structure is aligned to supporting and adding to those relationships.
						<br><br>Throughout the life of a relationship from project phase, to start up, to renewal, we seek to perform, to improve, to anticipate and to innovate in line with our customers' needs.</span></div>
					</div> 
				</div> -->
				<!-- Testimonial Block -->

				<!-- <div class="testimonial-block">
					<div class="inner-box">
						<div class="quote-icon flaticon-delivery-box" style="color:#eb0028; font-size: 35px;"></div> 
						<h3 style="color:black; font-weight: 600; line-height:50px;"><span style="color:#eb0028; font-weight: 600; line-height:50px;">Product</span> Leadership</h3>
						<div class="text-three">
							<ul style="list-style-type:square;">
								<li style="padding-bottom: 3%;">&#10004; BumiLink is a market leader in fully comprehensive integrated total logistics software. With our own in-house development we are able to customize at any level and at any point.</li>
								<li style="padding-bottom: 3%;">&#10004; BumiLink is the first to implement a fully integrated online grocery corporate & consumer supply chain software solution coupled with innovative forward logistics.
							</li>
							<li style="padding-bottom: 3%;">&#10004; BumiLink commitment to product leadership enables us to anticipate the future requirements of our customers, harness new technologies and respond to change.</li>
						   </ul>
						</div>
					</div> 
				</div> -->
				<!-- Testimonial Block -->

				<!-- <div class="testimonial-block">
					<div class="inner-box">
						<div class="quote-icon flaticon-verified" style="color:#eb0028; font-size: 35px;"></div> 
						<h3 style="color:black; font-weight: 600; line-height:50px;"><span style="color:#eb0028; font-weight: 600; line-height:50px;">Va</span>lue</h3>
						<div class="text-four">BumiLink builds all-round value for its customers by tailoring solutions to the precise needs of the customer and by our ability to optimize the performance and productivity of each solution. 
							<br><br>BumiLink offers long-term price differentiation by timely investment in relevant technologies and new product development and by investing directly in our customers' goals.</div>
						
					</div> 
				</div> -->
			</div>
		</div>
	</section>
	<!-- End Our way of business Section -->

	<section class="fullwidth-section">
		<div class="outer-container">
			<div class="clearfix">

				<!-- Left Column -->
				<div class="left-column" style="background-image:url(images/background/home-sender.png)">
					<div class="inner-column">
						<h3>Are You A Sender?</h3>
						<div class="text">We have diverse range of logistics services tailored to your needs in the market.</div>
						<a href="{{url('track')}}" class="theme-btn btn-style-one">Check Packages</a>
					</div>
				</div>

				<!-- Right Column -->
				<div class="right-column" style="background-image:url(images/background/home-shipper.png)">
					<div class="inner-column">
						<h3>Are You A Shipper?</h3>
						<div class="text">We offer a broad range of value-added services that enable us to operate as a single source logistics provider.</div>
						<a href="{{url('contact')}}" class="theme-btn btn-style-two">Contact us</a>
					</div>
				</div>

			</div>
		</div>
	</section>
	<!-- Main Header / Header Style Two -->
      @include('footer')
    <!--End Main Header -->
