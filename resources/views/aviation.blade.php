@include('header')
<!--End Main Header -->


	<!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.@$setting->header)}});">
    	<div class="auto-container">
        	<h2>{!!ucfirst(@$data->name)!!}</h2>
			<div class="separater"></div>
        </div>
    </section>

    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('solutions')}}">Solutions</a> <span>/</span></li>
                <li>{!!ucfirst(@$data->name)!!}</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->

    <!-- Cross Border Express Section -->
	<section class="testimonial-section gap" id="globalsameday" style="margin-top: 3%;">
		<div class="auto-container">

            <div class="row">
                <div class="col-lg-4">
                    <div class="text-center sm-mb-55px">
                        <img src="{{asset('storage/app/public/images/solutions/'.$data->image)}}" alt="{{@$data->image_name}}">
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="inner-column">
                        <div class="sec-title">   
                        <h3 style="font-family: Raleway;"> {!!@$data->name!!} </h3>

                            <div class="separater"></div>
                         <h5 style="font-weight: 500; padding-top: 2%; text-align: justify; padding-right: 5%; font-size: 18px;">{!!@$data->descriptions!!}</h5>
                </div> 
            </div>
        </div>
    </div>
   

	<!-- Aviation Description -->
	<section class="domestic-services-section gap" style="margin-top:-12%;">
		<div class="auto-container">
		<!-- <div class="sec-title centered">
			<h3 style="font-family:Raleway;">Apparel<span> Info</span></h3>
			<div class="separater"></div>  -->
            </div>
		</div>
        <hr style="margin-top:7%; color:#555555; ">
		<div class="tem-sec">
			<div class="row"> 
            </div>
		</div>
	</div>
</section>
<!-- End Aviation Info -->

<!-- Linked Overnight -->
<section class="nile-about-section" style="margin-top: -3%; ">
    @if(@$subcategory!=='' && @$subcategory!==null)
           @foreach(@$subcategory as $key=>$value)
    @if($loop->odd)
    <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
    <div class="auto-container">
        <div class="row clearfix">
                <div class="content-column col-lg-5 col-md-12 col-sm-12" style="margin-bottom: 5%;">
					<div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="sec-title-two sec-title">
                            <h2 style="color:black; font-weight: 600; line-height:50px;">{!!@$value->name!!}</h2>
							<div class="separater"></div>
						</div></div>
                <div class="about-text margin-tb-25px">
                    <h5 style="padding-bottom: 2%;">
                    <ul>
                          {!! @$value->descriptions !!}
                    </ul>
                    </h5>
                </div>
                <!-- <a href="#" class="nile-bottom sm">Read More</a> -->

            </div>
            <div class="col-lg-6">
                <img src="{{asset('images/resource/aviation-bg.png')}}" alt="" style="padding-left: 7%;">
            </div>
            <div style="margin-bottom: 35%;"></div>
        </div>
    </div>
    @else
    <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
    <div class="auto-container">
        <div class="row clearfix">
               <div class="col-lg-6">
                <img src="{{asset('images/resource/aviation-bg.png')}}" alt="" style="padding-left: 7%;">
            </div>
            <div style="margin-bottom: 35%;"></div>
                <div class="content-column col-lg-5 col-md-12 col-sm-12" style="margin-bottom: 5%;">
                    <div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="sec-title-two sec-title">
                           <?php if(str_word_count(@$value->name)==1){ ?>
                                        <?php
                                        $output=substr(@$value->name, 2, strlen(@$value->name));
                                        $output1=substr(@$value->name, 0, 2);
                                        ?>
                                       <h2 style="color:black; font-weight: 600; line-height:50px;"><?php  echo @$output1;?><span style="color:#eb0028; font-weight: 600; line-height:50px;"> <?php echo @$output;?> </span> </h2>
                                    <?php }else{ ?>
                                        <?php $output2=strtok(@$value->name, " ");
                                        $output3=substr(strstr(@$value->name," "), 1);
                                        ?>
                                        <h2 style="color:black; font-weight: 600; line-height:50px;">
                                    <?php echo @$output2; ?><span style="color:#eb0028; font-weight: 600; line-height:50px;"> <?php echo @$output3;?> </span> 
                                    <?php } ?> </h2>
                            <div class="separater"></div>
                        </div></div>
                <div class="about-text margin-tb-25px">
                    <h5 style="padding-bottom: 2%;">
                    <ul>
                        {!! @$value->descriptions !!}
                    </ul>
                    </h5>
                </div>
                <!-- <a href="#" class="nile-bottom sm">Read More</a> -->

            </div>
            
        </div>
    </div>
    @endif
    @endforeach
    @endif


</div>
<!-- End We offer Section --> 
@include('footer')