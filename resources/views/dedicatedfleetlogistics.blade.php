@include('header')
<!--End Main Header -->


	<!--Page Title-->
   <section class="page-title" style="background-image:url({{asset('storage/app/public/images/settings/'.@$setting->header)}});">
    	<div class="auto-container">
        	<h2>{!!ucfirst(@$data->name)!!}</h2>
			<div class="separater"></div>
        </div>
    </section>

    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('solutions')}}">Solutions</a> <span>/</span></li>
                <li>{!!ucfirst(@$data->name)!!}</li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->

    <!-- Cross Border Express Section -->
	<section class="testimonial-section gap" style="margin-top: 3%;">
		<section class="auto-container">

            <div class="row">
                <div class="col-lg-4">
                    <div class="text-center sm-mb-55px">
                        <img src="{{asset('storage/app/public/images/solutions/'.$data->image)}}" alt="" style="margin-top: 0%;">
                    </div> 
                </div>
                <div class="col-lg-8">
                    <div class="inner-column">
                        <div class="sec-title">
                            <h3 style="font-family: Raleway;"> {!!@$dedicatedfleetlogistics->name!!} </h3>
                            <div class="separater"></div>                        		
                        		<h5 style="font-weight: 500; padding-top: 2%; text-align: justify; padding-right: 5%; font-size: 17px;">{!!@$data->descriptions!!} </h5>

                		</div>
           			</div>
        		</div>

				<div class="col-lg-12">
                    <div class="inner-column">

				<div class="tem-sec" style="padding-top: 3%; margin-right: 1%; margin-bottom: 0%;">
					<div class="row">

				 @if(@$dedicatedfleetlogistics!=='' && @$dedicatedfleetlogistics!==null)
                    @foreach(@$dedicatedfleetlogistics->getSubCategory as $key=>$value)
                      @if($key<2)
                      <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
                    	<div class="col-lg-5 col-sm-6">
                        	<div class="dedicated-fleet wowallow @if($loop->odd) background-main-color fadeInLeft @else background-second-color fadeInRight  @endif" data-wow-delay="0ms" data-wow-duration="1500ms">
                                @if($loop->odd)
                                <h2 style="font-weight: 600; text-align: left;">{!!@$value->name!!}</h2>
                            	<hr style="border-color: white;">
                            	@else
                            	<h2 style="font-weight: 600; color: #eb0028; text-align: left;">{!!@$value->name!!}</h2>
                            	<hr>
                            	@endif
                            	<div class="text" style="text-align: justify; position:relative;">{!!@$value->descriptions!!}
								</div>
                        	</div>
                    	</div>
                    	@endif
                   @endforeach
                @endif

                		</div>
           			</div>
        		</div>
    		</div>
		</section>
	</section>

<!-- Key Benefits & What We Offer Accordion -->
<section class="services-section-two" style="margin-top: -3%; ">
    <div class="auto-container">
        <!-- <div class="row clearfix">  -->
			<div class="tem-sec">
				<div class="row">
			<div class="col-lg-4">
                <div class="sec-title wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                	<h3 style="font-family: Raleway;">{!!@$dedicatedfleetlogistics->getSubCategory['2']['name']!!}</h3>					
					<div class="separater"></div>
					</div>
            </div>

			<div class="col-lg-7">
			<div id="accordion" class="nile-accordion margin-top-30px sm-mb-45px">
				@if(@$dedicatedfleetlogistics->getSubCategory['2']['childsubcategory'] !=='')
                   @foreach(@$dedicatedfleetlogistics->getSubCategory['2']['childsubcategory'] as $key=>$value)
                   <span id="{{preg_replace('/\s+/','',@$value->name)}}"></span>
				<div class="card">
					<div class="card-header" id="headingOne{{$key}}">
						<h5 class="mb-0">
							<button class="btn btn-block btn-link @if($key==0) active @endif" data-toggle="collapse" data-target="#collapseOne{{$key}}" aria-expanded="true" aria-controls="collapseOne{{$key}}" style="font-size: 17px; font-weight:600;"><i class="fa {{@$value->class_name}}" style="padding-right:2%;"></i> {!!@$value->name!!}</button>
						</h5>
					</div>

					<div id="collapseOne{{$key}}" class="collapse @if($key==0) show @endif" aria-labelledby="headingOne" data-parent="#accordion">
						<div class="card-body">
						<ul style="font-size:16px; line-height: 30px; color: black;">
							{!! @$value->descriptions !!}
						</ul>
						</div>
					</div>
				</div>
				@endforeach
				@endif				
			</div>
            </div>
            <div style="margin-bottom: 5%;"></div>
        </div>
    </div>
</div></section>
<!-- End Key Benefits & What We Offer Section -->
@include('footer')