<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Quote;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;

class QuoteController extends Controller
{
    //index
    public function listdata(){
         //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', Quote::class);
        //policy     
        $list = Quote::orderBy('id', 'DESC')->get();
        return view('admin.user_quote_list',compact('list'));
    }
    //view
    public function viewdata($id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', Quote::class);
        //policy
        $data =  Quote::findOrFail($id);
        $data->quote_type="Read";
        $data->save();
        $moreinformation = json_decode($data->details);
        return view('admin.user_quote_view',compact('data','moreinformation'));
    }

}