<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\UserFeedback;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;

class FeedbackController extends Controller
{
    //index
    public function listdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserFeedback::class);
        //policy        
        $list = UserFeedback::orderBy('id', 'DESC')->get();
        return view('admin.user_feedback_list',compact('list'));
    }
    //view
    public function viewdata($id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserFeedback::class);
        //policy
        $data =  UserFeedback::findOrFail($id);
        $data->message_type="Read";
        $data->save();
        return view('admin.user_feedback_view',compact('data'));
    }

}