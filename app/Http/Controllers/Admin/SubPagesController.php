<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Usefullink;
use App\Models\Solutions;
use App\Models\Pages;
use App\Models\SubPages;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class SubPagesController extends Controller
{
    //index
    public function listdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', SubPages::class);
        //policy
        $list = SubPages::where('deleted_at',null)->orderBy('id', 'DESC')->get();
        return view('admin.subcategory_pages_list',compact('list'));
    }
    //create
    public function createdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('create', SubPages::class);
        //policy
        $categoryData = Pages::where('status','0')->where('deleted_at',null)->get();
        return view('admin.create_subcategory_pages',compact('categoryData'));
    }
    //view
    public function viewdata($id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', SubPages::class);
        //policy
        $data =  SubPages::findOrFail($id);
        $categoryData = Pages::where('status','0')->where('deleted_at',null)->get();
        return view('admin.update_subcategory_pages',compact('data','categoryData'));
    }
    
    //create
    public function savedata(Request $request)
    {   //policy
         $user = auth()->guard('admin')->user();
         $user->can('create', SubPages::class);
        //policy
        $validated = Validator::make($request->all(),[
        'category' => 'required|exists:pages,id',
        'name' => 'required|min:3',
        'file' => 'nullable|mimes:png,jpg,jpeg|max:2048',
        //'example' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        //create data
        
        $solution = new SubPages();
        if($request->hasFile('file')){
        $name = $request->file('file')->getClientOriginalName();
        $path = $request->file('file')->store('public/images/subpages');
        $solution->image_name  = $name;
        $solution->image  = explode ("/", $path)[3];
        }
        $solution->name = $request->name;
        $solution->class_name = ($request->class_name) ? $request->class_name : '';
        $solution->category_id = $request->category;
        $solution->status  = $request->status;
        $solution->position  = $request->position;
        $solution->descriptions  = ($request->description) ? $request->description : '';
        $solution->save();
        return redirect('admin/subpages')->with('success','Added sucessfully.');
    }
    //update
    public function updatedata(Request $request, $id)
    {
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', SubPages::class);
        //policy
         $validated = Validator::make($request->all(),[
        'name' => 'required|min:3|unique:sub_pages,name,'.$id,
        'file' => 'sometimes|mimes:png,jpg,jpeg|max:2048',
        'category' => 'required|exists:pages,id',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }        
        //update data
        $solution = SubPages::findOrFail($id);
        if($request->hasFile('file'))
         {
            //delete old files
            $old_path = 'public/images/subpages/'.$solution->image;
            if(Storage::disk('public')->exists('images/subpages/'.$solution->image)){
               Storage::disk('public')->delete('images/subpages/'.$solution->image);
             }
            //delete old files
            $name = $request->file('file')->getClientOriginalName(); 
            $path = $request->file('file')->store('public/images/subpages');
            $solution->image_name  = $name;
            $solution->image  = explode ("/", $path)[3];
         }
        $solution->name = $request->name;
        $solution->category_id  = $request->category;
        $solution->position  = $request->position;
        $solution->class_name = ($request->class_name) ? $request->class_name : '';
        $solution->status  = $request->status;
        $solution->descriptions  = ($request->description) ? $request->description : '';        
        $solution->save();
        return redirect('admin/subpages')->with('success','updated sucessfully.');
    }

}