<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Quote;
use App\Models\UserFeedback;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class DashboardController extends Controller
{
    //index
    public function dashboard(Request $request){
        $todayfeedback = UserFeedback::whereDate('created_at',Carbon::today())->count();
        $totalFeedback = UserFeedback::all();
        $todayquote = Quote::whereDate('created_at',Carbon::today())->count();
        $totalquote = Quote::all();
        return view('admin.index',compact('todayfeedback','totalFeedback','todayquote','totalquote'));
    }
}