<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Usefullink;
use App\Models\OurBusiness;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class OurBusinessController extends Controller
{
    //index
    public function listdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', OurBusiness::class);
        //policy
        $list = OurBusiness::where('deleted_at',null)->orderBy('id', 'DESC')->get();
        return view('admin.our_business_list',compact('list'));
    }
    //create
    public function createdata(){
         //policy
         $user = auth()->guard('admin')->user();
         $user->can('create', OurBusiness::class);
        //policy
        return view('admin.create_our_business');
    }
    //view
    public function viewdata($id){
         //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', OurBusiness::class);
        //policy
        $data =  OurBusiness::findOrFail($id);
        return view('admin.update_our_business',compact('data'));
    }
    //delete
    /*public function deletedata(Request $request, $id){        
        $getuserData = Usefullink::findOrFail($id);
        $getuserData->deleted_at = Carbon::now();
        $getuserData->save();        
        return redirect('admin/usefullink')->with('success','Deleted sucessfully.');
    }*/
    //create
    public function savedata(Request $request)
    {   //policy
         $user = auth()->guard('admin')->user();
         $user->can('create', OurBusiness::class);
        //policy
        $validated = Validator::make($request->all(),[
        'title' => 'required|min:3|unique:our_business,title',
        'class_name' => 'required',
        'example' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        //create data
        $business = new OurBusiness();
        $business->title = $request->title;
        $business->description  = $request->example;
        $business->class_name  = $request->class_name;
        $business->status  = $request->status;
        $business->save();
        //alert()->success('Success.','Added sucessfully.')->autoclose(2500); 
        return redirect('admin/ourbusiness')->with('success','Added sucessfully.');
    }
    //update
    public function updatedata(Request $request, $id)
    {    //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', OurBusiness::class);
         //policy
         $validated = Validator::make($request->all(),[
        'title' => 'required|min:3|unique:our_business,title,'.$id,
        'class_name' => 'required',
        'example' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput();
        }
        //update data
        $business = OurBusiness::findOrFail($id);
        $business->title = $request->title;
        $business->class_name  = $request->class_name;
        $business->status  = $request->status;
        $business->description  = $request->example;        
        $business->save();
        return redirect('admin/ourbusiness')->with('success','updated sucessfully.');
    }

}