<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\UserLoginHistroy;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;

class UserLoginHistroyController extends Controller
{
    //index
    public function listdata(){
        //dd('hi');
        //policy
        /* $user = auth()->guard('admin')->user();
         $user->can('view', UserLoginHistroy::class);*/
        //policy
        $list = UserLoginHistroy::orderBy('id', 'DESC')->get();
        return view('admin.user_login_histroy_list',compact('list'));
    }

}