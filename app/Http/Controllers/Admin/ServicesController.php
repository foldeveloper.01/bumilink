<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Usefullink;
use App\Models\Services;
use App\Models\Services1;
use App\Models\Services2;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class ServicesController extends Controller
{
    //index
    public function listdata(){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('view', Services::class);
        //policy
        $list = Services::where('id','!=','6')->where('deleted_at',null)->orderBy('id', 'DESC')->get();
        return view('admin.services_list',compact('list'));
    }
    //create
    public function createdata(){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('create', Services::class);
        //policy
        return view('admin.create_services');
    }
    //view
    public function viewdata($id){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('view', Services::class);
        //policy
        $data =  Services::findOrFail($id);
        return view('admin.update_services',compact('data'));
    }
    //create
    public function savedata(Request $request)
    {
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('create', Services::class);
        //policy
        $validated = Validator::make($request->all(),[
        'name' => 'required|min:3|unique:services,name',
        'file' => 'nullable|mimes:png,jpg,jpeg|max:2048',
        'page_url' => 'required',
        //'description' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        //create data
        $services = new Services();
        if($request->hasFile('file'))
         {
            $name = $request->file('file')->getClientOriginalName();
            $path = $request->file('file')->store('public/images/services');
            $services->image_name  = $name;
            $services->image  = explode ("/", $path)[3];
         }                
        $services->name = $request->name;
        $services->page_url  = strtolower($request->page_url);
        $services->status  = $request->status;
        $services->descriptions  = ($request->description) ? $request->description : '';        
        $services->class_name  = ($request->description) ? $request->class_name : '';        
        $services->save();        
        return redirect('admin/services')->with('success','Added sucessfully.');
    }
    //update
    public function updatedata(Request $request, $id)
    {
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('update', Services::class);
        //policy
         $validated = Validator::make($request->all(),[
        'name' => 'required|min:3|unique:services,name,'.$id,
        'file' => 'sometimes|mimes:png,jpg,jpeg|max:2048',
        'description' => 'required',
        //'page_url' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }        
        //update data
        $services = Services::findOrFail($id);
        if($request->hasFile('file'))
         {
            //delete old files
            $old_path = 'public/images/services/'.$services->image;
            if(Storage::disk('public')->exists('images/services/'.$services->image)){
               Storage::disk('public')->delete('images/services/'.$services->image);
             }
            //delete old files
            $name = $request->file('file')->getClientOriginalName(); 
            $path = $request->file('file')->store('public/images/services');
            $services->image_name  = $name;
            $services->image  = explode ("/", $path)[3];
         }
        $services->name = $request->name;
        //$services->page_url  = strtolower($request->page_url);
        $services->status  = $request->status;
        $services->descriptions  = ($request->description) ? $request->description : '';        
        $services->class_name  = ($request->class_name) ? $request->class_name : '';        
        $services->save();
        //alert()->success('Success.','updated sucessfully.')->autoclose(1000);
        return redirect('admin/services')->with('success','updated sucessfully.');
    }

    public function listdata1(){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('view', Services1::class);
        //policy
        $list = Services1::where('deleted_at',null)->orderBy('id', 'DESC')->get();
        return view('admin.services_list1',compact('list'));
    }
    //create
    public function createdata1(){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('create', Services1::class);
        //policy
        $category = Services::where('status','0')->where('deleted_at',null)->get();
        return view('admin.create_services1',compact('category'));
    }
    //view
    public function viewdata1($id){        
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('update', Services1::class);
        //policy        
        $data =  Services1::findOrFail($id);
        $category = Services::where('id',$data->category_id)->where('status','0')->where('deleted_at',null)->get();
        return view('admin.update_services1',compact('data','category'));
    }
    //create
    public function savedata1(Request $request)
    {
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('create', Services1::class);
        //policy
        $validated = Validator::make($request->all(),[
        'category' => 'required|exists:services,id',
        'name' => 'required|min:3|',
        //'class_name' => 'required',
        'file' => 'nullable|mimes:png,jpg,jpeg|max:2048',
        //'description' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        //create data
        

        $services = new Services1();
        if($request->hasFile('file'))
         {
            $name = $request->file('file')->getClientOriginalName();
            $path = $request->file('file')->store('public/images/services');
            $services->image_name  = $name;
            $services->image  = explode ("/", $path)[3];
         }
        $services->name = $request->name;
        $services->category_id  = $request->category;
        $services->status  = $request->status;
        $services->text  = ($request->text) ? $request->text : '';
        $services->descriptions  = ($request->description) ? $request->description : '';
        $services->position  = ($request->position) ? $request->position : '';
        $services->class_name  = ($request->class_name) ? $request->class_name : '';
        $services->save();
        //alert()->success('Success.','Added sucessfully.')->autoclose(2500); 
        return redirect('admin/services/category')->with('success','Added sucessfully.');
    }
    //update
    public function updatedata1(Request $request, $id)
    {
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('update', Services1::class);
        //policy
         $validated = Validator::make($request->all(),[
        'category' => 'required|exists:services,id',
        'name' => 'required|min:3|',
        'file' => 'sometimes|mimes:png,jpg,jpeg|max:2048',
        //'description' => 'required',
        //'page_url' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }        
        //update data
        $services = Services1::findOrFail($id);
        if($request->hasFile('file'))
         {
            //delete old files
            $old_path = 'public/images/services/'.$services->image;
            if(Storage::disk('public')->exists('images/services/'.$services->image)){
               Storage::disk('public')->delete('images/services/'.$services->image);
             }
            //delete old files
            $name = $request->file('file')->getClientOriginalName(); 
            $path = $request->file('file')->store('public/images/services');
            $services->image_name  = $name;
            $services->image  = explode ("/", $path)[3];
         }
        $services->name = $request->name;
        $services->category_id  = $request->category;
        $services->status  = $request->status;
        $services->descriptions  = ($request->description) ? $request->description : '';
        $services->position  = ($request->position) ? $request->position : '';
        $services->class_name  = ($request->class_name) ? $request->class_name : '';
        $services->text  = ($request->text) ? $request->text : '';
        $services->save();
        //alert()->success('Success.','updated sucessfully.')->autoclose(2500);
        return redirect('admin/services/category')->with('success','updated sucessfully.');
    }

    public function listdata2(){
         //policy
        $user = auth()->guard('admin')->user();
        $user->can('view', Services2::class);
        //policy
        $list = Services2::where('deleted_at',null)->orderBy('id', 'DESC')->get();
        return view('admin.services_list2',compact('list'));
    }
    //create
    public function createdata2(){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('create', Services2::class);
        //policy 
        $category = Services::where('status','0')->where('deleted_at',null)->get();
        $subcategory = Services1::where('status','0')->where('deleted_at',null)->get();
        return view('admin.create_services2',compact('category'));
    }
    //view
    public function viewdata2($id){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('update', Services2::class);
        //policy
        $data =  Services2::findOrFail($id);
        $category = Services::where('id',$data->category_id)->where('status','0')->where('deleted_at',null)->get();
        $subcategory = Services1::where('category_id',$data->category_id)->where('status','0')->where('deleted_at',null)->get();
        return view('admin.update_services2',compact('data','category','subcategory'));
    }
    //getsubcategory
    public function getservicessubcategory($id){        
        $data['data'] =  Services1::where('category_id',$id)->select('id','name')->get();
        return response()->json($data); exit;
    }
    //create
    public function savedata2(Request $request)
    {   //policy
        $user = auth()->guard('admin')->user();
        $user->can('create', Services2::class);
        //policy 
        $validated = Validator::make($request->all(),[
        'category' => 'required|exists:services,id',
        'subcategory' => 'required|exists:services1,id',
        'name' => 'required|min:3',
        //'class_name' => 'required',
        'file' => 'sometimes|mimes:png,jpg,jpeg|max:2048',
        //'description' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        //create data
        

        $services = new Services2();
        if($request->hasFile('file'))
         {
            $name = $request->file('file')->getClientOriginalName();
            $path = $request->file('file')->store('public/images/services');
            $services->image_name  = $name;
            $services->image  = explode ("/", $path)[3];
         }
        $services->name = $request->name;
        $services->category_id  = $request->category;
        $services->subcategory_id  = $request->subcategory;
        $services->status  = $request->status;
        $services->descriptions  = ($request->description) ? $request->description : '';
        $services->position  = ($request->position) ? $request->position : '';
        $services->class_name  = ($request->class_name) ? $request->class_name : '';
        $services->save();
        //alert()->success('Success.','Added sucessfully.')->autoclose(2500); 
        return redirect('admin/services/category/subcategory')->with('success','Added sucessfully.');
    }
    //update
    public function updatedata2(Request $request, $id)
    {
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('update', Services2::class);
        //policy 
         $validated = Validator::make($request->all(),[
        'category' => 'required|exists:services,id',
        'subcategory' => 'required|exists:services1,id',
        'name' => 'required|min:3|',
        'file' => 'sometimes|mimes:png,jpg,jpeg|max:2048',
        //'description' => 'required',
        //'page_url' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }        
        //update data
        $services = Services2::findOrFail($id);
        if($request->hasFile('file'))
         {
            //delete old files
            $old_path = 'public/images/services/'.$services->image;
            if(Storage::disk('public')->exists('images/services/'.$services->image)){
               Storage::disk('public')->delete('images/services/'.$services->image);
             }
            //delete old files
            $name = $request->file('file')->getClientOriginalName(); 
            $path = $request->file('file')->store('public/images/services');
            $services->image_name  = $name;
            $services->image  = explode ("/", $path)[3];
         }
        $services->name = $request->name;
        $services->category_id  = $request->category;
        $services->subcategory_id  = $request->subcategory;
        $services->status  = $request->status;
        $services->descriptions  = ($request->description) ? $request->description : '';
        $services->position  = ($request->position) ? $request->position : '';
        $services->class_name  = ($request->class_name) ? $request->class_name : '';
        $services->save();
        //alert()->success('Success.','updated sucessfully.')->autoclose(2500);
        return redirect('admin/services/category/subcategory')->with('success','updated sucessfully.');
    }

}