<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Usefullink;
use App\Models\Solutions;
use App\Models\Pages;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class PagesController extends Controller
{
    //index
    public function listdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', Pages::class);
        //policy
        $list = Pages::where('deleted_at',null)->orderBy('id', 'DESC')->get();
        return view('admin.pages_list',compact('list'));
    }
    //create
    public function createdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('create', Pages::class);
        //policy
        return view('admin.create_pages');
    }
    //view
    public function viewdata($id){
         //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Pages::class);
        //policy
        $data =  Pages::findOrFail($id);
        return view('admin.update_pages',compact('data'));
    }
    
    //create
    public function savedata(Request $request)
    {   //policy
         $user = auth()->guard('admin')->user();
         $user->can('create', Pages::class);
        //policy
        $validated = Validator::make($request->all(),[
        'name' => 'required|min:3|unique:solutions,name',
        'file' => 'sometimes|mimes:png,jpg,jpeg|max:2048',
        //'description' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        //create data
        $solution = new Pages();
        if($request->hasFile('file'))
         {
          $name = $request->file('file')->getClientOriginalName();
          $path = $request->file('file')->store('public/images/pages');
          $solution->image_name  = $name;
          $solution->image  = explode ("/", $path)[3];
         }        
        $solution->name = $request->name;
        $solution->status  = $request->status;
        $solution->page_url  = strtolower($request->page_url);
        $solution->descriptions  = ($request->example) ? $request->example : '';
        $solution->save();
        //alert()->success('Success.','Added sucessfully.')->autoclose(2500);
        return redirect('admin/pages')->with('success','Added sucessfully.');
    }
    //update
    public function updatedata(Request $request, $id)
    {
         //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Pages::class);
        //policy
         $validated = Validator::make($request->all(),[
        'name' => 'required|min:3|unique:solutions,name,'.$id,
        'file' => 'sometimes|mimes:png,jpg,jpeg|max:2048',
        //'description' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }        
        //update data
        $solution = Pages::findOrFail($id);
        if($request->hasFile('file')) 
         {
            //delete old files
            $old_path = 'public/images/pages/'.$solution->image;
            if(Storage::disk('public')->exists('images/pages/'.$solution->image)){
               Storage::disk('public')->delete('images/pages/'.$solution->image);
             }
            //delete old files
            $name = $request->file('file')->getClientOriginalName(); 
            $path = $request->file('file')->store('public/images/pages');
            $solution->image_name  = $name;
            $solution->image  = explode ("/", $path)[3];
         }
        $solution->name = $request->name;
        $solution->status  = $request->status;
        $solution->descriptions  = ($request->example) ? $request->example : '';        
        $solution->save();
        //alert()->success('Success.','Updated sucessfully.')->autoclose(2500);
        return redirect('admin/pages')->with('success','updated sucessfully.');
    }

}