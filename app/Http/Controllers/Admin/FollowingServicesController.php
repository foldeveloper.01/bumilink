<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\FollowingServices;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;

class FollowingServicesController extends Controller
{
    //index
    public function listdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', FollowingServices::class);
        //policy
        $list = FollowingServices::where('deleted_at',null)->orderBy('id', 'DESC')->get();
        return view('admin.followingservicelist',compact('list'));
    }
    //create
    public function createdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('create', FollowingServices::class);
        //policy
        return view('admin.createfollowingservice');
    }
    //view
    public function viewdata($id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', FollowingServices::class);
        //policy
        $data =  FollowingServices::findOrFail($id);
        return view('admin.updatefollowingservice',compact('data'));
    }
    //delete
    public function deletedata(Request $request, $id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('delete', FollowingServices::class);
        //policy       
        $getuserData = FollowingServices::findOrFail($id);
        $getuserData->deleted_at = Carbon::now();
        $getuserData->save();        
        return redirect('admin/followingservices')->with('success','Deleted sucessfully.');
    }
    //create
    public function savedata(Request $request)
    {   //policy
         $user = auth()->guard('admin')->user();
         $user->can('create', FollowingServices::class);
        //policy
        $validated = Validator::make($request->all(),[
        'name' => 'required|min:3|unique:following_services,name',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        //create data
        $user = new FollowingServices();
        $user->name = $request->name;
        $user->status  = $request->status;
        $user->save();
        //alert()->success('Success.','Added sucessfully.')->autoclose(2500);
        return redirect('admin/followingservices')->with('success','Added sucessfully.');
    }
    //update
    public function updatedata(Request $request, $id)
    {    //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', FollowingServices::class);
        //policy
         $validated = Validator::make($request->all(),[
        'name' => 'required|min:3|unique:following_services,name,'.$id,
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput();
        }        
        //update data
        $user = FollowingServices::find($id);
        $user->name = $request->name;
        $user->status  = $request->status;
        $user->save();
        //alert()->success('Success.','updated sucessfully.')->autoclose(2500);
        return redirect('admin/followingservices')->with('success','updated sucessfully.');
    }

}