<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Heardabout;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;

class HeardaboutController extends Controller
{
    //index
    public function listdata(){
         //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', Heardabout::class);
        //policy
        $list = Heardabout::where('deleted_at',null)->orderBy('id', 'DESC')->get();
        return view('admin.heard_about_list',compact('list'));
    }
    //create
    public function createdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('create', Heardabout::class);
        //policy
        return view('admin.create_heard_about');
    }
    //view
    public function viewdata($id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Heardabout::class);
        //policy
        $data =  Heardabout::findOrFail($id);
        return view('admin.update_heard_about',compact('data'));
    }
    //delete
    public function deletedata(Request $request, $id){
         //policy
         $user = auth()->guard('admin')->user();
         $user->can('delete', Heardabout::class);
        //policy       
        $getuserData = Heardabout::findOrFail($id);
        $getuserData->deleted_at = Carbon::now();
        $getuserData->save();    
        //alert()->success('Success.','Deleted sucessfully.')->autoclose(2500);    
        return redirect('admin/heardabout')->with('success','Deleted sucessfully.');
    }
    //create
    public function savedata(Request $request)
    {   //policy
         $user = auth()->guard('admin')->user();
         $user->can('create', Heardabout::class);
        //policy
        $validated = Validator::make($request->all(),[
        'name' => 'required|min:5|unique:contact_by,name',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        //create data
        $user = new Heardabout();
        $user->name = $request->name;
        $user->status  = $request->status;
        $user->save();
        //alert()->success('Success.','Added sucessfully.')->autoclose(2500);  
        return redirect('admin/heardabout')->with('success','Added sucessfully.');
    }
    //update
    public function updatedata(Request $request, $id)
    {   //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Heardabout::class);
        //policy
         $validated = Validator::make($request->all(),[
        'name' => 'required|min:5|unique:contact_by,name,'.$id,
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput();
        }        
        //update data
        $user = Heardabout::find($id);
        $user->name = $request->name;
        $user->status  = $request->status;
        $user->save();
        //alert()->success('Success.','updated sucessfully.')->autoclose(2500);
        return redirect('admin/heardabout')->with('success','updated sucessfully.');
    }

}