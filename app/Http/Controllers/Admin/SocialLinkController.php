<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Sociallink;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;

class SocialLinkController extends Controller
{
    //index
    public function listdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', Sociallink::class);
        //policy
        $list = Sociallink::where('deleted_at',null)->orderBy('id', 'DESC')->get();
        return view('admin.social_link_list',compact('list'));
    }
    //create
    public function createdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('create', Sociallink::class);
        //policy
        return view('admin.create_social_link');
    }
    //view
    public function viewdata($id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Sociallink::class);
        //policy
        $data =  Sociallink::findOrFail($id);
        return view('admin.update_social_link',compact('data'));
    }
    //delete
    public function deletedata(Request $request, $id){ 
         //policy
         $user = auth()->guard('admin')->user();
         $user->can('delete', Sociallink::class);
        //policy      
        $getuserData = Sociallink::findOrFail($id);
        $getuserData->deleted_at = Carbon::now();
        $getuserData->save();
        //alert()->success('Success.','Deleted sucessfully.')->autoclose(2500);
        return redirect('admin/sociallink')->with('success','Deleted sucessfully.');
    }
    //create
    public function savedata(Request $request)
    {   //policy
         $user = auth()->guard('admin')->user();
         $user->can('create', Sociallink::class);
        //policy
        $validated = Validator::make($request->all(),[
        'name' => 'required|min:5|unique:socia_llink,name',
        'link' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        //create data
        $user = new Sociallink();
        $user->name = $request->name;
        $user->status  = $request->status;
        $user->link  = $request->link;
        $user->position  = $request->position;
        $user->hover  = ($request->hover) ? $request->hover : '';
        $user->save();
        //alert()->success('Success.','Added sucessfully.')->autoclose(2500);
        return redirect('admin/sociallink')->with('success','Added sucessfully.');
    }
    //update
    public function updatedata(Request $request, $id)
    {
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Sociallink::class);
        //policy
         $validated = Validator::make($request->all(),[
        'name' => 'required|unique:socia_llink,name,'.$id,
        'link' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }        
        //update data
        $user = Sociallink::find($id);
        $user->name = $request->name;
        $user->link  = $request->link;
        $user->status  = $request->status;
        $user->position  = $request->position;
        $user->hover  = ($request->hover) ? $request->hover : '';
        $user->save();
        //alert()->success('Success.','Updated sucessfully.')->autoclose(2500);
        return redirect('admin/sociallink')->with('success','updated sucessfully.');
    }

}