<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Usefullink;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;

class UsefullinkController extends Controller
{
    //index
    public function listdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', Usefullink::class);
        //policy
        $list = Usefullink::where('deleted_at',null)->orderBy('id', 'DESC')->get();
        return view('admin.useful_link_list',compact('list'));
    }
    //create
    public function createdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('create', Usefullink::class);
        //policy
        return view('admin.create_useful_link');
    }
    //view
    public function viewdata($id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Usefullink::class);
        //policy
        $data =  Usefullink::findOrFail($id);
        return view('admin.update_useful_link',compact('data'));
    }
    //delete
    public function deletedata(Request $request, $id){     
         //policy
         $user = auth()->guard('admin')->user();
         $user->can('delete', Usefullink::class);
        //policy   
        $getuserData = Usefullink::findOrFail($id);
        $getuserData->deleted_at = Carbon::now();
        $getuserData->save();
        alert()->success('Success.','Deleted sucessfully.')->autoclose(2500);        
        return redirect('admin/usefullink');
    }
    //create
    public function savedata(Request $request)
    {    //policy
         $user = auth()->guard('admin')->user();
         $user->can('create', Usefullink::class);
        //policy
        $validated = Validator::make($request->all(),[
        'name' => 'required|min:5',
        /*'link' => 'required',*/
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        //create data
        $user = new Usefullink();
        $user->name = $request->name;
        $user->status  = $request->status;
        $user->link  = ($request->link) ? $request->link : '';
        $user->hover  = ($request->hover) ? $request->hover : '';
        $user->save();
        //alert()->success('Success.','Added sucessfully.')->autoclose(2500);
        return redirect('admin/usefullink')->with('success','updated sucessfully.');
    }
    //update
    public function updatedata(Request $request, $id)
    {   //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Usefullink::class);
        //policy
         $validated = Validator::make($request->all(),[
        'name' => 'required|min:5',
        /*'link' => 'required',*/
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput();
        }        
        //update data
        $user = Usefullink::find($id);
        $user->name = $request->name;
        $user->link  = $request->link;
        $user->status  = $request->status;
        $user->hover  = ($request->hover) ? $request->hover : '';
        $user->save();
        //alert()->success('Success.','Updated sucessfully.')->autoclose(2500);
        return redirect('admin/usefullink')->with('success','updated sucessfully.');
    }

}