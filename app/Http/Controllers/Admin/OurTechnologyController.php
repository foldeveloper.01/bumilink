<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Usefullink;
use App\Models\OurTechnology;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class OurTechnologyController extends Controller
{
    //index
    public function listdata(){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('view', OurTechnology::class);
        //policy
        $list = OurTechnology::where('deleted_at',null)->orderBy('id', 'DESC')->get();
        return view('admin.our_solution_list',compact('list'));
    }
    //create
    public function createdata(){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('create', OurTechnology::class);
        //policy
        return view('admin.create_our_solutions');
    }
    //view
    public function viewdata($id){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('update', OurTechnology::class);
        //policy
        $data =  OurTechnology::findOrFail($id);
        return view('admin.update_our_solutions',compact('data'));
    }
    //delete
    /*public function deletedata(Request $request, $id){        
        $getuserData = Usefullink::findOrFail($id);
        $getuserData->deleted_at = Carbon::now();
        $getuserData->save();        
        return redirect('admin/usefullink')->with('success','Deleted sucessfully.');
    }*/
    //create
    public function savedata(Request $request)
    {   //policy
        $user = auth()->guard('admin')->user();
        $user->can('create', OurTechnology::class);
        //policy
        $validated = Validator::make($request->all(),[
        'title' => 'required|min:3|unique:our_technology,title',
        'class_name' => 'required',
        'example' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        //create data
        $technology = new OurTechnology();
        $technology->title = $request->title;
        $technology->description  = $request->example;
        $technology->class_name  = $request->class_name;
        $technology->status  = $request->status;
        $technology->save();
        return redirect('admin/ourtechnology')->with('success','Added sucessfully.');
    }
    //update
    public function updatedata(Request $request, $id)
    {   //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', OurTechnology::class);
        //policy
         $validated = Validator::make($request->all(),[
        'title' => 'required|min:3|unique:our_technology,title,'.$id,
        'class_name' => 'required',
        'example' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        //update data
        $technology = OurTechnology::findOrFail($id);
        $technology->title = $request->title;
        $technology->class_name  = $request->class_name;
        $technology->status  = $request->status;
        $technology->description  = $request->example;        
        $technology->save();
        return redirect('admin/ourtechnology')->with('success','updated sucessfully.');
    }

}