<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Usefullink;
use App\Models\Solutions;
use App\Models\Solutions1;
use App\Models\Solutions2;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class SolutionsController extends Controller
{
    //index
    public function listdata(){
         //policy
        $user = auth()->guard('admin')->user();
        $user->can('view', Solutions::class);
        //policy
        $list = Solutions::where('deleted_at',null)->orderBy('id', 'DESC')->get();
        return view('admin.solutions_list',compact('list'));
    }
    //create
    public function createdata(){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('create', Solutions::class);
        //policy
        return view('admin.create_solutions');
    }
    //view
    public function viewdata($id){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('update', Solutions::class);
        //policy
        $data =  Solutions::findOrFail($id);
        return view('admin.update_solutions',compact('data'));
    }
    //create
    public function savedata(Request $request)
    {   //policy
        $user = auth()->guard('admin')->user();
        $user->can('create', Solutions::class);
        //policy
        $validated = Validator::make($request->all(),[
        'name' => 'required|min:3|unique:solutions,name',
        'file' => 'nullable|mimes:png,jpg,jpeg|max:2048',
        'page_url' => 'required',
        'description' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        //create data
        $solution = new Solutions();
        if($request->hasFile('file'))
         {
            $name = $request->file('file')->getClientOriginalName();
            $path = $request->file('file')->store('public/images/solutions');
            $solution->image_name  = $name;
            $solution->image  = explode ("/", $path)[3];
         }                
        $solution->name = $request->name;
        $solution->page_url  = strtolower($request->page_url);
        $solution->status  = $request->status;
        $solution->descriptions  = ($request->description) ? $request->description : '';        
        $solution->save();
        //alert()->success('Success.','Added sucessfully.')->autoclose(2500); 
        return redirect('admin/solutions')->with('success','Added sucessfully.');
    }
    //update
    public function updatedata(Request $request, $id)
    {
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('update', Solutions::class);
        //policy
         $validated = Validator::make($request->all(),[
        'name' => 'required|min:3|unique:solutions,name,'.$id,
        'file' => 'sometimes|mimes:png,jpg,jpeg|max:2048',
        'description' => 'required',
        'page_url' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }        
        //update data
        $solution = Solutions::findOrFail($id);
        if($request->hasFile('file'))
         {
            //delete old files
            $old_path = 'public/images/solutions/'.$solution->image;
            if(Storage::disk('public')->exists('images/solutions/'.$solution->image)){
               Storage::disk('public')->delete('images/solutions/'.$solution->image);
             }
            //delete old files
            $name = $request->file('file')->getClientOriginalName(); 
            $path = $request->file('file')->store('public/images/solutions');
            $solution->image_name  = $name;
            $solution->image  = explode ("/", $path)[3];
         }
        $solution->name = $request->name;
        //$solution->page_url  = strtolower($request->page_url);
        $solution->status  = $request->status;
        $solution->descriptions  = ($request->description) ? $request->description : '';        
        $solution->save();
        //alert()->success('Success.','updated sucessfully.')->autoclose(2500);
        return redirect('admin/solutions')->with('success','updated sucessfully.');
    }

    public function listdata1(){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('view', Solutions1::class);
        //policy
        $list = Solutions1::where('deleted_at',null)->orderBy('id', 'DESC')->get();
        return view('admin.solutions_list1',compact('list'));
    }
    //create
    public function createdata1(){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('create', Solutions1::class);
        //policy
        $category = Solutions::where('status','0')->where('deleted_at',null)->get();
        return view('admin.create_solutions1',compact('category'));
    }
    //view
    public function viewdata1($id){ 
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('update', Solutions1::class);
        //policy      
        $data =  Solutions1::findOrFail($id);
        $category = Solutions::where('id',$data->category_id)->where('status','0')->where('deleted_at',null)->get();
        return view('admin.update_solutions1',compact('data','category'));
    }
    //create
    public function savedata1(Request $request)
    { //policy
        $user = auth()->guard('admin')->user();
        $user->can('create', Solutions1::class);
        //policy 
        $validated = Validator::make($request->all(),[
        'category' => 'required|exists:solutions,id',
        'name' => 'required|min:3|',
        //'class_name' => 'required',
        'file' => 'nullable|mimes:png,jpg,jpeg|max:2048',
        //'description' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        //create data
        

        $solution = new Solutions1();
        if($request->hasFile('file'))
         {
            $name = $request->file('file')->getClientOriginalName();
            $path = $request->file('file')->store('public/images/solutions');
            $solution->image_name  = $name;
            $solution->image  = explode ("/", $path)[3];
         }
        $solution->name = $request->name;
        $solution->category_id  = $request->category;
        $solution->status  = $request->status;
        $solution->descriptions  = ($request->description) ? $request->description : '';
        $solution->position  = ($request->position) ? $request->position : '';
        $solution->class_name  = ($request->class_name) ? $request->class_name : '';
        $solution->save();
        //alert()->success('Success.','Added sucessfully.')->autoclose(2500); 
        return redirect('admin/solutions/category1')->with('success','Added sucessfully.');
    }
    //update
    public function updatedata1(Request $request, $id)
    { //policy
        $user = auth()->guard('admin')->user();
        $user->can('update', Solutions1::class);
        //policy 
         $validated = Validator::make($request->all(),[
        'category' => 'required|exists:solutions,id',
        'name' => 'required|min:3|',
        'file' => 'sometimes|mimes:png,jpg,jpeg|max:2048',
        //'description' => 'required',
        //'page_url' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }        
        //update data
        $solution = Solutions1::findOrFail($id);
        if($request->hasFile('file'))
         {
            //delete old files
            $old_path = 'public/images/solutions/'.$solution->image;
            if(Storage::disk('public')->exists('images/solutions/'.$solution->image)){
               Storage::disk('public')->delete('images/solutions/'.$solution->image);
             }
            //delete old files
            $name = $request->file('file')->getClientOriginalName(); 
            $path = $request->file('file')->store('public/images/solutions');
            $solution->image_name  = $name;
            $solution->image  = explode ("/", $path)[3];
         }
        $solution->name = $request->name;
        $solution->category_id  = $request->category;
        $solution->status  = $request->status;
        $solution->descriptions  = ($request->description) ? $request->description : '';
        $solution->position  = ($request->position) ? $request->position : '';
        $solution->class_name  = ($request->class_name) ? $request->class_name : '';
        $solution->save();
        //alert()->success('Success.','updated sucessfully.')->autoclose(2500);
        return redirect('admin/solutions/category1')->with('success','updated sucessfully.');
    }

    public function listdata2(){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('view', Solutions2::class);
        //policy 
        $list = Solutions2::where('deleted_at',null)->orderBy('id', 'DESC')->get();
        return view('admin.solutions_list2',compact('list'));
    }
    //create
    public function createdata2(){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('create', Solutions2::class);
        //policy 
        $category = Solutions::where('status','0')->where('deleted_at',null)->get();
        $subcategory = Solutions1::where('status','0')->where('deleted_at',null)->get();
        return view('admin.create_solutions2',compact('category'));
    }
    //view
    public function viewdata2($id){
        //policy
        $user = auth()->guard('admin')->user();
        $user->can('update', Solutions2::class);
        //policy 
        $data =  Solutions2::findOrFail($id);
        $category = Solutions::where('id',$data->category_id)->where('status','0')->where('deleted_at',null)->get();
        $subcategory = Solutions1::where('category_id',$data->category_id)->where('status','0')->where('deleted_at',null)->get();
        return view('admin.update_solutions2',compact('data','category','subcategory'));
    }
    //getsubcategory
    public function getsolutionssubcategory($id){        
        $data['data'] =  Solutions1::where('category_id',$id)->select('id','name')->get();
        return response()->json($data); exit;
    }
    //create
    public function savedata2(Request $request)
    {  //policy
        $user = auth()->guard('admin')->user();
        $user->can('create', Solutions2::class);
        //policy 
        $validated = Validator::make($request->all(),[
        'category' => 'required|exists:solutions,id',
        'subcategory' => 'required|exists:solutions1,id',
        'name' => 'required|min:3',
        //'class_name' => 'required',
        'file' => 'sometimes|mimes:png,jpg,jpeg|max:2048',
        //'description' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        //create data
        

        $solution = new Solutions2();
        if($request->hasFile('file'))
         {
            $name = $request->file('file')->getClientOriginalName();
            $path = $request->file('file')->store('public/images/solutions');
            $solution->image_name  = $name;
            $solution->image  = explode ("/", $path)[3];
         }
        $solution->name = $request->name;
        $solution->category_id  = $request->category;
        $solution->subcategory_id  = $request->subcategory;
        $solution->status  = $request->status;
        $solution->descriptions  = ($request->description) ? $request->description : '';
        $solution->position  = ($request->position) ? $request->position : '';
        $solution->class_name  = ($request->class_name) ? $request->class_name : '';
        $solution->save();
        //alert()->success('Success.','Added sucessfully.')->autoclose(2500); 
        return redirect('admin/solutions/category1/subcategory1')->with('success','Added sucessfully.');
    }
    //update
    public function updatedata2(Request $request, $id)
    {   //policy
        $user = auth()->guard('admin')->user();
        $user->can('update', Solutions2::class);
        //policy 
         $validated = Validator::make($request->all(),[
        'category' => 'required|exists:solutions,id',
        'subcategory' => 'required|exists:solutions1,id',
        'name' => 'required|min:3|',
        'file' => 'sometimes|mimes:png,jpg,jpeg|max:2048',
        //'description' => 'required',
        //'page_url' => 'required',
        'status' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }        
        //update data
        $solution = Solutions2::findOrFail($id);
        if($request->hasFile('file'))
         {
            //delete old files
            $old_path = 'public/images/solutions/'.$solution->image;
            if(Storage::disk('public')->exists('images/solutions/'.$solution->image)){
               Storage::disk('public')->delete('images/solutions/'.$solution->image);
             }
            //delete old files
            $name = $request->file('file')->getClientOriginalName(); 
            $path = $request->file('file')->store('public/images/solutions');
            $solution->image_name  = $name;
            $solution->image  = explode ("/", $path)[3];
         }
        $solution->name = $request->name;
        $solution->category_id  = $request->category;
        $solution->subcategory_id  = $request->subcategory;
        $solution->status  = $request->status;
        $solution->descriptions  = ($request->description) ? $request->description : '';
        $solution->position  = ($request->position) ? $request->position : '';
        $solution->class_name  = ($request->class_name) ? $request->class_name : '';
        $solution->save();
        //alert()->success('Success.','updated sucessfully.')->autoclose(2500);
        return redirect('admin/solutions/category1/subcategory1')->with('success','updated sucessfully.');
    }

}