<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\UserActivity;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;

class UserActivityController extends Controller
{
    //index
    public function listdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
        $list = UserActivity::orderBy('id', 'DESC')->get();
        return view('admin.user_activity_list',compact('list'));
    }

}