<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use App\Models\FollowingServices;
use App\Models\Contactby;
use App\Models\Heardabout;
use App\Models\Country;
use App\Models\UserFeedback;
use App\Models\UserFeedbackFollowingService;
use App\Models\UserFeedbackContactBy;
use App\Models\Solutions;
use App\Models\Solutions1;
use App\Models\Solutions2;
use App\Models\Services;
use App\Models\Services1;
use App\Models\Services2;
use App\Models\Pages;
use App\Models\SubPages;
use App\Models\Quote;
use RealRashid\SweetAlert\Facades\Alert;

class FrontController extends Controller
{
    public function index(){
        $category= Pages::where('page_url','visionandmission')->where('deleted_at',null)->first();
        $category1= Pages::where('page_url','welcometobumilink')->where('deleted_at',null)->first();
        $vission_data = SubPages::where('category_id',@$category->id)->get();
        $welcome_bumilink = SubPages::where('category_id',@$category1->id)->get();
        $services = Services::where('status', '0')->limit(5)->get();
        return view('index',compact('vission_data','category', 'welcome_bumilink', 'category1', 'services'));
    }
    public function getaquote(){
        $country = Country::where('nicename','!=','')->select('nicename')->get();
        return view('getaquote',compact('country'));
    }
    public function about(){
        $category= Pages::where('page_url','visionandmission')->where('deleted_at',null)->first();
        $vission_data = SubPages::where('category_id',@$category->id)->get();
        $category1= Pages::where('page_url','whyusebumilink')->where('deleted_at',null)->first();
        $whyusebumilink = SubPages::where('category_id',@$category1->id)->get();
        return view('about',compact('category', 'vission_data', 'category1', 'whyusebumilink'));
    }public function services(){
        $data=Services::where('deleted_at',null)->where('id','!=','6')->where('status','0')->get();
        return view('services',compact('data'));
    }public function domesticservices(){
        $data =Services::with('getSubCategory.childsubcategory')->where('page_url','domesticservices')->where('deleted_at',null)->first();
        return view('domesticservices',compact('data'));
    }public function international(){
        $data =Services::with('getSubCategory.childsubcategory')->where('page_url','international')->where('deleted_at',null)->first();
        return view('international',compact('data'));
    }public function crossborderexpress(){
        $data =Services::with('getSubCategory.childsubcategory')->where('page_url','crossborderexpress')->where('deleted_at',null)->first();
        return view('crossborderexpress',compact('data'));
    }public function globallogistic(){
        $data =Services::with('getSubCategory.childsubcategory')->where('page_url','globallogistic')->where('deleted_at',null)->first();
        return view('globallogistic',compact('data'));
    }public function custombrokerage(){
        $data =Services::with('getSubCategory.childsubcategory')->where('page_url','custombrokerage')->where('deleted_at',null)->first();
        //dd($data);
        return view('custombrokerage',compact('data'));
    }public function cargoinsurance(){
        $data =Services::with('getSubCategory.childsubcategory')->where('page_url','cargoinsurance')->where('deleted_at',null)->first();
        return view('cargoinsurance',compact('data'));
    }
    public function phpinfo(){ 
        return view('phpinfo');
    }

    /*public function apparel(){ 
        return view('apparel');
    }public function aviation(){ 
        return view('aviation');
    }public function banking(){ 
        return view('banking');
    }public function ecommerce(){ 
        return view('ecommerce');
    }public function fmcg(){ 
        return view('fmcg');
    }public function healthcaremedicallogistics(){
        return view('healthcaremedicallogistics');
    }public function manufacturinglogistics(){
        return view('manufacturinglogistics');
    }public function technology(){
        return view('technology');
    }public function customstaxconsultancy(){
        return view('customstaxconsultancy');
    }public function dedicatedfleetlogistics(){
        return view('dedicatedfleetlogistics');
    }public function supplychainmanagement(){
        return view('supplychainmanagement');
    }public function wms(){
        return view('wms');
    }*/

    public function track(){
        return view('track');
    }public function solutions(){
        $data=Solutions::where('deleted_at',null)->where('status','0')->get();
        return view('solutions',compact('data'));
    }public function singlesolutionsdata(Request $request, $ulrname){
        $data=Solutions::where('page_url',$ulrname)->where('deleted_at',null)->first();
        if(is_object($data)){
            $supplychainmanagement=[]; $dedicatedfleetlogistics=[];$subcategory=[]; 
            $bumilink_transport_cate=[];$customstaxconsultancy=[];$bumilink_transport_subcate=[];$manufacturecate=[];
            $manufacturecate_subcate=[];$ecommerce=[];$fmcg=[]; $healthcare=[]; $manufacture=[]; 
            //start
            if($request->segment(2)== 'ecommerce'){
                $ecommerce=Solutions::with('getSubCategory.childsubcategory')->where('page_url','ecommerce')->where('deleted_at',null)->first();
            }
            //end
            //start
            if($request->segment(2)== 'technology'){
                $subcategory = Solutions1::where('category_id',$data->id)->where('deleted_at',null)->get();
            }
            //end            
            //start
            if($request->segment(2)== 'fmcg'){
                $fmcg=Solutions::with('getSubCategory.childsubcategory')->where('page_url','fmcg')->where('deleted_at',null)->first();
            }
            //end
            //start
            if($request->segment(2)== 'healthcaremedicallogistics'){
                $healthcare=Solutions::with('getSubCategory.childsubcategory')->where('page_url','healthcaremedicallogistics')->where('deleted_at',null)->first();
            }
            //end
            //start
            if($request->segment(2)== 'manufacturinglogistics'){
                $manufacture = Solutions::with('getSubCategory.childsubcategory')->where('page_url','manufacturinglogistics')->where('deleted_at',null)->first();
            }
            //end
            //start
            if($request->segment(2)== 'supplychainmanagement'){      
            $subcategory = Solutions1::where('category_id',$data->id)->where('deleted_at',null)->get();
            $supplychainmanagement = Solutions::with('getSubCategory.childsubcategory')->where('page_url','supplychainmanagement')->where('deleted_at',null)->first();
            }
            //end
            //start
            if($request->segment(2)== 'dedicatedfleetlogistics'){
                $dedicatedfleetlogistics = Solutions::with('getSubCategory.childsubcategory')->where('page_url','dedicatedfleetlogistics')->where('deleted_at',null)->first();
            }
            //end 
            //start
            if($request->segment(2)== 'customstaxconsultancy'){
                $customstaxconsultancy = Solutions::with('getSubCategory.childsubcategory')->where('page_url','customstaxconsultancy')->where('deleted_at',null)->first();
            }
            //end
            //start 
            if($request->segment(2)== 'apparel' || $request->segment(2)=='aviation' || $request->segment(2)=='banking' || $request->segment(2)== 'wms'){
                $subcategory = Solutions1::where('category_id',$data->id)->where('deleted_at',null)->get();
            }
            //end
           return view($ulrname,compact('data', 'subcategory', 'supplychainmanagement', 'bumilink_transport_cate', 'bumilink_transport_subcate','manufacturecate_subcate', 'manufacturecate', 'ecommerce', 'fmcg', 'healthcare', 'manufacture', 'dedicatedfleetlogistics', 'customstaxconsultancy'));
        }
        return redirect('solutions');
    }
    public function contact(){
        \Illuminate\Support\Facades\Artisan::call('storage:link');
        $following_services = FollowingServices::where('status','0')->where('deleted_at',null)->get();
        $contactby = Contactby::where('status','0')->where('deleted_at',null)->get();
        $heredabout = Heardabout::where('status','0')->where('deleted_at',null)->get();
        $country = Country::orderBy('name', 'ASC')->get();
        return view('contact',compact('following_services','contactby', 'heredabout','country'));
    }
    public function savegetaquote(Request $request){
        
        $validated = Validator::make($request->all(),[        
        'myfile' => 'required|max:2048'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput();
        }
        $name = $request->file('myfile')->getClientOriginalName();
        $path = $request->file('myfile')->store('public/images/getaquote');
        //create data
        $quota = new Quote();
        $quota->company_name  = $request->user_companyname;
        $quota->company_email  = $request->user_email;
        $quota->user_name  = $request->user_name;
        $quota->file_name  = $name;
        $quota->details  = json_encode($request->all());
        $quota->quote_type  = 'Unread';
        $quota->file  = explode ("/", $path)[3];
        $quota->save();
        return back()->with('form_success', 'Thank you for contacting us here at Bumilink. We will be in touch soon.');
    } 

    public function SaveFeedback(Request $request){
        //dd($request->all());
         $validated = Validator::make($request->all(),[
        'firstname' => 'required',
        'company' => 'required',
        'title' => 'required',
        'mobile' => 'required',
        'email' => 'required',
        'country' => 'required',
        'address' => 'required',
        'postal' => 'required',
        'city' => 'required',
        'state' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput();
        }
        //create data
        $userFeedback = new UserFeedback();
        $userFeedback->firstname = $request->firstname;
        $userFeedback->lastname  = ($request->lastname)? $request->lastname:'';
        $userFeedback->company_name  = $request->company;
        $userFeedback->company_title  = $request->title;
        $userFeedback->phone  = ($request->phone) ? $request->phone : '';
        $userFeedback->email  = $request->email;
        $userFeedback->country  = $request->country;
        $userFeedback->fax  = ($request->fax) ? $request->fax : '';
        $userFeedback->mobile  = ($request->mobile) ? $request->mobile : '';      
        $userFeedback->address  = ($request->address) ? $request->address : '';
        $userFeedback->postal  = ($request->postal) ? $request->postal : '';
        $userFeedback->city  = ($request->city) ? $request->city : '';
        $userFeedback->state  = ($request->state) ? $request->state : '';    
        $userFeedback->heardabout  = ($request->heardabout) ? $request->heardabout : '';    
        $userFeedback->message  = ($request->message) ? $request->message : '';
        $userFeedback->message_type  = 'Unread';
        $userFeedback->save();
        if(is_countable($request->followingservice) && count($request->followingservice)>0){
            foreach ($request->followingservice as $key => $value) {
             $followingdata = new UserFeedbackFollowingService();
             $followingdata->feedback_id = $userFeedback->id;
             $followingdata->following_services_id = $value;
             $followingdata->save();
            }      
         }
        if(is_countable($request->contactby) && count($request->contactby) >0){
            foreach ($request->contactby as $key => $value) {
             $contactby = new UserFeedbackContactBy();
             $contactby->feedback_id = $userFeedback->id;
             $contactby->contact_by_id = $value;
             $contactby->save();
            }
         }
       //alert()->success('success.','Thanks for your feedback it is much appreciated')->autoclose(3500);
       // return redirect('contact')->with('success.','Thanks for your feedback it is much appreciated');
       return back()->with('form_success', 'Thanks for your feedback it is much appreciated');
    } 
}