<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\UserAccessLevel;
use App\Models\MenuLists;
use App\Models\Services;

class ServicesPolicies
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any enquiries.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the enquiry.
     *
     * @param  \App\User  $user
     * @param  \App\Enquiry  $enquiry
     * @return mixed
     */
    public function view(User $user, Services $services)
    {
        if($user->role!=1){
            $getMenu = MenuLists::where('menu_url','enquiry')->first()->id;
              $user_permi = UserDesigPermission::where('department_id',$user->role)->where('menu_id',$getMenu)->first();
              if(!isset($user_permi->menu_view)){
                 abort(403,'You do not have permission to perform this task.');
              }             
              if($user_permi->menu_view!=1){
                 abort(403,'You do not have permission to perform this task.');
              }
            }
           return true;
    }

    /**
     * Determine whether the user can create enquiries.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if($user->role!=1){
            $getMenu = MenuLists::where('menu_url','enquiry')->first()->id;
              $user_permi = UserDesigPermission::where('department_id',$user->role)->where('menu_id',$getMenu)->first();
              if(!isset($user_permi->menu_create)){
                 abort(403,'You do not have permission to perform this task.');
              }
              if($user_permi->menu_create!=1){
                 abort(403,'You do not have permission to perform this task.');
              }
            }
           return true;
    }

    /**
     * Determine whether the user can update the enquiry.
     *
     * @param  \App\User  $user
     * @param  \App\Enquiry  $enquiry
     * @return mixed
     */
    public function update(User $user, Enquiry $enquiry)
    {
       if($user->role!=1){
            $getMenu = MenuLists::where('menu_url','enquiry')->first()->id;
              $user_permi = UserDesigPermission::where('department_id',$user->role)->where('menu_id',$getMenu)->first();
              if(!isset($user_permi->menu_edit)){
                 abort(403,'You do not have permission to perform this task.');
              }             
              if($user_permi->menu_edit!=1){
                 abort(403,'You do not have permission to perform this task.');
              }
            }
           return true;
    }

    /**
     * Determine whether the user can delete the enquiry.
     *
     * @param  \App\User  $user
     * @param  \App\Enquiry  $enquiry
     * @return mixed
     */
    public function delete(User $user, Enquiry $enquiry)
    {
        if($user->role!=1){
            $getMenu = MenuLists::where('menu_url','enquiry')->first()->id;
              $user_permi = UserDesigPermission::where('department_id',$user->role)->where('menu_id',$getMenu)->first();
              if(!isset($user_permi->menu_delete)){
                 abort(403,'You do not have permission to perform this task.');
              }             
              if($user_permi->menu_delete!=1){
                 abort(403,'You do not have permission to perform this task.');
              }
            }
           return true;
    }

    /**
     * Determine whether the user can restore the enquiry.
     *
     * @param  \App\User  $user
     * @param  \App\Enquiry  $enquiry
     * @return mixed
     */
    public function restore(User $user, Enquiry $enquiry)
    {
        return true;
    }

    /**
     * Determine whether the user can permanently delete the enquiry.
     *
     * @param  \App\User  $user
     * @param  \App\Enquiry  $enquiry
     * @return mixed
     */
    public function forceDelete(User $user, Enquiry $enquiry)
    {
        return true;
    }
}
