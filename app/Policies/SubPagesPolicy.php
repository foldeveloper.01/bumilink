<?php

namespace App\Policies;

use App\Models\OurBusiness;
use App\Models\Admin;
use App\Models\MenuLists;
use App\Models\UserAccessLevel;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubPagesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Services  $services
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(Admin $user)
    {
        if($user->admin_type_id!=1){
            $getMenu = MenuLists::where('menu_url','subpages')->first()->id;
              $user_permi = UserAccessLevel::where('admin_type_id',$user->admin_type_id)->where('menu_id',$getMenu)->first();
              if(!isset($user_permi)){
                 abort(403,'You do not have permission to perform this task.');
              }
              if($user_permi->permission_view!=1){
                 abort(403,'You do not have permission to perform this task.');
              }
        }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(Admin $user)
    {
        if($user->admin_type_id!=1){
            $getMenu = MenuLists::where('menu_url','subpages')->first()->id;
              $user_permi = UserAccessLevel::where('admin_type_id',$user->admin_type_id)->where('menu_id',$getMenu)->first();
              if(!isset($user_permi)){
                 abort(403,'You do not have permission to perform this task.');
              }
              if($user_permi->permission_create!=1){
                 abort(403,'You do not have permission to perform this task.');
              }
        }
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Services  $services
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(Admin $user)
    {
        if($user->admin_type_id!=1){
            $getMenu = MenuLists::where('menu_url','subpages')->first()->id;
              $user_permi = UserAccessLevel::where('admin_type_id',$user->admin_type_id)->where('menu_id',$getMenu)->first();
              if(!isset($user_permi)){
                 abort(403,'You do not have permission to perform this task.');
              }
              if($user_permi->permission_update!=1){
                 abort(403,'You do not have permission to perform this task.');
              }
            }
        return true;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Services  $services
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(Admin $user)
    {
            if($user->admin_type_id!=1){
            $getMenu = MenuLists::where('menu_url','subpages')->first()->id;
              $user_permi = UserAccessLevel::where('admin_type_id',$user->admin_type_id)->where('menu_id',$getMenu)->first();
              if(!isset($user_permi)){
                 abort(403,'You do not have permission to perform this task.');
              }
              if($user_permi->permission_delete!=1){
                 abort(403,'You do not have permission to perform this task.');
              }
            }
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Services  $services
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(Admin $user, Services $services)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Services  $services
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(Admin $user, Services $services)
    {
        //
    }
}
