<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Solutions;
use App\Models\Solutions1;
use App\Traits\ModelEventLogger;

class Solutions2 extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'solutions2';
    protected $fillable = [
        'category_id', 'subcategory_id', 'name', 'descriptions', 'position', 'image', 'image_name', 'status', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function getCategory($id)
    {
        return Solutions::findOrFail($id);
    }

    public function getSubCategory($id)
    {
        return Solutions1::findOrFail($id);
    }
   
}
