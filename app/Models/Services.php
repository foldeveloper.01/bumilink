<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Services1;
use App\Traits\ModelEventLogger;

class Services extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'services';
    protected $fillable = [
        'name', 'descriptions', 'class_name', 'page_url', 'image', 'image_name', 'status', 'created_at', 'updated_at', 'deleted_at'
    ];
    public function getSubCategory()
    {
        return $this->hasMany(Services1::class, 'category_id')->where('status','==','1');
    }
}
