<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class UserFeedbackFollowingService extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'feedback_following_service';
    protected $fillable = [
        'feedback_id', 'following_services_id', 'created_at', 'updated_at'
    ];

    public function getFollowingService($id)
    {
      return FollowingServices::findOrFail($id);
    }
   
}
