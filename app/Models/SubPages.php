<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Pages;
use App\Traits\ModelEventLogger;

class SubPages extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'sub_pages';
    protected $fillable = [
        'name', 'descriptions', 'category_id', 'class_name','image', 'image_name', 'position', 'status', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function getCategory($value='')
    {
        return Pages::findOrFail($value);
    }
   
}
