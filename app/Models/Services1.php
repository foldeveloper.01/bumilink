<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Services1;
use App\Models\Services2;
use App\Traits\ModelEventLogger;

class Services1 extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'services1';
    protected $fillable = [
        'category_id', 'name', 'text', 'descriptions', 'position', 'image', 'image_name', 'status', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function getCategory($id)
    {
        return Services::findOrFail($id);
    }
    public function child_subcategory()
    {
        return $this->hasMany(Services2::class, 'subcategory_id');
    }
    public function childsubcategory()
    {
        return $this->child_subcategory()->where('status','0');
    }
   
}
