<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\UserFeedbackFollowingService;
use App\Models\UserFeedbackContactBy;

class UserFeedback extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'feedback';
    protected $fillable = [
        'firstname', 'lastname', 'company_name', 'company_title', 'phone', 'fax', 'mobile',
        'email', 'address', 'postal', 'message_type', 'heardabout', 'city', 'state', 'country', 'message', 'created_at', 'updated_at'
    ];

    public function getFeedbackFollowings($id)
    {
      return UserFeedbackFollowingService::where('feedback_id',$id)->get();
    }

    public function getFeedbackContactBy($id)
    {
      return UserFeedbackContactBy::where('feedback_id',$id)->get();
    }
   
}
