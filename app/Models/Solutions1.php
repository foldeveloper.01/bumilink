<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Solutions;
use App\Models\Solutions2;
use App\Traits\ModelEventLogger;

class Solutions1 extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'solutions1';
    protected $fillable = [
        'category_id', 'name', 'descriptions', 'position', 'image', 'image_name', 'status', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function getCategory($id)
    {
        return Solutions::findOrFail($id);
    }
    public function child_subcategory()
    {
        return $this->hasMany(Solutions2::class, 'subcategory_id');
    }
    public function childsubcategory()
    {
        return $this->child_subcategory()->where('status','0');
    }
   
}
