<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\UserFeedbackFollowingService;

class UserFeedbackContactBy extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'feedback_contact_by';
    protected $fillable = [
        'feedback_id', 'contact_by_id', 'created_at', 'updated_at'
    ];

    public function getFeedbackContactBy($id)
    {
      return Contactby::findOrFail($id);
    }


   
}
