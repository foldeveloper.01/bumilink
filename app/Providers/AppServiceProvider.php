<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\Usefullink;
use App\Models\Sociallink;
use App\Models\Settings;
use App\Models\OurTechnology;
use App\Models\OurBusiness;
use PDO;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //header and footer values get
         View::composer('*', function ($view) {
          $sitesetting = Settings::first();
          $usefullink = Usefullink::where('status','0')->where('deleted_at',null)->get();
          $sociallink = Sociallink::orderBy('position', 'asc')->where('status','0')->where('deleted_at',null)->get();
          $ourtechnology = OurTechnology::where('status','0')->where('deleted_at',null)->get();
          $ourbuiness = OurBusiness::where('status','0')->where('deleted_at',null)->get();
          $view->with('usefullink',$usefullink)
               ->with('setting',$sitesetting)
               ->with('sociallink',$sociallink)
               ->with('ourbuiness',$ourbuiness)
               ->with('ourtechnology',$ourtechnology);
         });
    }
}
