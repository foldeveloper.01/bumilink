<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\Services;
use App\Models\Services1;
use App\Models\Services2;
use App\Policies\ServicesPolicy;
use App\Policies\ServicesPolicy1;
use App\Policies\ServicesPolicy2;
use App\Models\Solutions;
use App\Models\Solutions1;
use App\Models\Solutions2;
use App\Models\OurTechnology;
use App\Models\OurBusiness;
use App\Models\Pages;
use App\Models\SubPages;
use App\Models\UserFeedback;
use App\Models\Quote;
use App\Models\FollowingServices;
use App\Models\Contactby;
use App\Models\Heardabout;
use App\Models\Sociallink;
use App\Models\Usefullink;
use App\Models\Settings;
use App\Models\Admin;
use App\Models\UserActivity;
use App\Models\SmtpSettings;
use App\Models\UserLoginHistroy;
use App\Policies\SolutionsPolicy;
use App\Policies\SolutionsPolicy1;
use App\Policies\SolutionsPolicy2;
use App\Policies\OurTechnologyPolicy;
use App\Policies\OurBusinessPolicy;
use App\Policies\PagesPolicy;
use App\Policies\SubPagesPolicy;
use App\Policies\UserFeedbackPolicy;
use App\Policies\QuotePolicy;
use App\Policies\ContactbyPolicy;
use App\Policies\HeardaboutPolicy;
use App\Policies\SociallinkPolicy;
use App\Policies\UsefullinkPolicy;
use App\Policies\SettingsPolicy;
use App\Policies\AdminPolicy;
use App\Policies\UserActivityPolicy;
use App\Policies\UserLoginHistroyPolicy;
use App\Policies\FollowingServicesPolicy;
use App\Policies\SmtpSettingsPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
         Services::class => ServicesPolicy::class,
         Services1::class => ServicesPolicy1::class,
         Services2::class => ServicesPolicy2::class,
         Solutions::class => SolutionsPolicy::class,
         Solutions1::class => SolutionsPolicy1::class,
         Solutions2::class => SolutionsPolicy2::class,
         OurTechnology::class => OurTechnologyPolicy::class,
         OurBusiness::class => OurBusinessPolicy::class,
         Pages::class => PagesPolicy::class,
         SubPages::class => SubPagesPolicy::class,
         UserFeedback::class => UserFeedbackPolicy::class,
         Quote::class => QuotePolicy::class,
         FollowingServices::class => FollowingServicesPolicy::class,
         Contactby::class => ContactbyPolicy::class,
         Heardabout::class => HeardaboutPolicy::class,
         Sociallink::class => SociallinkPolicy::class,
         Usefullink::class => UsefullinkPolicy::class,
         Settings::class => SettingsPolicy::class,
         Admin::class => AdminPolicy::class,
         UserActivity::class => UserActivityPolicy::class,
         UserLoginHistroy::class => UserLoginHistroyPolicy::class,
         SmtpSettings::class => SmtpSettingsPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
