<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminAuthController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\SettingsController;
use App\Http\Controllers\Admin\FollowingServicesController;
use App\Http\Controllers\Admin\ContactbyController;
use App\Http\Controllers\Admin\HeardaboutController;
use App\Http\Controllers\Admin\SocialLinkController;
use App\Http\Controllers\Admin\UsefullinkController;
use App\Http\Controllers\Admin\SolutionsController;
use App\Http\Controllers\Admin\ServicesController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\Admin\FeedbackController;
use App\Http\Controllers\Admin\QuoteController;
use App\Http\Controllers\Admin\OurTechnologyController;
use App\Http\Controllers\Admin\OurBusinessController;
use App\Http\Controllers\Admin\PagesController;
use App\Http\Controllers\Admin\SubPagesController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UserLoginHistroyController;
use App\Http\Controllers\Admin\UserActivityController;
use App\Http\Controllers\Admin\AccessLevelController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    //front end
    Route::get('/', [FrontController::class, 'index']);
    Route::get('contact', [FrontController::class, 'contact']);
    Route::get('about', [FrontController::class, 'about']);
    Route::get('services', [FrontController::class, 'services']);
    Route::get('getaquote', [FrontController::class, 'getaquote']);
    Route::post('getaquote', [FrontController::class, 'savegetaquote']);
    Route::get('domesticservices', [FrontController::class, 'domesticservices']);
    Route::get('international', [FrontController::class, 'international']);
    Route::get('crossborderexpress', [FrontController::class, 'crossborderexpress']);
    Route::get('globallogistic', [FrontController::class, 'globallogistic']);
    Route::get('custombrokerage', [FrontController::class, 'custombrokerage']);
    Route::get('cargoinsurance', [FrontController::class, 'cargoinsurance']);
    Route::get('phpinfo', [FrontController::class, 'phpinfo']);
    //Clear Cache facade value:
    Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
    });

    //Reoptimized class loader:
    Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
    });

    //Route cache:
    Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
    });

    //Clear Route cache:
    Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
    });

    //Clear View cache:
    Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
    });

    //Clear Config cache:
    Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
    });

    Route::get('generate', function (){
    \Illuminate\Support\Facades\Artisan::call('storage:link');
    echo 'ok';
    });

   /* Route::get('apparel', [FrontController::class, 'apparel']);
    Route::get('aviation', [FrontController::class, 'aviation']);
    Route::get('banking', [FrontController::class, 'banking']);
    Route::get('ecommerce', [FrontController::class, 'ecommerce']);
    Route::get('fmcg', [FrontController::class, 'fmcg']);
    Route::get('healthcaremedicallogistics', [FrontController::class, 'healthcaremedicallogistics']);
    Route::get('manufacturinglogistics', [FrontController::class, 'manufacturinglogistics']);
    Route::get('technology', [FrontController::class, 'technology']);
    Route::get('customstaxconsultancy', [FrontController::class, 'customstaxconsultancy']);
    Route::get('dedicatedfleetlogistics', [FrontController::class, 'dedicatedfleetlogistics']);
    Route::get('supplychainmanagement', [FrontController::class, 'supplychainmanagement']);
    Route::get('wms', [FrontController::class, 'wms']);*/
    Route::get('track', [FrontController::class, 'track']);
    Route::get('solutions', [FrontController::class, 'solutions']);
    Route::get('solutions/{name}', [FrontController::class, 'singlesolutionsdata']);
    Route::post('savefeedback', [FrontController::class, 'SaveFeedback']);
    //end front end
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/login', [AdminAuthController::class, 'getLogin'])->name('adminLogin');
    Route::post('/login', [AdminAuthController::class, 'postLogin'])->name('adminLoginPost');
    Route::get('/registerr', [AdminAuthController::class, 'register'])->name('registerr');
    Route::get('/logout', [AdminAuthController::class, 'getLogout']);
    Route::get('/forgotpassword', [AdminAuthController::class, 'forgotPassword'])->name('forgotpassword');
    Route::post('/forgotpassword', [AdminAuthController::class, 'gentrateUserPassword'])->name('forgotpassword');

    Route::group(['middleware' => 'adminauth'], function () {
        //dashboard
        Route::get('/', [DashboardController::class, 'dashboard'])->name('adminDashboard');
        //user list
        Route::get('/admins', [UserController::class, 'userslist']);
        Route::get('/admins/create', [UserController::class, 'createuser']);
        Route::post('/admins/save', [UserController::class, 'saveuser']);
        Route::get('/admins/update/{id}', [UserController::class, 'updateuser']);
        Route::post('/admins/update/{id}', [UserController::class, 'edituser']);
        Route::any('/admins/delete/{id}', [UserController::class, 'deleteuser']);
        Route::get('/admins/view/{id}', [UserController::class, 'viewuser']);
        Route::any('/admins/emailSend/{id}', [UserController::class, 'ResetUserPasswordToMail']);
        //settings
        Route::get('/settings', [SettingsController::class, 'settings']);
        Route::post('/settings', [SettingsController::class, 'savesettings']);
        Route::get('/smtpsettings', [SettingsController::class, 'smtpsettings']);
        Route::post('/smtpsettings', [SettingsController::class, 'savesmtpsettings']);
        //following services
        Route::get('followingservices', [FollowingServicesController::class, 'listdata']);
        Route::get('followingservices/create', [FollowingServicesController::class, 'createdata']);
        Route::post('followingservices/save', [FollowingServicesController::class, 'savedata']);
        Route::get('followingservices/update/{id}', [FollowingServicesController::class, 'viewdata']);
        Route::post('followingservices/update/{id}', [FollowingServicesController::class, 'updatedata']);
        Route::any('followingservice/delete/{id}', [FollowingServicesController::class, 'deletedata']);
        //contactBy
        Route::get('contactby', [ContactbyController::class, 'listdata']);
        Route::get('contactby/create', [ContactbyController::class, 'createdata']);
        Route::post('contactby/save', [ContactbyController::class, 'savedata']);
        Route::get('/contactby/update/{id}', [ContactbyController::class, 'viewdata']);
        Route::post('/contactby/update/{id}', [ContactbyController::class, 'updatedata']);
        Route::any('/contactby/delete/{id}', [ContactbyController::class, 'deletedata']);
        //contactBy
        Route::get('/heardabout', [HeardaboutController::class, 'listdata']);
        Route::get('/heardabout/create', [HeardaboutController::class, 'createdata']);
        Route::post('/heardabout/save', [HeardaboutController::class, 'savedata']);
        Route::get('/heardabout/update/{id}', [HeardaboutController::class, 'viewdata']);
        Route::post('/heardabout/update/{id}', [HeardaboutController::class, 'updatedata']);
        Route::any('/heardabout/delete/{id}', [HeardaboutController::class, 'deletedata']);
        //sociallink
        Route::get('/sociallink', [SocialLinkController::class, 'listdata']);
        Route::get('/sociallink/create', [SocialLinkController::class, 'createdata']);
        Route::post('/sociallink/save', [SocialLinkController::class, 'savedata']);
        Route::get('/sociallink/update/{id}', [SocialLinkController::class, 'viewdata']);
        Route::post('/sociallink/update/{id}', [SocialLinkController::class, 'updatedata']);
        Route::any('/sociallink/delete/{id}', [SocialLinkController::class, 'deletedata']);
        //useful links
        Route::get('/usefullink', [UsefullinkController::class, 'listdata']);
        Route::get('/usefullink/create', [UsefullinkController::class, 'createdata']);
        Route::post('/usefullink/save', [UsefullinkController::class, 'savedata']);
        Route::get('/usefullink/update/{id}', [UsefullinkController::class, 'viewdata']);
        Route::post('/usefullink/update/{id}', [UsefullinkController::class, 'updatedata']);
        Route::any('/usefullink/delete/{id}', [UsefullinkController::class, 'deletedata']);
        //solutions
       /* Route::get('/solutions', [SolutionsController::class, 'listdata']);
        Route::get('/createsolutions', [SolutionsController::class, 'createdata']);
        Route::post('/savesolutions', [SolutionsController::class, 'savedata']);
        Route::get('/updateusolutions/{id}', [SolutionsController::class, 'viewdata']);
        Route::post('/updateusolutions/{id}', [SolutionsController::class, 'updatedata']);

        Route::get('/solutions1', [SolutionsController::class, 'listdata1']);
        Route::get('/createsolutions1', [SolutionsController::class, 'createdata1']);
        Route::post('/savesolutions1', [SolutionsController::class, 'savedata1']);
        Route::get('/updateusolutions1/{id}', [SolutionsController::class, 'viewdata1']);
        Route::post('/updateusolutions1/{id}', [SolutionsController::class, 'updatedata1']);

        Route::get('/solutions2', [SolutionsController::class, 'listdata2']);
        Route::get('/createsolutions2', [SolutionsController::class, 'createdata2']);
        Route::post('/savesolutions2', [SolutionsController::class, 'savedata2']);
        Route::get('/updateusolutions2/{id}', [SolutionsController::class, 'viewdata2']);
        Route::post('/updateusolutions2/{id}', [SolutionsController::class, 'updatedata2']);
        Route::get('/getsolutionsubcategory/{id}', [SolutionsController::class, 'getsolutionssubcategory']);*/
        Route::get('/solutions', [SolutionsController::class, 'listdata']);
        Route::get('/solutions/create', [SolutionsController::class, 'createdata']);
        Route::post('/solutions/save', [SolutionsController::class, 'savedata']);
        Route::get('/solutions/update/{id}', [SolutionsController::class, 'viewdata']);
        Route::post('/solutions/update/{id}', [SolutionsController::class, 'updatedata']);

        Route::get('/solutions/category1', [SolutionsController::class, 'listdata1']);
        Route::get('/solutions/category1/create', [SolutionsController::class, 'createdata1']);
        Route::post('/solutions/category1/save', [SolutionsController::class, 'savedata1']);
        Route::get('/solutions/category1/update/{id}', [SolutionsController::class, 'viewdata1']);
        Route::post('/solutions/category1/update/{id}', [SolutionsController::class, 'updatedata1']);

        Route::get('/solutions/category1/subcategory1', [SolutionsController::class, 'listdata2']);
        Route::get('/solutions/category1/subcategory1/create', [SolutionsController::class, 'createdata2']);
        Route::post('/solutions/category1/subcategory1/save', [SolutionsController::class, 'savedata2']);
        Route::get('/solutions/category1/subcategory1/update/{id}', [SolutionsController::class, 'viewdata2']);
        Route::post('/solutions/category1/subcategory1/update/{id}', [SolutionsController::class, 'updatedata2']);
        Route::get('/solutions/category/subcategory/getsolutionsubcategory/{id}', [SolutionsController::class, 'getsolutionssubcategory']);
        //user menu access leval
        Route::get('/accesslevel', [AccessLevelController::class, 'listdata']);
        Route::post('/accesslevel/useraccesslevel', [AccessLevelController::class, 'savedata']);
        //end

        //services
        Route::get('/services', [ServicesController::class, 'listdata']);
        Route::get('/services/create', [ServicesController::class, 'createdata']);
        Route::post('/services/save', [ServicesController::class, 'savedata']);
        Route::get('/services/update/{id}', [ServicesController::class, 'viewdata']);
        Route::post('/services/update/{id}', [ServicesController::class, 'updatedata']);

        Route::get('/services/category', [ServicesController::class, 'listdata1']);
        Route::get('/services/category/create', [ServicesController::class, 'createdata1']);
        Route::post('/services/category/save', [ServicesController::class, 'savedata1']);
        Route::get('/services/category/update/{id}', [ServicesController::class, 'viewdata1']);
        Route::post('/services/category/update/{id}', [ServicesController::class, 'updatedata1']);

        Route::get('/services/category/subcategory', [ServicesController::class, 'listdata2']);
        Route::get('/services/category/subcategory/create', [ServicesController::class, 'createdata2']);
        Route::post('/services/category/subcategory/save', [ServicesController::class, 'savedata2']);
        Route::get('/services/category/subcategory/update/{id}', [ServicesController::class, 'viewdata2']);
        Route::post('/services/category/subcategory/update/{id}', [ServicesController::class, 'updatedata2']);
        Route::get('/services/category/subcategory/getservicessubcategory/{id}', [ServicesController::class, 'getservicessubcategory']);
        //user login histroy
        Route::get('/loginhistroy', [UserLoginHistroyController::class, 'listdata']);
        //user login histroy
        Route::get('/useractivity', [UserActivityController::class, 'listdata']);
        //user feedback
        Route::get('/userfeedback', [FeedbackController::class, 'listdata']);
        Route::get('/userfeedbackview/{id}', [FeedbackController::class, 'viewdata']);
        //user feedback
        Route::get('/quote', [QuoteController::class, 'listdata']);
        Route::get('/quoteview/{id}', [QuoteController::class, 'viewdata']);
        //Our Technology
        Route::get('/ourtechnology', [OurTechnologyController::class, 'listdata']);
        Route::get('/ourtechnology/create', [OurTechnologyController::class, 'createdata']);
        Route::post('/ourtechnology/save', [OurTechnologyController::class, 'savedata']);
        Route::get('/ourtechnology/update/{id}', [OurTechnologyController::class, 'viewdata']);
        Route::post('/ourtechnology/update/{id}', [OurTechnologyController::class, 'updatedata']);
        //our business
        Route::get('ourbusiness', [OurBusinessController::class, 'listdata']);
        Route::get('ourbusiness/create', [OurBusinessController::class, 'createdata']);
        Route::post('ourbusiness/save', [OurBusinessController::class, 'savedata']);
        Route::get('/ourbusiness/update/{id}', [OurBusinessController::class, 'viewdata']);
        Route::post('/ourbusiness/update/{id}', [OurBusinessController::class, 'updatedata']);
        //Category
        Route::get('pages', [PagesController::class, 'listdata']);
        Route::get('pages/create', [PagesController::class, 'createdata']);
        Route::post('pages/save', [PagesController::class, 'savedata']);
        Route::get('pages/update/{id}', [PagesController::class, 'viewdata']);
        Route::post('pages/update/{id}', [PagesController::class, 'updatedata']);
        //sub Category
        Route::get('subpages', [SubPagesController::class, 'listdata']);
        Route::get('subpages/create', [SubPagesController::class, 'createdata']);
        Route::post('subpages/save', [SubPagesController::class, 'savedata']);
        Route::get('subpages/update/{id}', [SubPagesController::class, 'viewdata']);
        Route::post('subpages/update/{id}', [SubPagesController::class, 'updatedata']);

    });
});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
