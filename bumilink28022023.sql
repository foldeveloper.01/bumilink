-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2023 at 10:55 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bumilink`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `number` varchar(15) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `admin_type_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `remember_token` varchar(100) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `firstname`, `lastname`, `email`, `number`, `email_verified_at`, `password`, `admin_type_id`, `image`, `status`, `created_at`, `updated_at`, `remember_token`, `deleted_at`) VALUES
(1, 'Admin', NULL, 'admin@gmail.com', NULL, NULL, '$2a$12$xqW0YZ.AP5dhuOOmAOGliuMpVbHxxxkz2SW4DmORAjo50Jhhv8wW2', 1, NULL, 0, '2023-02-27 17:43:52', '2023-02-28 01:45:05', NULL, NULL),
(2, 'sample1', NULL, 'sample1@gmail.com', NULL, NULL, '$2y$10$GwhiQUDiDxmu9NWBFMDwQubdfWSiXKCg3251T18ggXp4M8QmLEqDm', 3, NULL, 0, '2023-02-28 01:26:07', '2023-02-28 01:26:25', NULL, '2023-02-28 09:26:25');

-- --------------------------------------------------------

--
-- Table structure for table `admin_types`
--

CREATE TABLE `admin_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_types`
--

INSERT INTO `admin_types` (`id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Superadmin', '0', '2023-02-17 05:06:59', '2023-02-24 04:52:47'),
(2, 'Admin', '0', '2023-02-24 04:52:14', '2023-02-24 04:52:14'),
(3, 'Developer', '0', '2023-02-27 02:43:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contact_by`
--

CREATE TABLE `contact_by` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `contact_by`
--

INSERT INTO `contact_by` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Email', 0, '2023-02-21 01:07:09', '2023-02-21 23:36:45', NULL),
(2, 'Phone', 0, '2023-02-21 01:07:17', '2023-02-21 01:07:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `firstname` varchar(200) DEFAULT NULL,
  `lastname` varchar(200) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_title` varchar(255) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `postal` varchar(30) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `heardabout` varchar(255) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `message_type` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feedback_contact_by`
--

CREATE TABLE `feedback_contact_by` (
  `id` int(11) NOT NULL,
  `feedback_id` int(11) DEFAULT NULL,
  `contact_by_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feedback_following_service`
--

CREATE TABLE `feedback_following_service` (
  `id` int(11) NOT NULL,
  `feedback_id` int(11) DEFAULT NULL,
  `following_services_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `following_services`
--

CREATE TABLE `following_services` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `following_services`
--

INSERT INTO `following_services` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Global Logistics Services', 0, '2023-02-20 17:28:20', '2023-02-21 00:50:15', NULL),
(3, 'Air Freight Services', 0, '2023-02-20 17:53:56', '2023-02-21 00:50:27', NULL),
(4, 'Domestic Services', 0, '2023-02-20 17:54:11', '2023-02-21 00:50:40', NULL),
(5, 'Warehousing & Distribution', 0, '2023-02-21 00:50:52', '2023-02-22 01:43:33', NULL),
(6, 'Ground Shipping Services/ Transportation', 0, '2023-02-21 00:51:02', '2023-02-21 00:51:02', NULL),
(7, 'Third Party Logistics', 0, '2023-02-21 00:51:13', '2023-02-21 00:51:13', NULL),
(8, 'Ocean Freight Services', 0, '2023-02-21 00:51:21', '2023-02-21 00:51:21', NULL),
(9, 'Customs Brokerage', 0, '2023-02-21 00:51:38', '2023-02-21 23:42:22', NULL),
(10, 'E-Commerce Solutions', 0, '2023-02-21 00:51:49', '2023-02-21 00:51:49', NULL),
(11, 'Technology Support', 0, '2023-02-21 00:52:03', '2023-02-21 00:52:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `heard_about`
--

CREATE TABLE `heard_about` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `heard_about`
--

INSERT INTO `heard_about` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Newspapers', 0, '2023-02-21 01:12:55', '2023-02-21 01:12:55', NULL),
(2, 'Website', 0, '2023-02-21 01:13:03', '2023-02-21 01:13:03', NULL),
(3, 'Advertisement', 0, '2023-02-21 01:13:19', '2023-02-21 01:13:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_admin_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2023_02_17_032028_admin_types', 1);

-- --------------------------------------------------------

--
-- Table structure for table `our_business`
--

CREATE TABLE `our_business` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `our_business`
--

INSERT INTO `our_business` (`id`, `title`, `description`, `class_name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Operational Excellence', '<p><font color=\"#202124\" face=\"consolas, lucida console, courier new, monospace\"><span style=\"font-size: 12px; white-space: pre-wrap;\">BumiLink’s operational standards have been honed in demanding environments as diverse as Formula 1,   Manufacturing, Grocery Consumer Supply Chains, Credit card Distribution, Industrial Logistics, Oil &amp; Gas, Aviation &amp; Aerospace, Publishing and Fulfillment. We apply our learning across all our sectors, and we are building and rolling out defined processes and best practice to ensure that every customer, project and site will benefit.</span></font><br></p>', 'quote-icon flaticon-warehouse', 0, '2023-02-23 23:34:32', '2023-02-23 23:34:32', NULL),
(2, 'Customer Relations', 'Understanding a company\'s requirements is the first step in creating a tailor-made solution. We start by listening and challenging. We require all our staff to understand and develop their relationship with customers whether operational, administrative or commercial. Our management structure is aligned to supporting and adding to those relationships.\r\n						<br><br>Throughout the life of a relationship from project phase, to start up, to renewal, we seek to perform, to improve, to anticipate and to innovate in line with our customers\' needs.', 'quote-icon flaticon-hand-shake-1', 0, '2023-02-23 23:35:58', '2023-02-23 23:35:58', NULL),
(3, 'Product  Leadership', '<ul style=\"list-style-type:square;\">\r\n								<li style=\"padding-bottom: 3%;\">✔ BumiLink is a market leader in fully comprehensive integrated total logistics software. With our own in-house development we are able to customize at any level and at any point.</li><li style=\"padding-bottom: 3%;\">✔ BumiLink is the first to implement a fully integrated online grocery corporate &amp; consumer supply chain software solution coupled with innovative forward logistics.</li>\r\n							<li style=\"padding-bottom: 3%;\">✔ BumiLink commitment to product leadership enables us to anticipate the future requirements of our customers, harness new technologies and respond to change.</li><br>\r\n						   </ul>', 'quote-icon flaticon-delivery-box', 0, '2023-02-23 23:37:21', '2023-02-23 23:50:41', NULL),
(4, 'Value', 'BumiLink builds all-round value for its customers by tailoring solutions to the precise needs of the customer and by our ability to optimize the performance and productivity of each solution. \r\n							<br><br>BumiLink offers long-term price differentiation by timely investment in relevant technologies and new product development and by investing directly in our customers\' goals.', 'quote-icon flaticon-verified', 0, '2023-02-23 23:38:34', '2023-02-23 23:38:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `our_technology`
--

CREATE TABLE `our_technology` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `our_technology`
--

INSERT INTO `our_technology` (`id`, `title`, `description`, `class_name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Logistics & Warehouse Management Software', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 15px; text-align: justify;\">BLink warehouse management system transforms warehouse operations into fully integrated logistics and fulfillment business. By enabling a company to take orders from multiple sources, process them in real time with mobile and automating the shipping and billing processes, organizational efficiency will improve, with positive effect on the bottom line. BLink is a comprehensive, easy to use software package for managing 3rd Party Warehouse operations. BLink features the ability to manage multiple warehouses, customers, products, rates, carriers, consignees, inbound receipts, bills of landing, and much more. BLink also produces reports of inventory, activity, and a wide variety of management and customer and product specific information.</span><br></p>', 'flaticon-cloud-computing-3', 0, '2023-02-23 19:58:54', '2023-02-23 20:27:32', NULL),
(2, 'Ongoing Support', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 15px; text-align: justify;\">Having a successful software solution goes beyond installation and implementation. BLink offers full product support to help you troubleshoot your system, implement new features, and identify potential areas of product improvement. Whether you’re speaking to a support consultant over the phone or conversing via email, our staff is trained to look at the whole picture, not merely the question being asked. This helps ensure that your system stays optimized for your business and runs smoothly and continually.</span><br></p>', 'flaticon-support', 0, '2023-02-23 20:23:14', '2023-02-23 20:28:36', NULL),
(3, 'By Phone and Online', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 15px; text-align: justify;\">Our support organization is always available to you during normal business hours, but we also offer an optional support plan to assist after-hours and via a paging service. For those who prefer a more \"self-service\" option, our support website provides timely articles and product updates available only to current BLink support customers.</span><br></p>', 'flaticon-chat', 0, '2023-02-23 20:29:04', '2023-02-23 20:29:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `descriptions` text DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `image` text DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `descriptions`, `image_name`, `image`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Welcome To Bumilink', '', 'home-image01.png', 'Tgt8nkppm9qSGg89Pju1r12X5wcEQ5ikSiZbULv5.png', 0, '2023-02-24 09:30:10', '2023-02-24 01:45:18', NULL),
(2, 'Vision and Mission', '', 'home-vision.png', 'oBnYe7t4QKyR6HFPV8LwAeZB9a8GKOTFmGe3B43O.png', 0, '2023-02-24 09:46:21', '2023-02-24 01:46:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quote`
--

CREATE TABLE `quote` (
  `id` int(11) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_email` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `quote_type` varchar(10) DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `site_name` varchar(255) DEFAULT NULL,
  `site_link` text DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `meta_keywords` text DEFAULT NULL,
  `meta_descriptions` text DEFAULT NULL,
  `header` text DEFAULT NULL,
  `logo` text DEFAULT NULL,
  `favicon` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `site_name`, `site_link`, `email`, `phone`, `fax`, `address`, `meta_keywords`, `meta_descriptions`, `header`, `logo`, `favicon`, `created_at`, `updated_at`) VALUES
(4, 'Bumilink Global Logistic Sdn Bhd', 'sitetwo', 'cs@bumilink.com.my', '+ 60351225355', '+ 60351225355', 'No. 40 & 40A, Jalan Tabla 33/21,\r\nShah Alam Technology Park Seksyen 33\r\n40400 Shah Alam, Selangor Darul Ehsan, Malaysiaa', 'Meta kewords', 'Meta Description', '0a1RTwIfyWf59RRXoiFAOiGCDO7MPQ1MqBlgdciP.png', 'tmB6EkVvsAhAfdZdMv475ongpMGypmhaDMgWvfKH.png', 'BnbKhhn9G4Oh9dcjD8M8nTT4CpvN7s2Gshe97YK1.png', '2023-02-21 00:18:34', '2023-02-26 22:32:50');

-- --------------------------------------------------------

--
-- Table structure for table `socia_llink`
--

CREATE TABLE `socia_llink` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `link` text DEFAULT NULL,
  `hover` text DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `socia_llink`
--

INSERT INTO `socia_llink` (`id`, `name`, `link`, `hover`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'facebook', 'https://www.facebook.com/', '', 0, '2023-02-20 22:34:37', '2023-02-20 23:36:30', NULL),
(2, 'twitter', 'https://twitter.com/i/flow/login?input_flow_data=%7B%22requested_variant%22%3A%22eyJsYW5nIjoiZW4ifQ%3D%3D%22%7D', '', 0, '2023-02-20 22:35:14', '2023-02-20 23:36:18', NULL),
(3, 'linkedin', 'https://www.linkedin.com/login', '', 0, '2023-02-20 22:35:46', '2023-02-20 23:35:07', NULL),
(4, 'google', 'https://accounts.google.com/v3/signin/identifier?dsh=S1929742651%3A1676961391700975&flowName=GlifWebSignIn&flowEntry=ServiceLogin&ifkv=AWnogHdv17fLXh9sPf5xKr0paA39czu_6s78C5W6YCPjqPgEop-IlIkkjWzaZTMKhN-qdxv87Uo7Wg', '', 0, '2023-02-20 22:37:06', '2023-02-20 23:36:02', NULL),
(5, 'instagram', 'https://www.instagram.com/accounts/login/', '', 0, '2023-02-20 22:37:54', '2023-02-20 23:35:53', NULL),
(6, 'youtube', 'https://www.youtube.com/', '', 0, '2023-02-20 22:38:22', '2023-02-20 23:35:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `solutions`
--

CREATE TABLE `solutions` (
  `id` int(11) NOT NULL,
  `page_url` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `descriptions` text DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `image` text DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `solutions`
--

INSERT INTO `solutions` (`id`, `page_url`, `name`, `descriptions`, `image_name`, `image`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'apparel', 'Apparel', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">Bumilink serves customers in fashion apparel, textiles, and footwear and accessories sectors. We can provide end-to-end solutions including air and ocean freight forwarding, customs clearance and ground transportation.</li></ul>', 'solution-apparel.png', '5LpYaABaywxcQC3gk43jEUseZhuFKXtGCanI4TOB.png', 0, '2023-02-23 01:52:04', '2023-02-22 18:15:52', NULL),
(2, 'aviation', 'Aviation', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">In the aviation industry, time is critical. The last thing you want to think about is logistics. At Bumilink, we not only exceed the demands of the aviation community, we continue to look for ways to improve our service.</li></ul>', 'solution-aviation.png', '49c079hjVQTEbFzcNhROxsoMiF1r4IMAYncQECdz.png', 0, '2023-02-23 02:18:14', '2023-02-22 18:18:14', NULL),
(3, 'banking', 'Banking / Finance', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">As a Banking or Financial Services institution, you may have hundreds, in some cases thousands, of offices across the country. Even if you\'re part of a large regional bank, you may have a multitude of locations.</li></ul>', 'solution-banking-finance.png', 'xMrFI5pe7kkmSIlWL480MTBgUYljrE8y2YBiczf4.png', 0, '2023-02-23 02:19:17', '2023-02-22 18:41:55', NULL),
(4, 'ecommerce', 'E-Commerce', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">Being a pioneer in logistics &amp; fulfillment for consumer products, Bumilink is the most competent partner when it comes to e-commerce fulfillment for both physical and digital products. Based on the expertise from many e-commerce projects with blue-chip companies, Bumilink knows how to implement and manage the seamless flow of goods, money, and information in e-business.</li></ul>', 'solution-e-commerce.png', '90n2ae7tKNsWKRToqyWMExsIgXf6MnZCmnLX6QVG.png', 0, '2023-02-23 02:20:15', '2023-02-22 18:20:15', NULL),
(5, 'fmcg', 'FMCG', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">Bumilink is a leading supplier of total logistic solutions to the Fast Moving Consumer Goods (FMCG) market. We have multi share warehouses, which are geographically capable of serving every area in Malaysia, and we offer the same within distribution and transport systems. At the same time, we continuously extend the flow of information in the logistics chain.</li></ul>', 'solution-fmcg.png', 'A190446MSAaaPM16UIv3QKMbgN67v5fbmNG36awZ.png', 0, '2023-02-23 02:22:02', '2023-02-22 18:22:02', NULL),
(6, 'healthcaremedicallogistics', 'Healthcare / Medical Logistics', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">We recognize that the products we handle are the very products that are destined for the emergency room or a doctor\'s office. By leveraging our deep experience in healthcare / medial products we can provide solutions that meet the critical and time-sensitive delivery requirements for your customers. Products that keep people and pets healthy to the products that save lives.</li></ul>', 'solution-healthcare.png', 'Cl6yspzebGYw2yTEzd1ZWGVj1wNWtTg7TvjN4pOx.png', 0, '2023-02-23 02:22:45', '2023-02-22 18:22:45', NULL),
(7, 'manufacturinglogistics', 'Manufacturing Logistics', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">At Bumilink, we understand that you want to focus you efforts on your core activities such as efficient manufacturing, product development, marketing and sales. That is why we have developed a range of Manufacturing Logistics solutions. We have developed our expertise of managing your Manufacturing Logistics for many years in the Automotive and Technology sector.</li></ul>', 'solution-manufacturing.png', 'OCYIGUZNmm2rN7FUUBDPTyJo5Cb7UE6vjxgZENeK.png', 0, '2023-02-23 02:23:20', '2023-02-22 18:23:20', NULL),
(8, 'technology', 'Technology', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">In order to service their clients, high-tech companies require efficient, timely and reliable logistics services. Bumilink Global Logistics helps high-tech companies to be more competitive. Bumilink operates across this market sector delivering computers, photocopiers and consumer electronics as well as supporting the aftermarket with spare parts.</li></ul>', 'solution-technology.png', 'MixW9whIUD7eHIvMLyrdegAh2Onsmq0SKJHxln2H.png', 0, '2023-02-23 02:24:04', '2023-02-22 18:24:04', NULL),
(9, 'customstaxconsultancy', 'Customs Tax Consultancy', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">Frequent changes in Malaysia import and export regulations, stricter enforcement, and severe penalties for non-compliance can have a huge impact on your business\' success in the domestic and international marketplace. More and more companies are relying on professional consultants to help them navigate the increasingly complex world of logistics compliance.</li></ul>', 'solution-customstax.png', '36TCNMD6l0TP8b5JFr86KfdTJT2x1YCTVpMbkCpb.png', 0, '2023-02-23 02:24:51', '2023-02-22 18:24:51', NULL),
(10, 'dedicatedfleetlogistics', 'Dedicated Fleet Logistics', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">Alleviate the hassles of private fleets by letting Bumilink run your transportation fleet for you. Gone are the headaches of driver retention, insurance, skyrocketing fuel costs, and maintenance, with a dedicated fleet conversion Bumilink can simplify your transportation operations and seamlessly integrate into your distribution operation. We can lease a fleet of trucks or buy your current fleet from you. You can even keep the same brand name on all your trucks. We can help you improve the flexibility of your organization no matter what the size of your company.</li></ul>', 'solution-dedicatedfleet.png', 'j0R5fOdODz1h0EGmkTZyDqBBNyCsB6lwhBAllnGD.png', 0, '2023-02-23 02:25:36', '2023-02-22 18:25:36', NULL),
(11, 'supplychainmanagement', 'Supply Chain Management', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">Linking a Company to its Customers &amp; Suppliers The supply chain determines how 100 percent of the company\'s value is delivered to its customers. Supply chain management as an integration of the planning, management, and control of value chains, encompassing suppliers, production, physical logistics, and customers. The objective is to achieve superior customer service with minimal total supply chain cost. We work with clients to achieve business performance improvement and sustainable competitive advantage.</li></ul>', 'solution-supplychain.png', 'JWWddNWSPtFerFhI7PzRZceJWPJnFaSEprlFHmMT.png', 0, '2023-02-23 02:26:20', '2023-02-22 18:26:20', NULL),
(12, 'wms', 'WMS', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">Our Warehouse Management System (WMS) is a fully integrated Information Technology system that incorporates a broad range of logistics capabilities. It is designed to be completely real-time, allowing the appropriate supply-chain program elements to fit a variety of ways of doing business for each customer. It is extremely flexible and designed for custom applications. We have implemented our WMS in a variety of operations including dedicated and shared facilities and have been able to ensure specific system requirements for our customers.</li></ul>', 'solution-wms.png', '1zga72GBI3PyOsX2tMfROeMsmVGdEDDB08vt9Ufm.png', 0, '2023-02-23 02:27:30', '2023-02-22 18:27:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_pages`
--

CREATE TABLE `sub_pages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `descriptions` text DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `image` text DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `position` varchar(10) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `useful_llink`
--

CREATE TABLE `useful_llink` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `link` text DEFAULT NULL,
  `hover` text DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `useful_llink`
--

INSERT INTO `useful_llink` (`id`, `name`, `link`, `hover`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Customs', NULL, '', 0, '2023-02-20 22:08:44', '2023-02-20 22:13:26', NULL),
(3, 'Dictionary & Translator', 'https://www.dictionary.com/', '', 0, '2023-02-20 22:13:53', '2023-02-20 22:13:53', NULL),
(4, 'Embassy Worldwide', 'http://www.helplinedatabase.com/embassy-database/', '', 0, '2023-02-20 22:14:25', '2023-02-20 22:14:25', NULL),
(5, 'World Clock', 'https://www.timeanddate.com/worldclock/', '', 0, '2023-02-20 22:14:47', '2023-02-20 22:14:47', NULL),
(6, 'World Holiday', 'http://www.earthcalendar.net/index.php', '', 0, '2023-02-20 22:15:30', '2023-02-20 22:15:30', NULL),
(7, 'World International City Codes', '', '', 0, '2023-02-20 22:15:48', '2023-02-20 22:15:48', NULL),
(8, 'World Map', 'https://www.google.com/maps/@1.5433728,103.7860864,13z', '', 0, '2023-02-20 22:16:06', '2023-02-20 22:16:06', NULL),
(9, 'Worldwide Phone Directory', 'http://www.the-acr.com/codes/cntrycd.htm#a', '', 0, '2023-02-20 22:16:26', '2023-02-20 22:16:26', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_types`
--
ALTER TABLE `admin_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_by`
--
ALTER TABLE `contact_by`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_contact_by`
--
ALTER TABLE `feedback_contact_by`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_following_service`
--
ALTER TABLE `feedback_following_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `following_services`
--
ALTER TABLE `following_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `heard_about`
--
ALTER TABLE `heard_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_business`
--
ALTER TABLE `our_business`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_technology`
--
ALTER TABLE `our_technology`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `quote`
--
ALTER TABLE `quote`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socia_llink`
--
ALTER TABLE `socia_llink`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `solutions`
--
ALTER TABLE `solutions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_pages`
--
ALTER TABLE `sub_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `useful_llink`
--
ALTER TABLE `useful_llink`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admin_types`
--
ALTER TABLE `admin_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contact_by`
--
ALTER TABLE `contact_by`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback_contact_by`
--
ALTER TABLE `feedback_contact_by`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback_following_service`
--
ALTER TABLE `feedback_following_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `following_services`
--
ALTER TABLE `following_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `heard_about`
--
ALTER TABLE `heard_about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `our_business`
--
ALTER TABLE `our_business`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `our_technology`
--
ALTER TABLE `our_technology`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `quote`
--
ALTER TABLE `quote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `socia_llink`
--
ALTER TABLE `socia_llink`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `solutions`
--
ALTER TABLE `solutions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sub_pages`
--
ALTER TABLE `sub_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `useful_llink`
--
ALTER TABLE `useful_llink`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
