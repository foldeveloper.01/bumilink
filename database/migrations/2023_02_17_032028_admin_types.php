<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AdminTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('admin_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->enum('status', array('0','1'))->default('0');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();
        });

        //insert admin types
        DB::table('admin_types')->truncate();
        $data = [ 'type'=> 'superadmin'];
        DB::table('admin_types')->insert($data);
        $data = [ 'name'=> 'superadmin',
                  'email'=> 'superadmin@gmail.com',
                  'password'=> 'superadmin@gmail.com',
                  'admin_type_id'=> '1'
                ];
        DB::table('admin')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('admin_types');
    }
}
