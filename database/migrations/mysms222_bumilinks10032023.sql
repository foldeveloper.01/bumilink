-- phpMyAdmin SQL Dump
-- version 4.9.11
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 10, 2023 at 05:46 PM
-- Server version: 5.7.41
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mysms222_bumilinks`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_type_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `firstname`, `lastname`, `email`, `number`, `email_verified_at`, `password`, `admin_type_id`, `image`, `status`, `created_at`, `updated_at`, `remember_token`, `deleted_at`) VALUES
(1, 'Admin', NULL, 'admin@gmail.com', '60123593905', NULL, '$2a$12$xqW0YZ.AP5dhuOOmAOGliuMpVbHxxxkz2SW4DmORAjo50Jhhv8wW2', 1, NULL, 0, '2023-02-27 17:43:52', '2023-03-08 02:38:52', NULL, NULL),
(3, 'Carmen', 'S', 'carmen@firstonline.com.my', '01123068049', NULL, '$2y$10$pYdVLGnSkolaejRh1Zz0veX.LgbM/3f5/xu/ueVn/FMmnxrSfR27m', 2, NULL, 0, '2023-03-07 22:36:05', '2023-03-08 19:42:50', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_types`
--

CREATE TABLE `admin_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_types`
--

INSERT INTO `admin_types` (`id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Superadmin', '0', '2023-02-17 05:06:59', '2023-02-24 04:52:47'),
(2, 'Admin', '0', '2023-02-24 04:52:14', '2023-02-24 04:52:14'),
(3, 'Developer', '0', '2023-02-27 02:43:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contact_by`
--

CREATE TABLE `contact_by` (
  `id` int(11) NOT NULL,
  `name` text,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact_by`
--

INSERT INTO `contact_by` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Email', 0, '2023-03-06 00:29:52', '2023-03-06 00:29:52', NULL),
(2, 'Phone', 0, '2023-03-06 00:30:03', '2023-03-06 00:30:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `firstname` varchar(200) DEFAULT NULL,
  `lastname` varchar(200) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_title` varchar(255) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` text,
  `postal` varchar(30) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `heardabout` varchar(255) DEFAULT NULL,
  `message` text,
  `message_type` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `firstname`, `lastname`, `company_name`, `company_title`, `phone`, `fax`, `mobile`, `email`, `address`, `postal`, `city`, `state`, `country`, `heardabout`, `message`, `message_type`, `created_at`, `updated_at`) VALUES
(1, 'bharathi', 'raja', 'first online', 'php developer', '9842363703', '', '9842363705', 'bharathi@gmail.com', 'pudukkottai,  tamilnadu india', '622402', 'ponnamaravthi', 'tamil nadu', 'India', 'Newspapers', 'sample testing', 'Read', '2023-03-06 22:45:54', '2023-03-06 22:47:09'),
(2, 'FO', 'Test', 'FO TEST', 'Ms', '+60193380012', '', '0123456789', 'jennyrubijane1@gmail.com', 'A-1-1, Jalan Multimedia 7/Ah, I-city,', '40000', 'Shah Alam', 'Selangor', 'Malaysia', 'Website', 'sfa', 'Read', '2023-03-06 22:50:50', '2023-03-06 22:51:31');

-- --------------------------------------------------------

--
-- Table structure for table `feedback_contact_by`
--

CREATE TABLE `feedback_contact_by` (
  `id` int(11) NOT NULL,
  `feedback_id` int(11) DEFAULT NULL,
  `contact_by_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feedback_contact_by`
--

INSERT INTO `feedback_contact_by` (`id`, `feedback_id`, `contact_by_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2023-03-06 22:45:55', '2023-03-06 22:45:55'),
(2, 2, 1, '2023-03-06 22:50:50', '2023-03-06 22:50:50');

-- --------------------------------------------------------

--
-- Table structure for table `feedback_following_service`
--

CREATE TABLE `feedback_following_service` (
  `id` int(11) NOT NULL,
  `feedback_id` int(11) DEFAULT NULL,
  `following_services_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feedback_following_service`
--

INSERT INTO `feedback_following_service` (`id`, `feedback_id`, `following_services_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2023-03-06 22:45:55', '2023-03-06 22:45:55'),
(2, 1, 3, '2023-03-06 22:45:55', '2023-03-06 22:45:55'),
(3, 1, 5, '2023-03-06 22:45:55', '2023-03-06 22:45:55'),
(4, 1, 7, '2023-03-06 22:45:55', '2023-03-06 22:45:55'),
(5, 2, 1, '2023-03-06 22:50:50', '2023-03-06 22:50:50'),
(6, 2, 8, '2023-03-06 22:50:50', '2023-03-06 22:50:50');

-- --------------------------------------------------------

--
-- Table structure for table `following_services`
--

CREATE TABLE `following_services` (
  `id` int(11) NOT NULL,
  `name` text,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `following_services`
--

INSERT INTO `following_services` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Global Logistics Services', 0, '2023-03-06 00:28:08', '2023-03-06 00:28:08', NULL),
(2, 'Air Freight Services', 0, '2023-03-06 00:28:19', '2023-03-06 00:28:19', NULL),
(3, 'Domestic Services', 0, '2023-03-06 00:28:28', '2023-03-06 00:28:28', NULL),
(4, 'Warehousing & Distribution', 0, '2023-03-06 00:28:38', '2023-03-06 00:28:38', NULL),
(5, 'Ground Shipping Services/ Transportation', 0, '2023-03-06 00:28:47', '2023-03-06 00:28:47', NULL),
(6, 'Third Party Logistics', 0, '2023-03-06 00:28:57', '2023-03-06 00:28:57', NULL),
(7, 'Ocean Freight Services', 0, '2023-03-06 00:29:06', '2023-03-06 00:29:06', NULL),
(8, 'Customs Brokerage', 0, '2023-03-06 00:29:17', '2023-03-06 00:29:17', NULL),
(9, 'E-Commerce Solutions', 0, '2023-03-06 00:29:26', '2023-03-06 00:29:26', NULL),
(10, 'Technology Support', 0, '2023-03-06 00:29:34', '2023-03-06 00:29:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `heard_about`
--

CREATE TABLE `heard_about` (
  `id` int(11) NOT NULL,
  `name` text,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `heard_about`
--

INSERT INTO `heard_about` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Newspapers', 0, '2023-03-06 00:30:25', '2023-03-06 00:30:25', NULL),
(2, 'Website', 0, '2023-03-06 00:30:35', '2023-03-06 00:30:35', NULL),
(3, 'Advertisement', 0, '2023-03-06 00:30:43', '2023-03-06 00:30:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_admin_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2023_02_17_032028_admin_types', 1);

-- --------------------------------------------------------

--
-- Table structure for table `our_business`
--

CREATE TABLE `our_business` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `class_name` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `our_business`
--

INSERT INTO `our_business` (`id`, `title`, `description`, `class_name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Operational Excellence', 'BumiLink’s operational standards have been honed in demanding environments as diverse as Formula 1,   Manufacturing, Grocery Consumer Supply Chains, Credit card Distribution, Industrial Logistics, Oil &amp; Gas, Aviation &amp; Aerospace, Publishing and Fulfillment. We apply our learning across all our sectors, and we are building and rolling out defined processes and best practice to ensure that every customer, project and site will benefit.', 'flaticon-warehouse', 0, '2023-03-05 20:52:01', '2023-03-05 20:55:36', NULL),
(2, 'Customer Relations', 'Understanding a company\'s requirements is the first step in creating a tailor-made solution. We start by listening and challenging. We require all our staff to understand and develop their relationship with customers whether operational, administrative or commercial. Our management structure is aligned to supporting and adding to those relationships.\r\n\r\n						<br><br>Throughout the life of a relationship from project phase, to start up, to renewal, we seek to perform, to improve, to anticipate and to innovate in line with our customers\' needs.', 'flaticon-hand-shake-1', 0, '2023-03-05 20:52:48', '2023-03-05 20:55:16', NULL),
(3, 'Product Leadership', '<ul style=\"list-style-type:square;\">\r\n								<li style=\"padding-bottom: 3%;\">✔ BumiLink is a market leader in fully comprehensive integrated total logistics software. With our own in-house development we are able to customize at any level and at any point.</li>\r\n								<li style=\"padding-bottom: 3%;\">✔ BumiLink is the first to implement a fully integrated online grocery corporate &amp; consumer supply chain software solution coupled with innovative forward logistics.\r\n							</li>\r\n							<li style=\"padding-bottom: 3%;\">✔ BumiLink commitment to product leadership enables us to anticipate the future requirements of our customers, harness new technologies and respond to change.</li>\r\n						</ul>', 'flaticon-delivery-box', 0, '2023-03-05 20:54:48', '2023-03-05 20:54:48', NULL),
(4, 'Value', 'BumiLink builds all-round value for its customers by tailoring solutions to the precise needs of the customer and by our ability to optimize the performance and productivity of each solution. \r\n							<br><br>BumiLink offers long-term price differentiation by timely investment in relevant technologies and new product development and by investing directly in our customers\' goals.', 'flaticon-verified', 0, '2023-03-05 20:56:19', '2023-03-05 20:56:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `our_technology`
--

CREATE TABLE `our_technology` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `class_name` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `our_technology`
--

INSERT INTO `our_technology` (`id`, `title`, `description`, `class_name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Logistics & Warehouse Management Software', 'BLink warehouse management system transforms warehouse operations into fully integrated logistics and fulfillment business. By enabling a company to take orders from multiple sources, process them in real time with mobile and automating the shipping and billing processes, organizational efficiency will improve, with positive effect on the bottom line. BLink is a comprehensive, easy to use software package for managing 3rd Party Warehouse operations. BLink features the ability to manage multiple warehouses, customers, products, rates, carriers, consignees, inbound receipts, bills of landing, and much more. BLink also produces reports of inventory, activity, and a wide variety of management and customer and product specific information.', 'flaticon-cloud-computing-3', 0, '2023-03-05 20:35:02', '2023-03-05 20:42:08', NULL),
(2, 'Ongoing Support', 'Having a successful software solution goes beyond installation and implementation. BLink offers full product support to help you troubleshoot your system, implement new features, and identify potential areas of product improvement. Whether you’re speaking to a support consultant over the phone or conversing via email, our staff is trained to look at the whole picture, not merely the question being asked. This helps ensure that your system stays optimized for your business and runs smoothly and continually', 'flaticon-support', 0, '2023-03-05 20:35:33', '2023-03-05 20:41:51', NULL),
(3, 'By Phone and Online', 'Our support organization is always available to you during normal business hours, but we also offer an optional support plan to assist after-hours and via a paging service. For those who prefer a more \"self-service\" option, our support website provides timely articles and product updates available only to current BLink support customers.', 'flaticon-chat', 0, '2023-03-05 20:36:11', '2023-03-05 20:41:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `descriptions` text,
  `image_name` varchar(255) DEFAULT NULL,
  `image` text,
  `status` int(11) DEFAULT NULL,
  `page_url` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `descriptions`, `image_name`, `image`, `status`, `page_url`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Vision and Mission', '', 'home-vision (2).png', 'LfjOGx1zTIx5o4yMQNDVXsIMY3U8jZw7BOYzESqm.png', 0, 'visionandmission', '2023-03-06 04:05:57', '2023-03-05 20:05:57', NULL),
(2, 'Welcome To Bumilink', '', 'home-image01 (2).png', 'Dj5j3EdLWGOG35u7RcelQ6Hcvzelx6LTfbt4joeB.png', 0, 'welcometobumilink', '2023-03-06 04:06:55', '2023-03-05 20:06:55', NULL),
(3, 'Why use Bumilink?', '<p>Because Bumilink offers you a single carrier responsibility for any type of service you require domestic, international or total logistics, with a unique ability to customize the services to your specific to needs.<br></p>', NULL, NULL, 0, 'whyusebumilink', '2023-03-06 05:54:26', '2023-03-05 21:54:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quote`
--

CREATE TABLE `quote` (
  `id` int(11) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_email` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `details` text,
  `file_name` varchar(255) DEFAULT NULL,
  `quote_type` varchar(10) DEFAULT NULL,
  `file` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `page_url` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `descriptions` text,
  `class_name` varchar(255) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `image` text,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `page_url`, `name`, `descriptions`, `class_name`, `image_name`, `image`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'domesticservices', 'Domestic Services', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Bumilink offers more time-definite delivery options than anyone. Regardless of the size or weight of your shipment or the time of day you need to ship - we\'ll guarantee your shipment will be delivered on time, door-to-door, anywhere Malaysia - with complete Web tracking available.</span><br></p>', 'flaticon-fast-delivery', 'home-service-1.png', 'hskd2LhEbmBxnhYJC0JCN3iETxnfh7tISZSJeQiW.png', 0, '2023-03-06 03:40:58', '2023-03-05 19:40:58', NULL),
(2, 'international', 'International', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Bumilink offers a full spectrum of value added international services to more than 800 cities in 205 countries. As your ideal partner to handle your international transportation and logistics requirements, we provide from small packages that require door-to-door delivery to large heavyweight machinary to remote areas, Bumilink will meet your deadline - around the clock, around the world.</span><br></p>', 'flaticon-airplane-shape', 'home-service-2.png', '8e3WyHntNIt0qrsvNx3hw4cMboea9ehBRFuTC0Pa.png', 0, '2023-03-06 03:42:32', '2023-03-05 19:42:32', NULL),
(3, 'globallogistic', 'Global Logistics', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">The same value added logistics services we provide in Malaysia we can provide on a global basis. This includes inventory management and reporting, warehousing, assembly and distribution, recovery and return and product merging programs.</span><br></p>', 'flaticon-sea-ship-with-containers', 'home-service-3.png', 'UNACaqc6zgH6OWFpppv5h2Z14nrzTKDcXN5bDb2E.png', 0, '2023-03-06 03:43:56', '2023-03-05 19:43:56', NULL),
(4, 'custombrokerage', 'Customs Brokerage', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Bumilink offers a full spectrum of value added international services to more than 800 cities in 205 countries. Bumilink is your ideal partner to handle your international transportation and logistics requirements. From small packages that require door-to-door delivery to large heavyweight machinary to remote areas, Bumilink will meet your deadline - around the clock, around the world.</span><br></p>', 'flaticon-shipping-and-delivery', 'home-service-4.png', 'xfaY4KJteVR1d9WkZW7MhO9r4L55tDLS5yUy05Ln.png', 0, '2023-03-06 03:45:06', '2023-03-05 19:45:06', NULL),
(5, 'cargoinsurance', 'Cargo Insurance', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">While your valuable merchandise is in your facilities, you have full control of the ways that it is moved and how it is handled. Once in transit by land, sea, and air, however, until your goods arrive at their final destination, they may be vulnerable to damage, loss, theft, and terrorist incidents. Bumilink can provide a full range of cargo insurance coverage as part of our integrated services.</span><br></p>', 'flaticon-help-operator', 'home-service-5.png', 'EfaPBkBNgH1AyflJtp7A3MLvhBF9tVRWJrf7kjkc.png', 0, '2023-03-06 03:46:25', '2023-03-05 19:46:25', NULL),
(6, 'crossborderexpress', 'Cross Border Express', '', '', NULL, NULL, 0, '2023-03-06 07:01:48', '2023-03-05 23:01:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services1`
--

CREATE TABLE `services1` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `text` text,
  `descriptions` text,
  `image_name` varchar(255) DEFAULT NULL,
  `image` text,
  `status` int(11) DEFAULT NULL,
  `position` varchar(10) DEFAULT NULL,
  `class_name` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `services1`
--

INSERT INTO `services1` (`id`, `category_id`, `name`, `text`, `descriptions`, `image_name`, `image`, `status`, `position`, `class_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Linked Same Day', '', '', NULL, NULL, 0, 'none', '', '2023-03-06 06:07:49', '2023-03-05 22:07:49', NULL),
(2, 1, 'Linked Overnight', 'Offers guaranteed overnight delivery at lower rates than our competitors.', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Next day delivery to over 20,000 communities in Malaysia available weekends and holidays, 365 days a year. Any size or shape package, no weight restrictions and delivery as early as 9:00 a.m. Real time Track &amp; Trace available.</span><br></p>', 'domesticservice-linked-overnight.png', 'IyOLhMwAK336e0MpTghfRx4Dtu25GR7M7YGKE4jo.png', 0, 'none', '', '2023-03-06 06:08:59', '2023-03-05 22:08:59', NULL),
(3, 1, 'Linked Economy', 'We ready to serve you anywhere and anytime and deliver your shipments within the shortest time', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Economical door-to-door delivery within 2 days to every postcode in Malaysia. Earliest 2nd day delivery time - at less cost. Packages of any size, shape or weight shipped and delivered.</span><br></p>', 'domesticservice-linked-economy.png', 'bUombAvIcxANoMgTUPFTyp86zSrugZt1hvfcXxEo.png', 0, 'none', '', '2023-03-06 06:10:12', '2023-03-05 22:10:12', NULL),
(4, 1, 'Transportation (LTL/FTL)', 'Truckload service available around the clock. You get added flexibility and earlier arrival to any postcode in Malaysia. Our Nationwide LTL service can save you money on lower minimums and improve delivery time by days.', '<p>BumiLink transportation network is one of the best in the industry, providing our customers with ways to reduce costs, increase efficiencies and optimize load capacity. Our network is dedicated, responsible, and always provides the highest level of ground transportation services with proactive and effective communication.<br></p>', NULL, NULL, 0, 'none', '', '2023-03-06 06:11:01', '2023-03-05 22:11:01', NULL),
(5, 1, 'Home Deliveries & e-Commerces', 'Our delivery services are available throughout whole Malaysia', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Full service delivery capabilities in all Malaysian postcodes for all weight deliveries. Appointments made for residential deliveries by Bumilink centre with flexible delivery times until 9:00 p.m. Two man delivery teams available with complete packing and unpacking capabilities. Email and web site tracking available around the clock.</span><br></p>', 'domesticservice-retailmerchandising.png', '5pezBWh9V4m7MfXd4QOGftHfR9G0fAkxoF4nwigL.png', 0, 'none', '', '2023-03-06 06:11:52', '2023-03-05 22:11:52', NULL),
(6, 1, 'Retail Merchandising', 'The fastest and most economical way to install your displays, fixtures and point-of-sale materials in retail outlets everywhere in Malaysia.', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Services offered: delivery, assembly and installation, in-store cleaning and maintenance, de-installation, warehousing and inventory management, local delivery, express services, training of retail sales personnel, inventory and product tracking, special events teams, nationwide support service.</span><br></p>', 'domesticservice-homedeliveries.png', 'WSkPBFaIZDopjVXfb230QvX5yE8UXVJsb3416glD.png', 0, 'none', '', '2023-03-06 06:12:38', '2023-03-05 22:12:38', NULL),
(7, 2, 'Global Same Day', '', '<p><span style=\"border: none; outline: none; font-weight: bolder; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\">We work round the clock to collect and deliver the same day for the most urgent shipments.</span><br></p>', 'international-globalsameday.png', 'JRWMI8CngKSmPlFEiR8oRk22x52A07ancltGChfx.png', 0, 'none', '', '2023-03-06 06:34:57', '2023-03-05 22:34:57', NULL),
(8, 2, 'International', '', '', NULL, NULL, 0, 'none', '', '2023-03-06 06:35:31', '2023-03-05 22:35:31', NULL),
(9, 2, 'Global Import Freight R/P', 'Deliver goods from 205 countries to your doorstep in Malaysia', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">We can provide a single source control on your purchased items from over 205 countries into Malaysia. The coordination on both air freight and ocean freight can be provided into every Malaysian entry port and airports with complete customs clearance, warehousing, distribution and other delivery options available.</span><br></p>', 'international-globalimport.png', 'hCiaEjGPFuIIMrjPoiRqk556ax2iTtcQIImWyLkU.png', 0, 'none', '', '2023-03-06 06:36:21', '2023-03-05 22:36:21', NULL),
(10, 2, 'Cross Border Express', 'We offer multiple door-to-door transportation services', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Our cross border express services segment offer multiple door-to-door transportation services from Package Express to Expedited Heavyweight to Economy Freight plus a special bonded LTL &amp; FCL service to and from two major Malaysia gateways Singapore and Thailand, and provide same-day Customs clearance, so freight moves smoothly and quickly to your customers.</span><br></p>', 'international-crossborder.png', 'EG0TbqJAZCdRfjeyysTRHmv3rGXzatut5nC0BANn.png', 0, 'none', '', '2023-03-06 06:37:20', '2023-03-05 22:37:20', NULL),
(11, 6, 'Cross Border Express', 'Our cross border express services segment offer multiple door-to-door transportation services from Package Express to Expedited Heavyweight to Economy Freight plus a special bonded LTL & FCL service to and from two major Malaysia gateways Singapore and Thailand, and provide same-day Customs clearance, so freight moves smoothly and quickly to your customers', '<h2 style=\"color:#eb0028; font-weight: 600;\"> Major Gateways Served</h2>\r\n                        <div class=\"des\">\r\n                            <ul>\r\n                                <li style=\"font-size: 16px; padding-top: 1.5%;\">✈  Corridor – Singapore\r\n                                </li> \r\n                                <li style=\"font-size: 16px; padding-top: 1.5%;\">✈  Corridor – Thailand\r\n                                </li> \r\n                              </ul>\r\n                              </div><br>\r\n                    \r\n                    <div class=\"inner-column\">\r\n                        <h2 style=\"color:#eb0028; font-weight: 600;\"> Key Services</h2>\r\n                        <div class=\"des\">\r\n                            <ul>\r\n                                <li style=\"font-size: 16px; padding-top: 1.5%;\">✔   Door-to-door pickup and delivery through our dedicated truck fleet \r\n                                </li> \r\n                                <li style=\"font-size: 16px; padding-top: 1.5%;\">✔   Northbound and southbound shipment consolidation and deconsolidation</li>\r\n                                <li style=\"font-size: 16px; padding-top: 1.5%;\">✔ Reverse logistics</li> \r\n                                <li style=\"font-size: 16px; padding-top: 1.5%;\">✔ Real-time shipment tracking and tracing available online\r\n                                </li> \r\n                              </ul>\r\n                              </div>\r\n                    </div>', 'international-globalsameday (2).png', 'NmGufDdgXiY2kxD9Cmd8dZ78bUNsKJPxZmhHsHQO.png', 0, 'none', '', '2023-03-06 07:04:48', '2023-03-05 23:04:48', NULL),
(12, 6, 'Key Benefits', '', '', NULL, NULL, 0, 'none', '', '2023-03-06 07:05:11', '2023-03-05 23:05:11', NULL),
(13, 3, 'Global Same Day', '', '', NULL, NULL, 0, 'none', '', '2023-03-06 07:27:45', '2023-03-05 23:27:45', NULL),
(15, 4, 'Custom Declaration', '', '', NULL, NULL, 0, 'none', '', '2023-03-06 07:48:51', '2023-03-05 23:48:51', NULL),
(16, 4, 'Key Services', '', '<ul>\r\n                        <li>✔ Classification of goods</li>\r\n                        <li>✔ Completion of complex documents</li>\r\n                        <li>✔ Comprehensive consulting</li>\r\n                        <li>✔ Licensed Customs brokers</li>\r\n                        <li>✔ Merchandise clearance through every Malaysia. port of entry</li>\r\n                        <li>✔ EDI connection to Customs via Automated Broker Interface (ABI)</li>\r\n                        <li>✔ Worldwide network of agents</li>\r\n                    </ul>', 'custombrokerage-keyservices.png', '5UPN266MfZIhKihjO6Tl7JlpB2wGiVG8kMYi8mhQ.png', 0, 'none', '', '2023-03-06 07:50:57', '2023-03-05 23:50:57', NULL),
(17, 4, 'Key Benefits', '', '<ul style=\"padding-bottom: 5%;\">\r\n                        <li>✔ Dedicated Bumilink resources monitor rapidly changing government regulations </li>\r\n                        <li>✔ Accurate classification of imports ensures smooth entry </li>\r\n                        <li>✔ All required paperwork is processed expeditiously </li>\r\n                        <li>✔ Delays minimized </li>\r\n                        <li>✔ Pre-arrival review of Customs documents enables quick and efficient release of goods </li>\r\n                    </ul>', 'custombrokerage-keybenefits.png', '72yt4vk55a1jQe08ryrd6u9QiSRS3jZGyoz9nEHI.png', 0, 'none', '', '2023-03-06 07:51:54', '2023-03-05 23:51:54', NULL),
(18, 5, 'Cargo Insurance', 'Truckload service available around the clock. You get added flexibility and earlier arrival to any postcode in Malaysia. Our Nationwide LTL service can save you money on lower minimums and improve delivery time by days.<br><br>\r\n\r\n					BumiLink transportation network is one of the best in the industry, providing our customers with ways to reduce costs, increase efficiencies and optimize load capacity. Our network is dedicated, responsible, and always provides the highest level of ground transportation services with proactive and effective communication.', '<div class=\"inner-column\">\r\n									<h2 style=\"color:#eb0028; font-weight: 700; font-family: Raleway;\">Key Services</h2>\r\n									<h5>Types of coverage:</h5>\r\n									<div class=\"des\">\r\n										<ul>\r\n											<li style=\"font-size: 16px; padding-top: 1.5%;\">✈  Door-to-door \r\n											</li> \r\n											<li style=\"font-size: 16px; padding-top: 1.5%;\">✈  Single source \r\n											</li> \r\n											<li style=\"font-size: 16px; padding-top: 1.5%;\">✈  All-risk coverage \r\n											</li> \r\n										  </ul>\r\n										  </div><br>\r\n								\r\n								<div class=\"inner-column\">\r\n									<h2 style=\"color:#eb0028; font-weight: 700; font-family: Raleway;\">Key Benefits</h2>\r\n									<div class=\"des\">\r\n										<ul>\r\n											<li style=\"font-size: 16px; padding-top: 1.5%;\">✔   Enhanced policy with unique clauses to protect 100% of your transit exposure  \r\n											</li> \r\n											<li style=\"font-size: 16px; padding-top: 1.5%;\">✔   Minimizes your transportation-related risks</li>\r\n											<li style=\"font-size: 16px; padding-top: 1.5%;\">✔ Insurance coverage specific to your needs </li> \r\n											<li style=\"font-size: 16px; padding-top: 1.5%;\">✔ Prompt &amp; efficient claims settlements</li>\r\n											<li style=\"font-size: 16px; padding-top: 1.5%;\">✔ Competitive rates</li> \r\n											<li style=\"font-size: 16px; padding-top: 1.5%;\">✔ Door-to-door single source coverage</li> \r\n										  </ul>\r\n										  </div>\r\n								</div> \r\n							</div>', 'cargoinsurance-keyservices.png', 'WiqCsUV43HjipIrCKXdoxvoARnZN3j0gO1mqhI1S.png', 0, 'none', '', '2023-03-06 07:58:54', '2023-03-06 00:00:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services2`
--

CREATE TABLE `services2` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `descriptions` text,
  `image_name` text,
  `image` text,
  `status` int(11) DEFAULT '0',
  `position` varchar(10) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `services2`
--

INSERT INTO `services2` (`id`, `category_id`, `subcategory_id`, `name`, `descriptions`, `image_name`, `image`, `status`, `position`, `class_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'Fastest Delivery', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">The fastest possible delivery, door-to-door, anywhere in Malaysia. Delivery within 2 to 8 hours, anytime, day or night, 365 days a year.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-route', '2023-03-05 22:14:00', '2023-03-05 22:14:00', NULL),
(2, 1, 1, 'Our Shipments', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">Perfect for shipping parts, products, documents and other urgent materials.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-delivery-box', '2023-03-05 22:14:44', '2023-03-05 22:14:44', NULL),
(3, 1, 1, 'Collect and Deliver', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">When you call, we collect immediately and deliver non-stop using the fastest means, whether aircraft, road vehicle or motorcycle the fastest and most direct route</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-collaboration', '2023-03-05 22:15:26', '2023-03-05 22:15:26', NULL),
(4, 1, 1, 'Work 7/24', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">We work round the clock to collect and deliver the same day for the most urgent shipments.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-clock-2', '2023-03-05 22:16:12', '2023-03-05 22:16:12', NULL),
(5, 1, 4, 'What you get from BumiLink Transport:', '<ul class=\"list-style-one\" style=\"padding-bottom:9.5%;\">\r\n									<li>The ability to meet tight deadlines with more flexibility\r\n									</li>\r\n									<li>Greater control over the safety and condition of your products during transportation\r\n									</li>\r\n									<li>The ease of dealing with one company, one-stop shopping\r\n									</li>\r\n									<li>Drivers that know and respect the importance of your cargo and dispatchers that are well versed in the needs of shippers and receivers\r\n									</li>\r\n									<li>The piece of mind knowing that you are dealing with a company that specializes in the demands of transporting unique products\r\n									</li>\r\n								</ul>', NULL, NULL, 0, 'none', 'flaticon-air-freight', '2023-03-05 22:17:47', '2023-03-05 22:27:55', NULL),
(6, 1, 4, 'We provide a full range of ground transportation services.', '<ul class=\"list-style-one\" style=\"margin-left:12%;\">\r\n										<li>Box / Canvas Trucks</li>\r\n										<li>Vans</li>\r\n										<li>Trailers</li>\r\n										<li>Reefers</li>\r\n										<li>Flatbeds</li>\r\n										<li>Logistical trailers</li>\r\n										<li>Team or Single Drivers</li>\r\n										<li>Full Truckload Services</li>\r\n										<li>Regional Truckload Shipping</li>\r\n										<li>National Truckload Shipping</li>\r\n										<li>Less-than-Truckload Services</li>\r\n								</ul>', NULL, NULL, 0, 'none', 'flaticon-moving-truck', '2023-03-05 22:19:24', '2023-03-05 22:26:05', NULL),
(7, 2, 7, 'Fastest Way to Ship', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">The fastest way to ship urgent or critical material from Malaysia to anywhere in the world.</span><br></p>', NULL, NULL, 0, 'left', 'flaticon-shipping-and-delivery', '2023-03-05 22:39:05', '2023-03-05 22:39:17', NULL),
(8, 2, 7, 'Goods in Any Size or Weight', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">Delivery of small packages to commercial shipments of any size or weight, 24 hours a day, 365 days a year.</span><br></p>', NULL, NULL, 0, 'left', 'flaticon-delivery-box', '2023-03-05 22:40:05', '2023-03-05 22:41:32', NULL),
(9, 2, 7, 'Various Shipping Method', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">When you call, we collect immediately and deliver non-stop using the fastest means, whether aircraft, road vehicle or motorcycle</span><br></p>', NULL, NULL, 0, 'right', 'flaticon-fast-delivery', '2023-03-05 22:40:42', '2023-03-05 22:41:53', NULL),
(10, 2, 7, 'Reach in Shortest Time', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">We will collect on time and deliver your goods with the fastest and most direct route.</span><br></p>', NULL, NULL, 0, 'right', 'flaticon-hourglass', '2023-03-05 22:42:43', '2023-03-05 22:42:55', NULL),
(11, 2, 8, 'Global Package Express', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">Our expedited door-to-door service for parcel-type shipments anywhere in the world. For shipping packages or for commercial shipments, we deliver direct to your customer\'s door with total tracking capabilities.</span><br></p>', NULL, NULL, 0, 'left', 'flaticon-air-freight', '2023-03-05 22:43:46', '2023-03-05 22:43:46', NULL),
(12, 2, 8, 'Global Economy Freight', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">A service that provides economical air export service from and to Malaysia with consolidations to more than 800 cities around the world. We can also provide multiple delivery options from airport-to-airport to door-to-door and DDP service</span><br></p>', NULL, NULL, 0, 'left', 'flaticon-moving-truck', '2023-03-05 22:44:15', '2023-03-05 22:45:39', NULL),
(13, 2, 8, 'Global Expedited Freight', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">This service provides a next flight out door-to-door air freight service for movement’s heavyweight from Malaysia to over 205 countries worldwide. Many destinations come with a money back guarantee with time-definite delivery.</span><br></p>', NULL, NULL, 0, 'right', 'flaticon-planet-earth', '2023-03-05 22:44:54', '2023-03-05 22:45:48', NULL),
(14, 2, 8, 'Global Ocean Freight (LCL/FCL)', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">Complete Ocean Freight services from all points in Malaysia to all overseas countries for both LCL and FCL traffic. Our international specialists can provide complete documentation and banking services, cargo insurance and export packing as well.</span><br></p>', NULL, NULL, 0, 'right', 'flaticon-ship', '2023-03-05 22:46:50', '2023-03-05 22:47:52', NULL),
(15, 6, 12, 'Deliver On Time', '<p>Merchandise delivered on time, on budget<br></p>', 'cargo-truck.png', 'mZ1sQ2g24Rkm48ULes3xwJJgwaD1MueBAToaqunL.png', 0, 'none', '', '2023-03-05 23:06:35', '2023-03-05 23:06:35', NULL),
(16, 6, 12, 'Cost savings', '<p><span style=\"color: rgb(33, 37, 41); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">All the goods were shipped in a most reasonable price</span><br></p>', 'calculator.png', '8d5E1AV4TlmR6cgHMQzx2lPvaZ54bKn9afaIuwRa.png', 0, 'none', '', '2023-03-05 23:07:29', '2023-03-05 23:07:29', NULL),
(17, 6, 12, 'One-party Responsibility', '<ul><li>- Management of all freight transportation requirement</li>\r\n                                <li>- Simplified invoicing (e.g., single invoice to process)</li>\r\n                            </ul>', 'responsibility.png', 'jQNiBQw6xupkh8qu4c5I7CEZIDz0JwzHtKfQcroG.png', 0, 'none', '', '2023-03-05 23:08:16', '2023-03-05 23:16:23', NULL),
(18, 6, 12, 'Efficiency', '<p><span style=\"color: rgb(33, 37, 41); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">Increased supply chain efficiency</span><br></p>', 'efficiency.png', 'OAVhJddAdvI3LL0TOaMkBSGJbgBGzKdkEhXSpv1e.png', 0, 'none', '', '2023-03-05 23:08:55', '2023-03-05 23:08:55', NULL),
(19, 6, 12, 'Effective Management System', '<p>State-of-the-art Warehouse Management System<br></p>', 'management.png', 'qLHgoI90CTI3K2r90HAPK36rcgUCHopzFHvNzsG7.png', 0, 'none', '', '2023-03-05 23:09:46', '2023-03-05 23:09:46', NULL),
(20, 6, 12, 'Professional Experienced Team', '<p><span style=\"color: rgb(33, 37, 41); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 15px; text-align: justify;\">Reduced order-to-fulfillment time</span><br></p>', 'team.png', 's8Wv26rl1Li536baaV4ZMRgGVlDmXPobar3cOj27.png', 0, 'none', '', '2023-03-05 23:10:22', '2023-03-05 23:10:22', NULL),
(21, 6, 12, 'Seamless Service', '<p>Offering full logistical services for the entire supply chain (land, sea, or air).<br></p>', 'customer-service.png', 'z629drc4HfEDigWpTcAHN26XVxnBX90kGj7PKUVI.png', 0, 'none', '', '2023-03-05 23:11:06', '2023-03-05 23:11:06', NULL),
(22, 6, 12, 'Customs Brokerage Expertise', '<p><span style=\"color: rgb(33, 37, 41); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">Malaysia, Singapore and Thailand Customs brokerage expertise</span><br></p>', 'custom-clearance.png', 'AX05vcsxRz6Xmsydn1ZZsp1RRl6xnWZfymXUflED.png', 0, 'none', '', '2023-03-05 23:11:40', '2023-03-05 23:11:40', NULL),
(27, 3, 13, 'Services', '<p><span style=\"color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 1.25rem; text-align: justify;\">Easy to focus on your business as we are providing comprehensive logistical services for the entire supply chain</span><br></p><p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">On the last decade, Bumilink has emerged as one of Malaysia\'s leading supplier of third party logistics, distribution and supply chain management. We make it easy for you to focus on your business, while we handle every aspect of your distribution and warehousing requirements.</span><br style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\"><br style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\"><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">With advanced technology, worldwide resources and the ability to customize solutions, we are your ideal 3PL partner.</span><br></p>', 'globallogistic-services (1).png', 'WGrNWLf6aFU9AdHyvFPsOU8Kxt2TDj5umEpScFsH.png', 0, 'none', '', '2023-03-05 23:40:30', '2023-03-05 23:40:30', NULL),
(28, 3, 13, 'Contract Warehousing and Distribution Service', '<h5 style=\"margin-bottom: 0px; padding-bottom: 18.5781px; border: none; outline: none; font-family: Lato, sans-serif; line-height: 1.6em; color: rgb(119, 119, 119); font-size: 1.25rem; position: relative; background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; text-align: justify;\">Establish a distribution center to bring you conveniently</h5><p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">We can establish a distribution center to your specifications anywhere in the Malaysia or overseas, including all functions from pick and pack, customer service to transportation planning. Or you can use our established facilities for fast, low cost distribution in many major markets.</span><br></p>', 'globallogistic-contractwarehousing (1).png', 'L99Fk6FCm9eO3H8mlBKjZK1WoSDkyUexcgVxffHk.png', 0, 'none', '', '2023-03-05 23:42:43', '2023-03-05 23:42:43', NULL),
(29, 3, 13, 'Corporate Distribution Management', '<p><span style=\"color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 1.25rem; text-align: justify;\">A management system for shipments to collect and deliver easily</span><br></p><p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">An innovative service that manages the total shipping and distribution functions for your company. Use our experts to handle every aspect of your shipping and you\'ll receive the lowest cost plan that meets your service requirements.</span><br></p>', 'globallogistic-corporatedistribution (1).png', 'TcSGsFO3m1wvx4ScT7fFxXMRjqHvFvyJ2v1P6QTp.png', 0, 'none', '', '2023-03-05 23:43:27', '2023-03-05 23:43:27', NULL),
(30, 3, 13, 'Import Inventory Management', '<p><span style=\"color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 1.25rem; text-align: justify;\">Our one-stop logistic services</span><br></p><p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">A service for managing the inventory and distribution of your imported merchandise. We can manage the total import inventory and distribution from your overseas suppliers to final delivery to your Malaysian customers.</span><br></p>', 'globallogistic-importinventory (1).png', '8sUAXXR3H2Z1WKUQljnehDm23ztDCJEqeLnNi5xd.png', 0, 'none', '', '2023-03-05 23:44:06', '2023-03-05 23:44:06', NULL),
(31, 4, 15, 'Shipments Clearance', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">When time-critical shipments arrive as required by your customers, your relationships with them are enhanced, helping your business succeed. As your partner, Bumilink streamlines the clearance of your shipments through Customs, minimizing disruptions to your supply chain and saving you money.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-shipping-and-delivery', '2023-03-05 23:52:56', '2023-03-05 23:52:56', NULL),
(32, 4, 15, 'Locate Strategically', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">Bumilink offices are strategically located coast-to-coast along the Malaysian border and at major air and vessel ports. We can clear merchandise and make final delivery through every port of entry into Malaysia. Our worldwide network of agents accommodates Customs clearance of your shipments destined for foreign countries.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-map-1', '2023-03-05 23:53:35', '2023-03-05 23:53:35', NULL),
(33, 4, 15, 'Customs Brokerage & Compliance', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">Bumilink accurately classifies your goods according to complex tariff schedules. We identify, complete, and manage the extensive documentation required, minimizing delays, and avoiding penalties, potential legal action, and product seizures.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-warehouse', '2023-03-05 23:54:09', '2023-03-05 23:54:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `site_name` varchar(255) DEFAULT NULL,
  `site_link` text,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `address` text,
  `meta_author` text,
  `meta_keywords` text,
  `meta_descriptions` text,
  `header` text,
  `logo` text,
  `favicon` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `site_name`, `site_link`, `email`, `phone`, `fax`, `address`, `meta_author`, `meta_keywords`, `meta_descriptions`, `header`, `logo`, `favicon`, `created_at`, `updated_at`) VALUES
(1, 'Bumilink Global Logistic Sdn Bhd', NULL, 'cs@bumilink.com.my', '+603 51225355', '+603 51225355', 'No. 40 & 40A, Jalan Tabla 33/21,\r\nShah Alam Technology Park Seksyen 33\r\n40400 Shah Alam, Selangor Darul Ehsan, Malaysia', '', '', '', '6uuuMXl4Kj6l2X823BKPxasxfseNtmEtSz1Bt6A6.png', 'v67O4SBYlDpooDb47Jc2xkPg6gPUhsJ4fEGyQV3u.png', NULL, '2023-03-05 17:55:03', '2023-03-07 00:53:57');

-- --------------------------------------------------------

--
-- Table structure for table `socia_llink`
--

CREATE TABLE `socia_llink` (
  `id` int(11) NOT NULL,
  `name` text,
  `link` text,
  `hover` text,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `socia_llink`
--

INSERT INTO `socia_llink` (`id`, `name`, `link`, `hover`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'facebook', '#', '', 0, '2023-03-06 00:31:03', '2023-03-06 00:31:03', NULL),
(2, 'twitter', '#', '', 0, '2023-03-06 00:31:11', '2023-03-06 00:31:11', NULL),
(3, 'instagram', '#', '', 0, '2023-03-06 00:31:18', '2023-03-06 00:31:18', NULL),
(4, 'google', '#', '', 0, '2023-03-06 00:31:24', '2023-03-06 00:31:24', NULL),
(5, 'linkedin', '#', 'LinkedIN', 0, '2023-03-06 00:31:30', '2023-03-09 21:03:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `solutions`
--

CREATE TABLE `solutions` (
  `id` int(11) NOT NULL,
  `page_url` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `descriptions` text,
  `image_name` varchar(255) DEFAULT NULL,
  `image` text,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `solutions`
--

INSERT INTO `solutions` (`id`, `page_url`, `name`, `descriptions`, `image_name`, `image`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'apparel', 'Apparel', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">Bumilink serves customers in fashion apparel, textiles, and footwear and accessories sectors. We can provide end-to-end solutions including air and ocean freight forwarding, customs clearance and ground transportation.</li></ul>', 'solution-apparel (2).png', 'aogmS8YKBQHCO1Rr1YHsoqr45YD6aIBiF4e39ldF.png', 0, '2023-03-06 08:01:57', '2023-03-06 00:01:57', NULL),
(2, 'aviation', 'Aviation', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">In the aviation industry, time is critical. The last thing you want to think about is logistics. At Bumilink, we not only exceed the demands of the aviation community, we continue to look for ways to improve our service.</li></ul>', 'solution-aviation (2).png', 'JliMJFqt8Vrd04kk8F5xOWcVXo9yQkKv4jDCv1tG.png', 0, '2023-03-06 08:02:25', '2023-03-06 00:02:25', NULL),
(3, 'banking', 'Banking / Finance', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">As a Banking or Financial Services institution, you may have hundreds, in some cases thousands, of offices across the country. Even if you\'re part of a large regional bank, you may have a multitude of locations.</li></ul>', 'solution-banking-finance (1).png', 'NwSmjRFNVCShA6OZI2jfS7OB1lRtTR46AVFTiizA.png', 0, '2023-03-06 08:02:58', '2023-03-06 00:02:58', NULL),
(4, 'ecommerce', 'E-Commerce', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">Being a pioneer in logistics &amp; fulfillment for consumer products, Bumilink is the most competent partner when it comes to e-commerce fulfillment for both physical and digital products. Based on the expertise from many e-commerce projects with blue-chip companies, Bumilink knows how to implement and manage the seamless flow of goods, money, and information in e-business.</li></ul>', 'solution-e-commerce (1).png', 'SvttX4MaRH6UAuvhc664f6kzdNtk0g7dh5fTCYVO.png', 0, '2023-03-06 08:03:26', '2023-03-06 00:03:26', NULL),
(5, 'fmcg', 'FMCG', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">Bumilink is a leading supplier of total logistic solutions to the Fast Moving Consumer Goods (FMCG) market. We have multi share warehouses, which are geographically capable of serving every area in Malaysia, and we offer the same within distribution and transport systems. At the same time, we continuously extend the flow of information in the logistics chain.</li></ul>', 'solution-fmcg (1).png', 'H9uuNNFj2wvuApQwcaqKYhKHgeLUlOO3lwPQXdfi.png', 0, '2023-03-06 08:03:59', '2023-03-06 00:03:59', NULL),
(6, 'healthcaremedicallogistics', 'Healthcare / Medical Logistics', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">We recognize that the products we handle are the very products that are destined for the emergency room or a doctor\'s office. By leveraging our deep experience in healthcare / medial products we can provide solutions that meet the critical and time-sensitive delivery requirements for your customers. Products that keep people and pets healthy to the products that save lives.</li></ul>', 'solution-healthcare (1).png', '96Q2xSsTUTQPVAEz9ymbjrmw5U94bcz63mPVtbZx.png', 0, '2023-03-06 08:04:35', '2023-03-06 00:04:35', NULL),
(7, 'manufacturinglogistics', 'Manufacturing Logistics', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">At Bumilink, we understand that you want to focus you efforts on your core activities such as efficient manufacturing, product development, marketing and sales. That is why we have developed a range of Manufacturing Logistics solutions. We have developed our expertise of managing your Manufacturing Logistics for many years in the Automotive and Technology sector.</li></ul>', 'solution-manufacturing (1).png', '52YBmw0DGKcz9qfk8ySB9zTwulJZA0qbyYgRpfUW.png', 0, '2023-03-06 08:05:00', '2023-03-06 00:05:00', NULL),
(8, 'technology', 'Technology', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">In order to service their clients, high-tech companies require efficient, timely and reliable logistics services. Bumilink Global Logistics helps high-tech companies to be more competitive. Bumilink operates across this market sector delivering computers, photocopiers and consumer electronics as well as supporting the aftermarket with spare parts.</li></ul>', 'solution-technology (1).png', 'owmzLC4iL1GThOuPRzvY2lpMNcmKVPbSF8nbdANW.png', 0, '2023-03-06 08:05:29', '2023-03-06 00:05:29', NULL),
(9, 'customstaxconsultancy', 'Customs Tax Consultancy', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">Frequent changes in Malaysia import and export regulations, stricter enforcement, and severe penalties for non-compliance can have a huge impact on your business\' success in the domestic and international marketplace. More and more companies are relying on professional consultants to help them navigate the increasingly complex world of logistics compliance.</li></ul>', 'solution-customstax (1).png', 'pFXAs01PbBffgzEykElOmn1hQGNJTjX5cBMsvLsW.png', 0, '2023-03-06 08:05:58', '2023-03-06 00:05:58', NULL),
(10, 'dedicatedfleetlogistics', 'Dedicated Fleet Logistics', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">Alleviate the hassles of private fleets by letting Bumilink run your transportation fleet for you. Gone are the headaches of driver retention, insurance, skyrocketing fuel costs, and maintenance, with a dedicated fleet conversion Bumilink can simplify your transportation operations and seamlessly integrate into your distribution operation. We can lease a fleet of trucks or buy your current fleet from you. You can even keep the same brand name on all your trucks. We can help you improve the flexibility of your organization no matter what the size of your company.</li></ul>', 'solution-dedicatedfleet (1).png', 'hDB4Ey4H3Jpd181Q2Vcf5HxELDPS1g4zBMItWKoS.png', 0, '2023-03-06 08:06:30', '2023-03-06 00:06:30', NULL),
(11, 'supplychainmanagement', 'Supply Chain Management', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">Linking a Company to its Customers &amp; Suppliers The supply chain determines how 100 percent of the company\'s value is delivered to its customers. Supply chain management as an integration of the planning, management, and control of value chains, encompassing suppliers, production, physical logistics, and customers. The objective is to achieve superior customer service with minimal total supply chain cost. We work with clients to achieve business performance improvement and sustainable competitive advantage.</li></ul>', 'solution-supplychain (1).png', 'kqt4GFYlqMvol2dwIEAf2Yvoo6ZDmUFXT6xb84Lm.png', 0, '2023-03-06 08:07:04', '2023-03-06 00:07:04', NULL),
(12, 'wms', 'WMS', '<ul class=\"fleet-list\" style=\"border: none; outline: none; position: relative; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif;\"><li style=\"margin-bottom: 8px; padding-right: 4.76562px; border: none; outline: none; list-style: none; position: relative; font-size: 15px; text-align: justify;\">Our Warehouse Management System (WMS) is a fully integrated Information Technology system that incorporates a broad range of logistics capabilities. It is designed to be completely real-time, allowing the appropriate supply-chain program elements to fit a variety of ways of doing business for each customer. It is extremely flexible and designed for custom applications. We have implemented our WMS in a variety of operations including dedicated and shared facilities and have been able to ensure specific system requirements for our customers.</li></ul>', 'solution-wms (1).png', 'HcvsEmff8Gnz7D3MNlaKvhk3Lb6NE3aEbcgXun8l.png', 0, '2023-03-06 08:07:33', '2023-03-06 00:07:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `solutions1`
--

CREATE TABLE `solutions1` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `descriptions` text,
  `image_name` varchar(255) DEFAULT NULL,
  `image` text,
  `status` int(11) DEFAULT NULL,
  `position` varchar(10) DEFAULT NULL,
  `class_name` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `solutions1`
--

INSERT INTO `solutions1` (`id`, `category_id`, `name`, `descriptions`, `image_name`, `image`, `status`, `position`, `class_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'More Time-Definite Delivery Options', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Bumilink offers more time-definite delivery options than anyone. Regardless of the size or weight of your shipment or the time of day you need to ship - we\'ll guarantee your shipment will be delivered on time, door-to-door, anywhere Malaysia - with complete Web tracking available.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-clock-2', '2023-03-06 08:12:16', '2023-03-06 00:12:16', NULL),
(2, 1, 'Apparel Industries Speed', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">In the textile and apparel industries speed to market is critical to success. Short product lifecycles and shifting consumer demands necessitate tight inventory management and complete supply chain visibility. Constantly changing and increasing SKUs require a logistics partner with deep experience to ensure that items are available when and where they are needed.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-moving-truck', '2023-03-06 08:16:19', '2023-03-06 00:16:19', NULL),
(3, 1, 'Provide End-to-End Solutions', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Bumilink serves customers in fashion apparel, textiles, and footwear and accessories sectors. We can provide end-to-end solutions including air and ocean freight forwarding, customs clearance and ground transportation.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-help', '2023-03-06 08:16:53', '2023-03-06 00:16:53', NULL),
(4, 1, 'We Prepare for Shipments Entries', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Our global freight management offices are fully staffed with licensed brokers and account representatives who prepare entries, ensure compliance with Customs and other governmental agencies, and expedites the clearance of your goods. Once the items are cleared, we can transport them to a distribution center and then on to a retail store or direct to consumers.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-startup', '2023-03-06 08:17:26', '2023-03-06 00:17:26', NULL),
(5, 1, 'We Offer', '<ul style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 20px;\"><li style=\"border: none; outline: none; list-style: none;\">✔ Air and ocean freight management</li><li style=\"border: none; outline: none; list-style: none;\">✔ Customs Clearance</li><li style=\"border: none; outline: none; list-style: none;\">✔ Ground transportation (LTL, Truckload, Small Package)</li><li style=\"border: none; outline: none; list-style: none;\">✔ Warehousing and Fulfillment</li><li style=\"border: none; outline: none; list-style: none;\">✔ Value-Added Services</li></ul>', 'apparel-bg (1).png', 'yadzdqRJ17vVbPKASGwSREnyE0HlCgFXF7vCoVN3.png', 0, 'other', '', '2023-03-06 08:20:33', '2023-03-06 00:21:23', NULL),
(6, 2, 'We Offer', '<p><span style=\"color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 20px;\">✔ Airfreight Services</span><br></p><li style=\"border: none; outline: none; list-style: none; color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 20px;\">✔ Xpress Repair &amp; Return</li><li style=\"border: none; outline: none; list-style: none; color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 20px;\">✔ Critical Spares &amp; Routine Shipments</li><li style=\"border: none; outline: none; list-style: none; color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 20px;\">✔ Priority and Deferred Services</li><li style=\"border: none; outline: none; list-style: none; color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 20px;\">✔ Aircraft On Ground (AOG)</li><li style=\"border: none; outline: none; list-style: none; color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 20px;\">✔ Supplier Management</li><li style=\"border: none; outline: none; list-style: none; color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 20px;\">✔ Air Charters</li><li style=\"border: none; outline: none; list-style: none; color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 20px;\">✔ Customized Handling</li><li style=\"border: none; outline: none; list-style: none; color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 20px;\">✔ Exclusive Vehicles</li><li style=\"border: none; outline: none; list-style: none; color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 20px;\">✔ High Value Product</li><li style=\"border: none; outline: none; list-style: none; color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 20px;\">✔ Classified Cargo</li><li style=\"border: none; outline: none; list-style: none; color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 20px;\">✔ Hand-to-Hand Signatures</li><li style=\"border: none; outline: none; list-style: none; color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 20px;\">✔ Aircraft Engine Movement</li><li style=\"border: none; outline: none; list-style: none; color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 20px;\">✔ Specific Customs Procedures</li>', 'aviation-bg (1).png', 'AqRP4wO2Af4hwIFoOEzufm2ZfEwlceRmopAZ05vp.png', 0, 'none', '', '2023-03-06 08:22:43', '2023-03-06 00:22:43', NULL),
(7, 3, 'Expensive and Time Consuming Task', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">It can become an expensive, time consuming and resource diverting task for you to directly undertake the inventory, stocking and movement of supplies, equipment and time sensitive documentation throughout your network.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-report', '2023-03-06 08:24:33', '2023-03-06 00:24:33', NULL),
(8, 3, 'Rapid Access and Checking', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">In addition, due to fault tolerant data, check and transaction systems, you might also require rapid access to critical parts at a moments notice; day or night.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-risk', '2023-03-06 08:24:59', '2023-03-06 00:24:59', NULL),
(9, 3, 'Provide Services to Solve Above Problems', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">At the Bumilink Global Logistics Group, we can provide these services, as well as the complete delivery of materials, supplies and equipment.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-help', '2023-03-06 08:25:28', '2023-03-06 00:25:28', NULL),
(10, 3, 'Professional and Experienced Teams', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">For Credit Card and Redemption distribution, you can also take comfort in the knowledge that the drivers and riders are not hired unless they pass a detailed background check.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-organization', '2023-03-06 08:26:03', '2023-03-06 00:26:03', NULL),
(11, 4, 'Logistics & Fulfillment Services', '<p><span style=\"color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 1.25rem; text-align: justify;\">All the services include:</span><br></p><ul style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\"><li style=\"border: none; outline: none; list-style: none;\">✔ Order processing &amp; order fulfillment</li><li style=\"border: none; outline: none; list-style: none;\">✔ High value inventory management with real-time stock-level visibility</li><li style=\"border: none; outline: none; list-style: none;\">✔ Pick, pack &amp; ship with automated packaging optimization</li><li style=\"border: none; outline: none; list-style: none;\">✔ Customized promotional fulfillment</li><li style=\"border: none; outline: none; list-style: none;\">✔ Best-in-class transportation system with automated carrier selection and track &amp; trace across all major carriers</li><li style=\"border: none; outline: none; list-style: none;\">✔ Returns management</li></ul>', 'e-commerce-bg.png', 'Vt9OLnr5a8VyfP5ZLtzlNuC3PBulbSmtpgblUnxk.png', 0, 'none', '', '2023-03-06 08:56:48', '2023-03-06 00:56:48', NULL),
(12, 4, 'E-Commerce Solutions', '', 'e-commerce-solutions.png', 'ZqvKrKJvMn7CAGVNBvXFHCIJU4KgjuIxcMQuA6W2.png', 0, 'none', '', '2023-03-06 08:58:49', '2023-03-06 00:58:49', NULL),
(13, 4, 'Development & Hosting', '', NULL, NULL, 0, 'none', '', '2023-03-06 08:59:11', '2023-03-06 00:59:11', NULL),
(14, 4, 'Marketing Fulfillment', '<h5 style=\"margin-bottom: 0px; padding-bottom: 12.9219px; border: none; outline: none; font-family: Lato, sans-serif; line-height: 1.6em; color: rgb(119, 119, 119); font-size: 1.25rem; position: relative; background: none rgb(243, 243, 243); text-align: justify;\">Being an expert in marketing fulfillment, Bumilink provides customized marketing fulfillment services including sourcing, production, printing, processing, distribution, as well as subscription management, call center and payment services.<br style=\"border: none; outline: none;\"><br style=\"border: none; outline: none;\"></h5><div class=\"des\" style=\"padding-right: 25.8438px; border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify; background-color: rgb(243, 243, 243); font-size: 17px;\">Based on this broad portfolio of services, Bumilink is your reliable partner when it comes to lead generation, marketing campaigns or partner programs.<br style=\"border: none; outline: none;\"><br style=\"border: none; outline: none;\">Examples for typical marketing fulfillment services provided by Bumilink are:<ul style=\"padding-top: 31.0156px; border: none; outline: none;\"><li style=\"border: none; outline: none; list-style: none;\"><span style=\"border: none; outline: none; font-weight: bolder;\">✔ Marketing Collateral Fulfillment &amp; Distribution</span></li><li style=\"border: none; outline: none; list-style: none;\"><span style=\"border: none; outline: none; font-weight: bolder;\">✔ Campaign Management &amp; Fulfillment</span></li><li style=\"border: none; outline: none; list-style: none;\"><span style=\"border: none; outline: none; font-weight: bolder;\">✔ Management of Partner Programs</span></li></ul></div>', NULL, NULL, 0, 'none', '', '2023-03-06 08:59:48', '2023-03-06 00:59:48', NULL),
(15, 5, 'Do You Know?', '', NULL, NULL, 0, 'none', '', '2023-03-07 01:29:57', '2023-03-06 17:29:57', NULL),
(16, 5, 'Our Key Benefits & What We Offer', '', NULL, NULL, 0, 'none', '', '2023-03-07 01:30:16', '2023-03-06 17:30:16', NULL),
(17, 6, 'Healthcare Supply Chain', '', NULL, NULL, 0, 'none', '', '2023-03-07 01:34:32', '2023-03-06 17:34:32', NULL),
(18, 6, 'We Offer', '<h5 style=\"margin-bottom: 0px; padding-bottom: 8.96875px; border: none; outline: none; font-family: Lato, sans-serif; line-height: 1.6em; color: rgb(119, 119, 119); font-size: 1.25rem; position: relative; background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"></h5><ul style=\"padding-bottom: 13.4531px; border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\"><li style=\"border: none; outline: none; list-style: none;\">✔ Scalable warehousing</li><li style=\"border: none; outline: none; list-style: none;\">✔ Clean room capabilities</li><li style=\"border: none; outline: none; list-style: none;\">✔ Enhanced security</li><li style=\"border: none; outline: none; list-style: none;\">✔ Supply chain visibility tools</li><li style=\"border: none; outline: none; list-style: none;\">✔ Expedited and overnight delivery options</li><li style=\"border: none; outline: none; list-style: none;\">✔ Global freight and import/export expertise</li><li style=\"border: none; outline: none; list-style: none;\">✔ Dry or multi-temp transportation and distribution</li><li style=\"border: none; outline: none; list-style: none;\">✔ Lot/Batch tracking</li><li style=\"border: none; outline: none; list-style: none;\">✔ Product Recall track and trace</li><li style=\"border: none; outline: none; list-style: none;\">✔ Returns Management</li></ul><p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">No list is comprehensive. We work with some of the most demanding customers in the healthcare industry and have a wealth of expertise to share. Request a consultationto learn more about Bumilink healthcare logistics services.</span><br></p>', 'healthcare-bg.png', 'Q8JUof0WQMbLa1j709Nrj539LCQbgK9xYqjN5R9M.png', 0, 'none', '', '2023-03-07 01:35:38', '2023-03-06 17:35:38', NULL),
(19, 7, 'That is why we have developed a range of Manufacturing Logistics solutions', '', NULL, NULL, 0, 'none', '', '2023-03-07 01:38:59', '2023-03-06 17:38:59', NULL),
(20, 8, 'Tracking From Time to Time', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Key to supporting our solutions is the use of best in class information technology. Key technology such as consignment tracking and global vehicle positioning allows us to improve the visibility and control of product and information throughout the entire supply chain.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-clock-3', '2023-03-07 01:46:29', '2023-03-06 17:46:29', NULL),
(21, 8, 'Provide Helpful Solutions', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Bumilink recognises that efficiency, security, reliability and timely responses are essential within these sectors and these are at the heart of all of our solutions. We are able to provide both dedicated and multi-user distribution and warehousing solutions, providing flexibility in our approach to optimising our clients operations.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-report', '2023-03-07 01:47:01', '2023-03-06 17:47:01', NULL),
(22, 8, 'Value Added Services', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">In addition, we provide an increasing number of value added services including installations &amp; demonstrations, advanced track and trace, highly secure distribution and inventory management amongst others.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-settings-1', '2023-03-07 01:47:34', '2023-03-06 17:47:34', NULL),
(23, 9, 'Our Key Services & Key Benefits', '', NULL, NULL, 0, 'none', '', '2023-03-07 01:48:08', '2023-03-06 17:48:08', NULL),
(24, 10, 'Our Transportation Network', 'BumiLink transportation network is one of the best in the industry, providing our customers with ways to reduce costs, increase efficiencies and optimize load capacity.', NULL, NULL, 0, 'none', '', '2023-03-07 01:51:11', '2023-03-06 17:51:11', NULL),
(25, 10, 'Ground Transportation Services', '<p style=\"text-align: justify; \"><font color=\"#212529\" face=\"Open Sans, sans-serif\"><span style=\"font-size: 16px;\">Our network is dedicated, responsible, and always provides the highest level of ground transportation services with proactive and effective communication.</span></font><br></p>', NULL, NULL, 0, 'none', '', '2023-03-07 01:51:30', '2023-03-06 18:03:43', NULL),
(26, 10, 'Bumilink Transportation Services', '', NULL, NULL, 0, 'none', '', '2023-03-07 01:51:55', '2023-03-06 17:51:55', NULL),
(27, 11, 'Customers & Suppliers', '', NULL, NULL, 0, 'none', '', '2023-03-07 02:06:55', '2023-03-06 18:06:55', NULL),
(28, 11, 'Our Solutions', '<div class=\"col-lg-7\" style=\"padding-right: 30px; padding-left: 15px; border: none; outline: none; width: 691.25px; flex-basis: 58.3333%; max-width: 58.3333%; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(243, 243, 243);\"><div class=\"about-text margin-tb-25px\" style=\"margin-top: 2px; margin-bottom: 5px; padding-bottom: 19.375px; border: none; outline: none; float: left; width: 646.25px; font-size: 16px; text-align: justify;\"><h5 style=\"margin-bottom: 0px; padding-right: 25.8438px; border: none; outline: none; font-family: Lato, sans-serif; line-height: 1.6em; font-size: 18px; position: relative; background: none;\"><span style=\"border: none; outline: none; font-weight: bolder;\">We tailor supply chain management principles to the specific characteristic of the sector. Taking fresh ideas from one sector, we can lift performance of clients in another. Our breadth and depth allows us to apply learning from fashion to electronics, from pharma wholesale to automotive service parts, and from retail to defense.</span><br style=\"border: none; outline: none;\"><br style=\"border: none; outline: none;\"></h5><div class=\"des\" style=\"padding-right: 25.8438px; border: none; outline: none;\">As a specialist supply chain consultancy, Bumilink focuses on the design, development and implementation of solutions to meet the challenges that our clients face.<br style=\"border: none; outline: none;\"><br style=\"border: none; outline: none;\">Our multi-disciplinary skills are tailored to suit the particular requirements of individual client projects - whether for a traditional assignment, or a placement within the client\'s project team.<br style=\"border: none; outline: none;\"><br style=\"border: none; outline: none;\">We combine our resources, capabilities and experience to suit the needs of each project, providing the following benefits to our clients<ul style=\"padding-top: 31.0156px; border: none; outline: none;\"><li style=\"border: none; outline: none; list-style: none;\"><span style=\"border: none; outline: none; font-weight: bolder;\">✔ We define a project to resolve your key issues, with clear deliverables and a maximum fixed budget</span></li><li style=\"border: none; outline: none; list-style: none;\"><span style=\"border: none; outline: none; font-weight: bolder;\">✔ We develop a thorough understanding of your business, in terms of operations, business drivers and metrics</span></li><li style=\"border: none; outline: none; list-style: none;\"><span style=\"border: none; outline: none; font-weight: bolder;\">✔ We use our models to assess costs, benefits and risks, which allows us to define the best solution</span></li><li style=\"border: none; outline: none; list-style: none;\"><span style=\"border: none; outline: none; font-weight: bolder;\">✔ We define the solution in detail, providing clear timetables, budgets and responsibilities</span></li><li style=\"border: none; outline: none; list-style: none;\"><span style=\"border: none; outline: none; font-weight: bolder;\">✔ We manage the implementation to deliver a successful solution</span></li><li></li></ul></div></div></div>', NULL, NULL, 0, 'none', 'We integrate solutions across all operations disciplines. For example, rather than taking production constraints as given input to supply chain planning, we aim to optimize the complete system with both lean production and planning approaches. Viewing market and customer service level requirements as a key driver of supply chain design, we work with marketing and sales, distribution channels, and pricing strategies.', '2023-03-07 02:07:30', '2023-03-06 18:07:30', NULL),
(29, 12, 'Warehouse Management System Elements:', '<ul style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: Lato, sans-serif; font-size: 16px;\"><li style=\"border: none; outline: none; list-style: none;\">✔ RF Based for all product movements</li><li style=\"border: none; outline: none; list-style: none;\">✔ RF ID Module</li><li style=\"border: none; outline: none; list-style: none;\">✔ Bar Code Scanning</li><li style=\"border: none; outline: none; list-style: none;\">✔ Real Time</li><li style=\"border: none; outline: none; list-style: none;\">✔ Directed Put Away, Moves, Replenishments, Picks</li><li style=\"border: none; outline: none; list-style: none;\">✔ All RF Movements can be Interleaved for maximum efficiency</li><li style=\"border: none; outline: none; list-style: none;\">✔ Internet Capable for web access,</li><li style=\"border: none; outline: none; list-style: none;\">✔ Order Management Modules</li><li style=\"border: none; outline: none; list-style: none;\">✔ Direct Interface with small parcel shipping systems</li><li style=\"border: none; outline: none; list-style: none;\">✔ Auto E-mail of Shipping/Tracking data</li><li style=\"border: none; outline: none; list-style: none;\">✔ Serial, Lot, Production code tracking at a variety of levels</li><li style=\"border: none; outline: none; list-style: none;\">✔ Product Rotation by a variety of methods</li><li style=\"border: none; outline: none; list-style: none;\">✔ Multiple picking methods can be implemented</li><li style=\"border: none; outline: none; list-style: none;\">✔ Mapping and Zoning of warehouse to ensure most efficient movement of products</li></ul>', 'wms-bg (1).png', 'asxDOro8nvYrqpbX24MYCcuNpkv0KkhyZSA1qaH7.png', 0, 'none', '', '2023-03-07 02:10:29', '2023-03-06 18:10:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `solutions2`
--

CREATE TABLE `solutions2` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `descriptions` text,
  `image_name` text,
  `image` text,
  `status` int(11) DEFAULT '0',
  `position` varchar(10) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `solutions2`
--

INSERT INTO `solutions2` (`id`, `category_id`, `subcategory_id`, `name`, `descriptions`, `image_name`, `image`, `status`, `position`, `class_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 12, 'Integrated Solutions Aid Lower Expenses', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">E-commerce has become a more and more important channel of distribution, especially for the Consumer Electronics Industry. To be successful in the e-commerce channel it is essential to have an integrated solution for customer service, IT, logistics and financial services because these fields often generate the highest costs.</span><br></p>', NULL, NULL, 0, 'left', 'flaticon-money-bag', '2023-03-06 01:00:45', '2023-03-06 01:01:30', NULL),
(2, 4, 12, 'We Offer A Complete and Integrated E-Commerce Solution', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">Bumilink offers you a complete and integrated e-commerce solution. We develop your online shop system (frontend &amp; backend) and manage the end-to-end fulfillment process to your customer (B2B, B2C) – whether you want to distribute your products physically or via a digital download.</span><br></p>', NULL, NULL, 0, 'right', 'flaticon-monitor', '2023-03-06 01:01:19', '2023-03-06 01:02:01', NULL),
(3, 4, 13, 'Systems Customization & Development', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Bumilink customizes e-commerce and ESD solutions according to individual customer requirements, leveraging the highly flexible and scalable existing IT infrastructure based on SAP R/3, MS Commerce Server, and other systems.</span><br style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\"><br style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\"><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Bumilink has experienced internal staff available that is responsible for implementations, customizations and component development on both SAP and MS Commerce Server (.NET).</span><br></p>', NULL, NULL, 0, 'left', 'flaticon-computer', '2023-03-06 01:02:57', '2023-03-06 01:02:57', NULL),
(4, 4, 13, 'Hosting & Systems Security', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Bumilink Group manages and maintains data centers in 3 regions worldwide. Reliable security management and protection of information is the foundation of our business as an IT service provider. Therefore Bumilink’s Group central data center meets the stringent ISO/IEC 27001:2005 certification.</span><br style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\"><br style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\"><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Since perfect security remains elusive, Bumilink’s Group strategy is to keep the impact of possible hazards as low as possible. This is ensured through the implementation of continuous improvement security measures as well as by regularly reviewing these measures for effectiveness and adjusting them to new situations.</span><br style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\"><br style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\"><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">This measures include backup data centers, redundant systems and detailed contingency and disaster recovery plans for highest failure safety and availability.</span><br style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\"><br style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\"><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">A professional Bumilink service desk provides customer service monitoring mission critical systems and applications 24/7 for clients worldwide.</span><br></p>', NULL, NULL, 0, 'right', 'flaticon-internet', '2023-03-06 01:03:38', '2023-03-06 01:03:52', NULL),
(5, 5, 15, 'Right Products to Customers in Right Time', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">Bumilink understands that in the FMCG industry, it all comes down to the right products in front of the right customers at the right time. FMCG companies must manage demand fluctuations, seasonality, intense pricing pressures and brand competition every day. Products in this industry move fast and you have to manage multiple points of the supply chain - inventory, shipping and customs clearance, sometimes all at once. For many companies, manual processes and a lack of technology lead to a sea of paperwork and real visibility to where your products are in the supply chain does not exist. Accurate forecasting is a pipe dream and missed shipments are status quo. Growth is good but will be short lived if you can\'t get your supply chain under control.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-clock-2', '2023-03-06 17:31:03', '2023-03-06 17:31:03', NULL),
(6, 5, 15, 'Outsourcing Supply Chain Needs to Bumilink', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">If this scenario sounds familiar, you may want to consider outsourcing your supply chain needs to Bumilink. Bumilink is a leader in the consumer packaged goods space and has the industry knowledge. This knowledge and expertise helps companies both large and small manage the complexities of their supply chain, through services that include flexible and scalable value-added and fulfillment, transportation and global freight management.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-moving-truck', '2023-03-06 17:31:51', '2023-03-06 17:31:51', NULL),
(7, 5, 16, 'E-Commerce Key Benefits', '<ul style=\"border: none; outline: none; font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify; font-size: 16px; line-height: 30px; color: black;\"><li style=\"border: none; outline: none; list-style: none;\">➢ Improve speed to market</li><li style=\"border: none; outline: none; list-style: none;\">➢ Aim visibility to orders in your supply chain</li><li style=\"border: none; outline: none; list-style: none;\">➢ Reduce inventory and operating costs through our campus model</li><li style=\"border: none; outline: none; list-style: none;\">➢ Scale your network needs</li><li style=\"border: none; outline: none; list-style: none;\">➢ Gain assistance with complex customs clearance requirements</li></ul>', NULL, NULL, 0, 'none', 'fa-info-circle', '2023-03-06 17:32:54', '2023-03-06 17:32:54', NULL),
(8, 5, 16, 'What We Offer', '<ul style=\"border: none; outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify; line-height: 30px; color: black;\"><li style=\"border: none; outline: none; list-style: none;\">➢ Value-added and very scalable warehousing and distribution services</li><li style=\"border: none; outline: none; list-style: none;\">➢ Inbound and outbound transportation management</li><li style=\"border: none; outline: none; list-style: none;\">➢ Global forwarding for air and ocean shipments</li><li style=\"border: none; outline: none; list-style: none;\">➢ Customs brokerage services</li><li style=\"border: none; outline: none; list-style: none;\">➢ Global trade consulting services</li><li style=\"border: none; outline: none; list-style: none;\">➢ Technology to enable efficiency and improve shipment visibility</li></ul><p><br style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\"><span style=\"border: none; outline: none; font-weight: bolder; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">We recommend you request a consultation to learn how Bumilink can customize a solution that will help you achieve your business objectives. We work with some of the most demanding and complex customers in the consumer packaged goods industry and are ready to share our expertise.</span><br></p>', NULL, NULL, 0, 'none', 'fa-star', '2023-03-06 17:33:38', '2023-03-06 17:33:38', NULL),
(9, 6, 17, 'Change Global Healthcare', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">From products that keep people and pets healthy to the products that save lives, healthcare / medical industry supply chain needs are unique and diverse. The industry is evolving, becoming more specialized as global healthcare needs change. In no other industry are the reliability and the speed of the supply chain more important.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-planet-earth', '2023-03-06 17:36:22', '2023-03-06 17:36:22', NULL),
(10, 6, 17, 'Covers Various Products', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">Bumilink has the expertise in healthcare / medical supply chain management to assist you in managing your distribution and transportation needs. We understand that this sector covers a broad range of products and supply chain needs, whether it’s enhanced security and traceability or temperature controlled storage or transportation.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-delivery-box', '2023-03-06 17:36:58', '2023-03-06 17:36:58', NULL),
(11, 6, 17, 'Provide Optimum Temperature', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">Whether your product has specific governmental regulations and handling requirements, Bumilink can work with you to develop a solution that is comprehensive and compliant. We can provide secure, dry and temperature controlled warehousing and transportation services from a dedicated or multi-client value added warehousing campus. Our global freight management services make sure you clear customs and that your product arrives by way of air or ocean, on time and as expected without incurring fines and penalties and without delays.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-warehouse', '2023-03-06 17:37:30', '2023-03-06 17:37:30', NULL),
(12, 6, 17, 'Handle Products with Care', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">We recognize that the products we handle are the very products that are destined for the emergency room or a doctor\'s office. By leveraging our deep experience in healthcare / medial products we can provide solutions that meet the critical and time-sensitive delivery requirements for your customers.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-shield', '2023-03-06 17:38:13', '2023-03-06 17:38:13', NULL),
(13, 7, 19, 'Inventory Planning', '<ul style=\"border: none; outline: none; font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify; font-size: 16px; line-height: 30px; color: black;\"><li style=\"border: none; outline: none; list-style: none;\">➢ Using the latest and most innovative systems, we maintain stock at the optimum level, to maintain your service flows at the lowest cost</li></ul>', NULL, NULL, 0, 'left', 'fa-info-circle', '2023-03-06 17:39:47', '2023-03-06 17:42:16', NULL),
(14, 7, 19, 'Sub-assembly', '<ul style=\"border: none; outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify; line-height: 30px; color: black;\"><li style=\"border: none; outline: none; list-style: none;\">➢ The assembly of components or finished goods from individual parts, to enable you to streamline your production</li></ul>', NULL, NULL, 0, 'left', 'fa-star', '2023-03-06 17:43:05', '2023-03-06 17:43:18', NULL),
(15, 7, 19, 'Packing', '<ul style=\"border: none; outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify; line-height: 30px; color: black;\"><li style=\"border: none; outline: none; list-style: none;\">➢ Packing, customization and repackaging services for export or customer specific instructions, to meet your customer expectations</li></ul><p><br style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\"><span style=\"border: none; outline: none; font-weight: bolder; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\">We have developed our expertise of managing your Manufacturing Logistics for many years in the Automotive and Technology sector.</span><br></p>', NULL, NULL, 0, 'right', 'fa-plane', '2023-03-06 17:44:17', '2023-03-06 17:44:27', NULL),
(16, 9, 23, 'Key Services', '<ul style=\"border: none; outline: none; font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify; font-size: 16px; line-height: 30px; color: black;\"><li style=\"border: none; outline: none; list-style: none;\">➢ Comprehensive regulatory compliance and consulting</li><ul style=\"margin-top: 12.125px; margin-bottom: 12.125px; border: none; outline: none; text-indent: 3%;\"><li style=\"border: none; outline: none; list-style: none;\">1. Compliance assessment and analysis</li><li style=\"border: none; outline: none; list-style: none;\">2. Documented compliance solutions and procedural recommendations</li><li style=\"border: none; outline: none; list-style: none;\">3. Implementation and administration of compliance programs</li></ul><li style=\"border: none; outline: none; list-style: none;\">➢ Design of information-based competencies</li><li style=\"border: none; outline: none; list-style: none;\">➢ Customer training, education, and seminars</li></ul>', NULL, NULL, 0, 'none', 'fa-star', '2023-03-06 17:48:48', '2023-03-06 17:48:48', NULL),
(17, 9, 23, 'Key Benefits', '<ul style=\"border: none; outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify; line-height: 30px; color: black;\"><li style=\"border: none; outline: none; list-style: none;\">➢ Multi-disciplinary expertise</li><li style=\"border: none; outline: none; list-style: none;\">➢ Industry reputation for leadership and reliability</li><li style=\"border: none; outline: none; list-style: none;\">➢ Customer-specific solutions for controlling risk and maximizing opportunities</li><li style=\"border: none; outline: none; list-style: none;\">➢ Regular review and communication of customer-specific trade regulations, Customs laws, and industry trends</li></ul>', NULL, NULL, 0, 'none', 'fa-check-circle', '2023-03-06 17:49:36', '2023-03-06 17:49:36', NULL),
(18, 10, 26, 'What you get from BumiLink Transport:', '<ul style=\"border: none; outline: none; font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify; font-size: 16px; line-height: 30px; color: black;\"><li style=\"border: none; outline: none; list-style: none;\">➢ The ability to meet tight deadlines with more flexibility</li><li style=\"border: none; outline: none; list-style: none;\">➢ Greater control over the safety and condition of your products during transportation</li><li style=\"border: none; outline: none; list-style: none;\">➢ The ease of dealing with one company, one-stop shopping</li><li style=\"border: none; outline: none; list-style: none;\">➢ Drivers that know and respect the importance of your cargo and dispatchers that are well versed in the needs of shippers and receivers</li><li style=\"border: none; outline: none; list-style: none;\">➢ The piece of mind knowing that you are dealing with a company that specializes in the demands of transporting unique products</li></ul>', NULL, NULL, 0, 'none', 'fa-info-circle', '2023-03-06 18:04:42', '2023-03-06 18:04:42', NULL),
(19, 10, 26, 'We provide a full range of ground transportation services.', '<ul style=\"border: none; outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify; line-height: 30px; color: black;\"><li style=\"border: none; outline: none; list-style: none;\">➢ Box / Canvas Trucks</li><li style=\"border: none; outline: none; list-style: none;\">➢ Vans</li><li style=\"border: none; outline: none; list-style: none;\">➢ Trailers</li><li style=\"border: none; outline: none; list-style: none;\">➢ Reefers</li><li style=\"border: none; outline: none; list-style: none;\">➢ Flatbeds</li><li style=\"border: none; outline: none; list-style: none;\">➢ Logistical trailers</li><li style=\"border: none; outline: none; list-style: none;\">➢ Team or Single Drivers</li><li style=\"border: none; outline: none; list-style: none;\">➢ Regional Truckload Shipping</li><li style=\"border: none; outline: none; list-style: none;\">➢ National Truckload Shipping</li><li style=\"border: none; outline: none; list-style: none;\">➢ Full Truckload Services</li><li style=\"border: none; outline: none; list-style: none;\">➢ Less-than-Truckload Services</li></ul>', NULL, NULL, 0, 'none', 'fa-star', '2023-03-06 18:05:30', '2023-03-06 18:06:16', NULL),
(20, 11, 27, 'Linking a Company to it\'s Customers & Suppliers', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">The supply chain determines how 100 percent of the company\'s value is delivered to its customers. Supply chain management as an integration of the planning, management, and control of value chains, encompassing suppliers, production, physical logistics, and customers. The objective is to achieve superior customer service with minimal total supply chain cost. We work with clients to achieve business performance improvement and sustainable competitive advantage.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-collaboration', '2023-03-06 18:08:19', '2023-03-06 18:08:19', NULL),
(21, 11, 27, 'Supply Chain Management – Our Approach', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">Effective supply chain management supports corporate strategy objectives such as globalization and growth, and in merger and acquisition or financial turnaround situations.</span><br style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\"><br style=\"border: none; outline: none; color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\"><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">Our approach is to create bottom line impact covering suppliers, production, physical logistics and customers.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-hand-shake-1', '2023-03-06 18:08:50', '2023-03-06 18:08:50', NULL),
(22, 11, 27, 'How We Work', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; text-align: justify;\">We apply deep and distinctive functional expertise to help clients overcome typical supply chain challenges; for example, how to cope with demand and supply uncertainty, how to build and optimize the supply chain organization, and which supply chain planning and control strategies to employ. We supplement our client experience with targeted research programs to investigate the practices that really drive supply chain performance, as opposed to ‘flavor of the month’ approaches.</span><br></p>', NULL, NULL, 0, 'none', 'flaticon-monitor', '2023-03-06 18:09:26', '2023-03-06 18:09:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_pages`
--

CREATE TABLE `sub_pages` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `descriptions` text,
  `image_name` varchar(255) DEFAULT NULL,
  `image` text,
  `status` int(11) DEFAULT NULL,
  `position` varchar(10) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sub_pages`
--

INSERT INTO `sub_pages` (`id`, `category_id`, `name`, `class_name`, `descriptions`, `image_name`, `image`, `status`, `position`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Our Vision', 'flaticon-planet-earth', '<p style=\"text-align: justify; \"><font color=\"#777777\" face=\"Open Sans, sans-serif\"><span style=\"font-size: 15px;\">Our vision is to be South East Asia’s leading logistics solutions specialist in the avenues of innovational solutions and market share whilst expanding into the international market.</span></font><br></p>', NULL, NULL, 0, 'none', '2023-03-06 04:08:05', '2023-03-05 20:09:53', NULL),
(2, 1, 'Our Mission', 'flaticon-target', '<p style=\"text-align: justify; \"><font color=\"#777777\" face=\"Open Sans, sans-serif\"><span style=\"font-size: 15px;\">Bumilink wants to be your transportation and logistics partner, assisting you in expanding your business by handling your freight, warehousing and distribution needs, wherever you require solutions. We want to be your resource partner for any logistics function that will make your business more efficient and cost effective. By being your logistics solution provider, we can show you dramatic savings in cost, expansion into new markets, and help you better meet customer demands.</span></font><br></p>', NULL, NULL, 0, 'none', '2023-03-06 04:08:51', '2023-03-05 20:09:38', NULL),
(3, 2, 'Delivers fully integrated logistics solutions', '', 'We tailored to the needs of our customers and is now recognized as one of the leading logistics providers in the market.', NULL, NULL, 0, 'none', '2023-03-06 04:10:38', '2023-03-05 20:32:35', NULL),
(4, 2, 'Diverse range of logistics services', '', 'Includes Global Warehousing &amp; Distribution, Global Freight Management, Global Ocean Freight, International Network Solutions, Multimodal Transport Operator, Multicountry Consolidation, Supply Chain Consulting and Total Integrated Logistics Management Software System.', NULL, NULL, 0, 'none', '2023-03-06 04:11:22', '2023-03-05 20:33:05', NULL),
(5, 2, 'Cargo Insurance', '', 'When you need to submit a claim, Bumilink makes sure that it is processed quickly and that you receive proper settlement with minimum delays.', NULL, NULL, 0, 'none', '2023-03-06 04:11:49', '2023-03-05 20:33:37', NULL),
(6, 3, 'Supply Chain Services', 'flaticon-fast-delivery', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 15px; text-align: justify;\">BumiLink operates a comprehensive range of supply chain solutions for its customers; from transportation of simple product flows to complex Freight Management Solutions and from temporary shared user warehousing to fully automated dedicated solutions.</span><br></p>', NULL, NULL, 0, 'left', '2023-03-06 05:55:26', '2023-03-05 21:55:26', NULL),
(7, 3, 'Value Enhancing Services', 'flaticon-air-freight', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 15px; text-align: justify;\">BumiLink has developed a range of services that extend our supply chain solutions to enhance customer value. These include adding value to product as it passes through the distribution channel, handling of non-merchandise for retailers, dealing with all aspects of reverse logistics and recycling through to managing one-stop \'4PL type\' solutions.</span><br></p>', NULL, NULL, 0, 'left', '2023-03-06 05:56:18', '2023-03-05 21:56:41', NULL),
(8, 3, 'Enabling Services', 'flaticon-sea-ship-with-containers', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 15px; text-align: justify;\">Many of the solutions we deliver involve a start up project or require a transfer of activity from existing in-house or third party. Professional management of the change process and access to specialist capabilities including solution design, property management and technology implementation are central to the delivery of a winning solution.</span><br></p>', NULL, NULL, 0, 'left', '2023-03-06 05:57:28', '2023-03-05 21:59:26', NULL),
(9, 3, 'Why should use BumiLink?', 'flaticon-delivery-box', '<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 15px; text-align: justify;\">We manage the flow of products and information with a laser focus on developing high performing teams. We bring technology-based, chain-wide logistics and distribution solutions to our clients with \"best of breed\" warehouse management and information systems. This strategy helps us expand our relationships with our clients, and allows us to leverage our investment in technology to drive down the client\'s total cost of distribution and logistics. Our process was developed from our deep experience in logistics and distribution design, implementation, operations, and systems deployment, coupled with an infectious commitment to continuous improvement and customer satisfaction, our clients view us as a critical key to their own success.</span><br></p>', NULL, NULL, 0, 'left', '2023-03-06 05:58:51', '2023-03-05 22:00:05', NULL),
(10, 3, 'High Service Standards', 'flaticon-engineer', '<p>We maintain the highest level of service standards, the most rigorous controls on security and a well trained professional staff in every aspect of transportation.<br></p>', 'why-section-01 (1).png', 'DAwWsUBlwSMav42GwheVSRmrDIIh4Peq9j2o5qoH.png', 0, 'right', '2023-03-06 06:01:29', '2023-03-05 22:01:29', NULL),
(11, 3, 'Shipments Tracking', 'flaticon-fast-delivery', '<p>With web technology, allowing you to keep updated on every transaction and to initiate shipments on our website, with numerous reports available.<br></p>', 'why-section-02 (1).png', '7cPoTCzIqiLpT696c592NHaMYYHRfXkyzATrMhN7.png', 0, 'right', '2023-03-06 06:02:39', '2023-03-05 22:02:39', NULL),
(12, 3, 'Affordable Price for Your Present Carriers', 'flaticon-shipping-and-delivery', '<p>And, we do it all at a price that can save you considerable money over your present carriers.<br></p>', 'why-section-03 (1).png', 'En5Q0AE9KONNCZ1470LATVTqh1uMSUSNSidUxPbG.png', 0, 'right', '2023-03-06 06:03:44', '2023-03-05 22:04:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `useful_llink`
--

CREATE TABLE `useful_llink` (
  `id` int(11) NOT NULL,
  `name` text,
  `link` text,
  `hover` text,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `useful_llink`
--

INSERT INTO `useful_llink` (`id`, `name`, `link`, `hover`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Currencies Converter', 'https://www.oanda.com/currency-converter/en/?from=EUR&to=USD&amount=1', '', 0, '2023-03-06 00:32:21', '2023-03-06 00:34:05', NULL),
(2, 'Customs', '#', '', 0, '2023-03-06 00:32:55', '2023-03-06 00:33:58', NULL),
(3, 'Dictionary & Translator', 'https://www.dictionary.com/', '', 0, '2023-03-06 00:34:28', '2023-03-06 00:34:28', NULL),
(4, 'Embassy Worldwide', 'http://www.helplinedatabase.com/embassy-database/', '', 0, '2023-03-06 00:34:54', '2023-03-06 00:34:54', NULL),
(5, 'World Clock', 'https://www.timeanddate.com/worldclock/', '', 0, '2023-03-06 00:35:18', '2023-03-06 00:35:18', NULL),
(6, 'World Holiday', 'http://www.earthcalendar.net/index.php', '', 0, '2023-03-06 00:35:42', '2023-03-06 00:35:42', NULL),
(7, 'World International City Codes', '#', '', 0, '2023-03-06 00:36:14', '2023-03-06 00:36:14', NULL),
(8, 'World Map', 'https://www.google.com/maps/@1.5433728,103.7860864,13z', '', 0, '2023-03-06 00:36:35', '2023-03-06 00:36:35', NULL),
(9, 'Worldwide Phone Directory', 'http://www.the-acr.com/codes/cntrycd.htm#a', '', 0, '2023-03-06 00:36:50', '2023-03-06 00:41:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_activity`
--

CREATE TABLE `user_activity` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `contentId` int(11) DEFAULT NULL,
  `contentType` text,
  `action` text,
  `description` text,
  `details` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_activity`
--

INSERT INTO `user_activity` (`id`, `user_id`, `contentId`, `contentType`, `action`, `description`, `details`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 'App\\Models\\Sociallink', 'update', 'Updated a Sociallink', '{\"hover\":\"LinkedIN\",\"updated_at\":\"2023-03-10 05:03:07\"}', '2023-03-09 21:03:07', '2023-03-09 21:03:07');

-- --------------------------------------------------------

--
-- Table structure for table `user_login_history`
--

CREATE TABLE `user_login_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_login_at` datetime DEFAULT NULL,
  `last_login_ip` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_login_history`
--

INSERT INTO `user_login_history` (`id`, `user_id`, `last_login_at`, `last_login_ip`, `created_at`, `updated_at`) VALUES
(1, 1, '2023-03-10 02:53:06', '103.95.32.4', '2023-03-09 18:53:06', '2023-03-09 18:53:06'),
(2, 1, '2023-03-10 05:02:30', '103.95.32.4', '2023-03-09 21:02:30', '2023-03-09 21:02:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_types`
--
ALTER TABLE `admin_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_by`
--
ALTER TABLE `contact_by`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_contact_by`
--
ALTER TABLE `feedback_contact_by`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_following_service`
--
ALTER TABLE `feedback_following_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `following_services`
--
ALTER TABLE `following_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `heard_about`
--
ALTER TABLE `heard_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_business`
--
ALTER TABLE `our_business`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_technology`
--
ALTER TABLE `our_technology`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `quote`
--
ALTER TABLE `quote`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services1`
--
ALTER TABLE `services1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services2`
--
ALTER TABLE `services2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socia_llink`
--
ALTER TABLE `socia_llink`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `solutions`
--
ALTER TABLE `solutions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `solutions1`
--
ALTER TABLE `solutions1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `solutions2`
--
ALTER TABLE `solutions2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_pages`
--
ALTER TABLE `sub_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `useful_llink`
--
ALTER TABLE `useful_llink`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_activity`
--
ALTER TABLE `user_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_login_history`
--
ALTER TABLE `user_login_history`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `admin_types`
--
ALTER TABLE `admin_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contact_by`
--
ALTER TABLE `contact_by`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `feedback_contact_by`
--
ALTER TABLE `feedback_contact_by`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `feedback_following_service`
--
ALTER TABLE `feedback_following_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `following_services`
--
ALTER TABLE `following_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `heard_about`
--
ALTER TABLE `heard_about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `our_business`
--
ALTER TABLE `our_business`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `our_technology`
--
ALTER TABLE `our_technology`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `quote`
--
ALTER TABLE `quote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `services1`
--
ALTER TABLE `services1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `services2`
--
ALTER TABLE `services2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `socia_llink`
--
ALTER TABLE `socia_llink`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `solutions`
--
ALTER TABLE `solutions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `solutions1`
--
ALTER TABLE `solutions1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `solutions2`
--
ALTER TABLE `solutions2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `sub_pages`
--
ALTER TABLE `sub_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `useful_llink`
--
ALTER TABLE `useful_llink`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_activity`
--
ALTER TABLE `user_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_login_history`
--
ALTER TABLE `user_login_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
